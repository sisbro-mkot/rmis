@extends('layout.app')

@section('isi')

<div class="card-deck bg-white" style="padding: 30px 10px 20px 40px">

  <div class="card text-white bg-primary col-sm-6 col-lg-2" data-toggle="modal" data-target="#Risiko">
      <div class="card-body">
        <p class="card-text text-center">1000</p>
        <p class="card-text text-center">Risiko</p>
      </div>
  </div>

  <div class="card text-white bg-warning col-sm-6 col-lg-2">
    <a href="{{url('lini1pengendalian')}}">
      <div class="card-body">
        <p class="card-text text-center">1500</p>
        <p class="card-text text-center">Existing Control</p>
      </div>
    </a>
  </div>

  <div class="card text-white bg-info col-sm-6 col-lg-2">
      <div class="card-body">
        <p class="card-text text-center">2000</p>
        <p class="card-text text-center">Penyebab</p>
      </div>
  </div>

  <div class="card text-white bg-success col-sm-6 col-lg-2">
      <div class="card-body">
        <p class="card-text text-center">2500</p>
        <p class="card-text text-center">RTP</p>
      </div>
  </div>

  <div class="card text-white bg-danger col-sm-6 col-lg-2">
      <div class="card-body">
        <p class="card-text text-center">3000</p>
        <p class="card-text text-center">Kejadian</p>
      </div>
  </div>

</div>

<div class="card-deck bg-white">
  lalala
</div>

<!-- The Modal -->

<div class="modal" tabindex="-1" id="Risiko" aria-hidden="true">
  <div class="modal-dialog modal-full" role="document">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Risiko Unit Kerja</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
            <div id="mitigasi" class="col-lg-6"></div>
            <div id="konteks" class="col-lg-6 lg-auto"></div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

@endsection

@push('js')

<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var dmitigasi = google.visualization.arrayToDataTable([
  ['Status', 'Jumlah'],
  ['Sudah Termitigasi', 8],
  ['Belum Termitigasi', 2]
]);

  var dkonteks = google.visualization.arrayToDataTable([
  ['Konteks', 'Jumlah'],
  ['Going Concern', 4],
  ['Sasaran Strategis/Program/Kegiatan', 3],
  ['Proses Bisnis', 3]
]);
  // Optional; add a title and set the width and height of the chart
  var omitigasi = {'title':'Status Mitigasi', 'width':600, 'height':200};
  var okonteks = {'title':'Risiko Per Konteks', 'width':600, 'height':200};

  // Display the chart inside the <div> element with id="piechart"
  var cmitigasi = new google.visualization.PieChart(document.getElementById('mitigasi'));
  var ckonteks = new google.visualization.PieChart(document.getElementById('konteks'));
  cmitigasi.draw(dmitigasi, omitigasi);
  ckonteks.draw(dkonteks, okonteks);
}


</script>

@endpush