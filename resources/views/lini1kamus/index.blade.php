@extends('layout.app')
 
@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    @if (Auth::user()->role_id == '3')
    <li class="breadcrumb-item"><a href="{{route('heatmapbpkp')}}"><i class="fa fa-home"></i> Home</a></li>
    @else
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    @endif
    <li class="breadcrumb-item active" aria-current="page">Kamus Risiko</li>
  </ol>
  <h6 class="slim-pagetitle">Kamus Risiko</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <a href="cetakkamus" class="btn btn-primary" target="_blank"><i class="icon ion-printer"></i> Cetak Semua Risiko</a>
    <a href="cetak01" class="btn btn-success" target="_blank"><i class="icon ion-printer"></i> Cetak R. Bencana</a>
    <a href="cetak02" class="btn btn-success" target="_blank"><i class="icon ion-printer"></i> Cetak R. Kebijakan</a>
    <a href="cetak03" class="btn btn-success" target="_blank"><i class="icon ion-printer"></i> Cetak R. Fraud</a>
    <a href="cetak04" class="btn btn-success" target="_blank"><i class="icon ion-printer"></i> Cetak R. Kepatuhan</a>
    <a href="cetak05" class="btn btn-success" target="_blank"><i class="icon ion-printer"></i> Cetak R. Operasional</a>
    <a href="cetak06" class="btn btn-success" target="_blank"><i class="icon ion-printer"></i> Cetak R. Stakeholder</a>
  </div>
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}

    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="3%">No.</th>
          <th width="15%" style="text-align: center;">Kategori Risiko</th>
          <th style="text-align: center;">Nama Risiko</th>
          <th style="text-align: center;">Metode Pencapaian Tujuan SPIP</th>
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($kamus as $item)
        <tr class="item{{$item->id}}">
          <td>{{$no++}}</td>
          <td>{{$item->nama_kategori_risiko}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td>{{$item->nama_tujuan_spip}}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
  </div>
</div>
@endsection

@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
