@extends('layout.app')
 
@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('lini1usul')}}">Usulan Risiko Baru</a></li>
    <li class="breadcrumb-item active" aria-current="page">Tambah Usulan</li>
  </ol>
  <h6 class="slim-pagetitle">Usulan Risiko Baru</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Tambah Usulan</h6>
  </div>


  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif


  @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
  <form class="form-horizontal mt-2" action="{{route('lini1usulan.store')}}" method="post">
    {{ csrf_field() }}
    <div class="box-body">

      <div class="form-group" id="data-umum">
        <label for="s_kd_instansiunitorg" class="col-sm-2 control-label">Nama Unit</label>
        <div class="col-sm-12">
          <select name="s_kd_instansiunitorg" class="form-control" id="s_kd_instansiunitorg" autofocus>
            @foreach($s_kd_instansiunitorg as $key)
              <option value="{{$key->s_kd_instansiunitorg}}" {{old('s_kd_instansiunitorg') == $key ? 'selected' : ''}}>{{$key->s_kd_instansiunitorg}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="nama_bagan_risiko" class="col-sm-2 control-label">Nama Risiko</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_bagan_risiko">{{old('nama_bagan_risiko')}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <label for="id_kategori_risiko" class="col-sm-2 control-label">Kategori Risiko</label>
        <div class="col-sm-12">
          <select name="id_kategori_risiko" class="form-control" id="id_kategori_risiko" autofocus>
            @foreach($id_kategori_risiko as $key => $value)
              <option value="{{$key}}" {{old('id_kategori_risiko') == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="id_tujuan_spip" class="col-sm-12 control-label">Metode Pencapaian Tujuan SPIP</label>
        <div class="col-sm-12">
          <select name="id_tujuan_spip" class="form-control" id="id_tujuan_spip" autofocus>
            @foreach($id_tujuan_spip as $key => $value)
              <option value="{{$key}}" {{old('id_tujuan_spip') == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini1usulan.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
</div>

@stop

@push('js')
<script type="text/javascript">
  $(document).ready(function() {
    $("#data-umum").hide();
    $("#s_kd_instansiunitorg").select2();
    $("#id_kategori_risiko").select2();
    $("#id_tujuan_spip").select2();
});
</script>
@endpush

