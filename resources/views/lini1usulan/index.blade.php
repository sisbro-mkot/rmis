@extends('layout.app')
 
@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    @if (Auth::user()->role_id == '3')
    <li class="breadcrumb-item"><a href="{{route('heatmapbpkp')}}"><i class="fa fa-home"></i> Home</a></li>
    @else
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    @endif
    <li class="breadcrumb-item active" aria-current="page">Usulan Risiko Baru</li>
  </ol>
  <h6 class="slim-pagetitle">Usulan Risiko Baru</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  @if (Auth::user()->role_id == '3')
  <div class="card-header">
  <a href="{{route('createusbpkp')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah Usulan</a>
  </div>
  @elseif (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
  <div class="card-header">
  <a href="{{route('createusunit')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah Usulan</a>
  </div>
  @endif
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}

    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="3%">No.</th>
          <th style="text-align: center;">Nama Risiko</th>
          <th width="15%" style="text-align: center;">Kategori Risiko</th>
          <th width="15%" style="text-align: center;">Metode Pencapaian Tujuan SPIP</th>
          <th width="15%" style="text-align: center;">Status Usulan</th>
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($kamus as $item)
        <tr class="item{{$item->id}}">
          <td>{{$no++}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td>{{$item->nama_kategori_risiko}}</td>
          <td>{{$item->nama_tujuan_spip}}</td>
          <td style="text-align: center;">
            @if($item->status_usulan == 1)
            <a href="{{route('lini1usulan.show', $item->id)}}" class="badge badge-primary"> Usulan Terkirim</a>
            @elseif($item->status_usulan == 2)
            <a href="{{route('lini1usulan.show', $item->id)}}" class="badge badge-danger"> Usulan Ditolak</a>
            @elseif($item->status_usulan == 3)
            <a href="{{route('lini1usulan.show', $item->id)}}" class="badge badge-warning"> Usulan Disetujui dengan Perbaikan</a>
            @elseif($item->status_usulan == 4)
            <a href="{{route('lini1usulan.show', $item->id)}}" class="badge badge-success"> Usulan Disetujui</a>
            @else
            Tidak ada data
            @endif
          </td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
  </div>
</div>
@endsection

@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
