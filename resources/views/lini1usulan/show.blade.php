@extends('layout.app')
 
@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('lini1usul')}}">Usulan Risiko Baru</a></li>
    <li class="breadcrumb-item active" aria-current="page">Detail Usulan Risiko Baru</li>
  </ol>
  <h6 class="slim-pagetitle">Detail Usulan Risiko Baru</h6>
</div><!-- slim-pageheader -->
<div class="card card-table"> 
  <div class="card-body">
    <div class="row">
      <div class="col-sm-12">  
        
        <span>Nama Risiko : </span>
        <p style="color: black;">{{$usulan->nama_bagan_risiko}}</p>     
      
        <span>Kategori Risiko : </span>
        <p style="color: black;">{{$usulan->nama_kategori_risiko}}</p>
  
        <span>Metode Pencapaian Tujuan SPIP : </span>
        <p style="color: black;">{{$usulan->nama_tujuan_spip}}</p>
      
        <span>Status Usulan : </span>
        <p>
          @if($usulan->status_usulan == 1)
            <span class="badge badge-primary">Usulan Terkirim</span>
          @elseif($usulan->status_usulan == 2)
            <span class="badge badge-danger">Usulan Ditolak</span>
          @elseif($usulan->status_usulan == 3)
            <span class="badge badge-warning">Usulan Disetujui dengan Perbaikan</span>
          @elseif($usulan->status_usulan == 4)
            <span class="badge badge-success">Usulan Disetujui</span>
          @else
            Tidak ada data
          @endif
        </p>

        @if(is_null($usulan->tanggapan))
        @else
        <span>Keterangan Usulan : </span>
          <p style="color: black;">{{$usulan->tanggapan}}</p>
        @endif

      </div>
    </div>
  </div>
</div>

@stop



