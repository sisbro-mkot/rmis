@extends('layout.app')
 
@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapbpkp')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Pelaporan Risiko</li>
  </ol>
  <h6 class="slim-pagetitle">Pelaporan Risiko </h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h5 style="color: black;">Pilih Unit Pemilik Risiko</h5>
    <div class="col-sm-12">
      <select class="form-control" name="pemilik_risiko" id="pemilik_risiko">
        
        @foreach($pemilik_risiko as $per)
        <option value="{{$per->id}}" >{{$per->s_nama_instansiunitorg}}</option>
        @endforeach

      </select>
    </div>
  </div>
  <div class="card-header">
    <h5 style="color: black;">Identifikasi Risiko</h5>
    <a href="cetakid" class="btn btn-primary" target="_blank" id="cetakid_btn"><i class="icon ion-printer"></i> Cetak PDF</a>
  </div>
  <div class="card-header">
    <h5 style="color: black;">Analisis Risiko</h5>
    <a href="cetakan" class="btn btn-primary" target="_blank" id="cetakan_btn"><i class="icon ion-printer"></i> Cetak PDF</a>
    <a href="excelan" class="btn btn-success" target="_blank"><i class="icon ion-ios-grid-view"></i> Ekspor Excel Seluruh Unit</a>
  </div>
  <div class="card-header">
    <h5 style="color: black;">Daftar Risiko Prioritas Unit Kerja</h5>
    <a href="cetakev" class="btn btn-primary" target="_blank" id="cetakev_btn"><i class="icon ion-printer"></i> Cetak PDF</a>
  </div>
  <div class="card-header">
    <h5 style="color: black;">Analisis Akar Masalah <span style="font-style: italic;">(Root Cause Analysis)</span></h5>
    <a href="cetakrca" class="btn btn-primary" target="_blank" id="cetakrca_btn"><i class="icon ion-printer"></i> Cetak PDF</a>
  </div>
  <div class="card-header">
    <h5 style="color: black;">Rencana Tindak Pengendalian</h5>
    <a href="cetakres" class="btn btn-primary" target="_blank" id="cetakres_btn"><i class="icon ion-printer"></i> Cetak PDF</a>
    <a href="excelrtp" class="btn btn-success" target="_blank"><i class="icon ion-ios-grid-view"></i> Ekspor Excel Seluruh Unit</a>
  </div>
  <div class="card-header">
    <h5 style="color: black;">Daftar Pemantauan Kegiatan Pengendalian</h5>
    <a href="cetakreal" class="btn btn-primary" target="_blank" id="cetakreal_btn"><i class="icon ion-printer"></i> Cetak PDF</a>
  </div>
  <div class="card-header">
    <h5 style="color: black;">Pemantauan Terhadap Keterjadian Risiko</h5>
    <a href="cetakmon" class="btn btn-primary" target="_blank" id="cetakmon_btn"><i class="icon ion-printer"></i> Cetak PDF</a>
    <a href="excelmon" class="btn btn-success" target="_blank"><i class="icon ion-ios-grid-view"></i> Ekspor Excel Seluruh Unit</a>
  </div>
  <div class="card-header">
    <h5 style="color: black;">Rekapitulasi Identifikasi s.d. Respons Risiko</h5>
    <a href="excelidbpkp" class="btn btn-success" target="_blank" id="excelidbpkp_btn"><i class="icon ion-ios-grid-view"></i> Ekspor Excel per Unit</a>
    <a href="excelidall" class="btn btn-success" target="_blank"><i class="icon ion-ios-grid-view"></i> Ekspor Excel Seluruh Unit</a>
  </div>
  <div class="card-header">
    <h5 style="color: black;">Rekapitulasi <i>Existing Control</i></h5>
    <a href="excelecbpkp" class="btn btn-success" target="_blank" id="excelecbpkp_btn"><i class="icon ion-ios-grid-view"></i> Ekspor Excel per Unit</a>
    <a href="excelecall" class="btn btn-success" target="_blank"><i class="icon ion-ios-grid-view"></i> Ekspor Excel Seluruh Unit</a>
  </div>
</div>
@endsection

@push('js')
<script type="text/javascript">
  $(document).ready(function() {
    $("#pemilik_risiko").select2();

      var areaID = $('#pemilik_risiko').val();
      console.log(areaID);
      var cetakid_link = "bcetakid/" + areaID;
      var cetakan_link = "bcetakan/" + areaID;
      var cetakev_link = "bcetakev/" + areaID;
      var cetakrca_link = "bcetakrca/" + areaID;
      var cetakres_link = "bcetakres/" + areaID;
      var cetakreal_link = "bcetakreal/" + areaID;
      var cetakmon_link = "bcetakmon/" + areaID;
      var excelidbpkp_link = "excelidbpkp/" + areaID;
      var excelecbpkp_link = "excelecbpkp/" + areaID;
      $('#cetakid_btn').attr('href', cetakid_link);
      $('#cetakan_btn').attr('href', cetakan_link);
      $('#cetakev_btn').attr('href', cetakev_link);
      $('#cetakrca_btn').attr('href', cetakrca_link);
      $('#cetakres_btn').attr('href', cetakres_link);
      $('#cetakreal_btn').attr('href', cetakreal_link);
      $('#cetakmon_btn').attr('href', cetakmon_link);
      $('#excelidbpkp_btn').attr('href', excelidbpkp_link);
      $('#excelecbpkp_btn').attr('href', excelecbpkp_link);
      
});
</script>

@endpush

@push('js1')
<script type="text/javascript">
  $(document).ready(function() {
    $('#pemilik_risiko').select2().on('change', function() {
      var areaID = $('#pemilik_risiko').val();
      console.log(areaID);
      var cetakid_link = "bcetakid/" + areaID;
      var cetakan_link = "bcetakan/" + areaID;
      var cetakev_link = "bcetakev/" + areaID;
      var cetakrca_link = "bcetakrca/" + areaID;
      var cetakres_link = "bcetakres/" + areaID;
      var cetakreal_link = "bcetakreal/" + areaID;
      var cetakmon_link = "bcetakmon/" + areaID;
      var excelidbpkp_link = "excelidbpkp/" + areaID;
      var excelecbpkp_link = "excelecbpkp/" + areaID;
      $('#cetakid_btn').attr('href', cetakid_link);
      $('#cetakan_btn').attr('href', cetakan_link);
      $('#cetakev_btn').attr('href', cetakev_link);
      $('#cetakrca_btn').attr('href', cetakrca_link);
      $('#cetakres_btn').attr('href', cetakres_link);
      $('#cetakreal_btn').attr('href', cetakreal_link);
      $('#cetakmon_btn').attr('href', cetakmon_link);
      $('#excelidbpkp_btn').attr('href', excelidbpkp_link);
      $('#excelecbpkp_btn').attr('href', excelecbpkp_link);

    });
  });

</script>
@endpush