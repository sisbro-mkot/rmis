<!DOCTYPE html>
<html><head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Respons Risiko</title>
    <style type="text/css">
    table {
    border-collapse: collapse;
    }
    table, th, td {
    border: 1px solid;
    }
    th, td {
    padding: 3;
    }
    .kop {width:100%; text-align: left;margin-bottom: 5px; border:none;}
    .kop tr td{border:none;}

    .kopsurat{text-align: center;}
    .kopsurat1{line-height:15px;text-align: center;font-family:sans-serif !important; font-size: 18px; margin-bottom: 6px;font-weight: bold}
    .kopsurat2{line-height:11px;text-align: center;font-family:sans-serif; font-size: 16px }
  </style>
</head><body>

<div align="center">
  <table class="kop">
    <tr>
        <td class="kopsurat" style="width:90%;">
          <p class="kopsurat1">DAFTAR PEMANTAUAN KEGIATAN PENGENDALIAN</p>
        </td>
    </tr>
    <tr>
        <td>
          <p style="font-family: sans-serif; font-size: 12px; font-weight: bold;">Nama Unit Pemilik Risiko: {{$unit->s_nama_instansiunitorg}}</p>
          <p style="font-family: sans-serif; font-size: 12px; font-weight: bold;">Tahun: {{$tahun}}</p>
          <p style="font-family: sans-serif; font-size: 12px; font-weight: bold;">Triwulan: {{$triwulan}}</p>
        </td>
    </tr>
  </table>

</div>

   <div>
      <table width="100%" style="font-family: sans-serif; font-size: 12px;">
        <thead>
        <tr align="center">
          <th width="10%">Kode Penyebab</th>
          <th>Pernyataan Risiko</th>
          <th>Kegiatan Pengendalian</th>
          <th>Penanggung Jawab</th>
          <th>Indikator Keluaran</th>
          <th>Target Waktu</th>
          <th>Realisasi Waktu</th>
          <th>Hambatan/Kendala</th>
        </tr>
        <tr style="background-color: #BDBDBD; font-style: italic; font-size: 8px" align="center">
          <th>1</th>
          <th>2</th>
          <th>3</th>
          <th>4</th>
          <th>5</th>
          <th>6</th>
          <th>7</th>
          <th>8</th>
        </tr>
        </thead>
        <?php $no=1; ?>
        @foreach($respons as $item)
        <tr class="item{{$item->id}}">
          <td>{{$item->kode_penyebab}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td>{{$item->kegiatan_pengendalian}}</td>
          <td>{{$item->s_nmjabdetail}}</td>
          <td>{{$item->nama_output}}</td>
          <td>{{$item->nama_periode_rencana}}</td>
          <td style="text-align: center;">{{Carbon\Carbon::parse($item->realisasi_waktu)->format('d/m/Y')}}</td>
          <td>{{$item->nama_hambatan}}</td>
        </tr>
        @endforeach
      </table>
    </div>

</body></html>