@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('peta')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Penetapan Konteks</li>
  </ol>
  <h6 class="slim-pagetitle">Penetapan Konteks Unit Kerja</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
<!--   <div class="card-header">
    <a href="{{url('createtetapkonteks')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah</a>
    <a href="{{url('lini2tetapkonteks')}}" class="btn btn-primary"><i class="icon ion-document"></i> Data</a>
  </div> -->
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}

    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="3%">No.</th>
          <th style="text-align: center;">Tahun</th>
          <th style="text-align: center;">Pemilik Risiko</th>
          <th style="text-align: center;">Jenis Konteks</th>
          <th style="text-align: center;">Konteks</th>
          @if (Auth::user()->role_id == '1')
          <th></th>
          @endif
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($konteks as $item)
        <tr class="item{{$item->id}}">
          <td>{{$no++}}</td>
          <td>{{$item->tahun}}</td>
          <td>{{$item->s_nmjabdetail_pemilik}}</td>
          <td>{{$item->nama_jns_konteks}}</td>
          <td>{{$item->nama_konteks}}</td>
          @if (Auth::user()->role_id == '1')
          <td>
            @if(is_null($item->catatan_hapus))
            <button class="btn btn-xs btn-danger btnHapus" type="button" data-formid="{{$item->id}}"><i class="icon ion-ios-trash"></i> Hapus</button>
            @else
            <button class="btn btn-xs btn-success btnBatal" type="button" data-formid="{{$item->id}}"><i class="icon ion-edit"></i> Batal Hapus</button>
            @endif
          </td>
          @endif
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
  </div>
</div>
@endsection

@if (Auth::user()->role_id == '1')
@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({

      "columnDefs": [ {
        "targets": 5,
        "orderable": false
        } ],
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
@else
@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({

      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
@endif

@push('js1')
<script type="text/javascript">
  $('.btnHapus').on('click', function(e) {
  var formid = $(this).attr('data-formid');
  var ask = window.confirm("Hapus data?");
    if (ask) {
        window.location.href = "lini2tetapkonteks/"+formid+"/delete";
    }
  });
  $('.btnBatal').on('click', function(e) {
  var formid = $(this).attr('data-formid');
  var ask = window.confirm("Pulihkan data?");
    if (ask) {
        window.location.href = "lini2tetapkonteks/"+formid+"/batal";
    }
  });
</script>
@endpush