@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="">Pemangku Kepentingan Unit Kerja</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Data</li>
  </ol>
  <h6 class="slim-pagetitle">Pemangku Kepentingan Unit Kerja</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Edit Data</h6>
  </div>
 

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif


  @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
  <form class="form-horizontal mt-2" action="{{route('lini1stakeholder.update', $penetapan_konteks->id_stakeholder)}}" method="post">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <div class="box-body">

      <div class="form-group">
        <div class="col-sm-1">
        <input class="form-control" type="text" value="{{$penetapan_konteks->id_stakeholder}}" id="id_penetapan_konteks" name="id_penetapan_konteks" hidden>
        </div>
      </div>
 
      <div class="form-group" id="data-umum">
        <label for="id_data_umum" class="col-sm-2 control-label">Pemilik Risiko</label>
        <div class="col-sm-12">
          <select name="id_data_umum" class="form-control" id="id_data_umum" autofocus>
            @foreach($id_data_umum as $key => $value)
              <option value="{{$key}}" {{$penetapan_konteks->id_data_umum == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="nama_stakeholder" class="col-sm-12 control-label">Nama Pemangku Kepentingan</label>
          <div class="col-sm-12">
            <input class="form-control" name="nama_stakeholder" id="nama_stakeholder" value="{{$penetapan_konteks->nama_stakeholder}}"></input>
          </div>
      </div>

      <div class="form-group">
        <label for="uraian" class="col-sm-12 control-label">Keterangan</label>
          <div class="col-sm-12">
            <input class="form-control" name="uraian" id="uraian" value="{{$penetapan_konteks->uraian}}"></input>
          </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini1stakeholder.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
</div>

@stop

@push('js')
<script type="text/javascript">
  $(document).ready(function() {
    $("#data-umum").hide();

});
</script>
@endpush
