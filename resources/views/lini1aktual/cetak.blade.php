<!DOCTYPE html>
<html><head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Pemantauan Risiko</title>
    <style type="text/css">
    table {
    border-collapse: collapse;
    }
    table, th, td {
    border: 1px solid;
    }
    th, td {
    padding: 3;
    }
    .kop {width:100%; text-align: left;margin-bottom: 5px; border:none;}
    .kop tr td{border:none;}

    .kopsurat{text-align: center;}
    .kopsurat1{line-height:15px;text-align: center;font-family:sans-serif !important; font-size: 18px; margin-bottom: 6px;font-weight: bold}
    .kopsurat2{text-align: right;font-family:sans-serif; font-size: 16px }
  </style>
</head><body>

<div align="center">
  <table class="kop">
    <tr>
        <td>
          <p class="kopsurat2">Lampiran 7</p>
        </td>
    </tr>
    <tr>
        <td class="kopsurat" style="width:90%;">
          <p class="kopsurat1">DAFTAR PEMANTAUAN LEVEL RISIKO</p>
        </td>
    </tr>
    <tr>
        <td>
          <p style="font-family: sans-serif; font-size: 12px; font-weight: bold;">Nama Unit Pemilik Risiko: {{$unit->s_nama_instansiunitorg}}</p>
          <p style="font-family: sans-serif; font-size: 12px; font-weight: bold;">Tahun: {{$tahun}}</p>
        </td>
    </tr>
  </table>

</div>

   <div>
      <table width="100%" style="font-family: sans-serif; font-size: 12px;">
        <thead>
        <tr align="center">
          <th width="10%" rowspan="2">Kode Risiko</th>
          <th rowspan="2">Pernyataan Risiko</th>
          <th rowspan="2">Uraian Dampak</th>
          <th rowspan="2">Kejadian Risiko 1 Tahun</th>
          <th colspan="3">Skor/Nilai Risiko yang Direspons</th>
          <th colspan="3">Skor/Nilai Risiko Aktual</th>
          <th rowspan="2">Deviasi</th>
          <th rowspan="2">Rekomendasi</th>
        </tr>
        <tr align="center">
          <th width="5%" >Frekuensi</th>
          <th width="5%" >Dampak</th>
          <th width="5%" >Nilai Risiko</th>
          <th width="5%" >Frekuensi</th>
          <th width="5%" >Dampak</th>
          <th width="5%" >Nilai Risiko</th>
        </tr>
        <tr style="background-color: #BDBDBD; font-style: italic; font-size: 8px" align="center">
          <th>1</th>
          <th>2</th>
          <th>3</th>
          <th>4</th>
          <th>5</th>
          <th>6</th>
          <th>7</th>
          <th>8</th>
          <th>9</th>
          <th>10</th>
          <th>11</th>
          <th>12</th>
        </tr>
        </thead>
        <?php $no=1; ?>
        @foreach($analisis as $item)
        <tr class="item{{$item->id}}">
          <td>{{$item->kode_identifikasi_risiko}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td>{{$item->nama_dampak}}</td>
          <td align="center">{{$item->count_keterjadian}}</td>
          <td align="center">
            @if($item->skor_kemungkinan_treated)
            {{$item->skor_kemungkinan_treated}}
            @else
            {{$item->skor_kemungkinan_residual}}
            @endif
          </td>
          <td align="center">
            @if($item->skor_dampak_treated)
            {{$item->skor_dampak_treated}}
            @else
            {{$item->skor_dampak_residual}}
            @endif
          </td>
          <td align="center">
            @if($item->skor_risiko_treated)
            {{$item->skor_risiko_treated}}
            @else
            {{$item->skor_risiko_residual}}
            @endif
          </td>
          <td align="center">{{$item->skor_kemungkinan_aktual}}</td>
          <td align="center">{{$item->skor_dampak_aktual}}</td>
          <td align="center">{{$item->skor_risiko_aktual}}</td>
          <td align="center">
            @if($item->skor_risiko_treated)
            {{$item->skor_risiko_treated - $item->skor_risiko_aktual}}
            @else
            {{$item->skor_risiko_residual - $item->skor_risiko_aktual}}
            @endif
          </td>
          <td align="center">{{$item->nama_rekomendasi}}</td>
        </tr>
        @endforeach
      </table>
    </div>

</body></html>