@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('lini1aktual.index')}}">Nilai Risiko Aktual Unit Kerja</a></li>
    <li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
  </ol>
  <h6 class="slim-pagetitle">Nilai Risiko Aktual {{$nama_instansiunitorg->s_nama_instansiunitorg}}</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Tambah Data</h6>
  </div>

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

  @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
  <form class="form-horizontal mt-2" action="{{route('lini1aktual.update', $aktual->id_pemantauan_level)}}" method="post">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <div class="box-body">

      <div class="form-group">
        <label for="id_identifikasi_read" class="col-sm-12 control-label">Kode dan Nama Risiko</label>
        <div class="col-sm-12">
          <input class="form-control" value="{{$id_identifikasi_read->kode_identifikasi_risiko}} - {{$id_identifikasi_read->nama_bagan_risiko}}" name="id_identifikasi_read" id="id_identifikasi_read" readonly></input>
        </div>
        <br>
        <label for="id_identifikasi_read" class="col-sm-12 control-label">Uraian Dampak</label>
        <div class="col-sm-12">
          <input class="form-control" value="{{$id_identifikasi_read->nama_dampak}}" name="id_identifikasi_read" id="id_identifikasi_read" readonly></input>
        </div>
      </div>

      <div class="form-inline">
        <label for="kemungkinan_residual_read" class="col-sm-2 control-label">Kemungkinan Residual : </label>
        <div class="col-sm-2">
          <input class="form-control" value="{{$id_identifikasi_read->skor_kemungkinan_residual}}" name="kemungkinan_residual_read" id="kemungkinan_residual_read" readonly></input>
        </div>
        <label for="dampak_residual_read" class="col-sm-2 control-label">Dampak Residual :</label>
        <div class="col-sm-2">
          <input class="form-control" value="{{$id_identifikasi_read->skor_dampak_residual}}" name="dampak_residual_read" id="dampak_residual_read" readonly></input>
        </div>
        <label for="risiko_residual_read" class="col-sm-2 control-label">Level Risiko Residual :</label>
        <div class="col-sm-2">
          <input class="form-control" value="{{$id_identifikasi_read->skor_risiko_residual}}" name="risiko_residual_read" id="risiko_residual_read" readonly></input>
        </div>
      </div>

      <br/>

      <div class="form-inline">
        <label for="kemungkinan_treated_read" class="col-sm-2 control-label">Kemungkinan Treated : </label>
        <div class="col-sm-2">
          <input class="form-control" value="{{$id_identifikasi_read->skor_kemungkinan_treated}}" name="kemungkinan_treated_read" id="kemungkinan_treated_read" readonly></input>
        </div>
        <label for="dampak_treated_read" class="col-sm-2 control-label">Dampak Treated :</label>
        <div class="col-sm-2">
          <input class="form-control" value="{{$id_identifikasi_read->skor_dampak_treated}}" name="dampak_treated_read" id="dampak_treated_read" readonly></input>
        </div>
        <label for="risiko_treated_read" class="col-sm-2 control-label">Level Risiko Treated :</label>
        <div class="col-sm-2">
          <input class="form-control" value="{{$id_identifikasi_read->skor_risiko_treated}}" name="risiko_treated_read" id="risiko_treated_read" readonly></input>
        </div>
      </div>

      <br/>

      <div class="form-inline">
        <label for="jml_keterjadian" class="col-sm-2 control-label">Jumlah Kejadian Tahun Ini : </label>
        <div class="col-sm-2">
          <input class="form-control" value="{{$id_keterjadian->jml_keterjadian}}" name="jml_keterjadian" id="jml_keterjadian" readonly></input>
        </div>
        <label for="max_dampak" class="col-sm-2 control-label">Level Dampak Maksimal :</label>
        <div class="col-sm-2">
          <input class="form-control" value="{{$id_keterjadian->max_dampak}}" name="max_dampak" id="max_dampak" readonly></input>
        </div>
      </div>

      <br/>

      <div class="form-inline">
        <label for="kemungkinan_aktual" class="col-sm-2 control-label">Kemungkinan Aktual : </label>
        <i style="color: DodgerBlue;"class="fa fa-info-circle" data-toggle="modal" data-target="#modalKemungkinan" onclick="showKemungkinan(this)"></i>
        <div class="col-sm-1">
          <select class="form-control" name="kemungkinan_aktual" id="kemungkinan_aktual">
            @foreach($kemungkinan_aktual as $per)
            <option value="{{$per}}" {{$id_identifikasi_read->skor_kemungkinan_aktual == $per ? 'selected' : ''}}>{{$per}}</option>
            @endforeach
          </select>
        </div>
        <label for="dampak_aktual" class="col-sm-2 control-label">Dampak Aktual :</label>
        <i style="color: DodgerBlue;"class="fa fa-info-circle" data-toggle="modal" data-target="#modalDampak" onclick="showDampak(this)"></i>
        <div class="col-sm-1">
          <select class="form-control" name="dampak_aktual" id="dampak_aktual">
            @foreach($dampak_aktual as $per)
            <option value="{{$per}}" {{$id_identifikasi_read->skor_dampak_aktual == $per ? 'selected' : ''}}>{{$per}}</option>
            @endforeach
          </select>
        </div>
        <label for="id_matriks_aktual" class="col-sm-2 control-label">Level Risiko Aktual :</label>
        <div class="col-sm-1">
          <input class="form-control" value="" name="id_matriks_aktual_read" id="id_matriks_aktual_read" readonly></input>
        </div>
      </div>

      <br/>

      <div class="form-inline">
        <label for="deviasi" class="col-sm-2 control-label">Deviasi : </label>
        <div class="col-sm-2">
          <input class="form-control" value="" name="deviasi" id="deviasi" readonly></input>
        </div>
      </div>

      <br/>

      <div class="form-group">
        <label for="nama_rekomendasi" class="col-sm-12 control-label">Rekomendasi (diisi jika deviasi negatif):</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_rekomendasi" id="nama_rekomendasi">{{$aktual->nama_rekomendasi}}</textarea>
          </div>
      </div>

      <div class="form-group" id="data-risiko">
        <label for="id_identifikasi" class="col-sm-2 control-label">Identifikasi</label>
        <div class="col-sm-12">
          <select name="id_identifikasi" class="form-control" id="id_identifikasi" autofocus>
            @foreach($id_identifikasi as $key)
              <option value="{{$key->id_identifikasi}}" {{$key->id_identifikasi == $aktual->id_identifikasi ? 'selected' : ''}}>{{$key->id_identifikasi}}</option>
            @endforeach
          </select>
        </div>


        <div class="col-sm-2">
          <select name="id_matriks_aktual" class="form-control" id="id_matriks_aktual">
          <option value="0" disabled="true" selected="true"></option>
          <option value="">-- Pilih Kemungkingan/Dampak Risiko Aktual--</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini1aktual.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
</div>

<!-- Modal Kemungkinan -->

<div class="modal" tabindex="-1" id="modalKemungkinan" aria-hidden="true">
  <div class="modal-dialog modal-full" role="document">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h5 class="modal-title">Tabel Kriteria Kemungkinan</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="card card-table">
        <div class="pd-20">
        <div class="table-responsive-lg">
        <div class="table-wrapper">
        <table id="tabel-kemungkinan" class="table">
            <thead>
                <tr>
                    <th style="text-align: center;" rowspan="2">No.</th>
                    <th style="text-align: center;" rowspan="2">Level Kemungkinan</th>
                    <th style="text-align: center;" colspan="3">Kriteria Kemungkinan</th>
                </tr>
                <tr>
                    <th style="text-align: center;">Persentase dalam  1 Tahun</th>
                    <th style="text-align: center;">Jumlah Frekuensi dalam 1 Tahun</th>
                    <th style="text-align: center;">Kejadian Toleransi Rendah</th>
                </tr>
             </thead>
        </table>
        </div>
        </div>
        </div>
        </div>
      </div>

    </div>
  </div>
</div>

<!-- Modal Dampak -->

<div class="modal" tabindex="-1" id="modalDampak" aria-hidden="true">
  <div class="modal-dialog modal-full" role="document">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h5 class="modal-title">Tabel Kriteria Dampak</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="card card-table">
        <div class="pd-20">
        <div class="table-responsive-lg">
        <div class="table-wrapper">
        <table id="tabel-dampak" class="table">
            <thead>
                <tr>
                    <th style="text-align: center;" rowspan="2">No.</th>
                    <th style="text-align: center;" rowspan="2">Area Dampak</th>
                    <th style="text-align: center;" colspan="5">Level Dampak</th>
                </tr>
                <tr>
                    <th style="text-align: center;">Tidak Signifikan (1)</th>
                    <th style="text-align: center;">Minor (2)</th>
                    <th style="text-align: center;">Moderat (3)</th>
                    <th style="text-align: center;">Signifikan (4)</th>
                    <th style="text-align: center;">Sangat Signifikan (5)</th>
                </tr>
             </thead>
        </table>
        </div>
        </div>
        </div>
        </div>
      </div>

    </div>
  </div>
</div>

@stop

@push('js')
<script type="text/javascript">
  $(document).ready(function() {
    $("#data-risiko").hide();
    $("#id_identifikasi").select2();
    $("#kemungkinan_aktual").select2();
    $("#dampak_aktual").select2();

      var kemungkinanID = $('#kemungkinan_aktual').val();
      var dampakID = $('#dampak_aktual').val();
      var skorTreated = $('#risiko_treated_read').val();
      var skorResidual = $('#risiko_residual_read').val();
      console.log(kemungkinanID);
      if(kemungkinanID) {
          $.ajax({
              url: '../../lini2analisis/pilihSkor/'+kemungkinanID+'/'+dampakID,
              type: 'get',
              dataType: 'json',
              success:function(data) {
                console.log(data);
                  $('select[name="id_matriks_aktual"]').empty();
                  $.each(data, function(key, value) {
                      $('select[name="id_matriks_aktual"]').append('<option value="'+ value.id_matriks +'">'+ value.skor_risiko + '</option>');
                        var input = value.skor_risiko;
                        var text2 = document.getElementById("id_matriks_aktual_read");
                        text2.value = input;
                        var textDeviasi = document.getElementById("deviasi")

                        if(skorTreated) {
                          var nilaiDeviasi = skorTreated - input;
                          textDeviasi.value = nilaiDeviasi;
                        } else {
                          var nilaiDeviasi = skorResidual - input;
                          textDeviasi.value = nilaiDeviasi;
                        }
                  });

              }
          });
      } else {
          $('select[name="id_matriks_aktual"]').empty();
      }

    $("#id_matriks_aktual").select2();

});
</script>
@endpush


@push('js1')
  <script type="text/javascript">
    function showKemungkinan(ele) 
      {

          $.ajax({
              url: '../../lini1analisis/kemungkinan',
              type: 'get',
              dataType: 'json',
              success:function(data) {
                console.log(data);
                var t = $('#tabel-kemungkinan').DataTable({
                    searching: false, paging: false, info: false,
                    "bDestroy": true,
                    bJQueryUI: true,
                    aaData: data,
                    aoColumns: [
                        { mData: 'no' ,"fnRender": function( oObj ) { return oObj.aData[3].no }},
                        { mData: 'level' ,"fnRender": function( oObj ) { return oObj.aData[3].level }},
                        { mData: 'persentase' ,"fnRender": function( oObj ) { return oObj.aData[3].persentase }},
                        { mData: 'jumlah',"fnRender": function( oObj ) { return oObj.aData[3].jumlah }},
                        { mData: 'toleransi',"fnRender": function( oObj ) { return oObj.aData[3].toleransi }}
                              ],
                });

                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    });
                }).draw();

             }     
          });  
      };

    function showDampak(ele) 
      {

          $.ajax({
              url: '../../lini1analisis/dampak',
              type: 'get',
              dataType: 'json',
              success:function(data) {
                console.log(data);
                var t = $('#tabel-dampak').DataTable({
                    paging: false, info: false,
                    "bDestroy": true,
                    bJQueryUI: true,
                    aaData: data,
                    aoColumns: [
                        { mData: 'no' ,"fnRender": function( oObj ) { return oObj.aData[3].no }},
                        { mData: 'area' ,"fnRender": function( oObj ) { return oObj.aData[3].area }},
                        { mData: 'l1' ,"fnRender": function( oObj ) { return oObj.aData[3].l1 }},
                        { mData: 'l2',"fnRender": function( oObj ) { return oObj.aData[3].l2 }},
                        { mData: 'l3',"fnRender": function( oObj ) { return oObj.aData[3].l3 }},
                        { mData: 'l4',"fnRender": function( oObj ) { return oObj.aData[3].l4 }},
                        { mData: 'l5',"fnRender": function( oObj ) { return oObj.aData[3].l5 }}
                              ],
                });

                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    });
                }).draw();

             }     
          });  
      };



  $(document).ready(function() {
        $('#kemungkinan_aktual').select2().on('change', function() {
            var kemungkinanID = $(this).val();
            var dampakID = $('#dampak_aktual').val();
            var skorTreated = $('#risiko_treated_read').val();
            var skorResidual = $('#risiko_residual_read').val();
            console.log(kemungkinanID);
            if(kemungkinanID) {
                $.ajax({
                    url: '../../lini2analisis/pilihSkor/'+kemungkinanID+'/'+dampakID,
                    type: 'get',
                    dataType: 'json',
                    success:function(data) {
                      console.log(data);
                        $('select[name="id_matriks_aktual"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="id_matriks_aktual"]').append('<option value="'+ value.id_matriks +'">'+ value.skor_risiko + '</option>');
                            var input = value.skor_risiko;
                            var text2 = document.getElementById("id_matriks_aktual_read");
                            text2.value = input;
                            var textDeviasi = document.getElementById("deviasi")

                            if(skorTreated) {
                              var nilaiDeviasi = skorTreated - input;
                              textDeviasi.value = nilaiDeviasi;
                            } else {
                              var nilaiDeviasi = skorResidual - input;
                              textDeviasi.value = nilaiDeviasi;
                            }
                        });

                    }
                });
            } else {
                $('select[name="id_matriks_aktual"]').empty();
            }
        });

        $('#dampak_aktual').select2().on('change', function() {
            var dampakID = $(this).val();
            var kemungkinanID = $('#kemungkinan_aktual').val();
            var skorTreated = $('#risiko_treated_read').val();
            var skorResidual = $('#risiko_residual_read').val();
            console.log(dampakID);
            if(dampakID) {
                $.ajax({
                    url: '../../lini2analisis/pilihSkor/'+kemungkinanID+'/'+dampakID,
                    type: 'get',
                    dataType: 'json',
                    success:function(data) {
                      console.log(data);
                        $('select[name="id_matriks_aktual"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="id_matriks_aktual"]').append('<option value="'+ value.id_matriks +'">'+ value.skor_risiko + '</option>');
                            var input = value.skor_risiko;
                            var text2 = document.getElementById("id_matriks_aktual_read");
                            text2.value = input;
                            var textDeviasi = document.getElementById("deviasi")

                            if(skorTreated) {
                              var nilaiDeviasi = skorTreated - input;
                              textDeviasi.value = nilaiDeviasi;
                            } else {
                              var nilaiDeviasi = skorResidual - input;
                              textDeviasi.value = nilaiDeviasi;
                            }
                        });

                    }
                });
            } else {
                $('select[name="id_matriks_aktual"]').empty();
            }
        });

        $('#id_matriks_aktual').select2();

  });
  </script>
@endpush

