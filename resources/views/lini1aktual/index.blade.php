@extends('layout.app')
 
@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Proses Akhir Tahun</li>
  </ol>
  <h6 class="slim-pagetitle">Penilaian Risiko Aktual {{$unit->s_nama_instansiunitorg}}</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  @if($selisih > 0)
  <br/>
  <h5 style="color: black; text-align: center;">Belum akhir tahun</h5>
  <br/>
  @else
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}

    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="10%">Kode Risiko</th>
          <th style="text-align: center;">Nama Risiko</th>
          <th style="text-align: center;">Nama Dampak</th>
          <th style="text-align: center;">Nilai Risiko Aktual</th>
          <th style="text-align: center;">Status</th>
          @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
          <th></th>
          @endif
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($identifikasi as $item)
        <tr class="item{{$item->id}}">
          <td>{{$item->kode_identifikasi_risiko}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td>{{$item->nama_dampak}}</td>
          <td style="text-align: center;">{{$item->skor_risiko}}</td>
          <td style="text-align: center;">
            @if($item->ganti_tahun_risiko < 1)
            <span class="badge badge-danger"> Proses</span>
            @else
            <span class="badge badge-success"> Selesai</span>
            @endif
          </td>
          @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
          <td>
            @if($item->ganti_tahun_risiko < 1)
            <a href="{{route('aktualtambah', $item->id)}}" class="btn btn-primary btn-xs"> Input Risiko Aktual</a>
            @else
            <a href="{{route('aktualubah', $item->id_pemantauan_level)}}" class="btn btn-success btn-xs"> Edit Risiko Aktual</a>
            @endif
          </td>
          @endif
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
  </div>
  @endif
</div>
@endsection

@if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      "columnDefs": [ {
        "targets": 5,
        "orderable": false
        } ],
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
@else
@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
@endif