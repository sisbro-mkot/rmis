@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('peta')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Identifikasi Risiko</li>
  </ol>
  <h6 class="slim-pagetitle">Identifikasi Risiko Unit Kerja</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
<!--   <div class="card-header">
    <a href="{{route('createidentifikasi')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah</a>
    <a href="" class="btn btn-primary"><i class="icon ion-document"></i> Data</a>
  </div> -->
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-wrapper">
    {{ csrf_field() }}

    <table id="tbl-identifikasi" class="table display responsive">
      <thead align="center">
        <tr>
          <th width="3%">No.</th>
          <th style="text-align: center;">Tahun</th>
          <th style="text-align: center;">Pemilik Risiko</th>
          <th style="text-align: center;">Nama Risiko</th>
          <th style="text-align: center;">Nama Dampak</th>
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($identifikasi as $item)
        <tr class="item{{$item->id}}">
          <td>{{$no++}}</td>
          <td>{{$item->tahun}}</td>
          <td>{{$item->s_nmjabdetail}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td>{{$item->nama_dampak}}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
</div>
@endsection

@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
