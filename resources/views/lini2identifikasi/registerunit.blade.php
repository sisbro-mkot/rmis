@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('heatmap')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Pengelolaan Risiko</li>
  </ol>
  <h6 class="slim-pagetitle">Pengelolaan Risiko {{$unit->s_nama_instansiunitorg}}</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h7 style="color: black;">Selera Risiko: {{$unit->skor_selera}}</h7>
  </div>
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}
    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="5%">Kode Risiko</th>
          <th style="text-align: center;">Nama Risiko</th>
          <th style="text-align: center;">Uraian Dampak</th>
          <th style="text-align: center;">Level Residual Risk</th>
          <th style="text-align: center;">Level Treated Risk</th>
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($identifikasi as $item)
        <tr class="item{{$item->id}}">
          <td>{{$item->kode_identifikasi_risiko}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td>{{$item->nama_dampak}}</td>
          <td style="text-align: center;">{{$item->skor_risiko_residual}}</td>
          <td style="text-align: center;">{{$item->skor_risiko_treated}}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
  </div>
</div>
@endsection

@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({

      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
