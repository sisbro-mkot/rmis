@extends('layout.app')
 
@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Proses Akhir Tahun</li>
  </ol>
  <h6 class="slim-pagetitle">Penilaian Efektivitas Pengendalian {{$unit->s_nama_instansiunitorg}}</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  @if($selisih > 0)
  <br/>
  <h5 style="color: black; text-align: center;">Belum akhir tahun</h5>
  <br/>
  @else
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}

    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="10%">Kode Risiko</th>
          <th style="text-align: center;">Nama Risiko</th>
          <th style="text-align: center;">Nama Dampak</th>
          <th style="text-align: center;">Nama Pengendalian</th>
          <th style="text-align: center;">Sub Unsur SPIP</th>
          <th style="text-align: center;">Efektivitas</th>
          @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
          <th>Ubah Efektivitas</th>
          @endif
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($identifikasi as $item)
        <tr class="item{{$item->id}}">
          <td>{{$item->kode_identifikasi_risiko}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td>{{$item->nama_dampak}}</td>
          <td>
            @if($item->nama_pengendalian)
            {{$item->nama_pengendalian}}
            @elseif($item->kegiatan_pengendalian)
            {{$item->kegiatan_pengendalian}}
            @endif
          </td>
          <td>
            @if($item->nama_sub_unsur_ec)
            {{$item->nama_sub_unsur_ec}}
            @elseif($item->nama_sub_unsur_rtp)
            {{$item->nama_sub_unsur_rtp}}
            @endif
          </td>
          <td style="text-align: center;">
            @if($item->efektif_ec)
              @if($item->efektif_ec == '1')
              <span class="badge badge-success"> Efektif</span>
              @elseif($item->efektif_ec == '0')
              <span class="badge badge-danger"> Tidak Efektif</span>
              @endif
            @elseif($item->efektif_rtp)
              @if($item->efektif_rtp == '1')
              <span class="badge badge-success"> Efektif</span>
              @elseif($item->efektif_rtp == '0')
              <span class="badge badge-danger"> Tidak Efektif</span>
              @endif
            @endif
          </td>
          @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
          <td>
            <a href="{{route('aktualubah', $item->id)}}" class="btn btn-primary btn-xs"> Efektif</a>
            <a href="{{route('aktualubah', $item->id)}}" class="btn btn-danger btn-xs"> Tidak Efektif</a>
          </td>
          @endif
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
  </div>
  @endif
</div>
@endsection

@if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      "columnDefs": [ {
        "targets": 6,
        "orderable": false
        } ],
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
@else
@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
@endif