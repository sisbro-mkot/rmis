<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login | BPKP</title>
    <!--global css starts-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('bootstrap/css/bootstrap.min.css')}}">
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link rel="icon" href="{{URL::asset('images/favicon.png')}}" type="image/x-icon">
    <!--end of global css-->

    <!--page level css starts-->
    <link type="text/css" rel="stylesheet" href="{{URL::asset('icheck/skins/all.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/login.css')}}">
    <!--end of page level css-->
</head>

<body>
<div class="container">
    <!--Content Section Start -->
    <div class="row">
        <div class="box animation flipInX">
            <div class="box1">
            <img src="{{URL::asset('images/logo.png')}}" alt="logo" class="img-responsive mar">
            <h3 class="text-primary">Login</h3>
            <form action="{{route('login')}}" method="post">
            	{{ csrf_field() }}
                <div class="form-group">
                    <label class="sr-only"></label>
                    <input class="form-control" type="text" placeholder="Username" name="username" id="username" autofocus="autofocus">
                </div>
                <div class="form-group">
                    <label class="sr-only"></label>
                    <input class="form-control" type="password" placeholder="Password" name="password" id="password">
                </div>
                <input type="submit" class="btn btn-block btn-primary" value="Login">
            </form>
            </div>
        </div>
    </div>
    <!-- //Content Section End -->
</div>
<!--global js starts-->
<script type="text/javascript" src="{{URL::asset('js/jquery-3.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('bootstrap/js/bootstrap.min.js')}}"></script>
<!--global js end-->
<script type="text/javascript" src="{{URL::asset('icheck/icheck.min.js')}}"></script>
<script>
    $(document).ready(function(){
        $("input[type='checkbox']").iCheck({
            checkboxClass: 'icheckbox_minimal-blue'
        });
    });
</script>
</body>

</html>
