
<li class="nav-item 
{{ Request::is('dashlitbang') ? 'active' : null }}">
  <a class="nav-link" href="{{route('dashlitbang')}}">
    <i class="icon ion-ios-home-outline"></i>
    <span>Dashboard</span>
  </a>
</li>

<li class="nav-item with-sub 
{{ Request::is('stpeg') ? 'active' : null }}
">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-people-outline"></i>
    <span>Kepegawaian</span>
  </a>
  <div class="sub-item">
    <ul>
      <li><a href="{{route('stpeg')}}"><i class="fa fa-check-circle-o"></i> ST Per Pegawai</a></li>
    </ul>
  </div><!-- dropdown-menu -->
</li>

<li class="nav-item with-sub 
{{ Request::is('aktivitas') ? 'active' : null }}
{{ Request::is('stugas') ? 'active' : null }}
{{ Request::is('ndinas') ? 'active' : null }}
{{ Request::is('skep') ? 'active' : null }}
{{ Request::is('lit') ? 'active' : null }}">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-speedometer-outline"></i>
    <span>Kinerja</span>
  </a>
  <div class="sub-item">
    <ul>
      <li><a href="{{route('aktivitas')}}"><i class="fa fa-check-circle-o"></i> PKAU</a></li>
      <li><a href="{{route('stugas.index')}}"><i class="fa fa-check-circle-o"></i> Surat Tugas</a></li>
      <li><a href="{{route('ndinas.index')}}"><i class="fa fa-check-circle-o"></i> Nota Dinas</a></li>
      <!-- <li><a href="{{route('skep.index')}}"><i class="fa fa-check-circle-o"></i> Surat Keputusan</a></li> -->
      <li><a href="{{route('lit.index')}}"><i class="fa fa-check-circle-o"></i> Laporan</a></li>
    </ul>
  </div><!-- dropdown-menu -->
</li>

<li class="nav-item with-sub 
{{ Request::is('anggaran') ? 'active' : null }}
{{ Request::is('spj') ? 'active' : null }}">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-social-usd-outline"></i>
    <span>Keuangan</span>
  </a>
  <div class="sub-item">
    <ul>
    	<li><a href="{{route('anggaran')}}"><i class="fa fa-check-circle-o"></i> Anggaran</a></li>
      <!-- <li><a href="{{route('spj.index')}}"><i class="fa fa-check-circle-o"></i> SPJ</a></li> -->
    </ul>
  </div><!-- dropdown-menu -->
</li>

<li class="nav-item with-sub 
{{ Request::is('salaktiv') ? 'active' : null }}
{{ Request::is('salmak') ? 'active' : null }}
{{ Request::is('realst') ? 'active' : null }}
{{ Request::is('indikator') ? 'active' : null }}
{{ Request::is('manfaat') ? 'active' : null }}">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-printer-outline"></i>
    <span>Pelaporan</span>
  </a>
  <div class="sub-item">
    <ul>
      <!-- <li><a href="{{route('salaktiv')}}"><i class="fa fa-check-circle-o"></i> Saldo per PKAU</a></li>
      <li><a href="{{route('salmak')}}"><i class="fa fa-check-circle-o"></i> Saldo per MAK</a></li> -->
      <li><a href="{{route('realst')}}"><i class="fa fa-check-circle-o"></i> Keuangan dan Aktivitas per Bulan</a></li>
      <li><a href="{{route('indikator')}}"><i class="fa fa-check-circle-o"></i> Progress Kinerja</a></li>
      <li><a href="{{route('manfaat')}}"><i class="fa fa-check-circle-o"></i> Pemanfaatan</a></li>
    </ul>
  </div><!-- dropdown-menu -->
</li>