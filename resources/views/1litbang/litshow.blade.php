@extends('1layout.app')

@section('isi')

<div class="slim-pageheader">
	<ol class="breadcrumb slim-breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashlitbang')}}"><i class="fa fa-home"></i> Home</a></li>
		<li class="breadcrumb-item"><a href="{{route('lit.index')}}">Laporan</a></li>
		<li class="breadcrumb-item active" aria-current="page">Detail Laporan</li>
	</ol>
	<h6 class="slim-pagetitle">Detail Laporan</h6>
</div><!-- slim-pageheader -->

<div class="card card-table">	
	<div class="card-body">
		<div class="row">
			<div class="col-sm-6">	
			
				<span>Nomor Laporan : </span>
				<p style="color: black;">{{$anggaran->no_penelitian}}</p>
	
				<span>Tanggal Laporan : </span>
				<p style="color: black;">{{Carbon\Carbon::parse($anggaran->tgl_penelitian)->format('d M Y')}}</p>
			
				<span>Nama Laporan : </span>
				<p style="color: black;">{{$anggaran->nama_penelitian}}</p>

				<span>Jenis Laporan : </span>
				<p style="color: black;">{{$anggaran->ket_ikk}}</p>	

				<div class="form-group">
					<div class="row">
						<div class="col-sm-2">				
							<a href="{{route('lit.edit', $anggaran->id)}}" type="submit" class="btn btn-success"><i class="icon ion-android-create"></i> Edit Laporan</a>			
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	@if($anggaran->id_ikk == 1)
	<div class="pd-x-20">		
		<div class="table-responsive-lg">
			<div class="table-wrapper">
				{{ csrf_field() }}
				<table id="tbl-identifikasi" class="table table-bordered">
					<thead align="center">
						<tr>
							<th style="text-align: center;" colspan="9">Pemanfaatan</th>
						</tr>
						<tr>
							<th style="text-align: center;" colspan="5">Initial Outcome</th>
							<th style="text-align: center;" colspan="2">Intermediate Outcome</th>
							<th style="text-align: center;" colspan="2">End Outcome</th>
						</tr>
						<tr>
							<th style="text-align: center;">A1</th>
							<th style="text-align: center;">B1</th>
							<th style="text-align: center;">B2</th>
							<th style="text-align: center;">B3</th>
							<th style="text-align: center;">B4</th>
							<th style="text-align: center;">A1</th>
							<th style="text-align: center;">B1</th>
							<th style="text-align: center;">A1</th>
							<th style="text-align: center;">B1</th>
						</tr>
						<tr>
							<th style="text-align: center;">Distr Hsl</th>
							<th style="text-align: center;">Distr Hsl</th>
							<th style="text-align: center;">Seminar/ Konferensi</th>
							<th style="text-align: center;">Media Cetak/ Elektronik</th>
							<th style="text-align: center;">Buletin/ Leaflet</th>
							<th style="text-align: center;">Sosi/Ekspose</th>
							<th style="text-align: center;">Sosi/Ekspose/ Ref Penulisan</th>
							<th style="text-align: center;">Bahan Rapat/ Kbjkn/Kptsn</th>
							<th style="text-align: center;">Bahan Rapat/ Kebijakan/Keputusan</th>
						</tr>
					</thead>
					<tbody>						
						<tr class="item{{$anggaran->id}}">
							<td style="text-align: center;">
								@if($anggaran->init_a1 == 0)
									<i style="color: Red; font-size: 32px;"class="icon ion-android-cancel"></i>
								@else
									<i style="color: Green; font-size: 32px;"class="icon ion-android-checkbox"></i>
								@endif
							</td>
							<td style="text-align: center;">
								@if($anggaran->init_b1 == 0)
									<i style="color: Red; font-size: 32px;"class="icon ion-android-cancel"></i>
								@else
									<i style="color: Green; font-size: 32px;"class="icon ion-android-checkbox"></i>
								@endif
							</td>
							<td style="text-align: center;">
								@if($anggaran->init_b2 == 0)
									<i style="color: Red; font-size: 32px;"class="icon ion-android-cancel"></i>
								@else
									<i style="color: Green; font-size: 32px;"class="icon ion-android-checkbox"></i>
								@endif
							</td>
							<td style="text-align: center;">
								@if($anggaran->init_b3 == 0)
									<i style="color: Red; font-size: 32px;"class="icon ion-android-cancel"></i>
								@else
									<i style="color: Green; font-size: 32px;"class="icon ion-android-checkbox"></i>
								@endif
							</td>
							<td style="text-align: center;">
								@if($anggaran->init_b4 == 0)
									<i style="color: Red; font-size: 32px;"class="icon ion-android-cancel"></i>
								@else
									<i style="color: Green; font-size: 32px;"class="icon ion-android-checkbox"></i>
								@endif
							</td>
							<td style="text-align: center;">
								@if($anggaran->inter_a1 == 0)
									<i style="color: Red; font-size: 32px;"class="icon ion-android-cancel"></i>
								@else
									<i style="color: Green; font-size: 32px;"class="icon ion-android-checkbox"></i>
								@endif
							</td>
							<td style="text-align: center;">
								@if($anggaran->inter_b1 == 0)
									<i style="color: Red; font-size: 32px;"class="icon ion-android-cancel"></i>
								@else
									<i style="color: Green; font-size: 32px;"class="icon ion-android-checkbox"></i>
								@endif
							</td>
							<td style="text-align: center;">
								@if($anggaran->end_a1 == 0)
									<i style="color: Red; font-size: 32px;"class="icon ion-android-cancel"></i>
								@else
									<i style="color: Green; font-size: 32px;"class="icon ion-android-checkbox"></i>
								@endif
							</td>
							<td style="text-align: center;">
								@if($anggaran->end_b1 == 0)
									<i style="color: Red; font-size: 32px;"class="icon ion-android-cancel"></i>
								@else
									<i style="color: Green; font-size: 32px;"class="icon ion-android-checkbox"></i>
								@endif
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	@else
	@endif
</div>

@stop
