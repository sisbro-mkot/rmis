@extends('1layout.app')
 
@section('isi')

<div class="slim-pageheader">
	<ol class="breadcrumb slim-breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashlitbang')}}"><i class="fa fa-home"></i> Home</a></li>
		<li class="breadcrumb-item"><a href="{{route('spj.index')}}">Verifikasi SPJ</a></li>
		<li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
	</ol>
	<h6 class="slim-pagetitle">Tambah Data Verifikasi SPJ</h6>
</div><!-- slim-pageheader -->

<div class="card card-table">
	@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
		    @foreach ($errors->all() as $error)
		    <li>{{ $error }}</li>
		    @endforeach
		</ul>
	</div>
	@endif

	<div class="card-body">
		<div class="row">
			<div class="col-sm-3">
				<span>Nomor ST/ND/SK : </span>
				<p style="color: black;">{{$id_st_label->no_st}}</p>
			</div>
			<div class="col-sm-3">
				<span>Tanggal ST/ND/SK : </span>
				<p style="color: black;">{{Carbon\Carbon::parse($id_st_label->tgl_st)->format('d M Y')}}</p>
			</div>
			<div class="col-sm-3">	
				<span>Tanggal Mulai : </span>
				<p class="tanggal" style="color: black;">{{Carbon\Carbon::parse($id_st_label->tgl_mulai)->format('d M Y')}}</p>	
			</div>
			<div class="col-sm-3">
				<span>Tanggal Selesai : </span>
				<p class="tanggal" style="color: black;">{{Carbon\Carbon::parse($id_st_label->tgl_selesai)->format('d M Y')}}</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<span>Uraian ST/ND/SK : </span>
				<p style="color: black;">{{$id_st_label->uraian_st}}</p>
			</div>
		</div>
	</div>

	@if (Auth::user()->user_nip == '740006317'|Auth::user()->user_nip == '201800140'|Auth::user()->user_nip == '202000050')
	<form class="form-horizontal mt-2" action="{{route('spj.store')}}" method="post">
		{{ csrf_field() }}
		<div class="box-body">
			<div class="form-group">
				<div class="row" id="id_st_hide">
					<select name="id_st" class="form-control" id="id_st" autofocus>
						@foreach($id_st as $key)
						<option value="{{$key->id}}">{{$key->id}}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group">
				<label for="id_mapping" class="col-sm-12 control-label">Pembebanan Anggaran</label>
				<div class="col-sm-12">
					<select name="id_mapping" class="form-control" id="id_mapping" autofocus>
						@foreach($id_mapping as $key)
						<option value="{{$key->id}}">{{$key->mak}} - {{$key->nmitem}}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group">
				<label for="nilai_spj" class="col-sm-2 control-label">Nilai SPJ</label>
				<div class="col-sm-2">
					<input type="number" class="form-control" id="nilai_spj" name="nilai_spj" min="0" max="10000000000">
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-12">
					<button type="submit" class="btn btn-primary"> Simpan</button>
					<a href="{{route('stugas.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
				</div>
			</div>
		</div>
	</form>

	<div class="table-responsive-lg">
		<div class="table-wrapper">
			{{ csrf_field() }}
			<table id="tbl-identifikasi" class="table display">
				<thead align="center">
					<tr>
						<th width="5%">No.</th>
						<th width="20%" style="text-align: center;">MAK</th>
						<th style="text-align: center;">Uraian</th>
						<th style="text-align: center;">Nilai</th>
						<th style="text-align: center;"></th>
					</tr>
				</thead>
				<tbody>
					<?php $no=1; ?>
					@foreach($id_spj as $item)
					<tr class="item{{$item->id}}">
						<td style="text-align: center;">{{$no++}}</td>
						<td>{{$item->mak}}</td>
						<td>{{$item->nmitem}}</td>
						<td>{{number_format($item->nilai_spj, 0, ',', '.')}}</td>
						<td>
							<form method="POST" action="{{ route('spj.destroy', $item->id) }}">
							    {{ csrf_field() }}
							    {{ method_field('DELETE') }}
							    <button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('Yakin akan dihapus ?')"><i class="icon ion-ios-trash"></i> Hapus</button>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	@endif
</div>

@stop

@push('js')
<script type="text/javascript">
	$(document).ready(function() {
		$("#id_st_hide").hide();
		$("#id_st").select2();
		$("#id_mapping").select2();

		var STID = $('#id_st').val();
		if(STID){
			$.ajax({
				url: 'litbang/angkin/'+STID,
                type: 'get',
                dataType: 'json',
                success:function(data){
                	$('select[name="id_anggaran_kinerja"]').empty();
                	$.each(data, function(key, value) {
                		$('select[name="id_anggaran_kinerja"]').append('<option value="'+ value.id_anggaran_kinerja +'">'+ value.ket_mak + ' - ' + value.nama_mak + ' - ' + value.nama_sub_komponen + '</option>');
                	});
                }
			});
		} else {
			$('select[name="id_anggaran_kinerja"]').empty();
		}
	});
</script>
@endpush

@push('js1')
<script type="text/javascript">
	$(document).ready(function() {
		$('#id_st').select2().on('change', function() {
			var STID = $(this).val();
			if(STID){
				$.ajax({
					url: 'litbang/angkin/'+STID,
	                type: 'get',
	                dataType: 'json',
	                success:function(data){
	                	$('select[name="id_anggaran_kinerja"]').empty();
	                	$.each(data, function(key, value) {
	                		$('select[name="id_anggaran_kinerja"]').append('<option value="'+ value.id_anggaran_kinerja +'">'+ value.ket_mak + ' - ' + value.nama_mak + ' - ' + value.nama_sub_komponen + '</option>');
	                	});
	                }
	            });
	        } else {
				$('select[name="id_anggaran_kinerja"]').empty();
			}
		$("#id_anggaran_kinerja").select2();
		});
	});
</script>
@endpush