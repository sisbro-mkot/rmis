@extends('1layout.app')

@section('isi')

<div class="slim-pageheader">
	<ol class="breadcrumb slim-breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashlitbang')}}">
			<i class="fa fa-home"></i> Home</a>
		</li>
		<li class="breadcrumb-item active" aria-current="page">Pelaporan</li>
	</ol>
	<h6 class="slim-pagetitle">Progress per Indikator Kinerja</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
	<div class="pd-20">
		<div class="table-responsive-lg">
			<div class="table-wrapper">
				
				{{ csrf_field() }}
				<table id="tbl-identifikasi" class="table table-bordered">
					<thead align="center">
						<tr>
							<th style="text-align: center; vertical-align: middle;" width="5%">No.</th>
							<th style="text-align: center; vertical-align: middle;">Nama Indikator</th>
							<th style="text-align: center; vertical-align: middle;" width="15%">Target Tahun Ini</th>
							<th style="text-align: center; vertical-align: middle;" width="5%">Realisasi ST</th>
							<th style="text-align: center; vertical-align: middle;" width="5%">Realisasi Laporan</th>
						</tr>
					</thead>
				  	<tbody>
				  		<?php $no=1; ?>
				  		@foreach($anggaran as $item)						
						<tr class="item{{$item->id_ikk}}">
							<td>{{$no++}}</td>
							<td>
									{{$item->nama_ikk}}
							</td>
							<td>
									{{$item->target_ikk}} {{$item->satuan_ikk}}
							</td>
							<td style="text-align: center;">
								@if($item->jml_st > 0)
									{{$item->jml_st}}
								@else
									-
								@endif
							</td>
							<td style="text-align: center;">
								@if($item->jml_lap > 0)
									{{$item->jml_lap}}
								@else
									-
								@endif
							</td>
						</tr>
				  		@endforeach
				 	</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection

@push('js')
  <script>

  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
    	"columnDefs": [ {
        "targets": [0,1,2,3,4,5,6,7,8,9,10,11],
        "orderable": false
        } ],
		language: {
		searchPlaceholder: 'Cari...',
		sSearch: '',
		lengthMenu: '_MENU_ baris/halaman',
		}

    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush



