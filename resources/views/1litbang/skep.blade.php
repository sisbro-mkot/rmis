@extends('1layout.app')
 
@section('isi')

<div class="slim-pageheader">
	<ol class="breadcrumb slim-breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashlitbang')}}">
			<i class="fa fa-home"></i> Home</a>
		</li>
		<li class="breadcrumb-item active" aria-current="page">Surat Keputusan</li>
	</ol>
	<h6 class="slim-pagetitle">Daftar Surat Keputusan</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
	<div class="card-header">
		<a href="{{route('skepcreate')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Buat SK</a>
	</div>
	<div class="pd-20">
		<div class="table-responsive-lg">
			<div class="table-wrapper">
				
				{{ csrf_field() }}
				<table id="tbl-identifikasi" class="table display">
					<thead align="center">
						<tr>
							<th width="5%">No.</th>
							<th width="14%" style="text-align: center;">Nomor SK</th>
							<th width="11%" style="text-align: center;">Tanggal SK</th>
							<th style="text-align: center;">Uraian SK</th>
							<th style="text-align: center;">Nama PKAU</th>
							<th style="text-align: center;"></th>
						</tr>
					</thead>
				  	<tbody>
				  		<?php $no=1; ?>
				  		@foreach($tugas as $item)
						<tr class="item{{$item->id}}">
							<td>{{$no++}}</td>
							<td>{{$item->no_st}}</td>
							<td>{{Carbon\Carbon::parse($item->tgl_st)->format('d M Y')}}</td>
							<td>{{$item->uraian_st}}</td>
							<td>{{$item->nama_pkau}}</td>
							<td style="text-align: center;"><a href="{{route('skep.show', $item->id)}}" class="badge badge-primary"> Detail</a></td>
						</tr>
				  		@endforeach
				 	</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection

@push('js')
  <script>

  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
    	"columnDefs": [ {
        "targets": 5,
        "orderable": false
        } ],
		language: {
		searchPlaceholder: 'Cari...',
		sSearch: '',
		lengthMenu: '_MENU_ baris/halaman',
		}

    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
