@extends('1layout.app')

@section('isi')

<div class="slim-pageheader">
	<ol class="breadcrumb slim-breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashlitbang')}}"><i class="fa fa-home"></i> Home</a></li>
		<li class="breadcrumb-item"><a href="{{route('stugas.index')}}">Surat Tugas</a></li>
		<li class="breadcrumb-item active" aria-current="page">Detail Surat Tugas</li>
	</ol>
	<h6 class="slim-pagetitle">Detail Surat Tugas</h6>
</div><!-- slim-pageheader -->

<div class="card card-table">	
	<div class="card-body">
		<div class="row">
			<div class="col-sm-6">	
				
				<span>Konseptor ST : </span>
				<p style="color: black;">{{$anggaran->nama_bidang}}</p>			
			
				<span>Nomor ST : </span>
				<p style="color: black;">{{$anggaran->no_st}}</p>
	
				<span>Tanggal ST : </span>
				<p style="color: black;">{{Carbon\Carbon::parse($anggaran->tgl_st)->format('d M Y')}}</p>
			
				<span>Uraian ST : </span>
				<p style="color: black;">{{$anggaran->uraian_st}}</p>

				<span>Progress : </span>
				<p style="color: black;">
					@if($anggaran->progress)
					{{$anggaran->progress}} %
					@endif
				</p>

				<div class="form-group">
					<div class="row">
						<div class="col-sm-2">				
							<a href="{{route('stugas.edit', $anggaran->id)}}" type="submit" class="btn btn-success"><i class="icon ion-android-create"></i> Edit ST</a>			
						</div>
						<div class="col-sm-4">				
							<a href="{{route('stptambah', $anggaran->id)}}" type="submit" class="btn btn-primary"><i class="icon ion-android-create"></i> Tambah/Ubah Personil</a>				
						</div>
						<div class="col-sm-1">	
							<a href="{{route('spjcreate', $anggaran->id)}}" type="submit" class="btn btn-info"><i class="icon ion-android-create"></i> Buat SPJ</a>
						</div>
					</div>
				</div>

			</div>
			<div class="col-sm-6">	
		
				<span>Tanggal Mulai : </span>
				<p class="tanggal" style="color: black;">{{Carbon\Carbon::parse($anggaran->tgl_mulai)->format('d M Y')}}</p>	
	
				<span>Tanggal Selesai : </span>
				<p class="tanggal" style="color: black;">{{Carbon\Carbon::parse($anggaran->tgl_selesai)->format('d M Y')}}</p>

				<span>Jumlah Hari : </span>
				<p style="color: black;">{{$anggaran->jml_hari}} hari</p>	

				<span>Nama PKAU : </span>
				<p style="color: black;">{{$anggaran->kode_pkau}} - {{$anggaran->nama_pkau}}</p>

			</div>
		</div>
	</div>
	<div class="pd-x-20">		
		<div class="table-responsive-lg">
			<div class="table-wrapper">
				{{ csrf_field() }}
				<table id="tbl-identifikasi" class="table display">
					<thead align="center">
						<tr>
							<th width="5%">No.</th>
							<th width="20%" style="text-align: center;">NIP</th>
							<th style="text-align: center;">Nama</th>
							<th style="text-align: center;">Jabatan</th>
						</tr>
					</thead>
					<tbody>
						<?php $no=1; ?>
						@foreach($pegawai as $item)
						<tr class="item{{$item->id}}">
							<td style="text-align: center;">{{$no++}}</td>
							<td style="text-align: center;">{{$item->nipbaru}}</td>
							<td>{{$item->s_nama_lengkap}}</td>
							<td>{{$item->jabatan}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>	
</div>

@stop
