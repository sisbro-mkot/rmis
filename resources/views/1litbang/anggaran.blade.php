@extends('1layout.app')

@section('isi')

<div class="slim-pageheader">
	<ol class="breadcrumb slim-breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashlitbang')}}">
			<i class="fa fa-home"></i> Home</a>
		</li>
		<li class="breadcrumb-item active" aria-current="page">Anggaran</li>
	</ol>
	<h6 class="slim-pagetitle">Mapping Anggaran</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
	<div class="pd-20">
		<div class="table-responsive-lg">
			<div class="table-wrapper">
				
				{{ csrf_field() }}
				<table id="tbl-identifikasi" class="table display">
					<thead align="center">
						<tr>
							<th width="5%">No.</th>
							<th style="text-align: center;">MAK</th>
							<th style="text-align: center;">Uraian</th>
							<th style="text-align: center;">Pagu Anggaran</th>
							<th style="text-align: center;">Mapping PKAU</th>
							<th style="text-align: center;">Selisih</th>
							<th></th>
						</tr>
					</thead>
				  	<tbody>
				  		<?php $no=1; ?>
				  		@foreach($anggaran as $item)
						<tr class="item{{$item->id}}">
							<td>{{$no++}}</td>
							<td>{{$item->mak}}</td>
							<td>{{$item->nmitem}}</td>
							<td style="text-align: right;">{{$item->jumlah}}</td>
							<td style="text-align: right;">{{$item->nilai}}</td>
							<td style="text-align: right;">{{($item->jumlah) - ($item->nilai)}}</td>
							<td style="text-align: center;">
								<a href="{{route('mappingtambah', $item->id)}}" type="submit" class="btn btn-primary"><i class="icon ion-android-create"></i></a>
						</td>
						</tr>
				  		@endforeach
				 	</tbody>
				 	<tfoot>
				 		<tr>
				  			<td colspan="3" style="text-align: center;">Total</td>
				  			<td class="total" style="text-align: right;">{{$total}}</td>
				  			<td class="total" style="text-align: right;">{{$nilai}}</td>
				  			<td class="total" style="text-align: right;">{{$total - $nilai}}</td>
				  			<td></td>
				  		</tr>
				 	</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@push('js')
  <script>
  	$(document).ready( function() {
	  $("td.total").each(function() { $(this).html(parseFloat($(this).text()).toLocaleString('id-ID')); })
	})

  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      	"columnDefs": [ {
        "targets": [3,4,5],
        "render": function(data, type, row) {
        return Number(data).toLocaleString('id-ID');
      }	
        } ],
		language: {
		searchPlaceholder: 'Cari...',
		sSearch: '',
		lengthMenu: '_MENU_ baris/halaman',
		}

    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush



