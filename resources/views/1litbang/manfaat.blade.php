@extends('1layout.app')

@section('isi')

<div class="slim-pageheader">
	<ol class="breadcrumb slim-breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashlitbang')}}">
			<i class="fa fa-home"></i> Home</a>
		</li>
		<li class="breadcrumb-item active" aria-current="page">Pelaporan</li>
	</ol>
	<h6 class="slim-pagetitle">Pemanfaatan Hasil Penelitian</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
	<div class="pd-20">
		<div class="table-responsive-lg">
			<div class="table-wrapper">
				
				{{ csrf_field() }}
				<table id="tbl-identifikasi" class="table table-bordered">
					<thead align="center">
						<tr>
							<th style="text-align: center; vertical-align: middle;" rowspan="3">No.</th>
							<th style="text-align: center; vertical-align: middle;" rowspan="3">Nomor dan Tanggal Laporan</th>
							<th style="text-align: center; vertical-align: middle;" rowspan="3">Nama Laporan</th>
							<th style="text-align: center; vertical-align: middle;" colspan="5">Initial Outcome</th>
							<th style="text-align: center; vertical-align: middle;" colspan="2">Intermediate Outcome</th>
							<th style="text-align: center; vertical-align: middle;" colspan="2">End Outcome</th>
						</tr>
						<tr>
							<th style="text-align: center; vertical-align: middle;">A1</th>
							<th style="text-align: center; vertical-align: middle;">B1</th>
							<th style="text-align: center; vertical-align: middle;">B2</th>
							<th style="text-align: center; vertical-align: middle;">B3</th>
							<th style="text-align: center; vertical-align: middle;">B4</th>
							<th style="text-align: center; vertical-align: middle;">A1</th>
							<th style="text-align: center; vertical-align: middle;">B1</th>
							<th style="text-align: center; vertical-align: middle;">A1</th>
							<th style="text-align: center; vertical-align: middle;">B1</th>
						</tr>
						<tr>
							<th style="text-align: center; vertical-align: middle;" width="5%">Distr Hsl</th>
							<th style="text-align: center; vertical-align: middle;" width="5%">Distr Hsl</th>
							<th style="text-align: center; vertical-align: middle;" width="5%">Semi nar/ Konfe rensi</th>
							<th style="text-align: center; vertical-align: middle;" width="5%">Media Cetak/ Elek tronik</th>
							<th style="text-align: center; vertical-align: middle;" width="5%">Bule tin/ Leaf let</th>
							<th style="text-align: center; vertical-align: middle;" width="5%">Sosi/ Eks pose</th>
							<th style="text-align: center; vertical-align: middle;" width="5%">Sosi/ Eks pose/ Ref Penu lisan</th>
							<th style="text-align: center; vertical-align: middle;" width="5%">Bahan Rapat/ Kbjkn/ Kptsn</th>
							<th style="text-align: center; vertical-align: middle;" width="5%">Bahan Rapat/ Kbjkn/ Kptsn</th>
						</tr>
					</thead>
				  	<tbody>
				  		<?php $no=1; ?>
				  		@foreach($anggaran as $item)						
						<tr class="item{{$item->id_penelitian}}">
							<td>{{$no++}}</td>
							<td>
								<a href="{{route('lit.edit', $item->id_penelitian)}}" style="color: Black;" >
									{{$item->no_penelitian}} <br/> <br/> {{Carbon\Carbon::parse($item->tgl_penelitian)->format('d M Y')}}
								</a>
							</td>
							<td> 
								<a href="{{route('lit.edit', $item->id_penelitian)}}" style="color: Black;" >
									{{$item->nama_penelitian}}
								</a>
							</td>
							<td style="text-align: center;">
								@if($item->init_a1 == 0)
									<i style="color: Red; font-size: 21px;"class="icon ion-android-cancel"></i>
								@else
									<i style="color: Green; font-size: 21px;"class="icon ion-android-checkbox"></i>
								@endif
							</td>
							<td style="text-align: center;">
								@if($item->init_b1 == 0)
									<i style="color: Red; font-size: 21px;"class="icon ion-android-cancel"></i>
								@else
									<i style="color: Green; font-size: 21px;"class="icon ion-android-checkbox"></i>
								@endif
							</td>
							<td style="text-align: center;">
								@if($item->init_b2 == 0)
									<i style="color: Red; font-size: 21px;"class="icon ion-android-cancel"></i>
								@else
									<i style="color: Green; font-size: 21px;"class="icon ion-android-checkbox"></i>
								@endif
							</td>
							<td style="text-align: center;">
								@if($item->init_b3 == 0)
									<i style="color: Red; font-size: 21px;"class="icon ion-android-cancel"></i>
								@else
									<i style="color: Green; font-size: 21px;"class="icon ion-android-checkbox"></i>
								@endif
							</td>
							<td style="text-align: center;">
								@if($item->init_b4 == 0)
									<i style="color: Red; font-size: 21px;"class="icon ion-android-cancel"></i>
								@else
									<i style="color: Green; font-size: 21px;"class="icon ion-android-checkbox"></i>
								@endif
							</td>
							<td style="text-align: center;">
								@if($item->inter_a1 == 0)
									<i style="color: Red; font-size: 21px;"class="icon ion-android-cancel"></i>
								@else
									<i style="color: Green; font-size: 21px;"class="icon ion-android-checkbox"></i>
								@endif
							</td>
							<td style="text-align: center;">
								@if($item->inter_b1 == 0)
									<i style="color: Red; font-size: 21px;"class="icon ion-android-cancel"></i>
								@else
									<i style="color: Green; font-size: 21px;"class="icon ion-android-checkbox"></i>
								@endif
							</td>
							<td style="text-align: center;">
								@if($item->end_a1 == 0)
									<i style="color: Red; font-size: 21px;"class="icon ion-android-cancel"></i>
								@else
									<i style="color: Green; font-size: 21px;"class="icon ion-android-checkbox"></i>
								@endif
							</td>
							<td style="text-align: center;">
								@if($item->end_b1 == 0)
									<i style="color: Red; font-size: 21px;"class="icon ion-android-cancel"></i>
								@else
									<i style="color: Green; font-size: 21px;"class="icon ion-android-checkbox"></i>
								@endif
							</td>
						</tr>
				  		@endforeach
				 	</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@push('js')
  <script>

  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
    	"columnDefs": [ {
        "targets": [0,1,2,3,4,5,6,7,8,9,10,11],
        "orderable": false
        } ],
		language: {
		searchPlaceholder: 'Cari...',
		sSearch: '',
		lengthMenu: '_MENU_ baris/halaman',
		}

    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush



