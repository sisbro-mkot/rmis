@extends('1layout.app')

@section('isi')

<div class="slim-pageheader">
	<ol class="breadcrumb slim-breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashlitbang')}}">
			<i class="fa fa-home"></i> Home</a>
		</li>
		<li class="breadcrumb-item active" aria-current="page">Aktivitas</li>
	</ol>
	<h6 class="slim-pagetitle">Aktivitas Kinerja</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
	<div class="pd-20">
		<div class="table-responsive-lg">
			<div class="table-wrapper">
				
				{{ csrf_field() }}
				<table id="tbl-identifikasi" class="table display">
					<thead align="center">
						<tr>
							<th width="5%">No.</th>
							<th width="6%" style="text-align: center;">Kode PKAU</th>
							<th style="text-align: center;">Nama PKAU</th>
							<th style="text-align: center;">Jumlah ST</th>
							<th style="text-align: center;">Anggaran</th>
						</tr>
					</thead>
				  	<tbody>
				  		<?php $no=1; ?>
				  		@foreach($aktivitas as $item)
						<tr class="item{{$item->id}}">
							<td>{{$no++}}</td>
							<td>{{$item->kode_pkau}}</td>
							<td>{{$item->nama_pkau}}</td>
							@if($item->count_st == 0)
								<td  style="text-align: right;">-</td>
							@else
								<td  style="text-align: right;">{{$item->count_st}}&nbsp;&nbsp;<i style="color: DodgerBlue;"class="fa fa-info-circle" data-toggle="modal" data-target="#modalTable" data-id="{{$item->id}}" data-aktivitas="{{$item->nama_pkau}}" onclick="showModal(this)"></i></td>
							@endif
							@if($item->nilai_pkau == 0)
								<td  style="text-align: right;">-</td>
							@else
								<td  style="text-align: right;">{{number_format($item->nilai_pkau, 0, ',', '.')}}</td>
							@endif
						</tr>
				  		@endforeach
				 	</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- The Modal -->

<div class="modal" tabindex="-1" id="modalTable" aria-hidden="true">
	<div class="modal-dialog modal-full" role="document">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<h5 class="modal-title">
					Daftar ST untuk PKAU: "<span id="Aktivitas"></span>"
				</h5>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<div class="card card-table">
					<div class="pd-20">
						<div class="table-responsive-lg">
							<div class="table-wrapper">
								<table id="mytable" class="table">
									<thead>
										<tr>
											<th class="col-xs-1" data-field="nomor" style="text-align: center;">No.</th>
											<th class="col-xs-1" data-field="nost" style="text-align: center;">Nomor ST</th>
											<th class="col-xs-1" data-field="tglst" style="text-align: center;">Tanggal ST</th>
											<th class="col-xs-7" data-field="uraianst" style="text-align: center;">Uraian ST</th>
											<th class="col-xs-1" data-field="tglmulai" style="text-align: center;">Tanggal Mulai</th>
											<th class="col-xs-1" data-field="tglselesai" style="text-align: center;">Tanggal Selesai</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection

@push('js')
  <script>

  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({

		language: {
		searchPlaceholder: 'Cari...',
		sSearch: '',
		lengthMenu: '_MENU_ baris/halaman',
		}

    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush

@push('js1')
  <script type="text/javascript">
    function showModal(ele) 
      {
          var id= $(ele).attr('data-id');
          var akt= $(ele).attr('data-aktivitas');
          document.getElementById("Aktivitas").innerHTML = akt;

          $.ajax({
              url: 'litbang/showST/'+id,
              type: 'get',
              dataType: 'json',
              success:function(data) {
                console.log(data);
                var t = $('#mytable').DataTable({
                    "bDestroy": true,
                    bJQueryUI: true,
                    aaData: data,
                    aoColumns: [
                        { mData: 'nomor' ,"fnRender": function( oObj ) { return oObj.aData[3].nomor }},
                        { mData: 'nost' ,"fnRender": function( oObj ) { return oObj.aData[3].nost }},
                        { mData: 'tglst',"fnRender": function( oObj ) { return oObj.aData[3].tglst }},
                        { mData: 'uraianst',"fnRender": function( oObj ) { return oObj.aData[3].uraianst }},
                        { mData: 'tglmulai',"fnRender": function( oObj ) { return oObj.aData[3].tglmulai }},
                        { mData: 'tglselesai',"fnRender": function( oObj ) { return oObj.aData[3].tglselesai }}
                             ],
                });

                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    });
                }).draw();

             }     
          });  
      };

  </script>
@endpush


