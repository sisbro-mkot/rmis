@extends('1layout.app')

@section('isi')

<div class="slim-pageheader">
	<ol class="breadcrumb slim-breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashlitbang')}}"><i class="fa fa-home"></i> Home</a></li>
		<li class="breadcrumb-item"><a href="{{route('spj.index')}}">Verifikasi SPJ</a></li>
		<li class="breadcrumb-item active" aria-current="page">Detail SPJ</li>
	</ol>
	<h6 class="slim-pagetitle">Detail SPJ</h6>
</div><!-- slim-pageheader -->

<div class="card card-table">	
	<div class="card-body">
		<div class="row">
			<div class="col-sm-6">	
				
				<span>Tanggal Verifikasi SPJ : </span>
				<p class="tanggal" style="color: black;">{{Carbon\Carbon::parse($anggaran->tgl_spj)->format('d M Y')}}</p>				
			
				<span>Nomor ST/ND : </span>
				<p style="color: black;">{{$anggaran->no_st}}</p>
	
				<span>Tanggal ST/ND : </span>
				<p style="color: black;">{{Carbon\Carbon::parse($anggaran->tgl_st)->format('d M Y')}}</p>
			
				<span>Uraian ST/ND : </span>
				<p style="color: black;">{{$anggaran->uraian_st}}</p>
			
				<span>Aktivitas Kinerja : </span>
				@if($anggaran->nama_detil_aktivitas)
				<p style="color: black;">{{$anggaran->kode_aktivitas}} - {{$anggaran->nama_aktivitas}} - {{$anggaran->nama_detil_aktivitas}}</p>
				@else
				<p style="color: black;">{{$anggaran->kode_aktivitas}} - {{$anggaran->nama_aktivitas}}</p>
				@endif

			</div>
			<div class="col-sm-6">	
		
				<span>Sub Komponen : </span>
				<p style="color: black;">{{$anggaran->nama_sub_komponen}}</p>
	
				<span>MAK : </span>
				<p style="color: black;">{{$anggaran->ket_mak}} - {{$anggaran->nama_mak}}</p>

				<span>Nilai SPJ : </span>
				<p class="total" style="color: black;">{{$anggaran->nilai_spj}}</p>
	
				<span>Keterangan : </span>
				@if($anggaran->ket_spj)
				<p style="color: black;">{{$anggaran->ket_spj}}</p>
				@else
				<p style="color: black;">-</p>
				@endif

				<div class="form-group">
					<div class="row">
						@if (Auth::user()->user_nip == '740006317'|Auth::user()->user_nip == '201800140'|Auth::user()->user_nip == '202000050')
						<div class="col-sm-1">				
							<a href="{{route('spj.edit', $anggaran->id)}}" type="submit" class="btn btn-success"><i class="icon ion-android-create"></i> Edit SPJ</a>				
						</div>
						<!-- <div class="col-sm-11">
							<form method="POST" action="{{route('spj.destroy', $anggaran->id)}}">
							    {{ csrf_field() }}
							    {{ method_field('DELETE') }}
							    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus SPJ</button>
							</form>				
						</div> -->
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>

@stop

@push('js')
<script type="text/javascript">
	$(document).ready( function() {
		$("p.total").each(function() { $(this).html(parseFloat($(this).text()).toLocaleString('id-ID', {style: 'currency', currency: 'IDR'})); })
	});
</script>
@endpush