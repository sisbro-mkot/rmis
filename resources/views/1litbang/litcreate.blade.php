@extends('1layout.app')

@section('isi')

<div class="slim-pageheader">
	<ol class="breadcrumb slim-breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashlitbang')}}"><i class="fa fa-home"></i> Home</a></li>
		<li class="breadcrumb-item"><a href="{{route('lit.index')}}">Laporan</a></li>
		<li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
	</ol>
	<h6 class="slim-pagetitle">Tambah Laporan</h6>
</div><!-- slim-pageheader -->

<div class="card card-table">
	@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
		    @foreach ($errors->all() as $error)
		    <li>{{ $error }}</li>
		    @endforeach
		</ul>
	</div>
	@endif
	<form class="form-horizontal mt-2" action="{{route('lit.store')}}" method="post">
		{{ csrf_field() }}
		<div class="box-body">
			<div class="form-row px-4 pt-2">
				<div class="form-group col-sm-4">
					<label for="no_penelitian" class="control-label">Nomor Laporan</label>
					<input type="text" class="form-control" id="no_penelitian" name="no_penelitian">
				</div>

				<div class="form-group col-sm-3">
					<label for="tgl_penelitian" class="control-label">Tanggal Laporan</label>
					<input type="date" class="form-control" id="tgl_penelitian" name="tgl_penelitian" value="<?php echo date('Y-m-d'); ?>">
				</div>

				<div class="form-group col-sm-5">
					<label for="id_ikk" class="control-label">Jenis Laporan</label>
					<select name="id_ikk" class="form-control" id="id_ikk" autofocus>
						@foreach($id_ikk as $key)
						<option value="{{$key->id_ikk}}">{{$key->ket_ikk}}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-row px-4">
				<div class="form-group col-sm-12">
					<label for="nama_penelitian" class="control-label">Nama Laporan</label>
					<textarea class="form-control" id="nama_penelitian" name="nama_penelitian"></textarea>
				</div>
			</div>
			
			<div class="form-row px-4">
				<div class="form-group col-sm-12">
					<button type="submit" class="btn btn-primary"> Simpan</button>
					<a href="{{route('stugas.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
				</div>
			</div>
		</div>
	</form>
</div>

@stop

@push('js')
<script type="text/javascript">
	$(document).ready(function() {
		$("#id_ikk").select2();

	});
</script>
@endpush