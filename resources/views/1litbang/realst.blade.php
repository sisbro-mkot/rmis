@extends('1layout.app')

@section('isi')

<div class="slim-pageheader">
	<ol class="breadcrumb slim-breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashlitbang')}}">
			<i class="fa fa-home"></i> Home</a>
		</li>
		<li class="breadcrumb-item active" aria-current="page">Pelaporan</li>
	</ol>
	<h6 class="slim-pagetitle">Realisasi Keuangan dan Aktivitas per Bulan</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
	<div class="card-header">
		<div class="col-sm-3">
			<label for="bulan_st" class="control-label" style="text-align: center;">Bulan: </label>
			<select class="form-control" name="bulan_st" id="bulan_st">
				<option value="0" selected>Januari - Desember</option>
				@foreach($bulan_st as $key => $value)
				<option value="{{$key}}" >{{$value}}</option>
				@endforeach
			</select>
		</div>
	</div>  
	<div class="pd-20">
		<div class="table-responsive-lg">
			<div class="table-wrapper">
				
				{{ csrf_field() }}
				<table id="tbl-identifikasi" class="table display">
					<thead align="center">
						<tr>
							<th width="5%">No.</th>
							<th style="text-align: center;"  width="15%">No. ST/ND/SK</th>
							<th style="text-align: center;">Uraian ST/ND/SK</th>
							<th style="text-align: center;">Progress</th>
							<th style="text-align: center;">Realisasi Keuangan</th>
							<th style="text-align: center;" width="10%">Bidang</th>
							<th style="text-align: center;">Kode PKAU</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@push('js')
<script type="text/javascript">
  $(document).ready(function() {
    $("#bulan_st").select2();

      var bulanID = $('#bulan_st').val();
      console.log(bulanID);
      if(bulanID > 0){ 
        $.ajax({
              url: 'litbang/pilihBulan/'+bulanID,
              type: 'get',
              dataType: 'json',
              success:function(data) {
                var t = $('#tbl-identifikasi').DataTable({                	
                    "columnDefs": [ {
                                      "className": "text-center",
                                      "targets": [3,4,5],
                                    }],
                    language: {
                                searchPlaceholder: 'Cari...',
                                sSearch: '',
                                lengthMenu: '_MENU_ baris/halaman',
                              },
                    "bDestroy": true,
                    bJQueryUI: true,
                    aaData: data,
                    aoColumns: [
                        { mData: 'nomor' ,"fnRender": function( oObj ) { return oObj.aData[3].nomor }},
                        { mData: 'st' ,"fnRender": function( oObj ) { return oObj.aData[3].st }},
                        { mData: 'uraian' ,"fnRender": function( oObj ) { return oObj.aData[3].uraian }},
                        { mData: 'progress',"mRender": function(data, type, full) { return (full['progress'] > 0) ? (full['progress'] + '%') : "-" }},                        
                        { mData: 'nilai',"mRender": function(data, type, full) { return (full['nilai'] > 0) ? (Number(full['nilai']).toLocaleString('id-ID')) : "-" }},
                        { mData: 'bidang' ,"fnRender": function( oObj ) { return oObj.aData[3].bidang }},
                        { mData: 'kode' ,"fnRender": function( oObj ) { return oObj.aData[3].kode }}
                              ],

                });

                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    });
                }).draw();

             }     
          }); 
      } else {
        $.ajax({
              url: 'litbang/semuaBulan/',
              type: 'get',
              dataType: 'json',
              success:function(data) {
                var t = $('#tbl-identifikasi').DataTable({                	
                    "columnDefs": [ {
                                      "className": "text-center",
                                      "targets": [3,4,5],
                                    }],
                    language: {
                                searchPlaceholder: 'Cari...',
                                sSearch: '',
                                lengthMenu: '_MENU_ baris/halaman',
                              },
                    "bDestroy": true,
                    bJQueryUI: true,
                    aaData: data,
                    aoColumns: [
                        { mData: 'nomor' ,"fnRender": function( oObj ) { return oObj.aData[3].nomor }},
                        { mData: 'st' ,"fnRender": function( oObj ) { return oObj.aData[3].st }},
                        { mData: 'uraian' ,"fnRender": function( oObj ) { return oObj.aData[3].uraian }},
                        { mData: 'progress',"mRender": function(data, type, full) { return (full['progress'] > 0) ? (full['progress'] + '%') : "-" }},  
                        { mData: 'nilai',"mRender": function(data, type, full) { return (full['nilai'] > 0) ? (Number(full['nilai']).toLocaleString('id-ID')) : "-" }},
                        { mData: 'bidang' ,"fnRender": function( oObj ) { return oObj.aData[3].bidang }},
                        { mData: 'kode' ,"fnRender": function( oObj ) { return oObj.aData[3].kode }}
                              ],

                });

                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    });
                }).draw();

             }     
          });
      }
 
});
</script>
@endpush

@push('js1')
<script type="text/javascript">
$(document).ready(function() {
    $('#bulan_st').select2().on('change', function() {
		var bulanID = $(this).val();
		console.log(bulanID);
		if(bulanID > 0){ 
		$.ajax({
		      url: 'litbang/pilihBulan/'+bulanID,
		      type: 'get',
		      dataType: 'json',
		      success:function(data) {
		        var t = $('#tbl-identifikasi').DataTable({
		        	"paging": false,                	
		            "columnDefs": [ {
		                              "className": "text-center",
		                              "targets": [3,4,5],
		                            }],
		            language: {
		                        searchPlaceholder: 'Cari...',
		                        sSearch: '',
		                        lengthMenu: '_MENU_ baris/halaman',
		                      },
		            "bDestroy": true,
		            bJQueryUI: true,
		            aaData: data,
		            aoColumns: [
		                { mData: 'nomor' ,"fnRender": function( oObj ) { return oObj.aData[3].nomor }},
		                { mData: 'st' ,"fnRender": function( oObj ) { return oObj.aData[3].st }},
		                { mData: 'uraian' ,"fnRender": function( oObj ) { return oObj.aData[3].uraian }},
		                { mData: 'progress',"mRender": function(data, type, full) { return (full['progress'] > 0) ? (full['progress'] + '%') : "-" }},  
		                { mData: 'nilai',"mRender": function(data, type, full) { return (full['nilai'] > 0) ? (Number(full['nilai']).toLocaleString('id-ID')) : "-" }},
		                { mData: 'bidang' ,"fnRender": function( oObj ) { return oObj.aData[3].bidang }},
		                { mData: 'kode' ,"fnRender": function( oObj ) { return oObj.aData[3].kode }}
		                      ],

		        });

		        t.on( 'order.dt search.dt', function () {
		            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		                cell.innerHTML = i+1;
		            });
		        }).draw();

		     }     
		  }); 
		} else {
		$.ajax({
		      url: 'litbang/semuaBulan/',
		      type: 'get',
		      dataType: 'json',
		      success:function(data) {
		        var t = $('#tbl-identifikasi').DataTable({                	
		            "columnDefs": [ {
		                              "className": "text-center",
		                              "targets": [3,4,5],
		                            }],
		            language: {
		                        searchPlaceholder: 'Cari...',
		                        sSearch: '',
		                        lengthMenu: '_MENU_ baris/halaman',
		                      },
		            "bDestroy": true,
		            bJQueryUI: true,
		            aaData: data,
		            aoColumns: [
		                { mData: 'nomor' ,"fnRender": function( oObj ) { return oObj.aData[3].nomor }},
		                { mData: 'st' ,"fnRender": function( oObj ) { return oObj.aData[3].st }},
		                { mData: 'uraian' ,"fnRender": function( oObj ) { return oObj.aData[3].uraian }},
		                { mData: 'progress',"mRender": function(data, type, full) { return (full['progress'] > 0) ? (full['progress'] + '%') : "-" }},  
		                { mData: 'nilai',"mRender": function(data, type, full) { return (full['nilai'] > 0) ? (Number(full['nilai']).toLocaleString('id-ID')) : "-" }},
		                { mData: 'bidang' ,"fnRender": function( oObj ) { return oObj.aData[3].bidang }},
		                { mData: 'kode' ,"fnRender": function( oObj ) { return oObj.aData[3].kode }}
		                      ],

		        });

		        t.on( 'order.dt search.dt', function () {
		            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		                cell.innerHTML = i+1;
		            });
		        }).draw();

		     }     
		  });
		}
    });
});
</script>
@endpush

