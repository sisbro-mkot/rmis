@extends('1layout.app')

@section('isi')

<style type="text/css">
  body {  background-color: white;}
</style>

<div class="row">

</div>

<div class="row">
	<div class="col-sm-12">
	    <div id="kepegawaian"></div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
	    <div id="keuangan"></div>
	</div>
</div>

<!-- Tata Usaha -->
  <!-- The Modal -->

  <div class="modal" tabindex="-1" id="modalTU" aria-hidden="true">
    <div class="modal-dialog modal-full" role="document">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h5 class="modal-title">Surat Tugas Bagian Tata Usaha</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <div class="card card-table">
          <div class="pd-20">
          <div class="table-responsive-lg">
          <div class="table-wrapper">
          <table id="mytable" class="table">
              <thead>
                  <tr>
                      <th width="5%" style="text-align: center;">No.</th>
                      <th width="17%" style="text-align: center;">Nomor ST</th>
                      <th width="10%" style="text-align: center;">Tanggal ST</th>
                      <th style="text-align: center;">Uraian ST</th>
                      <th width="10%" style="text-align: center;">Tanggal Mulai</th>
                      <th width="10%" style="text-align: center;">Tanggal Selesai</th>
                      <th style="text-align: center;"></th>
                  </tr>
               </thead>
               <tbody>
               		<?php $no=1; ?>
      						@foreach($tu as $item)
      						<tr class="item{{$item->id_st}}">
      							<td>{{$no++}}</td>
      							<td>{{$item->no_st}}</td>
      							<td>{{Carbon\Carbon::parse($item->tgl_st)->format('d M Y')}}</td>
										<td>{{$item->uraian_st}}</td>
										<td>{{Carbon\Carbon::parse($item->tgl_mulai)->format('d M Y')}}</td>
										<td>{{Carbon\Carbon::parse($item->tgl_selesai)->format('d M Y')}}</td>
										<td style="text-align: center;"><a href="{{route('stugas.show', $item->id_st)}}" class="badge badge-primary"> Detail</a></td>
      						</tr>
      						@endforeach
               </tbody>
          </table>
          </div>
          </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- Penelitian -->
  <!-- The Modal -->

  <div class="modal" tabindex="-1" id="modalB1" aria-hidden="true">
    <div class="modal-dialog modal-full" role="document">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h5 class="modal-title">Surat Tugas Bidang Penelitian Pengawasan</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <div class="card card-table">
          <div class="pd-20">
          <div class="table-responsive-lg">
          <div class="table-wrapper">
          <table id="mytable" class="table">
              <thead>
                  <tr>
                      <th width="5%" style="text-align: center;">No.</th>
                      <th width="17%" style="text-align: center;">Nomor ST</th>
                      <th width="10%" style="text-align: center;">Tanggal ST</th>
                      <th style="text-align: center;">Uraian ST</th>
                      <th width="10%" style="text-align: center;">Tanggal Mulai</th>
                      <th width="10%" style="text-align: center;">Tanggal Selesai</th>
                      <th style="text-align: center;"></th>
                  </tr>
               </thead>
               <tbody>
               		<?php $no=1; ?>
      						@foreach($b1 as $item)
      						<tr class="item{{$item->id_st}}">
      							<td>{{$no++}}</td>
      							<td>{{$item->no_st}}</td>
      							<td>{{Carbon\Carbon::parse($item->tgl_st)->format('d M Y')}}</td>
										<td>{{$item->uraian_st}}</td>
										<td>{{Carbon\Carbon::parse($item->tgl_mulai)->format('d M Y')}}</td>
										<td>{{Carbon\Carbon::parse($item->tgl_selesai)->format('d M Y')}}</td>
										<td style="text-align: center;"><a href="{{route('stugas.show', $item->id_st)}}" class="badge badge-primary"> Detail</a></td>
      						</tr>
      						@endforeach
               </tbody>
          </table>
          </div>
          </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- Pengembangan -->
  <!-- The Modal -->

  <div class="modal" tabindex="-1" id="modalB2" aria-hidden="true">
    <div class="modal-dialog modal-full" role="document">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h5 class="modal-title">Surat Tugas Bidang Pengembangan dan Inovasi Pengawasan</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <div class="card card-table">
          <div class="pd-20">
          <div class="table-responsive-lg">
          <div class="table-wrapper">
          <table id="mytable" class="table">
              <thead>
                  <tr>
                      <th width="5%" style="text-align: center;">No.</th>
                      <th width="17%" style="text-align: center;">Nomor ST</th>
                      <th width="10%" style="text-align: center;">Tanggal ST</th>
                      <th style="text-align: center;">Uraian ST</th>
                      <th width="10%" style="text-align: center;">Tanggal Mulai</th>
                      <th width="10%" style="text-align: center;">Tanggal Selesai</th>
                      <th style="text-align: center;"></th>
                  </tr>
               </thead>
               <tbody>
               		<?php $no=1; ?>
      						@foreach($b2 as $item)
      						<tr class="item{{$item->id_st}}">
      							<td>{{$no++}}</td>
      							<td>{{$item->no_st}}</td>
      							<td>{{Carbon\Carbon::parse($item->tgl_st)->format('d M Y')}}</td>
										<td>{{$item->uraian_st}}</td>
										<td>{{Carbon\Carbon::parse($item->tgl_mulai)->format('d M Y')}}</td>
										<td>{{Carbon\Carbon::parse($item->tgl_selesai)->format('d M Y')}}</td>
										<td style="text-align: center;"><a href="{{route('stugas.show', $item->id_st)}}" class="badge badge-primary"> Detail</a></td>
      						</tr>
      						@endforeach
               </tbody>
          </table>
          </div>
          </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- IKK1 -->

        <div class="modal" tabindex="-1" id="IKK1" aria-hidden="true">
          <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">

              <!-- Modal Header -->
              <div class="modal-header">
              	<h5 style="color: black;">Laporan Hasil Penelitian dan Pengembangan Pengawasan</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <!-- Modal body -->
              <div class="modal-body">
                <div class="card card-table">
                <div class="pd-20">
                <div class="table-responsive-lg">
                <div class="table-wrapper">
                <table id="mytable" class="table">
                    <thead>
                        <tr>
			                      <th width="5%" style="text-align: center;">No.</th>
			                      <th width="17%" style="text-align: center;">Nomor Laporan</th>
			                      <th width="10%" style="text-align: center;">Tanggal Laporan</th>
			                      <th style="text-align: center;">Nama Laporan</th>
			                  </tr>
                     </thead>
                     <tbody>
			               		<?php $no=1; ?>
			      						@foreach($lap1 as $item)
			      						<tr class="item{{$item->id_penelitian}}">
			      							<td>{{$no++}}</td>
			      							<td>{{$item->no_penelitian}}</td>
			      							<td>{{Carbon\Carbon::parse($item->tgl_penelitian)->format('d M Y')}}</td>
													<td>{{$item->nama_penelitian}}</td>
			      						</tr>
			      						@endforeach
			               </tbody>
                </table>
                </div>
                </div>
                </div>
                </div>
              </div>

            </div>
          </div>
        </div>

<!-- IKK2 -->

        <div class="modal" tabindex="-1" id="IKK2" aria-hidden="true">
          <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">

              <!-- Modal Header -->
              <div class="modal-header">
              	<h5 style="color: black;">Laporan Pembinaan dan Koordinasi</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <!-- Modal body -->
              <div class="modal-body">
                <div class="card card-table">
                <div class="pd-20">
                <div class="table-responsive-lg">
                <div class="table-wrapper">
                <table id="mytable" class="table">
                    <thead>
                        <tr>
			                      <th width="5%" style="text-align: center;">No.</th>
			                      <th width="17%" style="text-align: center;">Nomor Laporan</th>
			                      <th width="10%" style="text-align: center;">Tanggal Laporan</th>
			                      <th style="text-align: center;">Nama Laporan</th>
			                  </tr>
                     </thead>
                     <tbody>
			               		<?php $no=1; ?>
			      						@foreach($lap2 as $item)
			      						<tr class="item{{$item->id_penelitian}}">
			      							<td>{{$no++}}</td>
			      							<td>{{$item->no_penelitian}}</td>
			      							<td>{{Carbon\Carbon::parse($item->tgl_penelitian)->format('d M Y')}}</td>
													<td>{{$item->nama_penelitian}}</td>
			      						</tr>
			      						@endforeach
			               </tbody>
                </table>
                </div>
                </div>
                </div>
                </div>
              </div>

            </div>
          </div>
        </div>

<!-- IKK3 -->

        <div class="modal" tabindex="-1" id="IKK3" aria-hidden="true">
          <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">

              <!-- Modal Header -->
              <div class="modal-header">
              	<h5 style="color: black;">Laporan Pengembangan Kompetensi SDM</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <!-- Modal body -->
              <div class="modal-body">
                <div class="card card-table">
                <div class="pd-20">
                <div class="table-responsive-lg">
                <div class="table-wrapper">
                <table id="mytable" class="table">
                    <thead>
                        <tr>
			                      <th width="5%" style="text-align: center;">No.</th>
			                      <th width="17%" style="text-align: center;">Nomor Laporan</th>
			                      <th width="10%" style="text-align: center;">Tanggal Laporan</th>
			                      <th style="text-align: center;">Nama Laporan</th>
			                  </tr>
                     </thead>
                     <tbody>
			               		<?php $no=1; ?>
			      						@foreach($lap3 as $item)
			      						<tr class="item{{$item->id_penelitian}}">
			      							<td>{{$no++}}</td>
			      							<td>{{$item->no_penelitian}}</td>
			      							<td>{{Carbon\Carbon::parse($item->tgl_penelitian)->format('d M Y')}}</td>
													<td>{{$item->nama_penelitian}}</td>
			      						</tr>
			      						@endforeach
			               </tbody>
                </table>
                </div>
                </div>
                </div>
                </div>
              </div>

            </div>
          </div>
        </div>

<!-- IKK4 -->

        <div class="modal" tabindex="-1" id="IKK4" aria-hidden="true">
          <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">

              <!-- Modal Header -->
              <div class="modal-header">
              	<h5 style="color: black;">Laporan Hasil Pengembangan KMS</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <!-- Modal body -->
              <div class="modal-body">
                <div class="card card-table">
                <div class="pd-20">
                <div class="table-responsive-lg">
                <div class="table-wrapper">
                <table id="mytable" class="table">
                    <thead>
                        <tr>
			                      <th width="5%" style="text-align: center;">No.</th>
			                      <th width="17%" style="text-align: center;">Nomor Laporan</th>
			                      <th width="10%" style="text-align: center;">Tanggal Laporan</th>
			                      <th style="text-align: center;">Nama Laporan</th>
			                  </tr>
                     </thead>
                     <tbody>
			               		<?php $no=1; ?>
			      						@foreach($lap4 as $item)
			      						<tr class="item{{$item->id_penelitian}}">
			      							<td>{{$no++}}</td>
			      							<td>{{$item->no_penelitian}}</td>
			      							<td>{{Carbon\Carbon::parse($item->tgl_penelitian)->format('d M Y')}}</td>
													<td>{{$item->nama_penelitian}}</td>
			      						</tr>
			      						@endforeach
			               </tbody>
                </table>
                </div>
                </div>
                </div>
                </div>
              </div>

            </div>
          </div>
        </div>

@endsection


@push('js')
  <script>
  	$(document).ready( function() {

    $.ajax({
              url: 'litbang/bismadan/',
              type: 'get',
              dataType: 'json',
              success:function(data) {
                console.log(data);
              }
          });

	  $("h6.card-title").each(function() { $(this).html(parseFloat($(this).text()).toLocaleString('id-ID', {style: 'currency', currency: 'IDR'})); })
	});
  	Highcharts.setOptions({

    lang: {
      decimalPoint: ',',
      thousandsSep: '.'
    }
	});

	Highcharts.chart('kepegawaian', {
		colors: [  'seagreen', 'darkolivegreen'],
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: 'Jumlah ST per Bidang (Posisi Hari Ini)'
	    },
	    xAxis: {
	        categories: [
	        	@foreach($pegawai as $dt)
              '{{$dt->nama_bidang}}',
          	@endforeach	            
	        ],
	        tickInterval: 1,
	        crosshair: true
	    },
	    yAxis: {
	        min: 0,
	        tickInterval: 1,
	        title: {
	            text: 'Jumlah ST'
	        }
	    },
	    tooltip: {
	        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	            '<td style="padding:0"><b>{point.y}</b></td></tr>',
	        footerFormat: '</table>',
	        shared: true,
	        useHTML: true
	    },
	    plotOptions: {
	        column: {
	            pointPadding: 0.2,
	            borderWidth: 0,
	            point: {
		        events: {
		          click: function(te) {
		            this.x == 0 ? 
		            	$('#modalB1').modal('show')
		             : (this.x == 1 ? 
		            	$('#modalB2').modal('show')
		             : 
		            	$('#modalTU').modal('show')
		            )
		          }
		        }
		      }
	        },
	        
	        cursor: 'pointer'   
	    },
	    series: [{
	        name: 'Jumlah ST',
	        data: [@foreach($pegawai as $dt)
                {{$dt->jml_st}},
                @endforeach]

	    }]
	});

	Highcharts.chart('keuangan', {
		colors: [  'seagreen', 'darkolivegreen'],
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: 'Anggaran dan Realisasi per Bidang/Bagian'
	    },
	    xAxis: {
	        categories: [
	        	
	            'Bidang Penelitian Pengawasan',
	            'Bidang Pengembangan dan Inovasi Pengawasan',
              'Bagian Tata Usaha'
	            
	        ],
	        crosshair: true
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Jumlah (Rp)'
	        }
	    },
	    tooltip: {
	        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	            '<td style="padding:0"><b>Rp {point.y:,.2f}</b></td></tr>',
	        footerFormat: '</table>',
	        shared: true,
	        useHTML: true
	    },
	    plotOptions: {
	        column: {
	            pointPadding: 0.2,
	            borderWidth: 0
	        }
	    },
	    series: [{
	        name: 'Anggaran',
	        data: [@foreach($anggaranch as $dt)
                {{$dt->anggaranb}},
                @endforeach]

	    }, {
	        name: 'Realisasi',
	        data: [@foreach($realisasich as $dt)
                {{$dt->realisasib}},
                @endforeach]

	    }]
	});
  </script>
@endpush

@push('js1')
<script>
	
</script>
@endpush