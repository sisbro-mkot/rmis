@extends('1layout.app')

@section('isi')

<div class="slim-pageheader">
	<ol class="breadcrumb slim-breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashlitbang')}}"><i class="fa fa-home"></i> Home</a></li>
		<li class="breadcrumb-item"><a href="{{route('stugas.index')}}">Surat Tugas</a></li>
		<li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
	</ol>
	<h6 class="slim-pagetitle">Tambah Data Surat Tugas</h6>
</div><!-- slim-pageheader -->

<div class="card card-table">
	@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
		    @foreach ($errors->all() as $error)
		    <li>{{ $error }}</li>
		    @endforeach
		</ul>
	</div>
	@endif
	<form class="form-horizontal mt-2" action="{{route('stugas.store')}}" method="post">
		{{ csrf_field() }}
		<div class="box-body">
			<div class="form-row px-4 pt-2">
				<div class="form-group col-sm-5">
					<label for="id_bidang" class="control-label">Konseptor ST</label>
					<select name="id_bidang" class="form-control" id="id_bidang" autofocus>
						@foreach($id_bidang as $key)
						<option value="{{$key->id_bidang}}">{{$key->kd_konsep}} ({{$key->nama_bidang}})</option>
						@endforeach
					</select>
				</div>

				<div class="form-group col-sm-4">
					<label for="no_st" class="control-label">Nomor ST</label>
					<input type="text" class="form-control" id="no_st" name="no_st">
				</div>

				<div class="form-group col-sm-3">
					<label for="tgl_st" class="control-label">Tanggal ST</label>
					<input type="date" class="form-control" id="tgl_st" name="tgl_st" value="<?php echo date('Y-m-d'); ?>">
				</div>
			</div>

			<div class="form-row px-4">
				<div class="form-group col-sm-12">
					<label for="uraian_st" class="control-label">Uraian ST</label>
					<textarea class="form-control" id="uraian_st" name="uraian_st"></textarea>
				</div>
			</div>

			<div class="form-row px-4">
				<div class="form-group col-sm-3">
					<label for="tgl_mulai" class="control-label">Tanggal Mulai</label>
					<input type="date" class="form-control" id="tgl_mulai" name="tgl_mulai" value="<?php echo date('Y-m-d'); ?>">
				</div>

				<div class="form-group col-sm-3">
					<label for="tgl_selesai" class="control-label">Tanggal Selesai</label>
					<input type="date" class="form-control" id="tgl_selesai" name="tgl_selesai" value="<?php echo date('Y-m-d'); ?>">
				</div>

				<div class="form-group col-sm-1">
					<label for="tgl_selesai" class="control-label">Jumlah Hari</label>
					<input type="number" class="form-control" id="jml_hari" name="jml_hari" min="1" max="365">
				</div>
			</div>
			
			<div class="form-row px-4">
				<div class="form-group col-sm-12">
					<label for="id_pkau" class="control-label">Nama PKAU</label>
					<select name="id_pkau" class="form-control" id="id_pkau" autofocus>
						@if (Auth::user()->user_nip == '740006317')
						@foreach($id_pkau as $key)
						<option value="{{$key->id_pkau}}">{{$key->kode_pkau}} - {{$key->nama_pkau}}</option>
						@endforeach
						@endif
					</select>
				</div>
			</div>
			
			<div class="form-row px-4">
				<div class="form-group col-sm-12">
					<button type="submit" class="btn btn-primary"> Simpan</button>
					<a href="{{route('stugas.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
				</div>
			</div>
		</div>
	</form>
</div>

@stop

@push('js')
<script type="text/javascript">
	$(document).ready(function() {
		$("#id_bidang").select2();
		$("#id_pkau").select2();

	});
</script>
@endpush
