@extends('1layout.app')

@section('isi')

<div class="slim-pageheader">
	<ol class="breadcrumb slim-breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashlitbang')}}"><i class="fa fa-home"></i> Home</a></li>
		<li class="breadcrumb-item"><a href="{{route('anggaran')}}">Anggaran</a></li>
		<li class="breadcrumb-item active" aria-current="page">Mapping PKAU</li>
	</ol>
	<h6 class="slim-pagetitle">Mapping PKAU</h6>
</div><!-- slim-pageheader -->

@if ($errors->any())
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<div class="card card-table">	
	<div class="card-body">
		<div class="row">
			<div class="col-sm-3">
				<span>MAK : </span>
				<p style="color: black;">{{$id_dja_label->mak}}</p>
			</div>
			<div class="col-sm-9">
				<span>Uraian : </span>
				<p style="color: black;">{{$id_dja_label->nmitem}}</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<span>Jumlah : </span>
				<p style="color: black;">{{$id_dja_label->jumlah}}</p>
			</div>
			<div class="col-sm-3">
				<span>Mapping : </span>
				<p style="color: black;">{{$id_dja_label->nilai}}</p>
			</div>
			<div class="col-sm-3">
				<span>Sisa : </span>
				<p style="color: black;">{{($id_dja_label->jumlah) - ($id_dja_label->nilai)}}</p>
			</div>
		</div>
	</div>
	<div class="pd-20">
	<form class="form-horizontal mt-2" action="{{route('anggaranc.store')}}" method="post">
		{{ csrf_field() }}
		<div class="form-group">
			<div class="row" id="id_hide">
				<select name="id_dja" class="form-control" id="id_dja" autofocus>
					@foreach($id_dja as $key)
					<option value="{{$key->id}}">{{$key->id}}</option>
					@endforeach
				</select>
			</div>
			<div class="row">
				<div class="col-sm-7">
					<label for="id_pkau" class="control-label">Nama PKAU :</label>
					<select name="id_pkau" class="form-control" id="id_pkau" autofocus>
						@foreach($id_pkau as $key)
						<option value="{{$key->id}}">{{$key->kode_pkau}} - {{$key->nama_pkau}}</option>
						@endforeach
					</select>
		    </div>
		    <div class="col-sm-5">
					<label for="nilai" class="control-label">Nilai Mapping :</label>
					<input type="number" class="form-control" id="nilai" name="nilai" min="0" max="100000000000">
		    </div>
			</div>
			<br/>
			<div class="row">
				<div class="col-sm-1">				
					<button type="submit" class="btn btn-primary"> Simpan</button>			
				</div>
			</div>
		</div>
	</form>
		<div class="table-responsive-lg">
			<div class="table-wrapper">
				{{ csrf_field() }}
				<table id="tbl-identifikasi" class="table display">
					<thead align="center">
						<tr>
							<th width="5%">No.</th>
							<th width="10%" style="text-align: center;">Kode PKAU</th>
							<th style="text-align: center;">Nama PKAU</th>
							<th style="text-align: center;">Nilai</th>
							<th style="text-align: center;"></th>
						</tr>
					</thead>
					<tbody>
						<?php $no=1; ?>
						@foreach($id_mapping as $item)
						<tr class="item{{$item->id}}">
							<td style="text-align: center;">{{$no++}}</td>
							<td>{{$item->kode_pkau}}</td>
							<td>{{$item->nama_pkau}}</td>
							<td>{{$item->nilai}}</td>
							<td>
								<form method="POST" action="{{ route('anggaranc.destroy', $item->id) }}">
								    {{ csrf_field() }}
								    {{ method_field('DELETE') }}
								    <button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('Yakin akan dihapus ?')"><i class="icon ion-ios-trash"></i> Hapus</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>	
</div>

@stop


@push('js')
<script type="text/javascript">
  $(document).ready(function() {
  	$("#id_hide").hide();
    $("#id_pkau").select2();

});
</script>
@endpush