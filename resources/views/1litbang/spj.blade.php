@extends('1layout.app')

@section('isi')

<div class="slim-pageheader">
	<ol class="breadcrumb slim-breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashlitbang')}}">
			<i class="fa fa-home"></i> Home</a>
		</li>
		<li class="breadcrumb-item active" aria-current="page">SPJ</li>
	</ol>
	<h6 class="slim-pagetitle">Verifikasi SPJ</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
	@if (Auth::user()->user_nip == '740006317'|Auth::user()->user_nip == '201800140'|Auth::user()->user_nip == '202000050')
	<div class="card-header">
		<a href="{{route('spjcreate')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Buat SPJ</a>
	</div>
	@endif
	<div class="pd-20">
		<div class="table-responsive-lg">
			<div class="table-wrapper">
				
				{{ csrf_field() }}
				<table id="tbl-identifikasi" class="table display">
					<thead align="center">
						<tr>
							<th width="5%">No.</th>
							<th style="text-align: center;">Nomor ST/ND</th>
							<th style="text-align: center;">Tanggal ST/ND</th>
							<th style="text-align: center;">Uraian ST/ND</th>
							<th style="text-align: center;">Nilai SPJ</th>
							<th></th>
						</tr>
					</thead>
				  	<tbody>
				  		<?php $no=1; ?>
				  		@foreach($anggaran as $item)

						<tr class="item{{$item->id}}">

							<td>
								<a href="{{route('spj.show', $item->id)}}" style="color: Black;" >
								{{$no++}}</a>
							</td>
							<td>
								<a href="{{route('spj.show', $item->id)}}" style="color: Black;" >{{$item->no_st}}</a>
							</td>
							<td>
								<a href="{{route('spj.show', $item->id)}}" style="color: Black;" >{{Carbon\Carbon::parse($item->tgl_st)->format('d M Y')}}</a>
							</td>
							<td>
								<a href="{{route('spj.show', $item->id)}}" style="color: Black;" >{{$item->uraian_st}}</a>
							</td>
							<td style="text-align: right;">{{$item->nilai_spj}}
							</td>
							@if($item->aktivitas_spj == $item->aktivitas_st)
							<td>
								<a href="{{route('spj.show', $item->id)}}" style="color: Black;" >
									<i style="color: Green;" class="fa fa-check-circle"></i>
								</a>
							</td>
							@else
							<td>
								<a href="{{route('spj.show', $item->id)}}" style="color: Black;" >
									<i style="color: Red;" class="fa fa-times-circle" data-toggle="tooltip" title="Data aktivitas di ST/ND tidak sama dengan pembebanan di SPJ."></i>
								</a>
							</td>
							@endif
						</tr>
				  		@endforeach
				 	</tbody>
				 	<tfoot>
				 		<tr>
				  			<td colspan="4" style="text-align: center;">Total</td>
				  			<td class="total">{{$total}}</td>
				  			<td></td>
				  		</tr>
				 	</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@push('js')
  <script>
  	$(document).ready( function() {
	  $("td.total").each(function() { $(this).html(parseFloat($(this).text()).toLocaleString('id-ID', {style: 'currency', currency: 'IDR'})); })
	})

  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      	"columnDefs": [ {
        "targets": 4,
        "render": function(data, type, row) {
        return Number(data).toLocaleString('id-ID', {
          style: 'currency',
          currency: 'IDR'});
      }	
        } ],
		language: {
		searchPlaceholder: 'Cari...',
		sSearch: '',
		lengthMenu: '_MENU_ baris/halaman',
		}

    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush



