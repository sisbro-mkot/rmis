@extends('1layout.app')
 
@section('isi')

<div class="slim-pageheader">
	<ol class="breadcrumb slim-breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashlitbang')}}">
			<i class="fa fa-home"></i> Home</a>
		</li>
		<li class="breadcrumb-item active" aria-current="page">Kepegawaian</li>
	</ol>
	<h6 class="slim-pagetitle">Surat Tugas per Pegawai</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
	<div class="card-header">
		<div class="row h-100 justify-content-center align-items-center">
			<label for="eselon3" class="col-sm-2 control-label">Nama Bidang/Bagian: </label>
		  <div class="col-sm-5">
		    <select class="form-control" name="eselon3" id="eselon3">
		      <option value="0" selected>Seluruh Bidang/Bagian</option>
		      @foreach($eselon3 as $per)
		      <option value="{{$per->id}}" >{{$per->nama_bidang}}</option>
		      @endforeach
		    </select>
		  </div>
		</div>
		<div class="row h-100 justify-content-center align-items-center">
			</br>
		</div>
		<div class="row h-100 justify-content-center align-items-center">
			<label for="tgl_mulai" class="col-sm-2 control-label">Antara Tanggal:</label>
      <div class="col-sm-2">
        <input type="date" class="form-control" id="tgl_mulai" name="tgl_mulai" value="<?php echo date("Y-m-d"); ?>">
      </div>
      <label for="tgl_selesai" class="col-sm-2 control-label">Sampai Dengan Tanggal:</label>
      <div class="col-sm-2">
        <input type="date" class="form-control" id="tgl_selesai" name="tgl_selesai" value="<?php echo date("Y-m-d"); ?>">
      </div>
		</div>
	</div>
	<div class="pd-20">
		<div class="table-responsive-lg">
			<div class="table-wrapper">
				
				{{ csrf_field() }}
				<table id="tbl-identifikasi" class="table display">
					<thead align="center">
						<tr>
							<th width="5%">No.</th>
							<th width="20%" style="text-align: center;">NIP</th>
							<th width="22%" >Nama Pegawai</th>
							<th>Jabatan</th>
							<th width="10%" style="text-align: center;">Jumlah ST</th>
							<th width="10%" style="text-align: center;">Jumlah Hari</th>
							<th style="text-align: center;"></th>
						</tr>
					</thead>				  	
				</table>
			</div>
		</div>
	</div>
</div>

<!-- The Modal -->
<div class="modal" tabindex="-1" id="modalTable" aria-hidden="true">
  <div class="modal-dialog modal-full" role="document">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h5 class="modal-title">Daftar ST atas nama "<span id="Risiko"></span>"</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="card card-table">
        	<div class="pd-20">
        		<div class="table-responsive-lg">
        			<div class="table-wrapper">
        				<table id="mytable" class="table">
          				<thead>
				            <tr>
				              <th style="text-align: center;" width="5%" data-field="no">No.</th>
				              <th style="text-align: center;" width="18%" data-field="nost">No. ST</th>
				              <th style="text-align: center;" data-field="tglst">Tanggal ST</th>
				              <th style="text-align: center;" data-field="urst">Uraian ST</th>
				              <th style="text-align: center;" data-field="mulai">Tanggal Mulai</th>
				              <th style="text-align: center;" data-field="selesai">Tanggal Selesai</th>
				              <th style="text-align: center;" data-field="hari">Jumlah Hari</th>
				            </tr>
									</thead>
        				</table>
      				</div>
      			</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection

@push('js')
<script type="text/javascript">
$(document).ready(function() {
	$("#eselon3").select2();

	var eselonID = $('#eselon3').val();
	var mulaiID = $('#tgl_mulai').val();
	var selesaiID = $('#tgl_selesai').val();

	if(eselonID > 0){ 
    $.ajax({
    	url: 'litbang/pilihSTP/'+eselonID+'/'+mulaiID+'/'+selesaiID,
      type: 'get',
      dataType: 'json',
      success:function(data) {
      	console.log(data);
      	var t = $('#tbl-identifikasi').DataTable({
      		"order": [[ 4, "desc" ]],
          "columnDefs": [{
            "searchable": false,
            "orderable": false,
            "targets": [0,6],
          },{
            "className": "text-center",
            "targets": [1,4,5],
          }],
          language: {
            searchPlaceholder: 'Cari...',
            sSearch: '',
            lengthMenu: '_MENU_ baris/halaman',
            info: 'Baris ke _START_ sampai _END_ dari total _TOTAL_ baris',
  					infoEmpty: 'Tidak ada data',
	      	},
          "bDestroy": true,
          bJQueryUI: true,
          aaData: data,
          aoColumns: [
						{ mData: 'id' ,"fnRender": function( oObj ) { return oObj.aData[3].id }},
            { mData: 'nipbaru' ,"fnRender": function( oObj ) { return oObj.aData[3].nipbaru }},
            { mData: 'nama' ,"fnRender": function( oObj ) { return oObj.aData[3].nama }},
            { mData: 'jabatan' ,"fnRender": function( oObj ) { return oObj.aData[3].jabatan }},
            { mData: 'jumlah' ,"fnRender": function( oObj ) { return oObj.aData[3].jumlah }},
            { mData: 'hari' ,"fnRender": function( oObj ) { return oObj.aData[3].hari }},
            { mData: 'detil',"mRender": function(data, type, full) { return (full['detil'] > 0)
              ? ('<a href="" class="badge badge-primary" data-toggle="modal" data-target="#modalTable" data-id="' + full['id'] + '" data-risiko="' + full['nama'] + '" onclick="showModal(this)"> Detail</a>')
              : ''}}
          ],
        });

        t.on( 'order.dt search.dt', function () {
          t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
          });
        }).draw();
			}     
    }); 
  } else {
  	$.ajax({
      url: 'litbang/semuaSTP/'+mulaiID+'/'+selesaiID,
      type: 'get',
      dataType: 'json',
      success:function(data) {
        console.log(data);
        var t = $('#tbl-identifikasi').DataTable({
        	"order": [[ 4, "desc" ]],
          "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": [0,6],
          },{
            "className": "text-center",
            "targets": [1,4,5],
          }],
          language: {
            searchPlaceholder: 'Cari...',
            sSearch: '',
            lengthMenu: '_MENU_ baris/halaman',
            info: 'Baris ke _START_ sampai _END_ dari total _TOTAL_ baris',
  					infoEmpty: 'Tidak ada data',
	      	},
          "bDestroy": true,
          bJQueryUI: true,
          aaData: data,
          aoColumns: [
            { mData: 'id' ,"fnRender": function( oObj ) { return oObj.aData[3].id }},
            { mData: 'nipbaru' ,"fnRender": function( oObj ) { return oObj.aData[3].nipbaru }},
            { mData: 'nama' ,"fnRender": function( oObj ) { return oObj.aData[3].nama }},
            { mData: 'jabatan' ,"fnRender": function( oObj ) { return oObj.aData[3].jabatan }},
            { mData: 'jumlah' ,"fnRender": function( oObj ) { return oObj.aData[3].jumlah }},
            { mData: 'hari' ,"fnRender": function( oObj ) { return oObj.aData[3].hari }},
            { mData: 'detil',"mRender": function(data, type, full) { return (full['detil'] > 0)
              ? ('<a href="" class="badge badge-primary" data-toggle="modal" data-target="#modalTable" data-id="' + full['id'] + '" data-risiko="' + full['nama'] + '" onclick="showModal(this)"> Detail</a>')
              : ''}}
          ],
        });

      	t.on( 'order.dt search.dt', function () {
	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	          cell.innerHTML = i+1;
	        });
      	}).draw();
			}     
  	});
	}
});
</script>
@endpush

@push('js1')
<script type="text/javascript">
$(document).ready(function() {
	$('#eselon3').select2().on('change', function() {

		var eselonID = $('#eselon3').val();
		var mulaiID = $('#tgl_mulai').val();
		var selesaiID = $('#tgl_selesai').val();

		if(eselonID > 0){ 
	    $.ajax({
	    	url: 'litbang/pilihSTP/'+eselonID+'/'+mulaiID+'/'+selesaiID,
	      type: 'get',
	      dataType: 'json',
	      success:function(data) {
	      	console.log(data);
	      	var t = $('#tbl-identifikasi').DataTable({
	      		"order": [[ 4, "desc" ]],
	          "columnDefs": [{
	            "searchable": false,
	            "orderable": false,
	            "targets": [0,6],
	          },{
	            "className": "text-center",
	            "targets": [1,4,5],
	          }],
	          language: {
	            searchPlaceholder: 'Cari...',
	            sSearch: '',
	            lengthMenu: '_MENU_ baris/halaman',
	            info: 'Baris ke _START_ sampai _END_ dari total _TOTAL_ baris',
    					infoEmpty: 'Tidak ada data',
	      		},
	          "bDestroy": true,
	          bJQueryUI: true,
	          aaData: data,
	          aoColumns: [
							{ mData: 'id' ,"fnRender": function( oObj ) { return oObj.aData[3].id }},
	            { mData: 'nipbaru' ,"fnRender": function( oObj ) { return oObj.aData[3].nipbaru }},
	            { mData: 'nama' ,"fnRender": function( oObj ) { return oObj.aData[3].nama }},
	            { mData: 'jabatan' ,"fnRender": function( oObj ) { return oObj.aData[3].jabatan }},
	            { mData: 'jumlah' ,"fnRender": function( oObj ) { return oObj.aData[3].jumlah }},
	            { mData: 'hari' ,"fnRender": function( oObj ) { return oObj.aData[3].hari }},
	            { mData: 'detil',"mRender": function(data, type, full) { return (full['detil'] > 0)
	              ? ('<a href="" class="badge badge-primary" data-toggle="modal" data-target="#modalTable" data-id="' + full['id'] + '" data-risiko="' + full['nama'] + '" onclick="showModal(this)"> Detail</a>')
	              : ''}}
	          ],
	        });

	        t.on( 'order.dt search.dt', function () {
	          t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = i+1;
	          });
	        }).draw();
				}     
	    }); 
	  } else {
	  	$.ajax({
	      url: 'litbang/semuaSTP/'+mulaiID+'/'+selesaiID,
	      type: 'get',
	      dataType: 'json',
	      success:function(data) {
	        console.log(data);
	        var t = $('#tbl-identifikasi').DataTable({
	        	"order": [[ 4, "desc" ]],
	          "columnDefs": [ {
	            "searchable": false,
	            "orderable": false,
	            "targets": [0,6],
	          },{
	            "className": "text-center",
	            "targets": [1,4,5],
	          }],
	          language: {
	            searchPlaceholder: 'Cari...',
	            sSearch: '',
	            lengthMenu: '_MENU_ baris/halaman',
	            info: 'Baris ke _START_ sampai _END_ dari total _TOTAL_ baris',
    					infoEmpty: 'Tidak ada data',
	      		},
	          "bDestroy": true,
	          bJQueryUI: true,
	          aaData: data,
	          aoColumns: [
	            { mData: 'id' ,"fnRender": function( oObj ) { return oObj.aData[3].id }},
	            { mData: 'nipbaru' ,"fnRender": function( oObj ) { return oObj.aData[3].nipbaru }},
	            { mData: 'nama' ,"fnRender": function( oObj ) { return oObj.aData[3].nama }},
	            { mData: 'jabatan' ,"fnRender": function( oObj ) { return oObj.aData[3].jabatan }},
	            { mData: 'jumlah' ,"fnRender": function( oObj ) { return oObj.aData[3].jumlah }},
	            { mData: 'hari' ,"fnRender": function( oObj ) { return oObj.aData[3].hari }},
	            { mData: 'detil',"mRender": function(data, type, full) { return (full['detil'] > 0)
	              ? ('<a href="" class="badge badge-primary" data-toggle="modal" data-target="#modalTable" data-id="' + full['id'] + '" data-risiko="' + full['nama'] + '" onclick="showModal(this)"> Detail</a>')
	              : ''}}
	          ],
	        });

	      	t.on( 'order.dt search.dt', function () {
		        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		          cell.innerHTML = i+1;
		        });
	      	}).draw();
				}     
	  	});
		}
	});

	$('#tgl_mulai').on('change', function() {

		var eselonID = $('#eselon3').val();
		var mulaiID = $('#tgl_mulai').val();
		var selesaiID = $('#tgl_selesai').val();

		if(eselonID > 0){ 
	    $.ajax({
	    	url: 'litbang/pilihSTP/'+eselonID+'/'+mulaiID+'/'+selesaiID,
	      type: 'get',
	      dataType: 'json',
	      success:function(data) {
	      	console.log(data);
	      	var t = $('#tbl-identifikasi').DataTable({
	      		"order": [[ 4, "desc" ]],
	          "columnDefs": [{
	            "searchable": false,
	            "orderable": false,
	            "targets": [0,6],
	          },{
	            "className": "text-center",
	            "targets": [1,4,5],
	          }],
	          language: {
	            searchPlaceholder: 'Cari...',
	            sSearch: '',
	            lengthMenu: '_MENU_ baris/halaman',
	            info: 'Baris ke _START_ sampai _END_ dari total _TOTAL_ baris',
    					infoEmpty: 'Tidak ada data',
	      		},
	          "bDestroy": true,
	          bJQueryUI: true,
	          aaData: data,
	          aoColumns: [
							{ mData: 'id' ,"fnRender": function( oObj ) { return oObj.aData[3].id }},
	            { mData: 'nipbaru' ,"fnRender": function( oObj ) { return oObj.aData[3].nipbaru }},
	            { mData: 'nama' ,"fnRender": function( oObj ) { return oObj.aData[3].nama }},
	            { mData: 'jabatan' ,"fnRender": function( oObj ) { return oObj.aData[3].jabatan }},
	            { mData: 'jumlah' ,"fnRender": function( oObj ) { return oObj.aData[3].jumlah }},
	            { mData: 'hari' ,"fnRender": function( oObj ) { return oObj.aData[3].hari }},
	            { mData: 'detil',"mRender": function(data, type, full) { return (full['detil'] > 0)
	              ? ('<a href="" class="badge badge-primary" data-toggle="modal" data-target="#modalTable" data-id="' + full['id'] + '" data-risiko="' + full['nama'] + '" onclick="showModal(this)"> Detail</a>')
	              : ''}}
	          ],
	        });

	        t.on( 'order.dt search.dt', function () {
	          t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = i+1;
	          });
	        }).draw();
				}     
	    }); 
	  } else {
	  	$.ajax({
	      url: 'litbang/semuaSTP/'+mulaiID+'/'+selesaiID,
	      type: 'get',
	      dataType: 'json',
	      success:function(data) {
	        console.log(data);
	        var t = $('#tbl-identifikasi').DataTable({
	        	"order": [[ 4, "desc" ]],
	          "columnDefs": [ {
	            "searchable": false,
	            "orderable": false,
	            "targets": [0,6],
	          },{
	            "className": "text-center",
	            "targets": [1,4,5],
	          }],
	          language: {
	            searchPlaceholder: 'Cari...',
	            sSearch: '',
	            lengthMenu: '_MENU_ baris/halaman',
	            info: 'Baris ke _START_ sampai _END_ dari total _TOTAL_ baris',
    					infoEmpty: 'Tidak ada data',
	      		},
	          "bDestroy": true,
	          bJQueryUI: true,
	          aaData: data,
	          aoColumns: [
	            { mData: 'id' ,"fnRender": function( oObj ) { return oObj.aData[3].id }},
	            { mData: 'nipbaru' ,"fnRender": function( oObj ) { return oObj.aData[3].nipbaru }},
	            { mData: 'nama' ,"fnRender": function( oObj ) { return oObj.aData[3].nama }},
	            { mData: 'jabatan' ,"fnRender": function( oObj ) { return oObj.aData[3].jabatan }},
	            { mData: 'jumlah' ,"fnRender": function( oObj ) { return oObj.aData[3].jumlah }},
	            { mData: 'hari' ,"fnRender": function( oObj ) { return oObj.aData[3].hari }},
	            { mData: 'detil',"mRender": function(data, type, full) { return (full['detil'] > 0)
	              ? ('<a href="" class="badge badge-primary" data-toggle="modal" data-target="#modalTable" data-id="' + full['id'] + '" data-risiko="' + full['nama'] + '" onclick="showModal(this)"> Detail</a>')
	              : ''}}
	          ],
	        });

	      	t.on( 'order.dt search.dt', function () {
		        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		          cell.innerHTML = i+1;
		        });
	      	}).draw();
				}     
	  	});
		}
	});

	$('#tgl_selesai').on('change', function() {

		var eselonID = $('#eselon3').val();
		var mulaiID = $('#tgl_mulai').val();
		var selesaiID = $('#tgl_selesai').val();

		if(eselonID > 0){ 
	    $.ajax({
	    	url: 'litbang/pilihSTP/'+eselonID+'/'+mulaiID+'/'+selesaiID,
	      type: 'get',
	      dataType: 'json',
	      success:function(data) {
	      	console.log(data);
	      	var t = $('#tbl-identifikasi').DataTable({
	      		"order": [[ 4, "desc" ]],
	          "columnDefs": [{
	            "searchable": false,
	            "orderable": false,
	            "targets": [0,6],
	          },{
	            "className": "text-center",
	            "targets": [1,4,5],
	          }],
	          language: {
	            searchPlaceholder: 'Cari...',
	            sSearch: '',
	            lengthMenu: '_MENU_ baris/halaman',
	            info: 'Baris ke _START_ sampai _END_ dari total _TOTAL_ baris',
    					infoEmpty: 'Tidak ada data',
	      		},
	          "bDestroy": true,
	          bJQueryUI: true,
	          aaData: data,
	          aoColumns: [
							{ mData: 'id' ,"fnRender": function( oObj ) { return oObj.aData[3].id }},
	            { mData: 'nipbaru' ,"fnRender": function( oObj ) { return oObj.aData[3].nipbaru }},
	            { mData: 'nama' ,"fnRender": function( oObj ) { return oObj.aData[3].nama }},
	            { mData: 'jabatan' ,"fnRender": function( oObj ) { return oObj.aData[3].jabatan }},
	            { mData: 'jumlah' ,"fnRender": function( oObj ) { return oObj.aData[3].jumlah }},
	            { mData: 'hari' ,"fnRender": function( oObj ) { return oObj.aData[3].hari }},
	            { mData: 'detil',"mRender": function(data, type, full) { return (full['detil'] > 0)
	              ? ('<a href="" class="badge badge-primary" data-toggle="modal" data-target="#modalTable" data-id="' + full['id'] + '" data-risiko="' + full['nama'] + '" onclick="showModal(this)"> Detail</a>')
	              : ''}}
	          ],
	        });

	        t.on( 'order.dt search.dt', function () {
	          t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = i+1;
	          });
	        }).draw();
				}     
	    }); 
	  } else {
	  	$.ajax({
	      url: 'litbang/semuaSTP/'+mulaiID+'/'+selesaiID,
	      type: 'get',
	      dataType: 'json',
	      success:function(data) {
	        console.log(data);
	        var t = $('#tbl-identifikasi').DataTable({
	        	"order": [[ 4, "desc" ]],
	          "columnDefs": [ {
	            "searchable": false,
	            "orderable": false,
	            "targets": [0,6],
	          },{
	            "className": "text-center",
	            "targets": [1,4,5],
	          }],
	          language: {
	            searchPlaceholder: 'Cari...',
	            sSearch: '',
	            lengthMenu: '_MENU_ baris/halaman',
	            info: 'Baris ke _START_ sampai _END_ dari total _TOTAL_ baris',
    					infoEmpty: 'Tidak ada data',
	      		},
	          "bDestroy": true,
	          bJQueryUI: true,
	          aaData: data,
	          aoColumns: [
	            { mData: 'id' ,"fnRender": function( oObj ) { return oObj.aData[3].id }},
	            { mData: 'nipbaru' ,"fnRender": function( oObj ) { return oObj.aData[3].nipbaru }},
	            { mData: 'nama' ,"fnRender": function( oObj ) { return oObj.aData[3].nama }},
	            { mData: 'jabatan' ,"fnRender": function( oObj ) { return oObj.aData[3].jabatan }},
	            { mData: 'jumlah' ,"fnRender": function( oObj ) { return oObj.aData[3].jumlah }},
	            { mData: 'hari' ,"fnRender": function( oObj ) { return oObj.aData[3].hari }},
	            { mData: 'detil',"mRender": function(data, type, full) { return (full['detil'] > 0)
	              ? ('<a href="" class="badge badge-primary" data-toggle="modal" data-target="#modalTable" data-id="' + full['id'] + '" data-risiko="' + full['nama'] + '" onclick="showModal(this)"> Detail</a>')
	              : ''}}
	          ],
	        });

	      	t.on( 'order.dt search.dt', function () {
		        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		          cell.innerHTML = i+1;
		        });
	      	}).draw();
				}     
	  	});
		}
	});
});

function showModal(ele) 
{
	var mulaiID = $('#tgl_mulai').val();
	var selesaiID = $('#tgl_selesai').val();
  var id = $(ele).attr('data-id');
  var risiko = $(ele).attr('data-risiko');
  document.getElementById("Risiko").innerHTML = risiko;
    

  $.ajax({
    url: 'litbang/showSTP/'+id+'/'+mulaiID+'/'+selesaiID,
    type: 'get',
    dataType: 'json',
    success:function(data) {
      console.log(data);
      var t = $('#mytable').DataTable({
        "columnDefs": [ {
          "searchable": false,
          "orderable": false,
          "targets": [0],
        }],
        language: {
	            searchPlaceholder: 'Cari...',
	            sSearch: '',
	            lengthMenu: '_MENU_ baris/halaman',
	            info: 'Baris ke _START_ sampai _END_ dari total _TOTAL_ baris',
    					infoEmpty: 'Tidak ada data',
	      },
        "bDestroy": true,
        bJQueryUI: true,
        aaData: data,
        aoColumns: [
          { mData: 'no' ,"fnRender": function( oObj ) { return oObj.aData[3].no }},
          { mData: 'nost' ,"fnRender": function( oObj ) { return oObj.aData[3].nost }},
          { mData: 'tglst',"fnRender": function( oObj ) { return oObj.aData[3].tglst }},
          { mData: 'urst',"fnRender": function( oObj ) { return oObj.aData[3].urst }},
          { mData: 'mulai',"fnRender": function( oObj ) { return oObj.aData[3].mulai }},
          { mData: 'selesai',"fnRender": function( oObj ) { return oObj.aData[3].selesai }},
          { mData: 'hari',"fnRender": function( oObj ) { return oObj.aData[3].hari }}
        ],
      });

      t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
        });
      }).draw();

		}     
  });  
};

</script>
@endpush