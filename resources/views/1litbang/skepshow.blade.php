@extends('1layout.app')

@section('isi')

<div class="slim-pageheader">
	<ol class="breadcrumb slim-breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashlitbang')}}"><i class="fa fa-home"></i> Home</a></li>
		<li class="breadcrumb-item"><a href="{{route('skep.index')}}">Surat Keputusan</a></li>
		<li class="breadcrumb-item active" aria-current="page">Detail Surat Keputusan</li>
	</ol>
	<h6 class="slim-pagetitle">Detail Surat Keputusan</h6>
</div><!-- slim-pageheader -->

<div class="card card-table">	
	<div class="card-body">
		<div class="row">
			<div class="col-sm-4">	
				
				<span>Konseptor SK : </span>
				<p style="color: black;">{{$anggaran->nama_bidang}}</p>

			</div>
			<div class="col-sm-2">	

				<span>Nomor SK : </span>
				<p style="color: black;">{{$anggaran->no_st}}</p>

			</div>
			<div class="col-sm-6">	

				<span>Tanggal SK : </span>
				<p style="color: black;">{{Carbon\Carbon::parse($anggaran->tgl_st)->format('d M Y')}}</p>

			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">	
				
				<span>Uraian SK : </span>
				<p style="color: black;">{{$anggaran->uraian_st}}</p>

				<div class="form-group">
					<div class="row">
						<div class="col-sm-2">				
							<a href="{{route('skep.edit', $anggaran->id)}}" type="submit" class="btn btn-success"><i class="icon ion-android-create"></i> Edit SK</a>				
						</div>
						<div class="col-sm-1">	
							<a href="{{route('spjcreate', $anggaran->id)}}" type="submit" class="btn btn-info"><i class="icon ion-android-create"></i> Buat SPJ</a>
						</div>
					</div>
				</div>

			</div>

			<div class="col-sm-6">	
				
				<span>Aktivitas Kinerja : </span>
				<p style="color: black;">{{$anggaran->kode_pkau}} - {{$anggaran->nama_pkau}}</p>

			</div>
		</div>
	</div>	
</div>

@stop
