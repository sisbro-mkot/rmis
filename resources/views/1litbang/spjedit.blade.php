@extends('1layout.app')

@section('isi')

<div class="slim-pageheader">
	<ol class="breadcrumb slim-breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashlitbang')}}"><i class="fa fa-home"></i> Home</a></li>
		<li class="breadcrumb-item"><a href="{{route('spj.index')}}">Verifikasi SPJ</a></li>
		<li class="breadcrumb-item active" aria-current="page">Edit Data</li>
	</ol>
	<h6 class="slim-pagetitle">Edit Data Verifikasi SPJ</h6>
</div><!-- slim-pageheader -->

<div class="card card-table">
	@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
		    @foreach ($errors->all() as $error)
		    <li>{{ $error }}</li>
		    @endforeach
		</ul>
	</div>
	@endif
	@if (Auth::user()->user_nip == '740006317'|Auth::user()->user_nip == '201800140'|Auth::user()->user_nip == '202000050')
	<form class="form-horizontal mt-2" action="{{route('spj.update', $spj->id_spj)}}" method="post">
		{{ csrf_field() }}
		{{ method_field('PUT') }}
		<div class="box-body">
			<div class="form-group">
				<div class="col-sm-1">
					<input class="form-control" type="text" value="{{$spj->id_spj}}" id="id_spj" name="id_spj" hidden>
				</div>
			</div>

			<div class="form-group">
				<label for="tgl_spj" class="col-sm-2 control-label">Tanggal Verifikasi SPJ</label>
				<div class="col-sm-2">
					<input type="date" class="form-control" id="tgl_spj" name="tgl_spj" max="<?php echo date("Y-m-d"); ?>" value="{{$spj->tgl_spj}}">
				</div>
			</div>

			<div class="form-group">
				<label for="id_st" class="col-sm-12 control-label">Nomor dan Uraian ST/ND</label>
				<div class="col-sm-12">
					<select name="id_st" class="form-control" id="id_st" autofocus>
						@foreach($id_st as $key)
						<option value="{{$key->id_st}}" {{$spj->id_st == $key->id_st ? 'selected' : ''}}>{{$key->no_st}} - {{$key->uraian_st}}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group">
				<label for="id_anggaran_kinerja" class="col-sm-12 control-label">Pembebanan Anggaran</label>
				<div class="col-sm-12">
					<select name="id_anggaran_kinerja" class="form-control" id="id_anggaran_kinerja" autofocus>
						@foreach($id_anggaran_kinerja as $key)
						<option value="{{$key->id_anggaran_kinerja}}" {{$spj->id_anggaran_kinerja == $key->id_anggaran_kinerja ? 'selected' : ''}}>{{$key->ket_mak}} - {{$key->nama_mak}} - {{$key->nama_sub_komponen}}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group">
				<label for="nilai_spj" class="col-sm-2 control-label">Nilai SPJ</label>
				<div class="col-sm-2">
					<input type="number" class="form-control" id="nilai_spj" name="nilai_spj" min="0" max="10000000000"  value="{{$spj->nilai_spj}}">
				</div>
			</div>

			<div class="form-group">
				<label for="ket_spj" class="col-sm-12 control-label">Keterangan</label>
				<div class="col-sm-12">
					<textarea class="form-control" id="ket_spj" name="ket_spj">{{$spj->ket_spj}}</textarea>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-12">
					<button type="submit" class="btn btn-primary"> Simpan</button>
					<a href="{{route('spj.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
				</div>
			</div>
		</div>
	</form>
	@endif
</div>

@stop

@push('js')
<script type="text/javascript">
	$(document).ready(function() {
		$("#id_st").select2();
		$("#id_anggaran_kinerja").select2();

		var STID = $('#id_st').val();
		if(STID){
			$.ajax({
				url: 'litbang/angkin/'+STID,
                type: 'get',
                dataType: 'json',
                success:function(data){
                	$('select[name="id_anggaran_kinerja"]').empty();
                	$.each(data, function(key, value) {
                		$('select[name="id_anggaran_kinerja"]').append('<option value="'+ value.id_anggaran_kinerja +'">'+ value.ket_mak + ' - ' + value.nama_mak + ' - ' + value.nama_sub_komponen + '</option>');
                	});
                }
			});
		} else {
			$('select[name="id_anggaran_kinerja"]').empty();
		}
	});
</script>
@endpush

@push('js1')
<script type="text/javascript">
	$(document).ready(function() {
		$('#id_st').select2().on('change', function() {
			var STID = $(this).val();
			if(STID){
				$.ajax({
					url: 'litbang/angkin/'+STID,
	                type: 'get',
	                dataType: 'json',
	                success:function(data){
	                	$('select[name="id_anggaran_kinerja"]').empty();
	                	$.each(data, function(key, value) {
	                		$('select[name="id_anggaran_kinerja"]').append('<option value="'+ value.id_anggaran_kinerja +'">'+ value.ket_mak + ' - ' + value.nama_mak + ' - ' + value.nama_sub_komponen + '</option>');
	                	});
	                }
	            });
	        } else {
				$('select[name="id_anggaran_kinerja"]').empty();
			}
		$("#id_anggaran_kinerja").select2();
		});
	});
</script>
@endpush