@extends('1layout.app')

@section('isi')

<div class="slim-pageheader">
	<ol class="breadcrumb slim-breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashlitbang')}}"><i class="fa fa-home"></i> Home</a></li>
		<li class="breadcrumb-item"><a href="{{route('lit.index')}}">Laporan</a></li>
		<li class="breadcrumb-item active" aria-current="page">Edit Data</li>
	</ol>
	<h6 class="slim-pagetitle">Edit Data Laporan</h6>
</div><!-- slim-pageheader -->

<div class="card card-table">
	@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
		    @foreach ($errors->all() as $error)
		    <li>{{ $error }}</li>
		    @endforeach
		</ul>
	</div>
	@endif
	<form class="form-horizontal mt-2" action="{{route('lit.update', $lit->id_penelitian)}}" method="post">
		{{ csrf_field() }}
		{{ method_field('PUT') }}
		<div class="box-body">
			<div class="form-row px-4 pt-2">			
				<div class="form-group col-sm-4">
					<label for="no_penelitian" class="control-label">Nomor Laporan</label>
					<input type="text" class="form-control" id="no_penelitian" name="no_penelitian" value="{{$lit->no_penelitian}}">
				</div>

				<div class="form-group col-sm-3">
					<label for="tgl_penelitian" class="control-label">Tanggal Laporan</label>
					<input type="date" class="form-control" id="tgl_penelitian" name="tgl_penelitian" value="{{$lit->tgl_penelitian}}">
				</div>

				<div class="form-group col-sm-5">
					<label for="id_ikk" class="control-label">Jenis Laporan</label>
					<select name="id_ikk" class="form-control" id="id_ikk" autofocus>
						@foreach($id_ikk as $key)
						<option value="{{$key->id_ikk}}" {{$lit->id_ikk == $key->id_ikk ? 'selected' : ''}}>{{$key->ket_ikk}}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-row px-4">
				<div class="form-group col-sm-12">
					<label for="nama_penelitian" class="control-label">Nama Laporan</label>
					<textarea class="form-control" id="nama_penelitian" name="nama_penelitian">{{$lit->nama_penelitian}}</textarea>
				</div>
			</div>

			@if($lit->id_ikk == 1)
			<div class="form-row px-4">
				<div class="form-group col-sm-4">
					<label class="control-label">Initial Outcome</label>
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" id="init_a1" name="init_a1" value=1 {{$lit->init_a1 == 1 ? 'checked' : ''}}>
					  	<label class="custom-control-label" for="init_a1">A1 - Distr Hsl</label>
					</div>
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" id="init_b1" name="init_b1" value=1 {{$lit->init_b1 == 1 ? 'checked' : ''}}>
					  	<label class="custom-control-label" for="init_b1">B1 - Distr Hsl</label>
					</div>
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" id="init_b2" name="init_b2" value=1 {{$lit->init_b2 == 1 ? 'checked' : ''}}>
					  	<label class="custom-control-label" for="init_b2">B2 - Seminar/Konferensi</label>
					</div>
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" id="init_b3" name="init_b3" value=1 {{$lit->init_b3 == 1 ? 'checked' : ''}}>
					  	<label class="custom-control-label" for="init_b3">B3 - Media Cetak/Elektronik</label>
					</div>
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" id="init_b4" name="init_b4" value=1 {{$lit->init_b4 == 1 ? 'checked' : ''}}>
					  	<label class="custom-control-label" for="init_b4">B4 - Buletin/Leaflet</label>
					</div>
				</div>
				<div class="form-group col-sm-4">
					<label class="control-label">Intermediate Outcome</label>
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" id="inter_a1" name="inter_a1" value=1 {{$lit->inter_a1 == 1 ? 'checked' : ''}}>
					  	<label class="custom-control-label" for="inter_a1">A1 - Sosi/Ekspose</label>
					</div>
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" id="inter_b1" name="inter_b1" value=1 {{$lit->inter_b1 == 1 ? 'checked' : ''}}>
					  	<label class="custom-control-label" for="inter_b1">B1 - Sosi/Ekspose/Ref Penulisan</label>
					</div>
				</div>
				<div class="form-group col-sm-4">
					<label class="control-label">End Outcome</label>
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" id="end_a1" name="end_a1" value=1 {{$lit->end_a1 == 1 ? 'checked' : ''}}>
					  	<label class="custom-control-label" for="end_a1">A1 - Bahan Rapat/Kbjkn/Kptsn</label>
					</div>
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" id="end_b1" name="end_b1" value=1 {{$lit->end_b1 == 1 ? 'checked' : ''}}>
					  	<label class="custom-control-label" for="end_b1">B1 - Bahan Rapat/Kebijakan/Keputusan</label>
					</div>
				</div>
			</div>
			@else
			@endif

			<div class="form-row px-4">
				<div class="form-group col-sm-12">
					<button type="submit" class="btn btn-primary"> Simpan</button>
					<a href="{{route('lit.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
				</div>
			</div>
		</div>
	</form>
</div>

@stop

@push('js')
<script type="text/javascript">
	$(document).ready(function() {
		$("#id_ikk").select2();

	});
</script>
@endpush
