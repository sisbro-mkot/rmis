@extends('1layout.app')

@section('isi')

<div class="slim-pageheader">
	<ol class="breadcrumb slim-breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashlitbang')}}">
			<i class="fa fa-home"></i> Home</a>
		</li>
		<li class="breadcrumb-item active" aria-current="page">Pelaporan</li>
	</ol>
	<h6 class="slim-pagetitle">Anggaran, Realisasi, dan Saldo per MAK</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
	<div class="pd-20">
		<div class="table-responsive-lg">
			<div class="table-wrapper">
				
				{{ csrf_field() }}
				<table id="tbl-identifikasi" class="table display">
					<thead align="center">
						<tr>
							<th width="5%">No.</th>
							<th style="text-align: center;">Kode Aktivitas</th>
							<th style="text-align: center;">Nama Sub Komponen</th>
							<th style="text-align: center;">Nama MAK</th>
							<th style="text-align: center;">Pagu Anggaran</th>
							<th style="text-align: center;">Realisasi</th>
							<th style="text-align: center;">Saldo</th>
						</tr>
					</thead>
				  	<tbody>
				  		<?php $no=1; ?>
				  		@foreach($anggaran as $item)
						<tr class="item{{$item->id}}">
							<td>{{$no++}}</td>
							<td style="text-align: center;">{{$item->kode_aktivitas}}</td>
							<td>{{$item->nama_sub_komponen}}</td>
							<td>{{$item->nama_mak}}</td>
							<td style="text-align: right;">{{$item->pagu_anggaran}}</td>
							<td style="text-align: right;">{{$item->nilai_spj}}</td>
							<td style="text-align: right;">{{$item->pagu_anggaran - $item->nilai_spj}}</td>
						</tr>
				  		@endforeach
				 	</tbody>
				 	<tfoot>
				 		<tr>
				  			<td colspan="4" style="text-align: center;">Total</td>
				  			<td class="total" style="text-align: right;">{{$totala}}</td>
				  			<td class="total" style="text-align: right;">{{$totalr}}</td>
				  			<td class="total" style="text-align: right;">{{$totala - $totalr}}</td>
				  		</tr>
				 	</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@push('js')
  <script>
  	$(document).ready( function() {
	  $("td.total").each(function() { $(this).html(parseFloat($(this).text()).toLocaleString('id-ID', {style: 'currency', currency: 'IDR'})); })
	})

  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      	"columnDefs": [ {
        "targets": [4,5,6],
        "render": function(data, type, row) {
        	if(data > 0){
        		return Number(data).toLocaleString('id-ID', {
          style: 'currency',
          currency: 'IDR'});
        	}else{return "-";}
      }	
        } ],
		language: {
		searchPlaceholder: 'Cari...',
		sSearch: '',
		lengthMenu: '_MENU_ baris/halaman',
		}

    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush



