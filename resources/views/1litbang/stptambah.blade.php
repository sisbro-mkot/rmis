@extends('1layout.app')

@section('isi')

<div class="slim-pageheader">
	<ol class="breadcrumb slim-breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashlitbang')}}"><i class="fa fa-home"></i> Home</a></li>
		<li class="breadcrumb-item"><a href="{{route('stugas.index')}}">Surat Tugas</a></li>
		<li class="breadcrumb-item active" aria-current="page">Tambah/Ubah Personil</li>
	</ol>
	<h6 class="slim-pagetitle">Tambah/Ubah Personil</h6>
</div><!-- slim-pageheader -->

@if ($errors->any())
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<div class="card card-table">	
	<div class="card-body">
		<div class="row">
			<div class="col-sm-3">
				<span>Nomor ST : </span>
				<p style="color: black;">{{$id_st_label->no_st}}</p>
			</div>
			<div class="col-sm-3">
				<span>Tanggal ST : </span>
				<p style="color: black;">{{Carbon\Carbon::parse($id_st_label->tgl_st)->format('d M Y')}}</p>
			</div>
			<div class="col-sm-3">	
				<span>Tanggal Mulai : </span>
				<p class="tanggal" style="color: black;">{{Carbon\Carbon::parse($id_st_label->tgl_mulai)->format('d M Y')}}</p>	
			</div>
			<div class="col-sm-3">
				<span>Tanggal Selesai : </span>
				<p class="tanggal" style="color: black;">{{Carbon\Carbon::parse($id_st_label->tgl_selesai)->format('d M Y')}}</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<span>Uraian ST : </span>
				<p style="color: black;">{{$id_st_label->uraian_st}}</p>
			</div>
		</div>
	</div>
	<div class="pd-20">
	<form class="form-horizontal mt-2" action="{{route('stp.store')}}" method="post">
		{{ csrf_field() }}
		<div class="form-group">
			<div class="row" id="id_st_hide">
				<select name="id_st" class="form-control" id="id_st" autofocus>
					@foreach($id_st as $key)
					<option value="{{$key->id}}">{{$key->id}}</option>
					@endforeach
				</select>
			</div>
			<div class="row">
				<div class="col-sm-9">
					<select name="id_pegawai" class="form-control" id="id_pegawai" autofocus>
						@foreach($id_pegawai as $key)
						<option value="{{$key->id}}">{{$key->nipbaru}} - {{$key->s_nama_lengkap}}</option>
						@endforeach
					</select>
		        </div>
				<div class="col-sm-1">				
					<button type="submit" class="btn btn-primary"> Pilih</button>			
				</div>
			</div>
		</div>
	</form>
		<div class="table-responsive-lg">
			<div class="table-wrapper">
				{{ csrf_field() }}
				<table id="tbl-identifikasi" class="table display">
					<thead align="center">
						<tr>
							<th width="5%">No.</th>
							<th width="20%" style="text-align: center;">NIP</th>
							<th style="text-align: center;">Nama</th>
							<th style="text-align: center;">Jabatan</th>
							<th style="text-align: center;"></th>
						</tr>
					</thead>
					<tbody>
						<?php $no=1; ?>
						@foreach($id_st_pegawai as $item)
						<tr class="item{{$item->id}}">
							<td style="text-align: center;">{{$no++}}</td>
							<td style="text-align: center;">{{$item->nipbaru}}</td>
							<td>{{$item->s_nama_lengkap}}</td>
							<td>{{$item->jabatan}}</td>
							<td>
								<form method="POST" action="{{ route('stp.destroy', $item->id) }}">
								    {{ csrf_field() }}
								    {{ method_field('DELETE') }}
								    <button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('Yakin akan dihapus ?')"><i class="icon ion-ios-trash"></i> Hapus</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>	
</div>

@stop


@push('js')
<script type="text/javascript">
  $(document).ready(function() {
  	$("#id_st_hide").hide();
    $("#id_pegawai").select2();

});
</script>
@endpush