@extends('layout.app')
@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('peta')}}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Beranda</li>
  </ol>
  <!-- <h6 class="slim-pagetitle">Selamat datang, {{Auth::user()->name}}</h6> -->
  @include('home._greeting')
</div><!-- slim-pageheader -->

<div class="row row-xs">
  <div class="col-sm-6 col-lg-3">
    <div class="card card-status">
      <div class="media">
        <i class="icon ion-ios-bookmarks-outline tx-purple"></i>
        <div class="media-body">
          <h1>{{number_format($unitkerja)}}</h1>
          <p>Total Unit Kerja Es. II</p>
        </div><!-- media-body -->
      </div><!-- media -->
    </div><!-- card -->
  </div><!-- col-3 -->
  <div class="col-sm-6 col-lg-3 mg-t-10 mg-sm-t-0">
    <div class="card card-status">
      <div class="media">
        <i class="icon ion-ios-people-outline tx-teal"></i>
        <div class="media-body">
          <h1>{{number_format($user)}}</h1>
          <p>Total Pengguna</p>
        </div><!-- media-body -->
      </div><!-- media -->
    </div><!-- card -->
  </div><!-- col-3 -->
  <div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0">
    <div class="card card-status">
      <div class="media">
        <i class="icon ion-ios-analytics-outline tx-primary"></i>
        <div class="media-body">
          <h1>{{number_format($baganrisiko)}}</h1>
          <p>Total Daftar Risiko</p>
        </div><!-- media-body -->
      </div><!-- media -->
    </div><!-- card -->
  </div><!-- col-3 -->
  <div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0">
    <div class="card card-status">
      <div class="media">
        <i class="icon ion-ios-browsers-outline tx-pink"></i>
        <div class="media-body">
          <h1>{{number_format($kategoririsiko)}}</h1>
          <p>Total Kategori Risiko</p>
        </div><!-- media-body -->
      </div><!-- media -->
    </div><!-- card -->
  </div><!-- col-3 -->
</div><!-- row -->

@endsection