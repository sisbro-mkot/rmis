@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('petapibr')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Perencanaan</li>
  </ol>
  <h6 class="slim-pagetitle">Perhitungan Nilai Risiko Komposit Auditable Unit</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
<!--   <div class="card-header">
    <a href="{{url('createmonitoring')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah</a>
    <a href="{{url('lini2monitoring')}}" class="btn btn-primary"><i class="icon ion-document"></i> Data</a>
  </div> -->
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}
    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="3%">No.</th>
          <th style="text-align: center;">Area Pengawasan (Nama Unit Pemilik Risiko)</th>
          <th style="text-align: center;">Rata-rata Level Kemungkinan</th>
          <th style="text-align: center;">Rata-rata Level Dampak</th>
          <th style="text-align: center;">Nilai Risiko Komposit</th>
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($perencanaan as $item)
        <tr class="item{{$item->id}}">
          <td>{{$no++}}</td>
          <td>{{$item->s_nama_instansiunitorg}}&nbsp;&nbsp;<i style="color: DodgerBlue;"class="fa fa-info-circle" data-toggle="modal" data-target="#modalTable" data-id="{{$item->id}}" onclick="showModal(this)"></i></td>
          <td style="text-align: center;">{{$item->rata_kemungkinan}}</td>
          <td style="text-align: center;">{{$item->rata_dampak}}</td>
          <td style="text-align: center;">{{$item->skor_risiko}}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
  </div>
</div>

<!-- The Modal -->

<div class="modal" tabindex="-1" id="modalTable" aria-hidden="true">
  <div class="modal-dialog modal-full" role="document">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h5 class="modal-title">Tabel Perhitungan Besaran Risiko</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="card card-table">
        <div class="pd-20">
        <div class="table-responsive-lg">
        <div class="table-wrapper">
        <table id="mytable" class="table">
            <thead>
                <tr>
                    <th class="col-xs-1" data-field="nomor">No.</th>
                    <th class="col-xs-2" data-field="kode">Kode Risiko</th>
                    <th class="col-xs-2" data-field="nama">Nama Risiko</th>
                    <th class="col-xs-1" data-field="kemungkinan">Level Kemungkinan</th>
                    <th class="col-xs-1" data-field="dampak">Level Dampak</th>
                    <th class="col-xs-1" data-field="risiko">Besaran Risiko</th>
                </tr>
             </thead>
        </table>
        </div>
        </div>
        </div>
        </div>
        <p class="ml-auto">Keterangan: Tidak termasuk risiko-risiko yang mempunyai besaran di bawah selera risiko</p>
      </div>

    </div>
  </div>
</div>

@endsection

@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({

      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush

@push('js1')
  <script type="text/javascript">
    function showModal(ele) 
      {
          var id= $(ele).attr('data-id');
          
          $.ajax({
              url: 'lini3perencanaan/level/'+id,
              type: 'get',
              dataType: 'json',
              success:function(data) {
                
                var t = $('#mytable').DataTable({
                    "columnDefs": [ {
                                      "searchable": false,
                                      "orderable": false,
                                      "targets": [0],
                                    }],

                    "bDestroy": true,
                    bJQueryUI: true,
                    aaData: data,
                    aoColumns: [
                        { mData: 'nomor' ,"fnRender": function( oObj ) { return oObj.aData[3].nomor }},
                        { mData: 'kode' ,"fnRender": function( oObj ) { return oObj.aData[3].kode }},
                        { mData: 'nama' ,"fnRender": function( oObj ) { return oObj.aData[3].nama }},
                        { mData: 'kemungkinan' ,"fnRender": function( oObj ) { return oObj.aData[3].kemungkinan }},
                        { mData: 'dampak' ,"fnRender": function( oObj ) { return oObj.aData[3].dampak }},
                        { mData: 'risiko' ,"fnRender": function( oObj ) { return oObj.aData[3].risiko }},
                             ],
                });

                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    });
                }).draw();

             }     
          });  
      };

  </script>
@endpush