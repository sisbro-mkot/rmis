@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapbpkp')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="">Penetapan Konteks</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Data</li>
  </ol>
  <h6 class="slim-pagetitle">Penetapan Konteks</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Edit Data</h6>
  </div>
 

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif


  @if (Auth::user()->role_id == '3')
  <form class="form-horizontal mt-2" action="{{route('lini1tetapkonteksbpkp.update', $penetapan_konteks->id_penetapan_konteks)}}" method="post">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <div class="box-body">

      <div class="form-group">
        <div class="col-sm-1">
        <input class="form-control" type="text" value="{{$penetapan_konteks->id_penetapan_konteks}}" id="id_penetapan_konteks" name="id_penetapan_konteks" hidden>
        <input class="form-control" type="text" value="{{$penetapan_konteks->id_konteks}}" id="id_ref_konteks" name="id_ref_konteks" hidden>
        </div>
      </div>
 
      <div class="form-group" id="data-umum">
        <label for="tahun" class="col-sm-2 control-label">Tahun</label>
        <div class="col-sm-2">
          <select class="form-control" name="tahun" id="tahun">
            @foreach((array) $tahun as $key)
            <option value="{{$key}}" {{$penetapan_konteks->tahun == $key ? 'selected' : ''}}>{{$key}}</option>
            @endforeach
          </select>
        </div>

        <label for="id_data_umum" class="col-sm-2 control-label">Pemilik Risiko</label>
        <div class="col-sm-12">
          <select name="id_data_umum" class="form-control" id="id_data_umum" autofocus>
            @foreach($id_data_umum as $key => $value)
              <option value="{{$key}}" {{$penetapan_konteks->id_data_umum == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="id_jns_konteks" class="col-sm-2 control-label">Jenis Konteks</label>
        <div class="col-sm-12">
          <select name="id_jns_konteks" class="form-control" id="id_jns_konteks" autofocus>
            @foreach($id_jns_konteks as $key => $value)
              <option value="{{$key}}" {{$jenis_konteks->id_jenis_konteks == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="id_konteks" class="col-sm-2 control-label">Konteks</label>
        <div class="col-sm-12">
          <select name="id_konteks" class="form-control" id="id_konteks">
          @foreach($id_konteks as $key => $value)
              <option value="{{$key}}" {{$jenis_konteks->id_konteks == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini1tetapkonteksbpkp.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
</div>

@stop

@push('js')
<script type="text/javascript">
  $(document).ready(function() {
    $("#data-umum").hide();
    $("#tahun").select2();
    $("#id_data_umum").select2();
    $("#id_jns_konteks").select2();

    var konteksID = $("#id_jns_konteks").val();
            if(konteksID) {
                $.ajax({
                    url: '../../lini2tetapkonteks/pilihKonteks/'+konteksID,
                    type: 'get',
                    dataType: 'json',
                    success:function(data) {
                      
                        var jnskonteksID = $("#id_ref_konteks").val();
                        console.log(jnskonteksID);
                        $('select[name="id_konteks"]').empty();
                        $.each(data, function(key, value) {
                          if(value.ket_konteks == ''){                            
                            if(jnskonteksID == value.id_konteks){
                              $('select[name="id_konteks"]').append('<option value="'+ value.id_konteks +'" selected>'+ value.nama_konteks + '</option>');
                            }else{
                              $('select[name="id_konteks"]').append('<option value="'+ value.id_konteks +'">'+ value.nama_konteks + '</option>');
                            }
                          }else{
                            if(jnskonteksID == value.id_konteks){
                              $('select[name="id_konteks"]').append('<option value="'+ value.id_konteks +'" selected>'+ value.nama_konteks + ' _ ' + value.ket_konteks + '</option>');
                            }else{
                              $('select[name="id_konteks"]').append('<option value="'+ value.id_konteks +'">'+ value.nama_konteks + ' _ ' + value.ket_konteks + '</option>');
                            }                            
                          }
                        });

                    }
                });
            } else {
                $('select[name="id_konteks"]').empty();
            };

    $("#id_konteks").select2();

});
</script>
@endpush

@push('js1')
  <script type="text/javascript">
  $(document).ready(function() {
        $('#id_jns_konteks').select2().on('change', function() {
            var konteksID = $(this).val();
            console.log(konteksID);
            if(konteksID) {
                $.ajax({
                    url: '../../lini2tetapkonteks/pilihKonteks/'+konteksID,
                    type: 'get',
                    dataType: 'json',
                    success:function(data) {
                      console.log(data);
                        $('select[name="id_konteks"]').empty();
                        $.each(data, function(key, value) {
                          if(value.ket_konteks == ''){
                            $('select[name="id_konteks"]').append('<option value="'+ value.id_konteks +'">'+ value.nama_konteks + '</option>');
                          }else{
                            $('select[name="id_konteks"]').append('<option value="'+ value.id_konteks +'">'+ value.nama_konteks + ' _ ' + value.ket_konteks + '</option>');
                          }
                        });

                    }
                });
            } else {
                $('select[name="id_konteks"]').empty();
            }
        });

        $('#id_konteks').select2()

  });
  </script>
@endpush