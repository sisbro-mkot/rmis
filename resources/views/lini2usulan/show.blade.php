@extends('layout.app')
 
@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmap')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('lini2usul')}}">Usulan Risiko Baru</a></li>
    <li class="breadcrumb-item active" aria-current="page">Detail Usulan Risiko Baru</li>
  </ol>
  <h6 class="slim-pagetitle">Detail Usulan Risiko Baru</h6>
</div><!-- slim-pageheader -->
<div class="card card-table"> 
  <div class="card-body">
    <div class="row">
      <div class="col-sm-12"> 

        <span>Nama Unit Pengusul : </span>
        <p style="color: black;">{{$usulan->s_skt_instansiunitorg}}</p>    
        
        <span>Nama Risiko : </span>
        <p style="color: black;">{{$usulan->nama_bagan_risiko}}</p>     
      
        <span>Kategori Risiko : </span>
        <p style="color: black;">{{$usulan->nama_kategori_risiko}}</p>
  
        <span>Metode Pencapaian Tujuan SPIP : </span>
        <p style="color: black;">{{$usulan->nama_tujuan_spip}}</p>
      
        <span>Status Usulan : </span>
        <p>
          @if($usulan->status_usulan == 1)
            <span class="badge badge-primary">Usulan Terkirim</span>
          @elseif($usulan->status_usulan == 2)
            <span class="badge badge-danger">Usulan Ditolak</span>
          @elseif($usulan->status_usulan == 3)
            <span class="badge badge-warning">Usulan Disetujui dengan Perbaikan</span>
          @elseif($usulan->status_usulan == 4)
            <span class="badge badge-success">Usulan Disetujui</span>
          @else
            Tidak ada data
          @endif
        </p>

        @if(is_null($usulan->tanggapan))
        @else
        <span>Keterangan Usulan : </span>
          <p style="color: black;">{{$usulan->tanggapan}}</p>
        @endif

        @if($usulan->status_ubah == "Y")
        @else
        <div class="form-group">
          <div class="row">
            <div class="col-sm-2">        
              <a href="{{route('lini2usulan.edit', $usulan->id)}}" type="submit" class="btn btn-success"><i class="icon ion-android-create"></i> Ubah Status Usulan</a>     
            </div>
          </div>
        </div>
        @endif

      </div>
    </div>
  </div>
</div>

@stop



