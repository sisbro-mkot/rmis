@extends('layout.app')
 
@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmap')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('lini2usul')}}">Usulan Risiko Baru</a></li>
    <li class="breadcrumb-item active" aria-current="page">Ubah Status Usulan</li>
  </ol>
  <h6 class="slim-pagetitle">Usulan Risiko Baru</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Ubah Status Usulan</h6>
  </div>


  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif


  @if (Auth::user()->role_id == '1')
  <form class="form-horizontal mt-2" action="{{route('lini2usulan.update', $usulan->id_usulan)}}" method="post">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <div class="box-body">

      <div class="form-group">
        <div class="col-sm-12">
          <span>Nama Unit Pengusul : </span>
          <p style="color: black;">{{$show->s_skt_instansiunitorg}}</p>    
          
          <span>Nama Risiko : </span>
          <p style="color: black;">{{$show->nama_bagan_risiko}}</p>     
        
          <span>Kategori Risiko : </span>
          <p style="color: black;">{{$show->nama_kategori_risiko}}</p>
    
          <span>Metode Pencapaian Tujuan SPIP : </span>
          <p style="color: black;">{{$show->nama_tujuan_spip}}</p>
        </div>
      </div>

      <div class="form-group">
        <label for="status_usulan" class="col-sm-12 control-label">Status Usulan</label>
        <div class="col-sm-12">
          <div class="form-check-inline">
            <label class="form-check-label">
              <input type="radio" class="form-check-input" value="2" name="status_usulan" {{$usulan->status_usulan == 2 ? 'checked' : ''}}>Ditolak
            </label>
          </div>
          <div class="form-check-inline">
            <label class="form-check-label">
              <input type="radio" class="form-check-input" value="3" name="status_usulan" {{$usulan->status_usulan == 3 ? 'checked' : ''}}>Disetujui dengan Perbaikan
            </label>
          </div>
          <div class="form-check-inline disabled">
            <label class="form-check-label">
              <input type="radio" class="form-check-input" value="4" name="status_usulan" {{$usulan->status_usulan == 4 ? 'checked' : ''}}>Disetujui
            </label>
          </div>
        </div>
      </div>

      <div class="form-group">
        <label for="tanggapan" class="col-sm-2 control-label">Keterangan Status Usulan :</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="tanggapan">{{$usulan->tanggapan}}</textarea>
          </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini2usulan.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
      <div class="form-group" id="data-umum">
        <label for="s_kd_instansiunitorg" class="col-sm-2 control-label">Nama Unit Pengusul :</label>
        <div class="col-sm-12">
          <select name="s_kd_instansiunitorg" class="form-control" id="s_kd_instansiunitorg">
            @foreach($s_kd_instansiunitorg as $key)
              <option value="{{$key->s_kd_instansiunitorg}}" {{$usulan->s_kd_instansiunitorg == $key->s_kd_instansiunitorg ? 'selected' : ''}}>{{$key->s_kd_instansiunitorg}}</option>
            @endforeach
          </select>
        </div>

        <label for="id_kategori_risiko" class="col-sm-2 control-label">Kategori Risiko :</label>
        <div class="col-sm-12">
          <select name="id_kategori_risiko" class="form-control" id="id_kategori_risiko">
            @foreach($id_kategori_risiko as $key)
              <option value="{{$key->id_kategori_risiko}}" {{$usulan->id_kategori_risiko == $key->id_kategori_risiko ? 'selected' : ''}}>{{$key->nama_kategori_risiko}}</option>
            @endforeach
          </select>
        </div>

        <label for="id_tujuan_spip" class="col-sm-2 control-label">Metode Pencapaian Tujuan SPIP :</label>
        <div class="col-sm-12">
          <select name="id_tujuan_spip" class="form-control" id="id_tujuan_spip">
            @foreach($id_tujuan_spip as $key)
              <option value="{{$key->id_tujuan_spip}}" {{$usulan->id_tujuan_spip == $key->id_tujuan_spip ? 'selected' : ''}}>{{$key->nama_tujuan_spip}}</option>
            @endforeach
          </select>
        </div>

        <div class="form-group">
        <label for="nama_bagan_risiko" class="col-sm-2 control-label">Nama Risiko :</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="nama_bagan_risiko">{{$usulan->nama_bagan_risiko}}</textarea>
          </div>
        </div>

      </div>

    </div>
      <!-- /.box-body -->
  </form>
  @endif
</div>

@stop


@push('js')
<script type="text/javascript">
  $(document).ready(function() {
    $("#data-umum").hide();
});
</script>
@endpush
