@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmapunit')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="">Analisis Risiko Unit Kerja</a></li>
    <li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
  </ol>
  <h6 class="slim-pagetitle">Analisis Risiko {{$nama_instansiunitorg->s_nama_instansiunitorg}}</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Tambah Data</h6>
  </div>

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

  @if (Auth::user()->role_id == '5'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
  <form class="form-horizontal mt-2" action="{{route('lini1analisis.store')}}" method="post">
    {{ csrf_field() }}
    <div class="box-body">

      <div class="form-group">
        <label for="id_identifikasi" class="col-sm-2 control-label">Nama Risiko</label>
        <div class="col-sm-12">
          <select name="id_identifikasi" class="form-control" id="id_identifikasi" autofocus>
            @foreach($id_identifikasi as $key)
              <option value="{{$key->id_identifikasi}}">{{$key->kode_identifikasi_risiko}} - {{$key->nama_bagan_risiko}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-inline">
        <label for="kemungkinan_inherent" class="col-sm-2 control-label">Kemungkinan Inherent :</label>
        <div class="col-sm-1">
          <select class="form-control" name="kemungkinan_inherent" id="kemungkinan_inherent">
            @foreach($kemungkinan_inherent as $per)
            <option value="{{$per}}" {{old('kemungkinan_inherent') == $per ? 'selected' : ''}}>{{$per}}</option>
            @endforeach
          </select>
        </div>
        <label for="dampak_inherent" class="col-sm-2 control-label">Dampak Inherent :</label>
        <div class="col-sm-1">
          <select class="form-control" name="dampak_inherent" id="dampak_inherent">
            @foreach($dampak_inherent as $per)
            <option value="{{$per}}" {{old('dampak_inherent') == $per ? 'selected' : ''}}>{{$per}}</option>
            @endforeach
          </select>
        </div>
        <label for="id_matriks_inherent" class="col-sm-2 control-label">Level Risiko Inherent :</label>
        <div class="col-sm-1">
          <input class="form-control" value="" name="id_matriks_inherent_read" id="id_matriks_inherent_read" readonly></input>
        </div>
      </div>
      <br/>
      <div class="form-group">
        <label for="existing_control" class="col-sm-12 control-label">Apakah terdapat pengendalian yang sudah ada (existing control)?</label>
        <div class="col-sm-2">
          <select class="form-control" name="existing_control" id="existing_control">
            @foreach($existing_control as $key => $value)
            <option value="{{$key}}" {{old('existing_control') == $per ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div> 

      <div class="form-group" id="nama-pengendalian">
        <label for="uraian_existing_control" class="col-sm-12 control-label">Nama pengendalian yang sudah ada (existing control)</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="uraian_existing_control">{{old('uraian_existing_control')}}</textarea>
          </div>
      </div>

      <div class="form-group" id="sub-unsur">
        <label for="id_sub_unsur" class="col-sm-2 control-label">Sub Unsur SPIP</label>
        <div class="col-sm-12">
          <select name="id_sub_unsur" class="form-control" id="id_sub_unsur" autofocus>
            @foreach($id_sub_unsur as $key => $value)
              <option value="{{$key}}" {{old('id_sub_unsur') == $key ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group" id="efektifitas-pengendalian">
        <label for="existing_control_memadai" class="col-sm-12 control-label">Apakah pengendalian yang sudah ada (existing control) telah memadai?</label>
        <div class="col-sm-2">
          <select class="form-control" name="existing_control_memadai" id="existing_control_memadai">
            @foreach($existing_control_memadai as $key => $value)
            <option value="{{$key}}" {{old('existing_control_memadai') == $per ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-inline" id="data-residual">
        <label for="kemungkinan_residual" class="col-sm-2 control-label">Kemungkinan Residual :</label>
        <div class="col-sm-1">
          <select class="form-control" name="kemungkinan_residual" id="kemungkinan_residual">
            @foreach($kemungkinan_residual as $per)
            <option value="{{$per}}" {{old('kemungkinan_residual') == $per ? 'selected' : ''}}>{{$per}}</option>
            @endforeach
          </select>
        </div>
        <label for="dampak_residual" class="col-sm-2 control-label">Dampak Residual :</label>
        <div class="col-sm-1">
          <select class="form-control" name="dampak_residual" id="dampak_residual">
            @foreach($dampak_residual as $per)
            <option value="{{$per}}" {{old('dampak_residual') == $per ? 'selected' : ''}}>{{$per}}</option>
            @endforeach
          </select>
        </div>
        <label for="id_matriks_residual" class="col-sm-2 control-label">Level Risiko Residual :</label>
        <div class="col-sm-1">
          <input class="form-control" value="" name="id_matriks_residual_read" id="id_matriks_residual_read" readonly></input>
        </div>
      </div>
      <br/>
      <div class="form-group" id="data-risiko">
        <div class="col-sm-2">
          <select name="id_matriks_inherent" class="form-control" id="id_matriks_inherent">
          <option value="0" disabled="true" selected="true"></option>
          <option value="">-- Pilih Kemungkingan/Dampak Risiko Inherent--</option>
          </select>
        </div>
        <div class="col-sm-2">
          <select name="id_matriks_residual" class="form-control" id="id_matriks_residual">
          <option value="0" disabled="true" selected="true"></option>
          <option value="">-- Pilih Kemungkingan/Dampak Risiko Residual--</option>
          </select>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini1analisis.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
</div>

@stop

@push('js')
<script type="text/javascript">
  $(document).ready(function() {
    $("#data-risiko").hide();
    $("#data-residual").hide();
    $("#nama-pengendalian").hide();
    $("#sub-unsur").hide();
    $("#efektifitas-pengendalian").hide();
    $("#id_identifikasi").select2();
    $("#id_sub_unsur").select2();
    $("#kemungkinan_inherent").select2();
    $("#dampak_inherent").select2();

      var kemungkinanID = $('#kemungkinan_inherent').val();
      var dampakID = $('#dampak_inherent').val();
      console.log(kemungkinanID);
      if(kemungkinanID) {
          $.ajax({
              url: '../../lini2analisis/pilihSkor/'+kemungkinanID+'/'+dampakID,
              type: 'get',
              dataType: 'json',
              success:function(data) {
                console.log(data);
                  $('select[name="id_matriks_inherent"]').empty();
                  $.each(data, function(key, value) {
                      $('select[name="id_matriks_inherent"]').append('<option value="'+ value.id_matriks +'">'+ value.skor_risiko + '</option>');
                        var input = value.skor_risiko;
                        var text2 = document.getElementById("id_matriks_inherent_read");
                        text2.value = input;
                  });

              }
          });
      } else {
          $('select[name="id_matriks_inherent"]').empty();
      }

    $("#id_matriks_inherent").select2();
    $("#existing_control").select2();
    $("#existing_control_memadai").select2();
    $("#kemungkinan_residual").select2();
    $("#dampak_residual").select2();

      var kemungkinanID = $('#kemungkinan_residual').val();
      var dampakID = $('#dampak_residual').val();
      console.log(kemungkinanID);
      if(kemungkinanID) {
          $.ajax({
              url: '../../lini2analisis/pilihSkor/'+kemungkinanID+'/'+dampakID,
              type: 'get',
              dataType: 'json',
              success:function(data) {
                console.log(data);
                  $('select[name="id_matriks_residual"]').empty();
                  $.each(data, function(key, value) {
                      $('select[name="id_matriks_residual"]').append('<option value="'+ value.id_matriks +'">'+ value.skor_risiko + '</option>');
                      var input = value.skor_risiko;
                      var text2 = document.getElementById("id_matriks_residual_read");
                      text2.value = input;
                  });

              }
          });
      } else {
          $('select[name="id_matriks_residual"]').empty();
      }

    $("#id_matriks_residual").select2();

});
</script>
@endpush


@push('js1')
  <script type="text/javascript">
  $(document).ready(function() {
        $('#kemungkinan_inherent').select2().on('change', function() {
            var kemungkinanID = $(this).val();
            var dampakID = $('#dampak_inherent').val();
            console.log(kemungkinanID);
            if(kemungkinanID) {
                $.ajax({
                    url: '../../lini2analisis/pilihSkor/'+kemungkinanID+'/'+dampakID,
                    type: 'get',
                    dataType: 'json',
                    success:function(data) {
                      console.log(data);
                        $('select[name="id_matriks_inherent"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="id_matriks_inherent"]').append('<option value="'+ value.id_matriks +'">'+ value.skor_risiko + '</option>');
                            var input = value.skor_risiko;
                            var text2 = document.getElementById("id_matriks_inherent_read");
                            text2.value = input;
                        });

                    }
                });
            } else {
                $('select[name="id_matriks_inherent"]').empty();
            }
        });

        $('#dampak_inherent').select2().on('change', function() {
            var dampakID = $(this).val();
            var kemungkinanID = $('#kemungkinan_inherent').val();
            console.log(dampakID);
            if(dampakID) {
                $.ajax({
                    url: '../../lini2analisis/pilihSkor/'+kemungkinanID+'/'+dampakID,
                    type: 'get',
                    dataType: 'json',
                    success:function(data) {
                      console.log(data);
                        $('select[name="id_matriks_inherent"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="id_matriks_inherent"]').append('<option value="'+ value.id_matriks +'">'+ value.skor_risiko + '</option>');
                            var input = value.skor_risiko;
                            var text2 = document.getElementById("id_matriks_inherent_read");
                            text2.value = input;
                        });

                    }
                });
            } else {
                $('select[name="id_matriks_inherent"]').empty();
            }
        });

        $('#id_matriks_inherent').select2();

        $("#existing_control").select2().on('change', function() {
          var controlID = $(this).val();
          if(controlID == "Y"){
            $("#data-residual").show();
            $("#nama-pengendalian").show();
            $("#sub-unsur").show();
            $("#efektifitas-pengendalian").show();
          } else {
            $("#data-residual").hide();
            $("#nama-pengendalian").hide();
            $("#sub-unsur").hide();
            $("#efektifitas-pengendalian").hide();
          }
        });

        $('#kemungkinan_residual').select2().on('change', function() {
            var kemungkinanID = $(this).val();
            var dampakID = $('#dampak_residual').val();
            console.log(kemungkinanID);
            if(kemungkinanID) {
                $.ajax({
                    url: '../../lini2analisis/pilihSkor/'+kemungkinanID+'/'+dampakID,
                    type: 'get',
                    dataType: 'json',
                    success:function(data) {
                      console.log(data);
                        $('select[name="id_matriks_residual"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="id_matriks_residual"]').append('<option value="'+ value.id_matriks +'">'+ value.skor_risiko + '</option>');
                            var input = value.skor_risiko;
                            var text2 = document.getElementById("id_matriks_residual_read");
                            text2.value = input;
                        });

                    }
                });
            } else {
                $('select[name="id_matriks_residual"]').empty();
            }
        });

        $('#dampak_residual').select2().on('change', function() {
            var dampakID = $(this).val();
            var kemungkinanID = $('#kemungkinan_residual').val();
            console.log(dampakID);
            if(dampakID) {
                $.ajax({
                    url: '../../lini2analisis/pilihSkor/'+kemungkinanID+'/'+dampakID,
                    type: 'get',
                    dataType: 'json',
                    success:function(data) {
                      console.log(data);
                        $('select[name="id_matriks_residual"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="id_matriks_residual"]').append('<option value="'+ value.id_matriks +'">'+ value.skor_risiko + '</option>');
                            var input = value.skor_risiko;
                            var text2 = document.getElementById("id_matriks_residual_read");
                            text2.value = input;
                        });

                    }
                });
            } else {
                $('select[name="id_matriks_residual"]').empty();
            }
        });

        $('#id_matriks_residual').select2();

  });
  </script>
@endpush

