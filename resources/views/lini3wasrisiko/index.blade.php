@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('petapibr')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Pelaksanaan PIBR</li>
  </ol>
  <h6 class="slim-pagetitle">Pengawasan Pelaksanaan Risiko</h6>
</div><!-- slim-pageheader -->
@if (Auth::user()->role_id == '8')

<div class="card card-table">
<div class="card-header">
  <label for="area_pengawasan" class="col-sm-12 control-label">Area Pengawasan: </label>
  <div class="col-sm-12">
    <select class="form-control" name="area_pengawasan" id="area_pengawasan">
      <option value="0" selected>Seluruh Unit Pemilik Risiko</option>
      @foreach($area_pengawasan as $per)
      <option value="{{$per->id}}" >{{$per->s_nama_instansiunitorg}}</option>
      @endforeach
    </select>
  </div>
</div>  
  <div class="pd-20">    
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}

    <table id="tbl-pengawasan" class="table display">
      <thead align="center">
        <tr>
          <th width="5%" rowspan="2" data-field="nomor">No.</th>
          <th width="10%" rowspan="2" data-field="kode">Kode Risiko</th>
          <th style="text-align: center;" rowspan="2" data-field="nama">Nama Risiko</th>
          <th style="text-align: center;" rowspan="2" data-field="dampak">Uraian Dampak</th>
          <th width="5%" style="text-align: center;" rowspan="2" data-field="inherent">Nilai Inherent Risk</th>
          <th style="text-align: center;" colspan="2">Pengendalian yang Sudah Ada</th>
          <th width="5%" style="text-align: center;" rowspan="2" data-field="residual">Nilai Residual Risk</th>
          <th style="text-align: center;" rowspan="2" data-field="edit">Simpulan atas Kecukupan Pengendalian</th>
        </tr>
        <tr>
          <th style="text-align: center;" data-field="jumlah">Jumlah Pengendalian</th>
          <th style="text-align: center;" data-field="memadai">Memadai/Tidak Memadai</th>
        </tr>
      </thead>
    </table>
  </div>
  </div>
  </div>

</div>

@endif

        <!-- The Modal -->

        <div class="modal" tabindex="-1" id="modalTable" aria-hidden="true">
          <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">

              <!-- Modal Header -->
              <div class="modal-header">
                <h5 class="modal-title">Pengendalian yang sudah ada atas risiko "<span id="Risiko"></span>"</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <!-- Modal body -->
              <div class="modal-body">
                <div class="card card-table">
                <div class="pd-20">
                <div class="table-responsive-lg">
                <div class="table-wrapper">
                <table id="mytable" class="table">
                    <thead>
                        <tr>
                            <th class="col-xs-1" data-field="nomor">No.</th>
                            <th class="col-xs-2" data-field="pengendalian">Nama Pengendalian</th>
                            <th class="col-xs-2" data-field="spip">Klasifikasi Sub Unsur SPIP</th>
                            <th class="col-xs-1" data-field="tahun">Tahun</th>
                        </tr>
                     </thead>
                </table>
                </div>
                </div>
                </div>
                </div>
              </div>

            </div>
          </div>
        </div>

        <!-- Modal Simpulan-->

        <div class="modal" tabindex="-1" id="modalSimpulan" aria-hidden="true">
          <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">

              <!-- Modal Header -->
              <div class="modal-header">
                <h5 class="modal-title">Simpulan atas Kecukupan Pengendalian</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <!-- Modal body -->
              <div class="modal-body">
                <div class="card card-table">
                {{ csrf_field() }}
                  <div class="box-body">
                      <div class="form-group">
                        <label for="risiko_read" class="col-sm-12 control-label">Kode dan Nama Risiko</label>
                        <div class="col-sm-12">
                          <input class="form-control" value="" name="risiko_read" id="risiko_read" readonly></input>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="dampak_read" class="col-sm-12 control-label">Uraian Dampak</label>
                        <div class="col-sm-12">
                          <textarea class="form-control" name="dampak_read" id="dampak_read" readonly></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="jenis_simpulan" class="col-sm-12 control-label">Simpulan atas Kecukupan Pengendalian</label>
                        <div class="col-sm-12">
                          <input class="form-control" value="" name="jenis_simpulan" id="jenis_simpulan" readonly></input>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="uraian_simpulan" class="col-sm-12 control-label">Uraian Simpulan</label>
                        <div class="col-sm-12">
                          <textarea class="form-control" name="uraian_simpulan" id="uraian_simpulan" readonly></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="uraian_rekomendasi" class="col-sm-12 control-label">Uraian Rekomendasi</label>
                        <div class="col-sm-12">
                          <textarea class="form-control" name="uraian_rekomendasi" id="uraian_rekomendasi" readonly></textarea>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <a href="" class="btn btn-success btn-xs" name="tombol_edit" id="tombol_edit"><i class="icon ion-edit"></i> Ubah Simpulan</a>
              </div>

            </div>
          </div>
        </div>


@endsection

@push('js')
<script type="text/javascript">
  $(document).ready(function() {
    $("#area_pengawasan").select2();

      var areaID = $('#area_pengawasan').val();
      console.log(areaID);
      if(areaID > 0){ 
        $.ajax({
              url: 'lini3wasrisiko/pilihArea/'+areaID,
              type: 'get',
              dataType: 'json',
              success:function(data) {
                console.log(data);
                var t = $('#tbl-pengawasan').DataTable({
                    "columnDefs": [ {
                                      "searchable": false,
                                      "orderable": false,
                                      "targets": [0,8],
                                    },{
                                      "className": "text-center",
                                      "targets": [4,5,6,7,8],
                                    }],
                    language: {
                                searchPlaceholder: 'Search...',
                                sSearch: '',
                                lengthMenu: '_MENU_ items/page',
                              },
                    "bDestroy": true,
                    bJQueryUI: true,
                    aaData: data,
                    aoColumns: [
                        { mData: 'nomor' ,"fnRender": function( oObj ) { return oObj.aData[3].nomor }},
                        { mData: 'kode' ,"fnRender": function( oObj ) { return oObj.aData[3].kode }},
                        { mData: 'nama' ,"fnRender": function( oObj ) { return oObj.aData[3].nama }},
                        { mData: 'dampak' ,"fnRender": function( oObj ) { return oObj.aData[3].dampak }},
                        { mData: 'inherent' ,"fnRender": function( oObj ) { return oObj.aData[3].inherent }},
                        { mData: 'jumlah',"mRender": function(data, type, full) { return (full['jumlah'] > 0)
                          ? (full['jumlah'] + '&nbsp;&nbsp;<i style="color: DodgerBlue;"class="fa fa-info-circle" data-toggle="modal" data-target="#modalTable" data-id="' + full['nomor'] + '" data-risiko="' + full['nama'] + '" onclick="showModal(this)">')
                          : '-'}},
                        { mData: 'memadai',"mRender": function(data, type, full) { return Number(full['residual']) > Number(full['selera']) ? 'Tidak Memadai' : 'Memadai'}},
                        { mData: 'residual' ,"fnRender": function( oObj ) { return oObj.aData[3].residual }},
                        { mData: 'edit',"mRender": function(data, type, full) { return (full['simpulan'] > 0)
                          ? (full['jenis'] + '&nbsp;&nbsp;<i style="color: DodgerBlue;"class="fa fa-info-circle" data-toggle="modal" data-target="#modalSimpulan" data-id="' + full['simpulan'] + '" onclick="showModal2(this)">')
                          : '<a class="btn btn-info btn-sm" href="lini3wasrisiko/' + full['nomor'] + '/tambah">' + 'Buat' + '</a>';
                        }}
                              ],
                });

                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    });
                }).draw();

             }     
          }); 
      } else {
        $.ajax({
              url: 'lini3wasrisiko/semuaArea/',
              type: 'get',
              dataType: 'json',
              success:function(data) {
                console.log(data);
                var t = $('#tbl-pengawasan').DataTable({
                    "columnDefs": [ {
                                      "searchable": false,
                                      "orderable": false,
                                      "targets": [0,8],
                                    },{
                                      "className": "text-center",
                                      "targets": [4,5,6,7,8],
                                    }],
                    language: {
                                searchPlaceholder: 'Search...',
                                sSearch: '',
                                lengthMenu: '_MENU_ items/page',
                              },
                    "bDestroy": true,
                    bJQueryUI: true,
                    aaData: data,
                    aoColumns: [
                        { mData: 'nomor' ,"fnRender": function( oObj ) { return oObj.aData[3].nomor }},
                        { mData: 'kode' ,"fnRender": function( oObj ) { return oObj.aData[3].kode }},
                        { mData: 'nama' ,"fnRender": function( oObj ) { return oObj.aData[3].nama }},
                        { mData: 'dampak' ,"fnRender": function( oObj ) { return oObj.aData[3].dampak }},
                        { mData: 'inherent' ,"fnRender": function( oObj ) { return oObj.aData[3].inherent }},
                        { mData: 'jumlah',"mRender": function(data, type, full) { return (full['jumlah'] > 0)
                          ? (full['jumlah'] + '&nbsp;&nbsp;<i style="color: DodgerBlue;"class="fa fa-info-circle" data-toggle="modal" data-target="#modalTable" data-id="' + full['nomor'] + '" data-risiko="' + full['nama'] + '" onclick="showModal(this)">')
                          : '-'}},
                        { mData: 'memadai',"mRender": function(data, type, full) { return Number(full['residual']) > Number(full['selera']) ? 'Tidak Memadai' : 'Memadai'}},
                        { mData: 'residual' ,"fnRender": function( oObj ) { return oObj.aData[3].residual }},
                        { mData: 'edit',"mRender": function(data, type, full) { return (full['simpulan'] > 0)
                          ? (full['jenis'] + '&nbsp;&nbsp;<i style="color: DodgerBlue;"class="fa fa-info-circle" data-toggle="modal" data-target="#modalSimpulan" data-id="' + full['simpulan'] + '" onclick="showModal2(this)">')
                          : '<a class="btn btn-info btn-sm" href="lini3wasrisiko/' + full['nomor'] + '/tambah">' + 'Buat' + '</a>';
                        }}
                              ],

                });

                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    });
                }).draw();

             }     
          });
      }
 
});
</script>
<script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      "columnDefs": [ {
        "targets": 7,
        "orderable": false
        } ],
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

</script>
@endpush

@push('js1')
  <script type="text/javascript">
    $(document).ready(function() {
      $('#area_pengawasan').select2().on('change', function() {
        var areaID = $('#area_pengawasan').val();
        console.log(areaID);
        if(areaID > 0){ 
          $.ajax({
                url: 'lini3wasrisiko/pilihArea/'+areaID,
                type: 'get',
                dataType: 'json',
                success:function(data) {
                  console.log(data);
                  var t = $('#tbl-pengawasan').DataTable({
                      "columnDefs": [ {
                                        "searchable": false,
                                        "orderable": false,
                                        "targets": [0,8],
                                      },{
                                      "className": "text-center",
                                      "targets": [4,5,6,7,8],
                                    }],
                      language: {
                                searchPlaceholder: 'Search...',
                                sSearch: '',
                                lengthMenu: '_MENU_ items/page',
                              },
                      "bDestroy": true,
                      bJQueryUI: true,
                      aaData: data,
                      aoColumns: [
                          { mData: 'nomor' ,"fnRender": function( oObj ) { return oObj.aData[3].nomor }},
                          { mData: 'kode' ,"fnRender": function( oObj ) { return oObj.aData[3].kode }},
                          { mData: 'nama' ,"fnRender": function( oObj ) { return oObj.aData[3].nama }},
                          { mData: 'dampak' ,"fnRender": function( oObj ) { return oObj.aData[3].dampak }},
                          { mData: 'inherent' ,"fnRender": function( oObj ) { return oObj.aData[3].inherent }},
                          { mData: 'jumlah',"mRender": function(data, type, full) { return (full['jumlah'] > 0)
                          ? (full['jumlah'] + '&nbsp;&nbsp;<i style="color: DodgerBlue;"class="fa fa-info-circle" data-toggle="modal" data-target="#modalTable" data-id="' + full['nomor'] + '" data-risiko="' + full['nama'] + '" onclick="showModal(this)">')
                          : '-'}},
                          { mData: 'memadai',"mRender": function(data, type, full) { return Number(full['residual']) > Number(full['selera']) ? 'Tidak Memadai' : 'Memadai'}},
                          { mData: 'residual' ,"fnRender": function( oObj ) { return oObj.aData[3].residual }},
                          { mData: 'edit',"mRender": function(data, type, full) { return (full['simpulan'] > 0)
                          ? (full['jenis'] + '&nbsp;&nbsp;<i style="color: DodgerBlue;"class="fa fa-info-circle" data-toggle="modal" data-target="#modalSimpulan" data-id="' + full['simpulan'] + '" onclick="showModal2(this)">')
                          : '<a class="btn btn-info btn-sm" href="lini3wasrisiko/' + full['nomor'] + '/tambah">' + 'Buat' + '</a>';
                          }}
                                ],
                  });

                  t.on( 'order.dt search.dt', function () {
                      t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                          cell.innerHTML = i+1;
                      });
                  }).draw();

               }     
            }); 
        } else {
          $.ajax({
                url: 'lini3wasrisiko/semuaArea/',
                type: 'get',
                dataType: 'json',
                success:function(data) {
                  console.log(data);
                  var t = $('#tbl-pengawasan').DataTable({
                      "columnDefs": [ {
                                        "searchable": false,
                                        "orderable": false,
                                        "targets": [0,8],
                                      },{
                                      "className": "text-center",
                                      "targets": [4,5,6,7,8],
                                    }],
                      language: {
                                searchPlaceholder: 'Search...',
                                sSearch: '',
                                lengthMenu: '_MENU_ items/page',
                              },
                      "bDestroy": true,
                      bJQueryUI: true,
                      aaData: data,
                      aoColumns: [
                          { mData: 'nomor' ,"fnRender": function( oObj ) { return oObj.aData[3].nomor }},
                          { mData: 'kode' ,"fnRender": function( oObj ) { return oObj.aData[3].kode }},
                          { mData: 'nama' ,"fnRender": function( oObj ) { return oObj.aData[3].nama }},
                          { mData: 'dampak' ,"fnRender": function( oObj ) { return oObj.aData[3].dampak }},
                          { mData: 'inherent' ,"fnRender": function( oObj ) { return oObj.aData[3].inherent }},
                          { mData: 'jumlah',"mRender": function(data, type, full) { return (full['jumlah'] > 0)
                          ? (full['jumlah'] + '&nbsp;&nbsp;<i style="color: DodgerBlue;"class="fa fa-info-circle" data-toggle="modal" data-target="#modalTable" data-id="' + full['nomor'] + '" data-risiko="' + full['nama'] + '" onclick="showModal(this)">')
                          : '-'}},
                          { mData: 'memadai',"mRender": function(data, type, full) { return Number(full['residual']) > Number(full['selera']) ? 'Tidak Memadai' : 'Memadai'}},
                          { mData: 'residual' ,"fnRender": function( oObj ) { return oObj.aData[3].residual }},
                          { mData: 'edit',"mRender": function(data, type, full) { return (full['simpulan'] > 0)
                          ? (full['jenis'] + '&nbsp;&nbsp;<i style="color: DodgerBlue;"class="fa fa-info-circle" data-toggle="modal" data-target="#modalSimpulan" data-id="' + full['simpulan'] + '" onclick="showModal2(this)">')
                          : '<a class="btn btn-info btn-sm" href="lini3wasrisiko/' + full['nomor'] + '/tambah">' + 'Buat' + '</a>';
                          }}
                                ],
                  });

                  t.on( 'order.dt search.dt', function () {
                      t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                          cell.innerHTML = i+1;
                      });
                  }).draw();

               }     
            });
          }
      });
    });


    function showModal(ele) 
      {
          var id= $(ele).attr('data-id');
          var risiko= $(ele).attr('data-risiko');
          document.getElementById("Risiko").innerHTML = risiko;
          console.log(id);
        

          $.ajax({
              url: 'lini1analisis/showECedit/'+id,
              type: 'get',
              dataType: 'json',
              success:function(data) {
                console.log(data);
                var t = $('#mytable').DataTable({
                    "columnDefs": [ {
                                      "searchable": false,
                                      "orderable": false,
                                      "targets": [0],
                                    }],

                    "bDestroy": true,
                    bJQueryUI: true,
                    aaData: data,
                    aoColumns: [
                        { mData: 'nomor' ,"fnRender": function( oObj ) { return oObj.aData[3].nomor }},
                        { mData: 'pengendalian' ,"fnRender": function( oObj ) { return oObj.aData[3].pengendalian }},
                        { mData: 'spip',"fnRender": function( oObj ) { return oObj.aData[3].spip }},
                        { mData: 'tahun',"fnRender": function( oObj ) { return oObj.aData[3].tahun }}
                              ],
                });

                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    });
                }).draw();

             }     
          });  
      };

    function showModal2(ele) 
      {
          var id = $(ele).attr('data-id');
          var link = "lini3wasrisiko/" + id + "/edit"
          document.getElementById("tombol_edit").href= link;      

          $.ajax({
              url: 'lini3wasrisiko/simpRisiko/'+id,
              type: 'get',
              dataType: 'json',
              success:function(data) {
                console.log(data);
                  $.each(data, function(key, value) {

                      var input1 = value.kode_identifikasi_risiko + ' - ' + value.nama_bagan_risiko;
                      var input2 = value.nama_dampak;
                      var input3 = value.nama_simpulan_risiko;
                      var input4 = value.kondisi_risiko;
                      var input5 = value.rekomendasi_risiko;

                      var text1 = document.getElementById("risiko_read");
                      var text2 = document.getElementById("dampak_read");
                      var text3 = document.getElementById("jenis_simpulan");
                      var text4 = document.getElementById("uraian_simpulan");
                      var text5 = document.getElementById("uraian_rekomendasi");

                      text1.value = input1;
                      text2.value = input2;
                      text3.value = input3;
                      text4.value = input4;
                      text5.value = input5;

                      var stat = value.id_status_temuan;
                      if(stat > 1){
                        $("#tombol_edit").hide();
                      }else{
                        $("#tombol_edit").show();
                      }
                  });

             }     
          });  
      };

  </script>
@endpush