@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('petapibr')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Pelaksanaan PIBR</li>
  </ol>
  <h6 class="slim-pagetitle">Pengawasan atas Pengelolaan Risiko</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Buat Simpulan</h6>
  </div>

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

  @if (Auth::user()->role_id == '8')
  <form class="form-horizontal mt-2" action="{{route('lini3wasrisiko.store')}}" method="post">
    {{ csrf_field() }}
    <div class="box-body">

      <div class="form-group">
        <div class="col-sm-1">
        <input class="form-control" type="text" value="{{$analisis->id_analisis}}" id="id_analisis" name="id_analisis" hidden>
        </div>
      </div>

      <div class="form-group">
        <label for="id_rtp_read" class="col-sm-12 control-label">Kode dan Nama Risiko</label>
        <div class="col-sm-12">
          <input class="form-control" value="{{$risiko->kode_identifikasi_risiko}} - {{$risiko->nama_bagan_risiko}}" name="id_rtp_read" id="id_rtp_read" readonly></input>
        </div>
      </div>
      <div class="form-group">
        <label for="id_rtp_read" class="col-sm-12 control-label">Uraian Dampak</label>
        <div class="col-sm-12">
          <textarea class="form-control" name="id_rtp_read" id="id_rtp_read" readonly>{{$risiko->nama_dampak}}</textarea>
        </div>
      </div>

      <div class="form-group">
        <label for="id_simpulan_risiko" class="col-sm-3 control-label">Simpulan atas Kecukupan Pengendalian</label>
        <div class="col-sm-3">
          <select name="id_simpulan_risiko" class="form-control" id="id_simpulan_risiko" autofocus>
            @foreach($id_simpulan_risiko as $key => $value)
              <option value="{{$key}}" {{$key == 2 ? 'selected' : ''}}>{{$value}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="kondisi_risiko" class="col-sm-12 control-label">Uraian Simpulan</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="kondisi_risiko"></textarea>
          </div>
      </div>
      <div class="form-group">
        <label for="rekomendasi_risiko" class="col-sm-12 control-label">Uraian Rekomendasi</label>
          <div class="col-sm-12">
            <textarea class="form-control" name="rekomendasi_risiko"></textarea>
          </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini3wasrisiko.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
</div>

@stop

@push('js')
<script type="text/javascript">
  $(document).ready(function() {

    $("#id_simpulan_risiko").select2();

});
</script>
@endpush


