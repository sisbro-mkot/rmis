@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('heatmap')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item"><a href="">Manajemen Pengguna</a></li>
    <li class="breadcrumb-item active" aria-current="page">Ubah Data</li>
  </ol>
  <h6 class="slim-pagetitle">Manajemen Pengguna</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h6 class="slim-card-title">Ubah Data</h6>
  </div>
 

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif


  @if (Auth::user()->role_id == '1')
  <form class="form-horizontal mt-2" action="{{route('simpanunituser', $vw->id)}}" method="post">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <div class="box-body">

      <div class="form-group">
        <div class="row pt-2 px-3">
          <div class="col-sm-1">
            <p style="color: black;">Nama:</p>
          </div>
          <div class="col-sm-11">
            <p style="color: black;">{{$renpeglast->s_nama_lengkap}}</p>
          </div>
        </div>
        <div class="row pt-2 px-3">
          <div class="col-sm-1">
            <p style="color: black;">NIP:</p>
          </div>
          <div class="col-sm-11">
            <p style="color: black;">{{$renpeglast->nipbaru}}</p>
          </div>
        </div>
        <div class="row pt-2 px-3">
          <div class="col-sm-1">
            <p style="color: black;">Unit Lama:</p>
          </div>
          <div class="col-sm-11">
            <p style="color: black;">{{$renpeglast->s_nama_instansiunitorg}}</p>
          </div>
        </div>
      </div>

      <div class="form-group">
        <label for="s_kd_instansiunitorg" class="col-sm-2 control-label">Unit Baru</label>
        <div class="col-sm-12">
          <select name="s_kd_instansiunitorg" class="form-control" id="s_kd_instansiunitorg" autofocus>
            @foreach($unit as $key)
              <option value="{{$key->s_kd_instansiunitorg}}" {{$renpeglast->s_kd_instansiunitorg == $key->s_kd_instansiunitorg ? 'selected' : ''}}>{{$key->s_nama_instansiunitorg}}</option>
            @endforeach
          </select>
        </div>
      </div>
      
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-12">
          <button type="submit" class="btn btn-primary"> Simpan</button>
          <a href="{{route('lini2paramuser.index')}}" type="submit" class="btn btn-danger"><i class="icon ion-android-cancel"></i> Batal</a>
        </div>
      </div>
      
    </div>
      <!-- /.box-body -->
  </form>
  @endif
</div>

@stop

@push('js')
<script type="text/javascript">
  $(document).ready(function() {

    $("#s_kd_instansiunitorg").select2();

});
</script>
@endpush