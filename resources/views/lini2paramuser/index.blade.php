@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('heatmap')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Manajemen Pengguna</li>
  </ol>
  <h6 class="slim-pagetitle">Manajemen Pengguna</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <a href="{{url('createparamuser')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah Data</a>
    <a href="{{url('ubahparamuser')}}" class="btn btn-success"><i class="icon ion-edit"></i> Ubah Data</a>
  </div>
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}

    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="3%">No.</th>
          <th style="text-align: center;">NIP</th>
          <th style="text-align: center;">Nama Pengguna</th>
          <th style="text-align: center;">Unit Kerja</th>
          <th style="text-align: center;">Peran Pengguna</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($peran as $item)
        <tr class="item{{$item->id}}">
          <td>{{$no++}}</td>
          <td>{{$item->nipbaru}}</td>
          <td>{{$item->s_nama_lengkap}}</td>
          <td>{{$item->s_skt_instansiunitorg}}</td>
          <td>{{$item->nama_role}}</td>
          <td>
            <a href="{{route('ubahunituser', $item->id_vw_renpeglast)}}" class="btn btn-success btn-xs"><i class="icon ion-edit"></i></a>
          </td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
  </div>
</div>
@endsection

@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({

      "columnDefs": [ {
        "targets": 5,
        "orderable": false
        } ],

      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }

    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
