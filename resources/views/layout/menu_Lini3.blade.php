<li class="nav-item {{ Request::is('petapibr') ? 'active' : null }}">
  <a class="nav-link" href="{{route('petapibr')}}">
    <i class="icon ion-ios-home-outline"></i>
    <span>Beranda</span>
  </a>
</li>

<li class="nav-item with-sub 

">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-filing-outline"></i>
    <span>Parameter</span>
  </a>
  <div class="sub-item">
    <ul>
<!--       <li><a href=""><i class="fa fa-check-circle-o"></i> Data Umum Unit Kerja</a></li>
      <li><a href=""><i class="fa fa-check-circle-o"></i> Panduan Penggunaan Aplikasi</a></li> -->
    </ul>
  </div><!-- dropdown-menu -->
</li>

<li class="nav-item with-sub 
{{ Request::is('lini3perencanaan') ? 'active' : null }} 
">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-book-outline"></i>
    <span>Pengawasan Intern</span>
  </a>
  <div class="sub-item">
    <ul>
    	<li><a href="{{route('lini3perencanaan.index')}}"><i class="fa fa-check-circle-o"></i> Perencanaan</a></li>
      <li><a href="{{url('lini3wasrisiko')}}"><i class="fa fa-check-circle-o"></i> Pelaksanaan</a></li>
<!--       <li><a href=""><i class="fa fa-check-circle-o"></i> Pelaksanaan</a></li>
      <li><a href=""><i class="fa fa-check-circle-o"></i> Pelaporan</a></li> -->
    </ul>
  </div><!-- dropdown-menu -->
</li>