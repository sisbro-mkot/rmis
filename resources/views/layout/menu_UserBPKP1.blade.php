<li class="nav-item {{ Request::is('userpemda1/home') ? 'active' : null }}">
  <a class="nav-link" href="{{route('userpemda1')}}">
    <i class="icon ion-ios-home-outline"></i>
    <span>Beranda</span>
  </a>
</li>
<li class="nav-item with-sub {{ Request::is('userpemda1/identifikasipemda1') ? 'active' : null }} {{ Request::is('userpemda1/analisispemda1') ? 'active' : null }} {{ Request::is('userpemda1/rtppemda1') ? 'active' : null }}">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-book-outline"></i>
    <span>Pengelolaan Risiko</span>
  </a>
  <div class="sub-item">
    <ul>
      <li><a href="{{route('identifikasipemda1.index')}}"><i class="fa fa-check-circle-o"></i> Identifikasi Risiko Organisasi</a></li>
      <li><a href="{{route('analisispemda1.index')}}"><i class="fa fa-check-circle-o"></i> Analisis &amp; Evaluasi Risiko Organisasi</a></li>
      <li><a href="{{route('rtppemda1.index')}}"><i class="fa fa-check-circle-o"></i> RTP Risiko Organisasi</a></li>
    </ul>
  </div><!-- dropdown-menu -->
</li>