@if (Auth::user()->role_id == '3')

<li class="nav-item 
{{ Request::is('heatmapbpkp') ? 'active' : null }}
{{ Request::is('heatmapbpkpi') ? 'active' : null }}
{{ Request::is('heatmapbpkpr') ? 'active' : null }}">
  <a class="nav-link" href="{{route('heatmapbpkp')}}">
    <i class="icon ion-ios-home-outline"></i>
    <span>Beranda</span>
  </a>
</li>

<li class="nav-item with-sub 
{{ Request::is('lini1databpkp') ? 'active' : null }}
{{ Request::is('lini1kamus') ? 'active' : null }}
{{ Request::is('lini1panduan') ? 'active' : null }}
{{ Request::is('lini1usul') ? 'active' : null }}">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-filing-outline"></i>
    <span>Parameter</span>
  </a>
  <div class="sub-item">
    <ul>
      <li><a href="{{route('lini1databpkp.index')}}"><i class="fa fa-check-circle-o"></i> Data Umum</a></li>
      <li><a href="{{route('lini1kamus')}}"><i class="fa fa-check-circle-o"></i> Kamus Risiko</a></li>
      <li><a href="{{route('lini1usul')}}"><i class="fa fa-check-circle-o"></i> Usulan Risiko Baru</a></li>
      <li><a href="{{route('lini1panduan')}}"><i class="fa fa-check-circle-o"></i> Panduan Penggunaan Aplikasi</a></li>
      <li><a target="_blank" href="https://drive.google.com/file/d/1c2qTh9dAglUzNL77-udKTtfRmzi9o63x/view?usp=sharing"><i class="fa fa-check-circle-o"></i> Petunjuk Pengaduan Aplikasi</a></li>
    </ul>
  </div><!-- dropdown-menu -->
</li>

<li class="nav-item with-sub 
{{ Request::is('lini1tetapkonteksbpkp') ? 'active' : null }} 
{{ Request::is('lini1identifikasibpkp') ? 'active' : null }}
{{ Request::is('lini1analisisbpkp') ? 'active' : null }} 
{{ Request::is('lini1evaluasibpkp') ? 'active' : null }}
{{ Request::is('lini1responsbpkp') ? 'active' : null }}
{{ Request::is('lini1realisasibpkp') ? 'active' : null }}
{{ Request::is('lini1monitoringbpkp') ? 'active' : null }}
{{ Request::is('lini1laporanbpkp') ? 'active' : null }}">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-book-outline"></i>
    <span>Pengelolaan Risiko</span>
  </a>
  <div class="sub-item">
    <ul>
      <li><a href="{{route('lini1tetapkonteksbpkp.index')}}"><i class="fa fa-check-circle-o"></i> Penetapan Konteks</a></li>
      <li><a href="{{route('lini1identifikasibpkp.index')}}"><i class="fa fa-check-circle-o"></i> Identifikasi Risiko</a></li>
      <li><a href="{{route('lini1analisisbpkp.index')}}"><i class="fa fa-check-circle-o"></i> Analisis Risiko</a></li>
      <li><a href="{{route('lini1evaluasibpkp.index')}}"><i class="fa fa-check-circle-o"></i> Evaluasi Risiko</a></li>
      <li><a href="{{route('lini1responsbpkp.index')}}"><i class="fa fa-check-circle-o"></i> Respons Risiko (RTP)</a></li>
      <li><a href="{{route('lini1realisasibpkp.index')}}"><i class="fa fa-check-circle-o"></i> Respons Risiko (Realisasi)</a></li>
      <li><a href="{{route('lini1monitoringbpkp.index')}}"><i class="fa fa-check-circle-o"></i> Pemantauan Risiko</a></li>
      <li><a href="{{route('lini1laporanbpkp.index')}}"><i class="fa fa-check-circle-o"></i> Pelaporan Risiko</a></li>
    </ul>
  </div><!-- dropdown-menu -->
</li>

@else

<li class="nav-item 
{{ Request::is('heatmapunit') ? 'active' : null }}
{{ Request::is('heatmapuniti') ? 'active' : null }}
{{ Request::is('heatmapunitr') ? 'active' : null }}">
  <a class="nav-link" href="{{route('heatmapunit')}}">
    <i class="icon ion-ios-home-outline"></i>
    <span>Beranda</span>
  </a>
</li>

<li class="nav-item with-sub 
{{ Request::is('lini1data') ? 'active' : null }}
{{ Request::is('lini1kamus') ? 'active' : null }}
{{ Request::is('lini1panduan') ? 'active' : null }}
{{ Request::is('lini1usul') ? 'active' : null }}">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-filing-outline"></i>
    <span>Parameter</span>
  </a>
  <div class="sub-item">
    <ul>
      <li><a href="{{route('lini1data.index')}}"><i class="fa fa-check-circle-o"></i> Data Umum Unit Kerja</a></li>
      <li><a href="{{route('lini1kamus')}}"><i class="fa fa-check-circle-o"></i> Kamus Risiko</a></li>
      @if (Auth::user()->role_id == '4'|Auth::user()->role_id == '5'|Auth::user()->role_id == '6'|Auth::user()->role_id == '7'|Auth::user()->role_id == '10')
      <li><a href="{{route('lini1usul')}}"><i class="fa fa-check-circle-o"></i> Usulan Risiko Baru</a></li>
      <li><a href="{{route('lini1panduan')}}"><i class="fa fa-check-circle-o"></i> Panduan Penggunaan Aplikasi</a></li>
      @endif
      <li><a target="_blank" href="https://drive.google.com/file/d/1c2qTh9dAglUzNL77-udKTtfRmzi9o63x/view?usp=sharing"><i class="fa fa-check-circle-o"></i> Petunjuk Pengaduan Aplikasi</a></li>
    </ul>
  </div><!-- dropdown-menu -->
</li>

<li class="nav-item with-sub 
{{ Request::is('lini1tetapkonteks') ? 'active' : null }} 
{{ Request::is('lini1identifikasi') ? 'active' : null }}
{{ Request::is('lini1analisis') ? 'active' : null }} 
{{ Request::is('lini1evaluasi') ? 'active' : null }}
{{ Request::is('lini1respons') ? 'active' : null }}
{{ Request::is('lini1realisasi') ? 'active' : null }}
{{ Request::is('lini1monitoring') ? 'active' : null }}
{{ Request::is('lini1laporan') ? 'active' : null }}
{{ Request::is('lini1aktual') ? 'active' : null }}">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-book-outline"></i>
    <span>Pengelolaan Risiko</span>
  </a>
  <div class="sub-item">
    <ul>
    	<li><a href="{{route('lini1tetapkonteks.index')}}"><i class="fa fa-check-circle-o"></i> Penetapan Konteks</a></li>
      <li><a href="{{route('lini1identifikasi.index')}}"><i class="fa fa-check-circle-o"></i> Identifikasi Risiko</a></li>
      <li><a href="{{route('lini1analisis.index')}}"><i class="fa fa-check-circle-o"></i> Analisis Risiko</a></li>
      <li><a href="{{route('lini1evaluasi.index')}}"><i class="fa fa-check-circle-o"></i> Evaluasi Risiko</a></li>
      <li><a href="{{route('lini1respons.index')}}"><i class="fa fa-check-circle-o"></i> Respons Risiko (RTP)</a></li>
      <li><a href="{{route('lini1realisasi.index')}}"><i class="fa fa-check-circle-o"></i> Respons Risiko (Realisasi)</a></li>
      <li><a href="{{route('lini1monitoring.index')}}"><i class="fa fa-check-circle-o"></i> Pemantauan Risiko</a></li>
      <li><a href="{{route('lini1laporan.index')}}"><i class="fa fa-check-circle-o"></i> Pelaporan Risiko</a></li>
      <li><a href="{{route('lini1aktual.index')}}"><i class="fa fa-check-circle-o"></i> Proses Akhir Tahun</a></li>
    </ul>
  </div><!-- dropdown-menu -->
</li>

@endif