<li class="nav-item {{ Request::is('adminpemda/home') ? 'active' : null }}">
  <a class="nav-link" href="{{route('adminpemda')}}">
    <i class="icon ion-ios-home-outline"></i>
    <span>Beranda</span>
  </a>
</li>
<li class="nav-item with-sub {{ Request::is('adminpemda/pemda') ? 'active' : null }} {{ Request::is('adminpemda/opd') ? 'active' : null }} {{ Request::is('adminpemda/kategori') ? 'active' : null }} {{ Request::is('adminpemda/baganrisiko') ? 'active' : null }}">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-book-outline"></i>
    <span>Parameter</span>
  </a>
  <div class="sub-item">
    <ul>
      <li><a href="{{route('pemda.index')}}"><i class="fa fa-check-circle-o"></i> Data Umum Organisasi</a></li>
      <li><a href="{{route('opd.index')}}"><i class="fa fa-check-circle-o"></i> Unit Kerja Organisasi</a></li>
      <li><a href="{{route('kategori.index')}}"><i class="fa fa-check-circle-o"></i> Kategori dan Selera Risiko</a></li>
      <li><a href="{{route('baganrisiko.index')}}"><i class="fa fa-check-circle-o"></i> Bagan Risiko Standar</a></li>
      <li><a href="{{route('proses.index')}}"><i class="fa fa-check-circle-o"></i> Daftar Kelompok Risiko</a></li>
    </ul>
  </div><!-- dropdown-menu -->
</li>
<li class="nav-item with-sub {{ Request::is('adminpemda/visi') ? 'active' : null }} {{ Request::is('adminpemda/misi') ? 'active' : null }} {{ Request::is('adminpemda/tujuan') ? 'active' : null }} {{ Request::is('adminpemda/sasaran') ? 'active' : null }} {{ Request::is('adminpemda/program') ? 'active' : null }} {{ Request::is('adminpemda/mapping') ? 'active' : null }}">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-filing-outline"></i>
    <span>Perencanaan Kinerja</span>
  </a>
  <div class="sub-item">
    <ul>
      <li><a href="{{route('visi.index')}}"><i class="fa fa-check-circle-o"></i> Visi</a></li>
      <li><a href="{{route('misi.index')}}"><i class="fa fa-check-circle-o"></i> Misi</a></li>
      <li><a href="{{route('tujuan.index')}}"><i class="fa fa-check-circle-o"></i> Tujuan</a></li>
      <li><a href="{{route('sasaran.index')}}"><i class="fa fa-check-circle-o"></i> Sasaran</a></li>
      <li><a href="{{route('program.index')}}"><i class="fa fa-check-circle-o"></i> Program</a></li>
      <li><a href="{{route('mapping.index')}}"><i class="fa fa-check-circle-o"></i> Mapping</a></li>
    </ul>
  </div><!-- dropdown-menu -->
</li>
<li class="nav-item with-sub {{ Request::is('adminpemda/manajemen-user') ? 'active' : null }}">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-gear-outline"></i>
    <span>Administrasi</span>
  </a>
  <div class="sub-item">
    <ul>
      <li><a href="{{route('manajemen-user.index')}}"><i class="fa fa-check-circle-o"></i> Manajemen User</a></li>
      <!-- <li><a href="#"><i class="fa fa-check-circle-o"></i> Ganti Password</a></li> -->
      <!-- <li><a href="#"><i class="fa fa-check-circle-o"></i> Log User</a></li> -->
    </ul>
  </div><!-- dropdown-menu -->
</li>