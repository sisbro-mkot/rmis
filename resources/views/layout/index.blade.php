<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>BPKP | Siger</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('sweetalert2/dist/sweetalert.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('bootstrap/css/bootstrap.min.css')}}">
  <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
  <link rel="icon" href="{{URL::asset('images/favicon.png')}}" type="image/x-icon">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('select2/dist/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('fontawesome/css/font-awesome.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('ionicons/css/ionicons.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('datatable/DataTables-1.10.16/css/dataTables.bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('css/AdminLTE.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('css/_all-skins.min.css')}}">

  <link rel="stylesheet" href="{{URL::asset('css/font.css')}}">
</head>
<!-- ADD THE CLASS sidebar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('layout.header')

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <!-- <img src="http://10.10.1.14:9081/servlet/api/foto/{{Auth::user()->user_nip}}" style="width: 45px;height: 45px" class="img-circle" alt="User Image"> -->
          <object data="http://10.10.1.14:9082/servlet/api/foto/{{Auth::user()->user_nip}}" type="image/png" width="45" height="45" class="img-circle" alt=""><img src="http://118.97.51.140:10001/map/public/foto/{{Auth::user()->user_nip}}.gif" width="45" height="45" class="img-circle" alt=""/></object>
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
      
      @if (Auth::user()->hasRole('Admin BPKP'))
				@include('layout.sidebarAdminPemda')
			@endif

			@if (Auth::user()->hasRole('User BPKP 1'))
				@include('layout.sidebarPemda1')
			@endif

			@if (Auth::user()->hasRole('User BPKP 2'))
				@include('layout.sidebarPemda2')
			@endif

			@if (Auth::user()->hasRole('Admin PWK'))
				@include('layout.sidebarAdminOPD')
			@endif

			@if (Auth::user()->hasRole('User PWK 1'))
				@include('layout.sidebarOPD1')
			@endif

			@if (Auth::user()->hasRole('User PWK 2'))
				@include('layout.sidebarOPD2')
			@endif

			@if (Auth::user()->hasRole('User PWK Kegiatan 1'))
				@include('layout.sidebarKegiatan1')
			@endif

			@if (Auth::user()->hasRole('User PWK Kegiatan 2'))
				@include('layout.sidebarKegiatan2')
			@endif

    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  @yield('isi')

  @include('layout.footer')
</div>
<!-- ./wrapper -->

<script src="{{URL::asset('js/jquery-3.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('sweetalert2/dist/sweetalert.min.js')}}"></script>
@include('sweet::alert')
<script src="{{URL::asset('js/popper.min.js')}}"></script>
<script src="{{URL::asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{URL::asset('datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('datatable/DataTables-1.10.16/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/jquery.slimscroll.min.js')}}"></script>
<script src="{{URL::asset('js/fastclick.js')}}"></script>
<script src="{{URL::asset('js/adminlte.min.js')}}"></script>
<script src="{{URL::asset('js/demo.js')}}"></script>
@stack('js')
@stack('js1')
</body>
</html>
