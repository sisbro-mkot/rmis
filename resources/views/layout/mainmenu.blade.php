<div class="slim-navbar">
  <div class="container-fluid">
    <ul class="nav">

      @if (Auth::user()->role_id == '3'|Auth::user()->role_id == '4'|Auth::user()->role_id == '5'|Auth::user()->role_id == '6'|Auth::user()->role_id == '7'|Auth::user()->role_id == '9'|Auth::user()->role_id == '10')
        @include('layout.menu_Lini1')
      @endif

      @if (Auth::user()->role_id == '1'|Auth::user()->role_id == '2')
        @include('layout.menu_Lini2')
      @endif

      @if (Auth::user()->role_id == '8')
        @include('layout.menu_Lini3')
      @endif

    </ul>
  </div><!-- container-fluid -->
</div><!-- slim-navbar -->