<li class="nav-item {{ Request::is('useropdkegiatan1/home') ? 'active' : null }}">
  <a class="nav-link" href="{{route('opdkegiatan1')}}">
    <i class="icon ion-ios-home-outline"></i>
    <span>Beranda</span>
  </a>
</li>
<li class="nav-item with-sub {{ Request::is('useropdkegiatan1/identifikasikegiatan1') ? 'active' : null }} {{ Request::is('useropdkegiatan1/analisiskegiatan1') ? 'active' : null }} {{ Request::is('useropdkegiatan1/rtpkegiatan1') ? 'active' : null }}">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-book-outline"></i>
    <span>Pengelolaan Risiko</span>
  </a>
  <div class="sub-item">
    <ul>
      <li><a href="{{route('identifikasikegiatan1.index')}}"><i class="fa fa-check-circle-o"></i> Identifikasi Risiko Kegiatan</a></li>
      <li><a href="{{route('analisiskegiatan1.index')}}"><i class="fa fa-check-circle-o"></i> Analisis Risiko Kegiatan</a></li>
      <li><a href="{{route('rtpkegiatan1.index')}}"><i class="fa fa-check-circle-o"></i> RTP Risiko Kegiatan</a></li>
    </ul>
  </div><!-- dropdown-menu -->
</li>