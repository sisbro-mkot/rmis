<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    	<li><a href="{{route('adminopd')}}"><i class="fa fa-home"></i> <span> Home</span></a></li>
        <li class="treeview 
            {{ Request::is('adminopd/misiopd') ? 'menu-open' : null }}
            {{ Request::is('adminopd/tujuanopd') ? 'menu-open' : null }}
            {{ Request::is('adminopd/sasaranopd') ? 'menu-open' : null }}
            {{ Request::is('adminopd/programopd') ? 'menu-open' : null }}
            {{ Request::is('adminopd/kegiatanopd') ? 'menu-open' : null }}
        ">
        	<a href="#">
            <i class="fa fa-tasks"></i> <span>Perencanaan Kinerja</span>
            <span class="pull-right-container">
            	<i class="fa fa-angle-left pull-right"></i>
            </span>
          	</a>
          	<ul class="treeview-menu" style="display: {{ Request::is('adminopd/misiopd') ? 'block' : '' }} {{ Request::is('adminopd/tujuanopd') ? 'block' : '' }} {{ Request::is('adminopd/sasaranopd') ? 'block' : '' }} {{ Request::is('adminopd/programopd') ? 'block' : '' }} {{ Request::is('adminopd/kegiatanopd') ? 'block' : '' }};">
            	<li class="{{ Request::is('adminopd/misiopd') ? 'active' : null }}"><a href="{{route('misiopd.index')}}"><i class="fa fa-check-circle-o"></i> Misi</a></li>
            	<li class="{{ Request::is('adminopd/tujuanopd') ? 'active' : null }}"><a href="{{route('tujuanopd.index')}}"><i class="fa fa-check-circle-o"></i> Tujuan</a></li>
            	<li class="{{ Request::is('adminopd/sasaranopd') ? 'active' : null }}"><a href="{{route('sasaranopd.index')}}"><i class="fa fa-check-circle-o"></i> Sasaran</a></li>
            	<li class="{{ Request::is('adminopd/programopd') ? 'active' : null }}"><a href="{{route('programopd.index')}}"><i class="fa fa-check-circle-o"></i> Program</a></li>
            	<li class="{{ Request::is('adminopd/kegiatanopd') ? 'active' : null }}"><a href="{{route('kegiatanopd.index')}}"><i class="fa fa-check-circle-o"></i> Kegiatan</a></li>
          	</ul>
        </li>
</ul>
