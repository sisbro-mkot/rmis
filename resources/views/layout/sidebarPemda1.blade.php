<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    	<li><a href="{{route('userpemda1')}}"><i class="fa fa-home"></i> <span> Home</span></a></li>
        <li class="treeview">
        	<a href="#">
            <i class="fa fa-tasks"></i> <span>Pengelolaan Risiko</span>
            <span class="pull-right-container">
            	<i class="fa fa-angle-left pull-right"></i>
            </span>
          	</a>
          	<ul class="treeview-menu">
            	<li><a href="{{route('identifikasipemda1.index')}}"><i class="fa fa-check-circle-o"></i> Identifikasi Risiko Pemda</a></li>
            	<li><a href="{{route('analisispemda1.index')}}"><i class="fa fa-check-circle-o"></i> Analisis &amp; Evaluasi Risiko Pemda</a></li>
            	<li><a href="{{route('rtppemda1.index')}}"><i class="fa fa-check-circle-o"></i> RTP Risiko Pemda</a></li>
          	</ul>
        </li>
</ul>
