<li class="nav-item {{ Request::is('adminopd/home') ? 'active' : null }}">
  <a class="nav-link" href="{{route('adminopd')}}">
    <i class="icon ion-ios-home-outline"></i>
    <span>Beranda</span>
  </a>
</li>
<li class="nav-item with-sub {{ Request::is('adminopd/misiopd') ? 'active' : null }} {{ Request::is('adminopd/tujuanopd') ? 'active' : null }} {{ Request::is('adminopd/sasaranopd') ? 'active' : null }} {{ Request::is('adminopd/programopd') ? 'active' : null }} {{ Request::is('adminopd/mappingopd') ? 'active' : null }}">
  <a class="nav-link" href="#" data-toggle="dropdown">
    <i class="icon ion-ios-filing-outline"></i>
    <span>Perencanaan Kinerja</span>
  </a>
  <div class="sub-item">
    <ul>
      <li><a href="{{route('misiopd.index')}}"><i class="fa fa-check-circle-o"></i> Misi</a></li>
      <li><a href="{{route('tujuanopd.index')}}"><i class="fa fa-check-circle-o"></i> Tujuan</a></li>
      <li><a href="{{route('sasaranopd.index')}}"><i class="fa fa-check-circle-o"></i> Sasaran</a></li>
      <li><a href="{{route('programopd.index')}}"><i class="fa fa-check-circle-o"></i> Program</a></li>
      <li><a href="{{route('kegiatanopd.index')}}"><i class="fa fa-check-circle-o"></i> Kegiatan</a></li>
      <li><a href="{{route('mappingopd.index')}}"><i class="fa fa-check-circle-o"></i> Mapping</a></li>
    </ul>
  </div><!-- dropdown-menu -->
</li>