  
<div class="container">
  <div class="row">
    <div class="col-lg-3">
      <div class="col-sm-6 col-lg-12">
        <div class="card card-status">
          <a href="{{url('peta')}}"><center><h1>Risiko</h1></center></a>
            <svg viewBox="0 0 29700 10000">
              <metadata id="CorelCorpID_0Corel-Layer"/>
              <a href="{{url('dashboardregister')}}">
                <polygon class="sbleft" points="13653,127 17266,9925 389,9925 389,127 "></polygon>
                <text x="25%" y="50%"  dominant-baseline="middle" text-anchor="middle" fill="white" font-weight="normal" font-size="5291.66px" font-family="Arial Rounded MT Bold">{{$risikoteridentifikasi}}</text>
                <text x="2794" y="8708"  fill="pink" font-weight="bold" font-size="1693.33px" font-family="Arial">Teridentifikasi</text>
              </a>
              <a href="{{url('skoranalisis')}}">
                <polygon class="sbright" points="29389,127 29389,9925 18720,9925 15107,127 "></polygon>
                <text x="80%" y="50%"  dominant-baseline="middle" text-anchor="middle" fill="white" font-weight="normal" font-size="5291.66px" font-family="Arial Rounded MT Bold">{{$risikotermitigasi}}</text>
                <text x="19322" y="8708"  fill="lime" font-weight="bold" font-size="1693.33px" font-family="Arial">Termitigasi</text>
              </a>
            </svg>
        </div><!-- card -->
      </div><!-- col-sm-6 -->
      <div class="col-sm-6 col-lg-12 mg-t-10 mg-sm-t-0">
        <div class="card card-status">
          <a href="{{url('penyebab')}}"><center><h1>Penyebab</h1></center></a>
            <svg viewBox="0 0 29700 10000">
              <metadata id="CorelCorpID_0Corel-Layer"/>
              <a href="{{url('penyebablist')}}">
                <polygon class="sbleft" points="13653,127 17266,9925 389,9925 389,127 "></polygon>
                <text x="25%" y="50%"  dominant-baseline="middle" text-anchor="middle" fill="white" font-weight="normal" font-size="5291.66px" font-family="Arial Rounded MT Bold">{{$sebabteridentifikasi}}</text>
                <text x="2794" y="8708"  fill="pink" font-weight="bold" font-size="1693.33px" font-family="Arial">Teridentifikasi</text>
              </a>
              <a href="{{url('penyebablist')}}">
                <polygon class="sbright" points="29389,127 29389,9925 18720,9925 15107,127 "></polygon>
                <text x="80%" y="50%"  dominant-baseline="middle" text-anchor="middle" fill="white" font-weight="normal" font-size="5291.66px" font-family="Arial Rounded MT Bold">{{$sebabtermitigasi}}</text>
                <text x="19322" y="8708"  fill="lime" font-weight="bold" font-size="1693.33px" font-family="Arial">Termitigasi</text>
              </a>
            </svg>
        </div><!-- card -->
      </div><!-- col-sm-6 -->
      <div class="col-sm-6 col-lg-12 mg-t-10 mg-lg-t-0">
        <div class="card card-status">
          <a href="{{url('rtp')}}"><center><h1>RTP</h1></center></a>
            <svg viewBox="0 0 29700 10000">
              <metadata id="CorelCorpID_0Corel-Layer"/>
              <a href="{{url('rtplist')}}">
                <polygon class="sbleft" points="13653,127 17266,9925 389,9925 389,127 "></polygon>
                <text x="25%" y="50%"  dominant-baseline="middle" text-anchor="middle" fill="white" font-weight="normal" font-size="5291.66px" font-family="Arial Rounded MT Bold">{{$rtpjadwal}}</text>
                <text x="4300" y="8708"  fill="pink" font-weight="bold" font-size="1693.33px" font-family="Arial">Terjadwal</text>
              </a>
              <a href="{{url('rtplist')}}">
                <polygon class="sbright" points="29389,127 29389,9925 18720,9925 15107,127 "></polygon>
                <text x="80%" y="50%"  dominant-baseline="middle" text-anchor="middle" fill="white" font-weight="normal" font-size="5291.66px" font-family="Arial Rounded MT Bold">{{$rtprealisasi}}</text>
                <text x="19322" y="8708"  fill="lime" font-weight="bold" font-size="1693.33px" font-family="Arial">Terealisasi</text>
              </a>
          </svg>
        </div><!-- card -->
      </div><!-- col-sm-6 -->
      <div class="col-sm-6 col-lg-12 mg-t-10 mg-lg-t-0">
        <div class="card card-status">
          <a href="{{url('insiden')}}"><center><h1>Insiden</h1></center></a>
            <svg viewBox="0 0 29700 10000">
              <metadata id="CorelCorpID_0Corel-Layer"/>
              <a href="{{url('insidenkejadian')}}">
                <polygon class="sbleft" points="370,122 7175,122 10820,9920 370,9920 "></polygon>
                <text x="15%" y="50%"  dominant-baseline="middle" text-anchor="middle" fill="white" font-weight="normal" font-size="4000px" font-family="Arial Rounded MT Bold">{{$insidenk}}</text>
                <text x="1512" y="8600"  fill="#FF99CC" font-weight="bold" font-size="1700px" font-family="Arial">Kejadian</text>
              </a>
              <a href="{{url('insidenrisiko')}}">
                <polygon class="sbmid" points="7649,122 16325,122 19970,9920 11294,9920 "></polygon>
                <text x="47%" y="50%"  dominant-baseline="middle" text-anchor="middle" fill="white" font-weight="normal" font-size="4000px" font-family="Arial Rounded MT Bold">{{$insidenr}}</text>
                <text x="12101" y="8600"  fill="#FFFF66" font-weight="bold" font-size="1700px" font-family="Arial">Risiko</text>
              </a>
              <a href="{{url('insidensebab')}}">
                <polygon class="sbright" points="16799,122 29370,122 29370,9920 20445,9920 "></polygon>
                <text x="82%" y="50%"  dominant-baseline="middle" text-anchor="middle" fill="white" font-weight="normal" font-size="4000px" font-family="Arial Rounded MT Bold">{{$insidenp}}</text>
                <text x="20553" y="8600"  fill="lime" font-weight="bold" font-size="1700px" font-family="Arial">Penyebab</text>
              </a>
            </svg>
        </div><!-- card -->
      </div><!-- col-sm-6 -->
    </div><!-- col-lg-3 -->

