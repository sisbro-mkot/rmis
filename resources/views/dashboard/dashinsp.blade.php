<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/logo.jpg')}}">
  <!-- Twitter -->
  <meta name="twitter:site" content="@themepixels">
  <meta name="twitter:creator" content="@themepixels">
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="Slim">
  <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
  <meta name="twitter:image" content="http://themepixels.me/slim/img/slim-social.png">

  <!-- Facebook -->
  <meta property="og:url" content="http://themepixels.me/slim">
  <meta property="og:title" content="Slim">
  <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

  <meta property="og:image" content="http://themepixels.me/slim/img/slim-social.png">
  <meta property="og:image:secure_url" content="http://themepixels.me/slim/img/slim-social.png">
  <meta property="og:image:type" content="image/png">
  <meta property="og:image:width" content="1200">
  <meta property="og:image:height" content="600">

  <!-- Meta -->
  <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
  <meta name="author" content="ThemePixels">

  <title>BPKP-Wide Risk Management System</title>
  <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
  <link rel="icon" href="{{URL::asset('images/favicon.png')}}" type="image/x-icon">
  <!-- Datatables -->
  <link href="{{asset('app/lib/datatables/css/jquery.dataTables.css')}}" rel="stylesheet">
  <link href="{{asset('app/lib/select2/css/select2.min.css')}}" rel="stylesheet">
  <!-- Sweetalert -->
  <!-- Sweetalert Css -->
  <link href="{{asset('sweetalert2/dist/sweetalert.min.css')}}" rel="stylesheet" type="text/css" >
  <!-- vendor css -->
  <link href="{{asset('app/lib/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
  <link href="{{asset('app/lib/Ionicons/css/ionicons.css')}}" rel="stylesheet">
  <link href="{{asset('app/lib/rickshaw/css/rickshaw.min.css')}}" rel="stylesheet">
  <link href="{{asset('app/lib/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
  <!-- Slim CSS -->
  <link rel="stylesheet" href="{{asset('app/css/slim.css')}}">
  <script src="{{asset('app/lib/jquery/js/jquery.js')}}"></script>
  <!-- SweetAlert Plugin Js -->
  <script src="{{asset('sweetalert2/dist/sweetalert.min.js')}}" type="text/javascript" ></script>
  <!-- Peta Indonesia -->
  <link rel="stylesheet" href="{{asset('app/css/peta.css')}}">
</head>
<body class="dashboard-3">
  <div class="slim-header">
    <div class="container-fluid">
      <div class="slim-header-left">
        <h2><a href="{{url('#')}}" style="color: #FF6600 !important;"><img class="logo" height="45px" src="{{asset('images/logo.png')}}">   <b>BPKP-<i>Wide Risk Management System</i></b></a></h2>
      </div><!-- slim-header-left -->
    </div><!-- container-fluid -->
  </div><!-- slim-header -->

  <div class="slim-mainpanel">

<div class="container">
  <div class="row">
    <iframe src="http://180.250.6.222:9300/bi/?perspective=dashboard&amp;pathRef=.my_folders%2FEndila%2FLATIHAN%2BENDILA&amp;ui_appbar=false&amp;ui_navbar=false&amp;shareMode=embedded&amp;action=view&amp;mode=dashboard&amp;subView=model0000017848eea10d_00000001" width="1200" height="600" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen=""></iframe>
  </div><!-- row -->
</div><!-- container -->



  </div><!-- slim-mainpanel -->

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="{{asset('app/lib/jquery/js/jquery.js')}}"></script>
    <script src="{{asset('app/lib/popper.js/js/popper.js')}}"></script>
    <script src="{{asset('app/lib/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{asset('app/lib/jquery.cookie/js/jquery.cookie.js')}}"></script>
    <script src="{{asset('app/lib/d3/js/d3.js')}}"></script>
    <script src="{{asset('app/lib/rickshaw/js/rickshaw.min.js')}}"></script>
    <script src="{{asset('app/lib/Flot/js/jquery.flot.js')}}"></script>
    <script src="{{asset('app/lib/Flot/js/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('app/lib/peity/js/jquery.peity.js')}}"></script>
    <script src="{{asset('app/js/slim.js')}}"></script>
    <script src="{{asset('app/js/ResizeSensor.js')}}"></script>
    <script src="{{asset('app/lib/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('app/lib/datatables-responsive/js/dataTables.responsive.js')}}"></script>
    <script src="{{asset('app/lib/select2/js/select2.min.js')}}"></script>


</body>
</html>