@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('heatmap')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Pelaporan Risiko</li>
  </ol>
  <h6 class="slim-pagetitle">Pelaporan Risiko </h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="card-header">
    <h5 style="color: black;">Pilih Unit Pemilik Risiko</h5>
    <div class="col-sm-12">
      <select class="form-control" name="pemilik_risiko" id="pemilik_risiko">
        
        @foreach($pemilik_risiko as $per)
        <option value="{{$per->id}}" >{{$per->s_nama_instansiunitorg}}</option>
        @endforeach

      </select>
    </div>
  </div>
  <div class="card-header">
    <h5 style="color: black;">Identifikasi Risiko</h5>
    <a href="cetakid" class="btn btn-primary" target="_blank" id="cetakid_btn"><i class="icon ion-printer"></i> Cetak PDF</a>
  </div>
  <div class="card-header">
    <h5 style="color: black;">Analisis Risiko</h5>
    <a href="cetakan" class="btn btn-primary" target="_blank" id="cetakan_btn"><i class="icon ion-printer"></i> Cetak PDF</a>
  </div>
  <div class="card-header">
    <h5 style="color: black;">Daftar Risiko Prioritas Unit Kerja</h5>
    <a href="cetakev" class="btn btn-primary" target="_blank" id="cetakev_btn"><i class="icon ion-printer"></i> Cetak PDF</a>
  </div>
  <div class="card-header">
    <h5 style="color: black;">Analisis Akar Masalah <span style="font-style: italic;">(Root Cause Analysis)</span></h5>
    <a href="cetakrca" class="btn btn-primary" target="_blank" id="cetakrca_btn"><i class="icon ion-printer"></i> Cetak PDF</a>
  </div>
  <div class="card-header">
    <h5 style="color: black;">Rencana Tindak Pengendalian</h5>
    <a href="cetakres" class="btn btn-primary" target="_blank" id="cetakres_btn"><i class="icon ion-printer"></i> Cetak PDF</a>
  </div>
  <div class="card-header">
    <h5 style="color: black;">Daftar Pemantauan Kegiatan Pengendalian</h5>
    <a href="cetakreal" class="btn btn-primary" target="_blank" id="cetakreal_btn"><i class="icon ion-printer"></i> Cetak PDF</a>
  </div>
  <div class="card-header">
    <h5 style="color: black;">Pemantauan Terhadap Keterjadian Risiko</h5>
    <a href="cetakmon" class="btn btn-primary" target="_blank" id="cetakmon_btn"><i class="icon ion-printer"></i> Cetak PDF</a>
  </div>
</div>
@endsection

@push('js')
<script type="text/javascript">
  $(document).ready(function() {
    $("#pemilik_risiko").select2();

      var areaID = $('#pemilik_risiko').val();
      console.log(areaID);
      var cetakid_link = "cetakid/" + areaID;
      var cetakan_link = "cetakan/" + areaID;
      var cetakev_link = "cetakev/" + areaID;
      var cetakrca_link = "cetakrca/" + areaID;
      var cetakres_link = "cetakres/" + areaID;
      var cetakreal_link = "cetakreal/" + areaID;
      var cetakmon_link = "cetakmon/" + areaID;
      $('#cetakid_btn').attr('href', cetakid_link);
      $('#cetakan_btn').attr('href', cetakan_link);
      $('#cetakev_btn').attr('href', cetakev_link);
      $('#cetakrca_btn').attr('href', cetakrca_link);
      $('#cetakres_btn').attr('href', cetakres_link);
      $('#cetakreal_btn').attr('href', cetakreal_link);
      $('#cetakmon_btn').attr('href', cetakmon_link);
      
});
</script>

@endpush

@push('js1')
<script type="text/javascript">
  $(document).ready(function() {
    $('#pemilik_risiko').select2().on('change', function() {
      var areaID = $('#pemilik_risiko').val();
      console.log(areaID);
      var cetakid_link = "cetakid/" + areaID;
      var cetakan_link = "cetakan/" + areaID;
      var cetakev_link = "cetakev/" + areaID;
      var cetakrca_link = "cetakrca/" + areaID;
      var cetakres_link = "cetakres/" + areaID;
      var cetakreal_link = "cetakreal/" + areaID;
      var cetakmon_link = "cetakmon/" + areaID;
      $('#cetakid_btn').attr('href', cetakid_link);
      $('#cetakan_btn').attr('href', cetakan_link);
      $('#cetakev_btn').attr('href', cetakev_link);
      $('#cetakrca_btn').attr('href', cetakrca_link);
      $('#cetakres_btn').attr('href', cetakres_link);
      $('#cetakreal_btn').attr('href', cetakreal_link);
      $('#cetakmon_btn').attr('href', cetakmon_link);

    });
  });

</script>
@endpush