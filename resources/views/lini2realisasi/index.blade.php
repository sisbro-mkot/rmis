@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('peta')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Respons Risiko</li>
  </ol>
  <h6 class="slim-pagetitle">Realisasi Kegiatan Pengendalian Unit Kerja </h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
<!--   <div class="card-header">
    <a href="{{url('createrealisasi')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah</a>
    <a href="{{url('lini2realisasi')}}" class="btn btn-primary"><i class="icon ion-document"></i> Data</a>
  </div> -->
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}
    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="3%">No.</th>
          <th style="text-align: center;">Nama Kegiatan Pengendalian</th>
          <th style="text-align: center;">Penanggung Jawab Kegiatan</th>
          <th style="text-align: center;">Periode Rencana</th>
          <th style="text-align: center;">Waktu Realisasi</th>
          <th style="text-align: center;">Nama Hambatan</th>
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($realisasi as $item)
        <tr class="item{{$item->id}}">
          <td>{{$no++}}</td>
          <td>{{$item->kegiatan_pengendalian}}</td>
          <td>{{$item->s_nmjabdetail}}</td>
          <td style="text-align: center;">{{$item->nama_periode_rencana}}</td>
          <td style="text-align: center;">{{Carbon\Carbon::parse($item->realisasi_waktu)->format('d M Y')}}</td>
          <td>{{$item->nama_hambatan}}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
  </div>
</div>
@endsection

@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({

      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
