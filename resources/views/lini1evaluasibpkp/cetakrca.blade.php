<!DOCTYPE html>
<html><head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Evaluasi Risiko</title>
    <style type="text/css">
    table {
    border-collapse: collapse;
    }
    table, th, td {
    border: 1px solid;
    }
    th, td {
    padding: 3;
    }
    .kop {width:100%; text-align: left;margin-bottom: 5px; border:none;}
    .kop tr td{border:none;}

    .kopsurat{text-align: center;}
    .kopsurat1{line-height:15px;text-align: center;font-family:sans-serif !important; font-size: 18px; margin-bottom: 6px;font-weight: bold}
    .kopsurat2{text-align: right;font-family:sans-serif; font-size: 16px }
  </style>
</head><body>

<div align="center">
  <table class="kop">
    <tr>
        <td class="kopsurat" style="width:90%;">
          <p class="kopsurat1">ANALISIS AKAR MASALAH</p>
        </td>
    </tr>
    <tr>
        <td>
          <p style="font-family: sans-serif; font-size: 12px; font-weight: bold;">Nama Entitas Pemilik Risiko: {{$unit->s_nama_instansiunitorg}}</p>
          <p style="font-family: sans-serif; font-size: 12px; font-weight: bold;">Tahun: {{$tahun}}</p>
        </td>
    </tr>
  </table>

</div>

   <div>
      <table width="100%" style="font-family: sans-serif; font-size: 12px;">
        <thead>
        <tr align="center">
          <th width="10%">Kode Risiko</th>
          <th>Pernyataan Risiko</th>
          <th style="font-style: italic;">Why 1</th>
          <th style="font-style: italic;">Why 2</th>
          <th style="font-style: italic;">Why 3</th>
          <th style="font-style: italic;">Why 4</th>
          <th style="font-style: italic;">Why 5</th>
          <th>Akar Penyebab</th>
          <th>Kode Penyebab</th>
        </tr>
        <tr style="background-color: #BDBDBD; font-style: italic; font-size: 8px" align="center">
          <th>1</th>
          <th>2</th>
          <th>3</th>
          <th>4</th>
          <th>5</th>
          <th>6</th>
          <th>7</th>
          <th>8</th>
          <th>9</th>
        </tr>
        </thead>
        <?php $no=1; ?>
        @foreach($evaluasi as $item)
        <tr class="item{{$item->id}}">
          <td>{{$item->kode_identifikasi_risiko}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td>{{$item->nama_penyebab_1}}</td>
          <td>{{$item->nama_penyebab_2}}</td>
          <td>{{$item->nama_penyebab_3}}</td>
          <td>{{$item->nama_penyebab_4}}</td>
          <td>{{$item->nama_penyebab_5}}</td>
          <td>{{$item->nama_akar_penyebab}}</td>
          <td>{{$item->kode_penyebab}}</td>
        </tr>
        @endforeach
      </table>
    </div>

</body></html>