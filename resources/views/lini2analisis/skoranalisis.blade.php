@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('heatmap')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Analisis Risiko</li>
  </ol>
  <h6 class="slim-pagetitle">Analisis Risiko Unit Kerja</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
<!--   <div class="card-header">
    <a href="{{url('createanalisis')}}" class="btn btn-primary"><i class="icon ion-plus-round"></i> Tambah</a>
    <a href="{{url('lini2analisis')}}" class="btn btn-primary"><i class="icon ion-document"></i> Data</a>
  </div> -->
  <!-- /.box-header -->
  <div class="pd-20">
    <div class="table-wrapper">
    {{ csrf_field() }}

    <table id="tbl-identifikasi" class="table display responsive">
      <thead align="center">
        <tr>
          <th width="5%">Kode Risiko</th>
          <th style="text-align: center;">Nama Risiko</th>
          <th style="text-align: center;">Nilai Selera Risiko</th>
          <th style="text-align: center;">Nilai Residual Risk</th>
          <th style="text-align: center;">Nilai Treated Risk</th>
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($analisis as $item)
        <tr class="item{{$item->id}}">
          <!-- <td>{{$no++}}</td> -->
          <td>{{$item->kode_identifikasi_risiko}}</td>
          <td>{{$item->nama_bagan_risiko}}</td>
          <td style="text-align: center;">{{$item->skor_selera}}</td>
          <td style="text-align: center;">{{$item->skor_risiko_residual}}</td>
          <td style="text-align: center;">{{$item->skor_risiko_treated}}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
</div>
@endsection

@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({
      responsive: true,
      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush
