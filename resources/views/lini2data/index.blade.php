@extends('layout.app')

@section('isi')
<div class="slim-pageheader">
  <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('heatmap')}}"><i class="fa fa-home"></i> Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Data Umum</li>
  </ol>
  <h6 class="slim-pagetitle">Data Umum Unit Kerja</h6>
</div><!-- slim-pageheader -->
<div class="card card-table">
  <div class="pd-20">
    <div class="table-responsive-lg">
    <div class="table-wrapper">
    {{ csrf_field() }}

    <table id="tbl-identifikasi" class="table display">
      <thead align="center">
        <tr>
          <th width="3%">No.</th>
          <th style="text-align: center;">Nama Pemilik Risiko</th>
          <th style="text-align: center;">Selera Risiko</th>
        </tr>
      </thead>
      <tbody>
      <?php $no=1; ?>
      @foreach($data as $item)
        <tr class="item{{$item->id}}">
          <td>{{$no++}}</td>
          <td>{{$item->s_nmjabdetail}}</td>
          <td style="text-align: center;">{{$item->skor_selera}}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
  </div>

</div>
@endsection

@push('js')
  <script>
  $(function(){
    'use strict';
    $('#tbl-identifikasi').DataTable({

      language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
      }
    });

    // Select2
    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
  });

  </script>
@endpush