<?php
 
/* 
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
*/

// Route::get('/', function() {
// 	return view('home.main');
// });
//  
// 
// v2.0 awal
// 
// Lini 1
//
Route::get('/lini1identifikasi/showST/{id}', 'lini1\Lini1IdentifikasiCtrl@showST2');
Route::get('/lini1analisis/kemungkinan', 'lini1\Lini1AnalisisCtrl@kemungkinan');
Route::get('/lini1analisis/dampak', 'lini1\Lini1AnalisisCtrl@dampak');
Route::get('/lini1analisis/showECedit/{id}', 'lini1\Lini1AnalisisCtrl@showECedit');
Route::get('/lini1analisis/showEC/{id}', 'lini1\Lini1AnalisisCtrl@showEC');
Route::get('/lini1realisasi/pilihPeriodeRTP/{id}', 'lini1\Lini1RealisasiCtrl@pilihPeriodeRTP');
Route::get('/lini1identifikasi/risikotreated', 'lini1\Lini1IdentifikasiCtrl@risikoTreated');
Route::get('/lini1identifikasi/risikoresidual', 'lini1\Lini1IdentifikasiCtrl@risikoResidual');
Route::get('/lini1identifikasi/risikoinherent', 'lini1\Lini1IdentifikasiCtrl@risikoInherent');
Route::get('/lini1identifikasi/api', 'lini1\Lini1IdentifikasiCtrl@tesAPI');

Route::group(['middleware' => ['web', 'auth', 'roles']],function() {
Route::group(['roles' => '3' or '4' or '5' or '6' or '7' or '9' or '10'],function() { 
Route::namespace('lini1')->group(function () {

Route::get('dashboardunit', ['uses' => 'Lini1DashboardCtrl@index', 'as' => 'dashboardunit']);

// Parameter
// 
// BPKP
Route::get('lini1databpkp', ['uses' => 'Lini1DataBPKPCtrl@index', 'as' => 'lini1databpkp']);
Route::resource('lini1databpkp', 'Lini1DataBPKPCtrl');
Route::get('createusbpkp', ['uses' => 'Lini1UsulanCtrl@createusbpkp', 'as' => 'createusbpkp']);
// Unit Kerja Mandiri
Route::get('lini1data', ['uses' => 'Lini1DataCtrl@index', 'as' => 'lini1data']);
Route::resource('lini1data', 'Lini1DataCtrl');
Route::get('lini1panduan', ['uses' => 'Lini1DokumenCtrl@panduan', 'as' => 'lini1panduan']);
Route::get('lini1kamus', ['uses' => 'Lini1KamusCtrl@index', 'as' => 'lini1kamus']);
Route::get('cetakkamus', 'Lini1KamusCtrl@cetak');
Route::get('cetak01', 'Lini1KamusCtrl@cetak01');
Route::get('cetak02', 'Lini1KamusCtrl@cetak02');
Route::get('cetak03', 'Lini1KamusCtrl@cetak03');
Route::get('cetak04', 'Lini1KamusCtrl@cetak04');
Route::get('cetak05', 'Lini1KamusCtrl@cetak05');
Route::get('cetak06', 'Lini1KamusCtrl@cetak06');
Route::get('createusunit', ['uses' => 'Lini1UsulanCtrl@createusunit', 'as' => 'createusunit']);
Route::get('lini1usul', ['uses' => 'Lini1UsulanCtrl@index', 'as' => 'lini1usul']);
Route::resource('lini1usulan', 'Lini1UsulanCtrl');

// Penetapan Konteks
// 
// BPKP
Route::get('createtkbpkp', ['uses' => 'Lini1TetapKonteksBPKPCtrl@create', 'as' => 'createtkbpkp']);
Route::get('lini1tetapkonteksbpkp', ['uses' => 'Lini1TetapKonteksBPKPCtrl@index', 'as' => 'lini1tetapkonteksbpkp']);
Route::resource('lini1tetapkonteksbpkp', 'Lini1TetapKonteksBPKPCtrl');
// Unit Kerja Mandiri
Route::get('exceltkunit', 'Lini1TetapKonteksCtrl@excel');
Route::get('createtkunit', ['uses' => 'Lini1TetapKonteksCtrl@create', 'as' => 'createtkunit']);
Route::get('lini1tetapkonteks', ['uses' => 'Lini1TetapKonteksCtrl@index', 'as' => 'lini1tetapkonteks']);
Route::resource('lini1tetapkonteks', 'Lini1TetapKonteksCtrl');
Route::get('createstakeunit', ['uses' => 'Lini1StakeholderCtrl@create', 'as' => 'createstakeunit']);
Route::get('lini1stakeholder', ['uses' => 'Lini1StakeholderCtrl@index', 'as' => 'lini1stakeholder']);
Route::resource('lini1stakeholder', 'Lini1StakeholderCtrl');


// Identifikasi Risiko
// 
// BPKP
Route::get('heatmapbpkp', ['uses' => 'Lini1IdentifikasiBPKPCtrl@getHeatmapBPKP', 'as' => 'heatmapbpkp']);
Route::get('heatmapbpkpi', ['uses' => 'Lini1IdentifikasiBPKPCtrl@getHeatmapBPKPi', 'as' => 'heatmapbpkpi']);
Route::get('heatmapbpkpr', ['uses' => 'Lini1IdentifikasiBPKPCtrl@getHeatmapBPKPr', 'as' => 'heatmapbpkpr']);
Route::get('dashboardregisterbpkp', 'Lini1IdentifikasiBPKPCtrl@getDashRegisterBPKP');
Route::get('dashboardregisterbpkp/{id}', 'Lini1IdentifikasiBPKPCtrl@getFilterRegisterBPKP');
Route::get('excelan', 'Lini1IdentifikasiBPKPCtrl@excelan');
Route::get('excelrtp', 'Lini1IdentifikasiBPKPCtrl@excelrtp');
Route::get('excelmon', 'Lini1IdentifikasiBPKPCtrl@excelmon');
Route::get('excelidbpkp/{id}', 'Lini1IdentifikasiBPKPCtrl@excel');
Route::get('excelidall', 'Lini1IdentifikasiBPKPCtrl@excelall');
Route::get('excelecbpkp/{id}', 'Lini1IdentifikasiBPKPCtrl@excelec');
Route::get('excelecall', 'Lini1IdentifikasiBPKPCtrl@excelecall');
Route::get('cetakidbpkp', 'Lini1IdentifikasiBPKPCtrl@cetak');
Route::get('createidbpkp', ['uses' => 'Lini1IdentifikasiBPKPCtrl@create', 'as' => 'createidbpkp']);
Route::get('lini1identifikasibpkp', ['uses' => 'Lini1IdentifikasiBPKPCtrl@index', 'as' => 'lini1identifikasibpkp']);
Route::resource('lini1identifikasibpkp', 'Lini1IdentifikasiBPKPCtrl');
// Unit Kerja Mandiri
Route::get('heatmapunit', ['uses' => 'Lini1IdentifikasiCtrl@getHeatmapUnit', 'as' => 'heatmapunit']);
Route::get('heatmapuniti', ['uses' => 'Lini1IdentifikasiCtrl@getHeatmapUniti', 'as' => 'heatmapuniti']);
Route::get('heatmapunitr', ['uses' => 'Lini1IdentifikasiCtrl@getHeatmapUnitr', 'as' => 'heatmapunitr']);
Route::get('dashboardregisterunit', 'Lini1IdentifikasiCtrl@getDashRegisterUnit');
Route::get('dashboardregisterunit/{id}', 'Lini1IdentifikasiCtrl@getFilterRegisterUnit');
Route::get('excelidunit', 'Lini1IdentifikasiCtrl@excel');
Route::get('excelecunit', 'Lini1IdentifikasiCtrl@excelec');
Route::get('cetakidunit', 'Lini1IdentifikasiCtrl@cetak');
Route::get('createidunit', ['uses' => 'Lini1IdentifikasiCtrl@create', 'as' => 'createidunit']);
Route::get('lini1identifikasi', ['uses' => 'Lini1IdentifikasiCtrl@index', 'as' => 'lini1identifikasi']);
Route::resource('lini1identifikasi', 'Lini1IdentifikasiCtrl');

// Analisis Risiko
// 
// BPKP
Route::get('skoranbpkp', 'Lini1AnalisisBPKPCtrl@getSkorAnBPKP');
Route::get('cetakanbpkp', 'Lini1AnalisisBPKPCtrl@cetak');
Route::get('createanbpkp', ['uses' => 'Lini1AnalisisBPKPCtrl@create', 'as' => 'createanbpkp']);
Route::get('lini1analisisbpkp', ['uses' => 'Lini1AnalisisBPKPCtrl@index', 'as' => 'lini1analisisbpkp']);
Route::resource('lini1analisisbpkp', 'Lini1AnalisisBPKPCtrl');
Route::get('lini1pengendalianbpkp/{id}/tambah', ['as' => 'lini1pengendalianbpkp.tambah', 'uses' => 'Lini1PengendalianBPKPCtrl@tambah']);
Route::get('lini1pengendalianbpkp', ['uses' => 'Lini1PengendalianBPKPCtrl@index', 'as' => 'lini1pengendalianbpkp']);
Route::resource('lini1pengendalianbpkp', 'Lini1PengendalianBPKPCtrl');
Route::get('lini1residualbpkp', ['uses' => 'Lini1ResidualBPKPCtrl@index', 'as' => 'lini1residualbpkp']);
Route::resource('lini1residualbpkp', 'Lini1ResidualBPKPCtrl');
// Unit Kerja Mandiri
Route::get('skoranunit', 'Lini1AnalisisCtrl@getSkorAnUnit');
Route::get('cetakanunit', 'Lini1AnalisisCtrl@cetak');
Route::get('cetaktrenunit', 'Lini1AnalisisCtrl@cetaktren');
Route::get('createanunit', ['uses' => 'Lini1AnalisisCtrl@create', 'as' => 'createanunit']);
Route::get('lini1analisis', ['uses' => 'Lini1AnalisisCtrl@index', 'as' => 'lini1analisis']);
Route::resource('lini1analisis', 'Lini1AnalisisCtrl');
Route::get('lini1pengendalian/{id}/tambah', ['as' => 'lini1pengendalian.tambah', 'uses' => 'Lini1PengendalianCtrl@tambah']);
Route::get('lini1pengendalian', ['uses' => 'Lini1PengendalianCtrl@index', 'as' => 'lini1pengendalian']);
Route::resource('lini1pengendalian', 'Lini1PengendalianCtrl');
Route::get('lini1residual', ['uses' => 'Lini1ResidualCtrl@index', 'as' => 'lini1residual']);
Route::resource('lini1residual', 'Lini1ResidualCtrl');

// Evaluasi Risiko
// 
// BPKP
Route::get('penyebabbpkp', 'Lini1EvaluasiBPKPCtrl@getPenyebabBPKP');
Route::get('penyebablistbpkp', 'Lini1EvaluasiBPKPCtrl@getPenyebabListBPKP');
Route::get('penyebablistbpkp/{id}', 'Lini1EvaluasiBPKPCtrl@getFilterPenyebabListBPKP');
Route::get('cetakevbpkp', 'Lini1EvaluasiBPKPCtrl@cetakRisiko');
Route::get('cetakrcabpkp', 'Lini1EvaluasiBPKPCtrl@cetakRCA');
Route::get('createevbpkp', ['uses' => 'Lini1EvaluasiBPKPCtrl@create', 'as' => 'createevbpkp']);
Route::get('lini1evaluasibpkp', ['uses' => 'Lini1EvaluasiBPKPCtrl@index', 'as' => 'lini1evaluasibpkp']);
Route::resource('lini1evaluasibpkp', 'Lini1EvaluasiBPKPCtrl');
// Unit Kerja Mandiri
Route::get('penyebabunit', 'Lini1EvaluasiCtrl@getPenyebabUnit');
Route::get('penyebablistunit', 'Lini1EvaluasiCtrl@getPenyebabListUnit');
Route::get('penyebablistunit/{id}', 'Lini1EvaluasiCtrl@getFilterPenyebabListUnit');
Route::get('cetakevunit', 'Lini1EvaluasiCtrl@cetakRisiko');
Route::get('cetakrcaunit', 'Lini1EvaluasiCtrl@cetakRCA');
Route::get('createevunit', ['uses' => 'Lini1EvaluasiCtrl@create', 'as' => 'createevunit']);
Route::get('lini1evaluasi', ['uses' => 'Lini1EvaluasiCtrl@index', 'as' => 'lini1evaluasi']);
Route::resource('lini1evaluasi', 'Lini1EvaluasiCtrl');

// Respons Risiko (Rencana)
// 
// BPKP
Route::get('rtpbpkp', 'Lini1ResponsBPKPCtrl@getRTPBPKP');
Route::get('rtplistbpkp', 'Lini1ResponsBPKPCtrl@getRTPListBPKP');
Route::get('rtplistcurbpkp', 'Lini1ResponsBPKPCtrl@getRTPListCurBPKP');
Route::get('cetakresbpkp', 'Lini1ResponsBPKPCtrl@cetak');
Route::get('createrespbpkp', ['uses' => 'Lini1ResponsBPKPCtrl@create', 'as' => 'createrespbpkp']);
Route::get('lini1responsbpkp', ['uses' => 'Lini1ResponsBPKPCtrl@index', 'as' => 'lini1responsbpkp']);
Route::resource('lini1responsbpkp', 'Lini1ResponsBPKPCtrl');
Route::get('lini1treatedbpkp', ['uses' => 'Lini1TreatedBPKPCtrl@index', 'as' => 'lini1treatedbpkp']);
Route::resource('lini1treatedbpkp', 'Lini1TreatedBPKPCtrl');
// Unit Kerja Mandiri
Route::get('rtpunit', 'Lini1ResponsCtrl@getRTPUnit');
Route::get('rtplistunit', 'Lini1ResponsCtrl@getRTPListUnit');
Route::get('rtplistcurunit', 'Lini1ResponsCtrl@getRTPListCurUnit');
Route::get('cetakresunit', 'Lini1ResponsCtrl@cetak');
Route::get('createrespunit', ['uses' => 'Lini1ResponsCtrl@create', 'as' => 'createrespunit']);
Route::get('lini1respons', ['uses' => 'Lini1ResponsCtrl@index', 'as' => 'lini1respons']);
Route::resource('lini1respons', 'Lini1ResponsCtrl');
Route::get('lini1treated', ['uses' => 'Lini1TreatedCtrl@index', 'as' => 'lini1treated']);
Route::resource('lini1treated', 'Lini1TreatedCtrl');

// Respons Risiko (Realisasi)
// 
// BPKP
Route::get('monitorbpkp', 'Lini1ResponsBPKPCtrl@getMonitorBPKP');
Route::get('reslambatbpkp', 'Lini1ResponsBPKPCtrl@getResLambatBPKP');
Route::get('cetakrealbpkp', 'Lini1RealisasiBPKPCtrl@cetak');
Route::get('createrealbpkp', ['uses' => 'Lini1RealisasiBPKPCtrl@create', 'as' => 'createrealbpkp']);
Route::get('lini1realisasibpkp', ['uses' => 'Lini1RealisasiBPKPCtrl@index', 'as' => 'lini1realisasibpkp']);
Route::resource('lini1realisasibpkp', 'Lini1RealisasiBPKPCtrl');
// Unit Kerja Mandiri
Route::get('monitorunit', 'Lini1ResponsCtrl@getMonitorUnit');
Route::get('reslambatunit', 'Lini1ResponsCtrl@getResLambatUnit');
Route::get('cetakrealunit', 'Lini1RealisasiCtrl@cetak');
Route::get('createrealunit', ['uses' => 'Lini1RealisasiCtrl@create', 'as' => 'createrealunit']);
Route::get('lini1realisasi', ['uses' => 'Lini1RealisasiCtrl@index', 'as' => 'lini1realisasi']);
Route::resource('lini1realisasi', 'Lini1RealisasiCtrl');

// Monitoring Risiko
// 
// BPKP
Route::get('insidenbpkp', 'Lini1MonitoringBPKPCtrl@getInsidenBPKP');
Route::get('inskejadianbpkp', 'Lini1MonitoringBPKPCtrl@getInsKejadianBPKP');
Route::get('insrisikobpkp', 'Lini1MonitoringBPKPCtrl@getInsRisikoBPKP');
Route::get('insrisikobpkp/{id}', 'Lini1MonitoringBPKPCtrl@getFilterInsRisikoBPKP');
Route::get('inssebabbpkp', 'Lini1MonitoringBPKPCtrl@getInsSebabBPKP');
Route::get('inssebabbpkp/{id}', 'Lini1MonitoringBPKPCtrl@getFilterInsSebabBPKP');
Route::get('cetakmonbpkp', 'Lini1MonitoringBPKPCtrl@cetak');
Route::get('createmonitorbpkp', ['uses' => 'Lini1MonitoringBPKPCtrl@create', 'as' => 'createmonitorbpkp']);
Route::get('lini1monitoringbpkp', ['uses' => 'Lini1MonitoringBPKPCtrl@index', 'as' => 'lini1monitoringbpkp']);
Route::resource('lini1monitoringbpkp', 'Lini1MonitoringBPKPCtrl');
// Unit Kerja Mandiri
Route::get('insidenunit', 'Lini1MonitoringCtrl@getInsidenUnit');
Route::get('inskejadianunit', 'Lini1MonitoringCtrl@getInsKejadianUnit');
Route::get('insrisikounit', 'Lini1MonitoringCtrl@getInsRisikoUnit');
Route::get('insrisikounit/{id}', 'Lini1MonitoringCtrl@getFilterInsRisikoUnit');
Route::get('inssebabunit', 'Lini1MonitoringCtrl@getInsSebabUnit');
Route::get('inssebabunit/{id}', 'Lini1MonitoringCtrl@getFilterInsSebabUnit');
Route::get('cetakmonunit', 'Lini1MonitoringCtrl@cetak');
Route::get('createmonitorunit', ['uses' => 'Lini1MonitoringCtrl@create', 'as' => 'createmonitorunit']);
Route::get('lini1monitoring', ['uses' => 'Lini1MonitoringCtrl@index', 'as' => 'lini1monitoring']);
Route::resource('lini1monitoring', 'Lini1MonitoringCtrl');

// Pelaporan Risiko
// 
// BPKP
Route::get('bcetakid/{id}', 'Lini1LaporanBPKPCtrl@cetakid');
Route::get('bcetakan/{id}', 'Lini1LaporanBPKPCtrl@cetakan');
Route::get('bcetakev/{id}', 'Lini1LaporanBPKPCtrl@cetakev');
Route::get('bcetakrca/{id}', 'Lini1LaporanBPKPCtrl@cetakrca');
Route::get('bcetakres/{id}', 'Lini1LaporanBPKPCtrl@cetakres');
Route::get('bcetakreal/{id}', 'Lini1LaporanBPKPCtrl@cetakreal');
Route::get('bcetakmon/{id}', 'Lini1LaporanBPKPCtrl@cetakmon');
Route::get('lini1laporanbpkp', ['uses' => 'Lini1LaporanBPKPCtrl@index', 'as' => 'lini1laporanbpkp']);
Route::resource('lini1laporanbpkp', 'Lini1LaporanBPKPCtrl');
// Unit Kerja Mandiri
Route::get('lini1laporan', ['uses' => 'Lini1LaporanCtrl@index', 'as' => 'lini1laporan']);
Route::resource('lini1laporan', 'Lini1LaporanCtrl');

// Proses Akhir Tahun (Risiko Aktual)
// 

// Unit Kerja Mandiri
Route::get('cetakaktunit', 'Lini1AktualCtrl@cetak');
Route::get('lini1aktual/{id}/tambah', ['as' => 'aktualtambah', 'uses' => 'Lini1AktualCtrl@tambah']);
Route::get('lini1aktual/{id}/ubah', ['as' => 'aktualubah', 'uses' => 'Lini1AktualCtrl@ubah']);
Route::get('lini1aktual', ['uses' => 'Lini1AktualCtrl@index', 'as' => 'lini1aktual']);
Route::resource('lini1aktual', 'Lini1AktualCtrl');

}); //namespace
}); //roles

}); //middleware
//
// Lini 2
//
Route::get('/lini2tetapkonteks/pilihKonteks/{id}', 'lini2\Lini2TetapKonteksCtrl@pilihKonteks');
Route::get('/lini2identifikasi/pilihKonteksUnit/{id}', 'lini2\Lini2IdentifikasiCtrl@pilihKonteksUnit');
Route::get('/lini2identifikasi/showRank/{id}', 'lini2\Lini2IdentifikasiCtrl@showRank');
Route::get('/lini2analisis/pilihRisikoUnit/{id}', 'lini2\Lini2AnalisisCtrl@pilihRisikoUnit');
Route::get('/lini2analisis/pilihSkor/{id}/{id2}', 'lini2\Lini2AnalisisCtrl@pilihSkor');
Route::get('/lini2evaluasi/pilihRisikoUnit2/{id}', 'lini2\Lini2EvaluasiCtrl@pilihRisikoUnit2');
Route::get('/lini2respons/pilihSebab/{id}', 'lini2\Lini2ResponsCtrl@pilihSebab');
Route::get('/lini2realisasi/pilihRTP/{id}', 'lini2\Lini2RealisasiCtrl@pilihRTP');
Route::get('/lini2realisasi/pilihPeriodeRTP/{id}', 'lini2\Lini2RealisasiCtrl@pilihPeriodeRTP');
Route::get('/lini2monitoring/pilihSebab2/{id}', 'lini2\Lini2MonitoringCtrl@pilihSebab2');

Route::group(['middleware' => ['web', 'auth', 'roles']],function() {
Route::group(['roles' => '1' or '2'],function() { 
Route::namespace('lini2')->group(function () {
// Parameter
Route::get('lini2data', ['uses' => 'Lini2DataCtrl@index', 'as' => 'lini2data']);
Route::resource('lini2data', 'Lini2DataCtrl');
Route::get('createparamkonteks', ['uses' => 'Lini2ParamKonteksCtrl@create', 'as' => 'createparamkonteks']);
Route::get('lini2paramkonteks', ['uses' => 'Lini2ParamKonteksCtrl@index', 'as' => 'lini2paramkonteks']);
Route::resource('lini2paramkonteks', 'Lini2ParamKonteksCtrl');
Route::get('excelkamus', 'Lini2ParamRisikoCtrl@excel');
Route::get('createparamrisiko', ['uses' => 'Lini2ParamRisikoCtrl@create', 'as' => 'createparamrisiko']);
Route::get('lini2paramrisiko', ['uses' => 'Lini2ParamRisikoCtrl@index', 'as' => 'lini2paramrisiko']);
Route::resource('lini2paramrisiko', 'Lini2ParamRisikoCtrl');
Route::get('createparamrole', ['uses' => 'Lini2ParamRoleCtrl@create', 'as' => 'createparamrole']);
Route::get('lini2paramrole', ['uses' => 'Lini2ParamRoleCtrl@index', 'as' => 'lini2paramrole']);
Route::resource('lini2paramrole', 'Lini2ParamRoleCtrl');
Route::get('createparamuser', ['uses' => 'Lini2ParamUserCtrl@create', 'as' => 'createparamuser']);
Route::get('ubahparamuser', ['uses' => 'Lini2ParamUserCtrl@ubah', 'as' => 'ubahparamuser']);
Route::get('ubahunituser/{id}/edit', ['uses' => 'Lini2ParamUserCtrl@ubahunit', 'as' => 'ubahunituser']);
Route::post('simpanparamuser', ['uses' => 'Lini2ParamUserCtrl@simpan', 'as' => 'simpanparamuser']);
Route::put('simpanunituser/update/{id}', ['as' => 'simpanunituser', 'uses' => 'Lini2ParamUserCtrl@simpanunit']);
Route::get('lini2paramuser', ['uses' => 'Lini2ParamUserCtrl@index', 'as' => 'lini2paramuser']);
Route::resource('lini2paramuser', 'Lini2ParamUserCtrl');
Route::get('lini2usul', ['uses' => 'Lini2UsulanCtrl@index', 'as' => 'lini2usul']);
Route::resource('lini2usulan', 'Lini2UsulanCtrl');
// Penetapan Konteks
Route::get('lini2tetapkonteks/{id}/delete', ['as' => 'lini2tetapkonteks.delete', 'uses' => 'Lini2TetapKonteksCtrl@delete']);
Route::get('lini2tetapkonteks/{id}/batal', ['as' => 'lini2tetapkonteks.batal', 'uses' => 'Lini2TetapKonteksCtrl@batal']);
Route::get('createtetapkonteks', ['uses' => 'Lini2TetapKonteksCtrl@create', 'as' => 'createtetapkonteks']);
Route::get('lini2tetapkonteks', ['uses' => 'Lini2TetapKonteksCtrl@index', 'as' => 'lini2tetapkonteks']);
Route::resource('lini2tetapkonteks', 'Lini2TetapKonteksCtrl');
// Identifikasi Risiko
Route::get('peta', ['uses' => 'Lini2IdentifikasiCtrl@getDashPeta', 'as' => 'peta']);
Route::get('heatmap', ['uses' => 'Lini2IdentifikasiCtrl@getHeatmap', 'as' => 'heatmap']);
Route::get('registerunit/{id}', 'Lini2IdentifikasiCtrl@getRegisterUnit');
Route::get('dashboardregister', 'Lini2IdentifikasiCtrl@getDashRegister');
Route::get('dashboardregister/{id}', 'Lini2IdentifikasiCtrl@getFilterRegister');
Route::get('createidentifikasi', ['uses' => 'Lini2IdentifikasiCtrl@create', 'as' => 'createidentifikasi']);
Route::get('lini2identifikasi', ['uses' => 'Lini2IdentifikasiCtrl@index', 'as' => 'lini2identifikasi']);
Route::resource('lini2identifikasi', 'Lini2IdentifikasiCtrl');
// Analisis Risiko
Route::get('skoranalisis', 'Lini2AnalisisCtrl@getSkorAnalisis');
Route::get('createanalisis', ['uses' => 'Lini2AnalisisCtrl@create', 'as' => 'createanalisis']);
Route::get('lini2analisis', ['uses' => 'Lini2AnalisisCtrl@index', 'as' => 'lini2analisis']);
Route::resource('lini2analisis', 'Lini2AnalisisCtrl');
// Evaluasi Risiko
Route::get('penyebab', 'Lini2EvaluasiCtrl@getPenyebab');
Route::get('penyebablist', 'Lini2EvaluasiCtrl@getPenyebabList');
Route::get('penyebablist/{id}', 'Lini2EvaluasiCtrl@getFilterPenyebabList');
Route::get('createevaluasi', ['uses' => 'Lini2EvaluasiCtrl@create', 'as' => 'createevaluasi']);
Route::get('lini2evaluasi', ['uses' => 'Lini2EvaluasiCtrl@index', 'as' => 'lini2evaluasi']);
Route::resource('lini2evaluasi', 'Lini2EvaluasiCtrl');
// Respons Risiko (Rencana)
Route::get('rtp', 'Lini2ResponsCtrl@getRTP');
Route::get('rtplist', 'Lini2ResponsCtrl@getRTPList');
Route::get('rtplistcurrent', 'Lini2ResponsCtrl@getRTPListCurrent');
Route::get('createrespons', ['uses' => 'Lini2ResponsCtrl@create', 'as' => 'createrespons']);
Route::get('lini2respons', ['uses' => 'Lini2ResponsCtrl@index', 'as' => 'lini2respons']);
Route::resource('lini2respons', 'Lini2ResponsCtrl');
Route::get('lini2treated', ['uses' => 'Lini2TreatedCtrl@index', 'as' => 'lini2treated']);
Route::resource('lini2treated', 'Lini2TreatedCtrl');
// Respons Risiko (Realisasi)
Route::get('monitor', 'Lini2ResponsCtrl@getMonitor');
Route::get('responslambat', 'Lini2ResponsCtrl@getResponsLambat');
Route::get('createrealisasi', ['uses' => 'Lini2RealisasiCtrl@create', 'as' => 'createrealisasi']);
Route::get('lini2realisasi', ['uses' => 'Lini2RealisasiCtrl@index', 'as' => 'lini2realisasi']);
Route::resource('lini2realisasi', 'Lini2RealisasiCtrl');
// Monitoring Risiko
Route::get('insiden', 'Lini2MonitoringCtrl@getInsiden');
Route::get('insidenkejadian', 'Lini2MonitoringCtrl@getInsidenKejadian');
Route::get('insidenrisiko', 'Lini2MonitoringCtrl@getInsidenRisiko');
Route::get('insidenrisiko/{id}', 'Lini2MonitoringCtrl@getFilterInsidenRisiko');
Route::get('insidensebab', 'Lini2MonitoringCtrl@getInsidenSebab');
Route::get('insidensebab/{id}', 'Lini2MonitoringCtrl@getFilterInsidenSebab');
Route::get('createmonitoring', ['uses' => 'Lini2MonitoringCtrl@create', 'as' => 'createmonitoring']);
Route::get('lini2monitoring', ['uses' => 'Lini2MonitoringCtrl@index', 'as' => 'lini2monitoring']);
Route::resource('lini2monitoring', 'Lini2MonitoringCtrl');
// Pelaporan Risiko
Route::get('cetakid/{id}', 'Lini2LaporanCtrl@cetakid');
Route::get('cetakan/{id}', 'Lini2LaporanCtrl@cetakan');
Route::get('cetakev/{id}', 'Lini2LaporanCtrl@cetakev');
Route::get('cetakrca/{id}', 'Lini2LaporanCtrl@cetakrca');
Route::get('cetakres/{id}', 'Lini2LaporanCtrl@cetakres');
Route::get('cetakreal/{id}', 'Lini2LaporanCtrl@cetakreal');
Route::get('cetakmon/{id}', 'Lini2LaporanCtrl@cetakmon');
Route::get('lini2laporan', ['uses' => 'Lini2LaporanCtrl@index', 'as' => 'lini2laporan']);
Route::resource('lini2laporan', 'Lini2LaporanCtrl');
}); //namespace
}); //roles
}); //middleware
//
//
// Lini 3
//
Route::get('/lini3perencanaan/level/{id}', 'lini3\Lini3PerencanaanCtrl@level');
Route::get('/lini3perencanaan/komposit/{id}', 'lini3\Lini3PerencanaanCtrl@komposit');
Route::get('/lini3wasrisiko/pilihArea/{id}', 'lini3\Lini3WasRisikoCtrl@pilihArea');
Route::get('/lini3wasrisiko/semuaArea/', 'lini3\Lini3WasRisikoCtrl@semuaArea');
Route::get('/lini3wasrisiko/simpRisiko/{id}', 'lini3\Lini3WasRisikoCtrl@simpRisiko');

Route::group(['middleware' => ['web', 'auth', 'roles']],function() {
Route::group(['roles' => '8'],function() { 
Route::namespace('lini3')->group(function () {
// Parameter

// Perencanaan
Route::get('petapibr', ['uses' => 'Lini3PerencanaanCtrl@getPetaPIBR', 'as' => 'petapibr']);
Route::get('lini3perencanaan', ['uses' => 'Lini3PerencanaanCtrl@index', 'as' => 'lini3perencanaan']);
Route::resource('lini3perencanaan', 'Lini3PerencanaanCtrl');

// Pelaksanaan
// 
// Risiko
Route::get('lini3wasrisiko/{id}/tambah', ['as' => 'lini3wasrisiko.tambah', 'uses' => 'Lini3WasRisikoCtrl@tambah']);
Route::get('lini3wasrisiko', ['uses' => 'Lini3WasRisikoCtrl@index', 'as' => 'lini3wasrisiko']);
Route::resource('lini3wasrisiko', 'Lini3WasRisikoCtrl');


// Pelaporan
}); //namespace
}); //roles
}); //middleware
//

// v2.0 akhir


//
// Monev Litbang
// 
Route::get('/litbang/showSTP/{id}/{mulai}/{selesai}', 'litbang\STPCtrl@showSTP');
Route::get('/litbang/pilihSTP/{es3}/{mulai}/{selesai}', 'litbang\STPCtrl@pilihSTP');
Route::get('/litbang/semuaSTP/{mulai}/{selesai}', 'litbang\STPCtrl@semuaSTP');
Route::get('/litbang/showST/{id}', 'litbang\AktivitasCtrl@showST');
Route::get('/litbang/angkin/{id}', 'litbang\SPJCtrl@angkin');
Route::get('/litbang/pilihBulan/{id}', 'litbang\PelaporanCtrl@pilihBulan');
Route::get('/litbang/semuaBulan/', 'litbang\PelaporanCtrl@semuaBulan');
Route::get('/litbang/bismadan', 'litbang\DashboardLitbangCtrl@bismadan');
Route::get('/danhome', 'litbang\DashboardLitbangCtrl@danhome');
Route::get('/artis', function() {
    $exitCode = Artisan::call('config:cache');
    return  redirect('/login');
});

Route::group(['middleware' => ['web2', 'auth']],function() {
Route::namespace('litbang')->group(function () {

Route::get('dashlitbang', ['uses' => 'DashboardLitbangCtrl@getDashLitbang', 'as' => 'dashlitbang']);

//Kepegawaian
Route::get('stpeg', ['uses' => 'STPCtrl@index', 'as' => 'stpeg']);
Route::get('stptambah/{id}/tambah', ['as' => 'stptambah', 'uses' => 'STPCtrl@tambah']);
Route::resource('stp', 'STPCtrl');

//Kinerja
Route::get('aktivitas', ['uses' => 'AktivitasCtrl@index', 'as' => 'aktivitas']);
Route::get('stugas', ['uses' => 'SuratTugasCtrl@index', 'as' => 'stugas']);
Route::get('stugascreate', ['uses' => 'SuratTugasCtrl@create', 'as' => 'stugascreate']);
Route::resource('stugas', 'SuratTugasCtrl');
Route::get('ndinas', ['uses' => 'NotaDinasCtrl@index', 'as' => 'ndinas']);
Route::get('ndinascreate', ['uses' => 'NotaDinasCtrl@create', 'as' => 'ndinascreate']);
Route::resource('ndinas', 'NotaDinasCtrl');
Route::get('skep', ['uses' => 'SuratKeputusanCtrl@index', 'as' => 'skep']);
Route::get('skepcreate', ['uses' => 'SuratKeputusanCtrl@create', 'as' => 'skepcreate']);
Route::resource('skep', 'SuratKeputusanCtrl');
Route::get('lit', ['uses' => 'PenelitianCtrl@index', 'as' => 'lit']);
Route::get('litcreate', ['uses' => 'PenelitianCtrl@create', 'as' => 'litcreate']);
Route::resource('lit', 'PenelitianCtrl');

//Keuangan
Route::get('anggaran', ['uses' => 'AnggaranCtrl@index', 'as' => 'anggaran']);
Route::get('mapping/{id}/tambah', ['as' => 'mappingtambah', 'uses' => 'AnggaranCtrl@tambah']);
Route::resource('anggaranc', 'AnggaranCtrl');
Route::get('spj', ['uses' => 'SPJCtrl@index', 'as' => 'spj']);
Route::get('spj/{id}/tambah', ['uses' => 'SPJCtrl@tambah', 'as' => 'spjcreate']);
Route::resource('spj', 'SPJCtrl');

//Pelaporan
Route::get('salaktiv', ['uses' => 'PelaporanCtrl@getSaldoAktivitas', 'as' => 'salaktiv']);
Route::get('salmak', ['uses' => 'PelaporanCtrl@getSaldoMAK', 'as' => 'salmak']);
Route::get('realst', ['uses' => 'PelaporanCtrl@getRealisasiST', 'as' => 'realst']);
Route::get('indikator', ['uses' => 'PelaporanCtrl@getIndikator', 'as' => 'indikator']);
Route::get('manfaat', ['uses' => 'PelaporanCtrl@getManfaat', 'as' => 'manfaat']);

}); //namespace
}); //middleware


// MOCK-UP

Route::get('/mockup/bagipagu', 'litbang\MockupCtrl@bagipagu');
Route::get('/mockup/costsheet', 'litbang\MockupCtrl@costsheet');
Route::get('/mockup/pagu', 'litbang\MockupCtrl@pagu');
Route::get('/mockup/surattugas', 'litbang\MockupCtrl@surattugas');
Route::get('/mockup/gaji', 'litbang\MockupCtrl@gaji');
Route::get('/mockup/gaji_detail', 'litbang\MockupCtrl@gajidetail');
Route::get('/mockup/permintaan_pbj', 'litbang\MockupCtrl@permintaanpbj');
Route::get('/mockup/sima_st', 'litbang\MockupCtrl@simast');
Route::get('/mockup/sima_tim', 'litbang\MockupCtrl@simatim');
Route::get('/mockup/itemcs', 'litbang\MockupCtrl@itemcs');

//
Route::get('/bisma/mapping_app/{id}', 'litbang\MappingAppCtrl@excel');


Route::get('/litbang/login', ['uses' => 'adminlitbang\AuthCtrl@getLogin','as' => 'litbanglogin']); 
Route::post('/litbang/login', ['uses' => 'adminlitbang\AuthCtrl@postLogin']);
Route::post('/litbang/logout', ['uses' => 'adminlitbang\AuthCtrl@getLogout','as' => 'litbanglogout']);
Route::get('/litbang/logout', ['uses' => 'adminlitbang\AuthCtrl@getLogout','as' => 'litbanglogout']);


Route::get('/', function() {
	return redirect('/login');
});

Route::get('/login', ['uses' => 'admin\AuthCtrl@getLogin','as' => 'login']); 
Route::post('/login', ['uses' => 'admin\AuthCtrl@postLogin']);
Route::get('/peran', ['uses' => 'admin\AuthCtrl@getPeran','as' => 'peran']); 
Route::post('/peran', ['uses' => 'admin\AuthCtrl@postPeran']);
Route::post('/logout', ['uses' => 'admin\AuthCtrl@getLogout','as' => 'logout']);
Route::get('/logout', ['uses' => 'admin\AuthCtrl@getLogout','as' => 'logout']);

Route::get('/dashrmiscognos', ['uses' => 'admin\DashKepalaCtrl@getDashKepala','as' => 'dashrmiscognos']); 
Route::get('/dashinsp', ['uses' => 'admin\DashKepalaCtrl@getDashInsp','as' => 'dashinsp']); 


