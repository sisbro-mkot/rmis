<?php

namespace App\Http\Controllers\lini3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;



class Lini3PerencanaanCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $perencanaan = DB::table('vw_risiko21')
            ->select('vw_risiko21.id_instansiunitorg as id',
                'vw_risiko21.s_nama_instansiunitorg as s_nama_instansiunitorg', 
                'vw_risiko21.rata_kemungkinan as rata_kemungkinan',
                'vw_risiko21.rata_dampak as rata_dampak',
                'ref_matriks.skor_risiko as skor_risiko'
                )
            ->leftjoin('ref_matriks', function($join){
                    $join->on('vw_risiko21.rata_kemungkinan','=','ref_matriks.skor_kemungkinan');
                    $join->on('vw_risiko21.rata_dampak','=','ref_matriks.skor_dampak');
                })
            ->orderBy('vw_risiko21.id_instansiunitorg')
            ->get();

        return view('lini3perencanaan.index', compact('perencanaan'));

    }

    public function level($id) 
    {  
        $control = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.id_instansiunitorg as nomor',
                    't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode',
                    'ref_bagan_risiko.nama_bagan_risiko as nama',
                    'ref_matriks_inherent.skor_kemungkinan as kemungkinan',
                    'ref_matriks_inherent.skor_dampak as dampak',
                    'ref_matriks_inherent.skor_risiko as risiko')
                ->join('ref_data_umum', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'ref_data_umum.s_kd_instansiunitorg')
                ->join('t_penetapan_konteks', 'ref_data_umum.id_data_umum', '=', 't_penetapan_konteks.id_data_umum')
                ->join('t_identifikasi_risiko', 't_penetapan_konteks.id_penetapan_konteks', '=', 't_identifikasi_risiko.id_penetapan_konteks')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('ref_matriks as ref_matriks_inherent', 't_analisis_risiko.id_matriks_inherent', '=', 'ref_matriks_inherent.id_matriks')
                ->where('wm_instansiunitorg.id_instansiunitorg', $id)
                ->whereRaw('ref_matriks_inherent.skor_risiko > ref_data_umum.skor_selera')
                ->where('t_penetapan_konteks.tahun', Carbon::now()->year)
                ->get();

        return json_encode($control);
    }

    public function komposit($id) 
    {  
        $komposit = DB::table('vw_risiko21')
            ->select('vw_risiko21.id_instansiunitorg as id',
                'vw_risiko21.s_nama_instansiunitorg as nama', 
                'vw_risiko21.rata_kemungkinan as kemungkinan',
                'vw_risiko21.rata_dampak as dampak',
                'ref_matriks.skor_risiko as risiko'
                )
            ->leftjoin('ref_matriks', function($join){
                    $join->on('vw_risiko21.rata_kemungkinan','=','ref_matriks.skor_kemungkinan');
                    $join->on('vw_risiko21.rata_dampak','=','ref_matriks.skor_dampak');
                })
            ->where('vw_risiko21.id_instansiunitorg', $id)
            ->get();

        return json_encode($komposit);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //             
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getPetaPIBR()
    {
        if(Auth::user()->role_id == '8') {

            $perencanaan = DB::table('vw_risiko21')
            ->select('vw_risiko21.id_instansiunitorg as id',
                'vw_risiko21.s_nama_instansiunitorg as s_nama_instansiunitorg', 
                'vw_risiko21.rata_kemungkinan as rata_kemungkinan',
                'vw_risiko21.rata_dampak as rata_dampak',
                'ref_matriks.skor_risiko as skor_risiko'
                )
            ->leftjoin('ref_matriks', function($join){
                    $join->on('vw_risiko21.rata_kemungkinan','=','ref_matriks.skor_kemungkinan');
                    $join->on('vw_risiko21.rata_dampak','=','ref_matriks.skor_dampak');
                })
            ->orderBy('vw_risiko21.id_instansiunitorg', 'ASC')
            ->get();

            return view('lini3perencanaan.petapibr', compact('perencanaan'));

        }

    }

}
