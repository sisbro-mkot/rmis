<?php

namespace App\Http\Controllers\lini3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;
use App\Models\dashboard\AnalisisRisiko;
use App\Models\dashboard\RefSimpulanRisiko;
use App\Models\dashboard\WasRisiko;


class Lini3WasRisikoCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->data['area_pengawasan'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.id_instansiunitorg as id', 'wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->get();


        return view('lini3wasrisiko.index', $this->data);
    }

    public function pilihArea($id)
    {
        $pelaksanaan = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as nomor', 
                            't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode', 
                            'ref_bagan_risiko.nama_bagan_risiko as nama', 
                            't_identifikasi_risiko.nama_dampak as dampak', 
                            'ref_matriks_inherent.skor_risiko as inherent', 
                            'Pengendalian.count_pengendalian as jumlah', 
                            'ref_data_umum.skor_selera as selera', 
                            'ref_matriks_residual.skor_risiko as residual',
                            't_was_risiko.id_was_risiko as simpulan',
                            'ref_simpulan_risiko.nama_simpulan_risiko as jenis')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_instansiunitorg', 'ref_data_umum.s_kd_instansiunitorg', '=', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_inherent', 't_analisis_risiko.id_matriks_inherent', '=', 'ref_matriks_inherent.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin(DB::raw('(SELECT id_identifikasi, COUNT(id_pengendalian) AS count_pengendalian FROM `t_pengendalian` GROUP BY id_identifikasi) AS Pengendalian'), 
                                    function($join)
                                    {
                                       $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'Pengendalian.id_identifikasi');
                                    })
                        ->leftjoin('t_was_risiko', 't_analisis_risiko.id_analisis', '=', 't_was_risiko.id_analisis')
                        ->leftjoin('ref_simpulan_risiko', 't_was_risiko.id_simpulan_risiko', '=', 'ref_simpulan_risiko.id_simpulan_risiko')
                        ->where('wm_instansiunitorg.id_instansiunitorg', $id)
                        ->get();

        return json_encode($pelaksanaan);
    } 

    public function semuaArea()
    {
        $pelaksanaan = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as nomor', 
                            't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode', 
                            'ref_bagan_risiko.nama_bagan_risiko as nama', 
                            't_identifikasi_risiko.nama_dampak as dampak', 
                            'ref_matriks_inherent.skor_risiko as inherent', 
                            'Pengendalian.count_pengendalian as jumlah', 
                            'ref_data_umum.skor_selera as selera', 
                            'ref_matriks_residual.skor_risiko as residual',
                            't_was_risiko.id_was_risiko as simpulan',
                            'ref_simpulan_risiko.nama_simpulan_risiko as jenis')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->leftjoin('ref_matriks as ref_matriks_inherent', 't_analisis_risiko.id_matriks_inherent', '=', 'ref_matriks_inherent.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin(DB::raw('(SELECT id_identifikasi, COUNT(id_pengendalian) AS count_pengendalian FROM `t_pengendalian` GROUP BY id_identifikasi) AS Pengendalian'), 
                                    function($join)
                                    {
                                       $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'Pengendalian.id_identifikasi');
                                    })
                        ->leftjoin('t_was_risiko', 't_analisis_risiko.id_analisis', '=', 't_was_risiko.id_analisis')
                        ->leftjoin('ref_simpulan_risiko', 't_was_risiko.id_simpulan_risiko', '=', 'ref_simpulan_risiko.id_simpulan_risiko')
                        ->get();

        return json_encode($pelaksanaan);
    }

    public function simpRisiko($id) 
    {  
        $simpulan = DB::table('t_analisis_risiko')
                ->select(
                    't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko',
                    'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko',
                    't_identifikasi_risiko.nama_dampak as nama_dampak',
                    'ref_simpulan_risiko.nama_simpulan_risiko as nama_simpulan_risiko',
                    't_was_risiko.kondisi_risiko as kondisi_risiko',
                    't_was_risiko.rekomendasi_risiko as rekomendasi_risiko',
                    't_was_risiko.id_status_temuan as id_status_temuan')
                ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_was_risiko', 't_analisis_risiko.id_analisis', '=', 't_was_risiko.id_analisis')
                ->join('ref_simpulan_risiko', 't_was_risiko.id_simpulan_risiko', '=', 'ref_simpulan_risiko.id_simpulan_risiko')
                ->where('t_was_risiko.id_was_risiko', $id)
                ->get();

        return json_encode($simpulan);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //             
    }

    public function tambah($id)
    {
            $this->data['analisis'] = AnalisisRisiko::find($id);
            $this->data['id_identifikasi'] = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_identifikasi_risiko.id_identifikasi as id_identifikasi')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->where('t_analisis_risiko.id_analisis', $id)
                ->get();
            $this->data['id_simpulan_risiko'] = RefSimpulanRisiko::pluck('nama_simpulan_risiko','id_simpulan_risiko');
            $this->data['risiko'] = DB::table('t_analisis_risiko')
                ->select('t_analisis_risiko.id_analisis as id', 't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_identifikasi_risiko.nama_dampak as nama_dampak')
                ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->where('t_analisis_risiko.id_analisis', $id)
                ->first();

            return view('lini3wasrisiko.createwasrisiko', $this->data); 

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check())
        {

            $rules = array(

                'id_simpulan_risiko' => 'required',
                'kondisi_risiko' => 'required',

            );
            $messages = [
                            'id_simpulan_risiko.required' => 'Silahkan pilih simpulan.',
                            'kondisi_risiko.required' => 'Silahkan isi/ketik uraian simpulan.',

            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $id = new WasRisiko;
                $id->id_analisis = Input::get('id_analisis');
                $id->tahun = Carbon::now()->format('Y');
                $id->id_simpulan_risiko = Input::get('id_simpulan_risiko');
                $id->kondisi_risiko = Input::get('kondisi_risiko');
                $id->rekomendasi_risiko = Input::get('rekomendasi_risiko');
                $id->id_status_temuan = 1;
           
                $id->user_create = Auth::user()->user_nip;

                $id->save();

                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini3wasrisiko.index');
                
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
            
            $this->data['id_identifikasi'] = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_identifikasi_risiko.id_identifikasi as id_identifikasi')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('t_was_risiko', 't_analisis_risiko.id_analisis', '=', 't_was_risiko.id_analisis')
                ->where('t_was_risiko.id_was_risiko', $id)
                ->get();
            $this->data['id_simpulan_risiko'] = RefSimpulanRisiko::pluck('nama_simpulan_risiko','id_simpulan_risiko');
            $this->data['risiko'] = DB::table('t_analisis_risiko')
                ->select('t_analisis_risiko.id_analisis as id', 't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_identifikasi_risiko.nama_dampak as nama_dampak')
                ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_was_risiko', 't_analisis_risiko.id_analisis', '=', 't_was_risiko.id_analisis')
                ->where('t_was_risiko.id_was_risiko', $id)
                ->first();
            $this->data['simpulan'] = WasRisiko::find($id);

            return view('lini3wasrisiko.editwasrisiko', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check())
        {

            $rules = array(

                'id_simpulan_risiko' => 'required',
                'kondisi_risiko' => 'required',

            );
            $messages = [
                            'id_simpulan_risiko.required' => 'Silahkan pilih simpulan.',
                            'kondisi_risiko.required' => 'Silahkan isi/ketik uraian simpulan.',

            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $id = WasRisiko::find($id);
                $id->id_analisis = Input::get('id_analisis');
                $id->tahun = Carbon::now()->format('Y');
                $id->id_simpulan_risiko = Input::get('id_simpulan_risiko');
                $id->kondisi_risiko = Input::get('kondisi_risiko');
                $id->rekomendasi_risiko = Input::get('rekomendasi_risiko');
                $id->id_status_temuan = 1;

                $now = Carbon::now('Asia/Jakarta');
                $id->updated_at = $now;           
                $id->user_update = Auth::user()->user_nip;

                $id->save();

                Alert::success('Data telah diubah.', 'Selamat');
                return redirect()->route('lini3wasrisiko.index');
                
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}