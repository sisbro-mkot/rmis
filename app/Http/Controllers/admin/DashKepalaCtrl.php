<?php

namespace App\Http\Controllers\admin;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Illuminate\Support\Facades\Input;
use Alert;
use App\Models\dashboard\IdentifikasiRisiko;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\RefBaganRisiko;


class DashKepalaCtrl extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getDashInsp()
    {
        return view('dashboard.dashinsp');
    }

    public function getDashKepala()
    {
            $now = Carbon::now()->year;
            $risikoteridentifikasi = DB::table('t_identifikasi_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.tahun', $now)
                        ->get()
                        ->count();
            $risikotermitigasi = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id', 'ref_data_umum.skor_selera as skor_selera', 'ref_matriks_residual.skor_risiko as skor_risiko_residual', 'ref_matriks_treated.skor_risiko as skor_risiko_treated')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko <= ref_data_umum.skor_selera)) OR (ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera)')
                        ->where('ref_data_umum.tahun', $now)
                        ->get()
                        ->count();
            $sebabteridentifikasi = DB::table('t_identifikasi_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                        ->where('ref_data_umum.tahun', $now)
                        ->get()
                        ->count();
            $sebabtermitigasi = DB::table('t_identifikasi_risiko')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.id_jns_sebab as id_jns_sebab', 't_rtp.id_penyebab as id_rtp')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                        ->join('t_rtp', 't_penyebab_rca.id_penyebab', '=', 't_rtp.id_penyebab')
                        ->where('ref_data_umum.tahun', $now)
                        ->groupBy('t_rtp.id_penyebab')
                        ->get()
                        ->count();
            $rtpjadwal = DB::table('t_identifikasi_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                        ->join('t_rtp', 't_penyebab_rca.id_penyebab', '=', 't_rtp.id_penyebab')
                        ->where('ref_data_umum.tahun', $now)
                        ->get()
                        ->count();
            $rtprealisasi = DB::table('t_identifikasi_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                        ->join('t_rtp', 't_penyebab_rca.id_penyebab', '=', 't_rtp.id_penyebab')
                        ->leftjoin('t_pemantauan', 't_rtp.id_rtp', '=', 't_pemantauan.id_rtp')
                        ->where('ref_data_umum.tahun', $now)
                        ->whereNotNull('realisasi_waktu')
                        ->get()
                        ->count();
            $insidenk = DB::table('t_keterjadian')
                        ->join('ref_data_umum', 't_keterjadian.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.tahun', $now)
                        ->count();
            $insidenr = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab','t_identifikasi_risiko.id_identifikasi')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.tahun', $now)
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $insidenp = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.tahun', $now)
                        ->groupBy('t_penyebab_rca.id_penyebab')
                        ->get()
                        ->count();
            $unitmaxes = DB::table('wm_instansiunitorg')
                      ->selectRaw('wm_instansiunitorg.id_instansiunitorg as id_instansiunitorg, wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg, COUNT(t_identifikasi_risiko.id_identifikasi) as jml_identifikasi')
                      ->join('ref_data_umum', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'ref_data_umum.s_kd_instansiunitorg')
                      ->leftjoin('t_penetapan_konteks', 'ref_data_umum.id_data_umum', '=', 't_penetapan_konteks.id_data_umum')
                      ->leftjoin('t_identifikasi_risiko', 't_penetapan_konteks.id_penetapan_konteks', '=', 't_identifikasi_risiko.id_penetapan_konteks')
                      ->where('ref_data_umum.tahun', $now)
                      ->groupBy('wm_instansiunitorg.id_instansiunitorg')
                      ->get();
            $units = DB::table('wm_instansiunitorg')
                        ->selectRaw('wm_instansiunitorg.id_instansiunitorg as id_instansiunitorg, wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg, COUNT(t_analisis_risiko.id_analisis) as jml_termitigasi')
                        ->join('ref_data_umum', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'ref_data_umum.s_kd_instansiunitorg')
                        ->leftjoin('t_penetapan_konteks', 'ref_data_umum.id_data_umum', '=', 't_penetapan_konteks.id_data_umum')
                        ->leftjoin('t_identifikasi_risiko', 't_penetapan_konteks.id_penetapan_konteks', '=', 't_identifikasi_risiko.id_penetapan_konteks')
                        ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko <= ref_data_umum.skor_selera)) OR (ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera) OR (ISNULL(ref_matriks_residual.skor_risiko))')
                        ->where('ref_data_umum.tahun', $now)
                        ->groupBy('wm_instansiunitorg.id_instansiunitorg')
                        ->get();
            $rankjumlah = DB::table('ref_bagan_risiko')
                    ->selectRaw('ref_bagan_risiko.id_bagan_risiko as id_bagan_risiko, ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko, COUNT(t_identifikasi_risiko.id_identifikasi) as jumlah_risiko')
                    ->leftjoin('t_identifikasi_risiko', 'ref_bagan_risiko.id_bagan_risiko', '=', 't_identifikasi_risiko.id_bagan_risiko')
                    ->leftjoin('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->leftjoin('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->groupBy('ref_bagan_risiko.id_bagan_risiko')
                    ->orderBy('jumlah_risiko', 'desc')
                    ->take(5)
                    ->get();
            $ranklevel = DB::table('ref_bagan_risiko')
                    ->selectRaw('ref_bagan_risiko.id_bagan_risiko as id_bagan_risiko, ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko, CEILING(AVG((CASE WHEN ISNULL(t_analisis_risiko.id_matriks_treated) THEN matriks_residual.skor_risiko ELSE matriks_treated.skor_risiko END))) AS level_risiko')
                    ->leftjoin('t_identifikasi_risiko', 'ref_bagan_risiko.id_bagan_risiko', '=', 't_identifikasi_risiko.id_bagan_risiko')
                    ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'matriks_treated.id_matriks')
                    ->leftjoin('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->leftjoin('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->groupBy('ref_bagan_risiko.id_bagan_risiko')
                    ->orderBy('level_risiko', 'desc')
                    ->take(5)
                    ->get();


            return view('dashboard.dashkepala',  
                compact('risikoteridentifikasi',
                'risikotermitigasi',
                'sebabteridentifikasi',
                'sebabtermitigasi',
                'rtpjadwal',
                'rtprealisasi',
                'insidenk',
                'insidenr',
                'insidenp',
                'unitmaxes',
                'units',
                'rankjumlah',
                'ranklevel'
                ));
    }

}
