<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\Models\adminpemda\OPD;
use App\Models\utils\Unitkerja;
use App\Models\adminpemda\UserHrm;

class AppCtrl extends Controller
{
    //

	public function getIndex()
    {   
//        if(Auth::user()->hasRole('Admin BPKP')) {
        if(Auth::user()->role_id == '1') {
            $this->data['unitkerja'] = Unitkerja::where('status','1')->count();
            $this->data['user'] = UserHrm::count();
            $this->data['baganrisiko'] = DB::table('tbl_baganrisiko')->count();
            $this->data['kategoririsiko'] = DB::table('tbl_kategori')->count();
            return view('home.adminpemda', $this->data);
        }

//        if (Auth::user()->hasRole('User BPKP 1')) {
        if (Auth::user()->role_id == '2') {

                $chart = DB::table('analisis_pemda')
                ->select('analisis_pemda.id as id', 
                    'ref_kemungkinan.nilai as kemungkinan',
                    'ref_dampak.nilai as dampak',
                    'analisis_pemda.tingkat_risiko as tingkat_risiko',
                    'tbl_baganrisiko.nama_risiko as nama_risiko', 'sasaran_pemda.nama_sasaran as nama_sasaran')

                ->join('ref_kemungkinan', 'ref_kemungkinan.id', 'analisis_pemda.kemungkinan_id')
                ->join('ref_dampak', 'ref_dampak.id', 'analisis_pemda.dampak_id')
                ->join('identifikasi_pemda', 'analisis_pemda.identifikasi_id', 'identifikasi_pemda.id')
                ->join('tbl_baganrisiko', 'identifikasi_pemda.risiko_id', 'tbl_baganrisiko.id')
                ->join('sasaran_pemda', 'identifikasi_pemda.sasaran_id', 'sasaran_pemda.id')
                ->orderBy('analisis_pemda.tingkat_risiko', 'desc')
                ->limit(10)
                ->get();

        $dt = DB::table('analisis_pemda')
                ->select('analisis_pemda.id as id','sasaran_pemda.nama_sasaran as nama_sasaran',
                 'tbl_baganrisiko.nama_risiko as nama_risiko', 'ref_kemungkinan.nilai as kemungkinan_id',
                  'ref_dampak.id as dampak_id', 'analisis_pemda.tingkat_risiko')

                ->join('identifikasi_pemda', 'analisis_pemda.identifikasi_id', '=', 'identifikasi_pemda.id')
                ->join('sasaran_pemda', 'identifikasi_pemda.sasaran_id', '=', 'sasaran_pemda.id')
                 ->join('tbl_baganrisiko', 'identifikasi_pemda.risiko_id', '=', 'tbl_baganrisiko.id')
                 ->join('ref_kemungkinan', 'analisis_pemda.kemungkinan_id', '=', 'ref_kemungkinan.id')
                 ->join('ref_dampak', 'analisis_pemda.dampak_id', '=', 'ref_dampak.id')
                 ->where('identifikasi_pemda.sisa_risiko','Ada')
                 ->orderBy('analisis_pemda.tingkat_risiko', 'desc')
                 ->get();

                return view('home.userpemda1', compact('chart', 'dt'));
            }

//            if (Auth::user()->hasRole('User BPKP 2')) {
            if (Auth::user()->role_id == '3') {

                $chart = DB::table('analisis_pemda')
                ->select('analisis_pemda.id as id', 
                    'ref_kemungkinan.nilai as kemungkinan',
                    'ref_dampak.nilai as dampak',
                    'analisis_pemda.tingkat_risiko as tingkat_risiko',
                    'tbl_baganrisiko.nama_risiko as nama_risiko', 'sasaran_pemda.nama_sasaran as nama_sasaran')

                ->join('ref_kemungkinan', 'ref_kemungkinan.id', 'analisis_pemda.kemungkinan_id')
                ->join('ref_dampak', 'ref_dampak.id', 'analisis_pemda.dampak_id')
                ->join('identifikasi_pemda', 'analisis_pemda.identifikasi_id', 'identifikasi_pemda.id')
                ->join('tbl_baganrisiko', 'identifikasi_pemda.risiko_id', 'tbl_baganrisiko.id')
                ->join('sasaran_pemda', 'identifikasi_pemda.sasaran_id', 'sasaran_pemda.id')
                ->orderBy('analisis_pemda.tingkat_risiko', 'desc')
                ->limit(10)
                ->get();

        $dt = DB::table('analisis_pemda')
                ->select('analisis_pemda.id as id','sasaran_pemda.nama_sasaran as nama_sasaran',
                 'tbl_baganrisiko.nama_risiko as nama_risiko', 'ref_kemungkinan.nilai as kemungkinan_id',
                  'ref_dampak.id as dampak_id', 'analisis_pemda.tingkat_risiko')

                ->join('identifikasi_pemda', 'analisis_pemda.identifikasi_id', '=', 'identifikasi_pemda.id')
                ->join('sasaran_pemda', 'identifikasi_pemda.sasaran_id', '=', 'sasaran_pemda.id')
                 ->join('tbl_baganrisiko', 'identifikasi_pemda.risiko_id', '=', 'tbl_baganrisiko.id')
                 ->join('ref_kemungkinan', 'analisis_pemda.kemungkinan_id', '=', 'ref_kemungkinan.id')
                 ->join('ref_dampak', 'analisis_pemda.dampak_id', '=', 'ref_dampak.id')
                 ->where('identifikasi_pemda.sisa_risiko','Ada')
                 ->orderBy('analisis_pemda.tingkat_risiko', 'desc')
                 ->get();


                return view('home.userpemda2', compact('chart', 'dt'));
            }

//            if (Auth::user()->hasRole('Admin Pemda')) {
            if (Auth::user()->role_id == '4') {
                // return "ADA CTRL";
            $chart = DB::table('analisis_opd')
                ->select('analisis_opd.id as id', 
                    'ref_kemungkinan.nilai as kemungkinan',
                    'ref_dampak.nilai as dampak',
                    'analisis_opd.tingkat_risiko as tingkat_risiko',
                    'tbl_baganrisiko.nama_risiko as nama_risiko', 'sasaran_opd.nama_sasaran as nama_sasaran')

                ->join('ref_kemungkinan', 'ref_kemungkinan.id', 'analisis_opd.kemungkinan_id')
                ->join('ref_dampak', 'ref_dampak.id', 'analisis_opd.dampak_id')
                ->join('identifikasi_opd', 'analisis_opd.identifikasi_id', 'identifikasi_opd.id')
                ->join('tbl_baganrisiko', 'identifikasi_opd.risiko_id', 'tbl_baganrisiko.id')
                ->join('sasaran_opd', 'identifikasi_opd.sasaran_id', 'sasaran_opd.id')
                ->where('identifikasi_opd.key_sort_unit', Auth::user()->key_sort_unit)
                ->orderBy('analisis_opd.tingkat_risiko', 'desc')
                ->limit(10)
                ->get();

            $dt = DB::table('analisis_opd')
                ->select('analisis_opd.id as id','sasaran_opd.nama_sasaran as nama_sasaran',
                 'tbl_baganrisiko.nama_risiko as nama_risiko', 'ref_kemungkinan.nilai as kemungkinan_id',
                  'ref_dampak.id as dampak_id', 'analisis_opd.tingkat_risiko')

                ->join('identifikasi_opd', 'analisis_opd.identifikasi_id', '=', 'identifikasi_opd.id')
                ->join('sasaran_opd', 'identifikasi_opd.sasaran_id', '=', 'sasaran_opd.id')
                 ->join('tbl_baganrisiko', 'identifikasi_opd.risiko_id', '=', 'tbl_baganrisiko.id')
                 ->join('ref_kemungkinan', 'analisis_opd.kemungkinan_id', '=', 'ref_kemungkinan.id')
                 ->join('ref_dampak', 'analisis_opd.dampak_id', '=', 'ref_dampak.id')
                 ->where('identifikasi_opd.key_sort_unit', Auth::user()->key_sort_unit)
                 ->where('identifikasi_opd.sisa_risiko','Ada')
                 ->orderBy('analisis_opd.tingkat_risiko', 'desc')
                 ->get();

            $nama_opd = Unitkerja::where('key_sort_unit', Auth::user()->key_sort_unit)->first();
                return view('home.adminopd', compact('chart', 'dt', 'nama_opd'));
            }

//            if (Auth::user()->hasRole('User PWK 1')) {
            if (Auth::user()->role_id == '5') {

                $chart = DB::table('analisis_opd')
                ->select('analisis_opd.id as id', 
                    'ref_kemungkinan.nilai as kemungkinan',
                    'ref_dampak.nilai as dampak',
                    'analisis_opd.tingkat_risiko as tingkat_risiko',
                    'tbl_baganrisiko.nama_risiko as nama_risiko', 'sasaran_opd.nama_sasaran as nama_sasaran')

                ->join('ref_kemungkinan', 'ref_kemungkinan.id', 'analisis_opd.kemungkinan_id')
                ->join('ref_dampak', 'ref_dampak.id', 'analisis_opd.dampak_id')
                ->join('identifikasi_opd', 'analisis_opd.identifikasi_id', 'identifikasi_opd.id')
                ->join('tbl_baganrisiko', 'identifikasi_opd.risiko_id', 'tbl_baganrisiko.id')
                ->join('sasaran_opd', 'identifikasi_opd.sasaran_id', 'sasaran_opd.id')
                ->where('identifikasi_opd.key_sort_unit', Auth::user()->key_sort_unit)
                ->orderBy('analisis_opd.tingkat_risiko', 'desc')
                ->limit(10)
                ->get();

            $dt = DB::table('analisis_opd')
                ->select('analisis_opd.id as id','sasaran_opd.nama_sasaran as nama_sasaran',
                 'tbl_baganrisiko.nama_risiko as nama_risiko', 'ref_kemungkinan.nilai as kemungkinan_id',
                  'ref_dampak.id as dampak_id', 'analisis_opd.tingkat_risiko')

                ->join('identifikasi_opd', 'analisis_opd.identifikasi_id', '=', 'identifikasi_opd.id')
                ->join('sasaran_opd', 'identifikasi_opd.sasaran_id', '=', 'sasaran_opd.id')
                 ->join('tbl_baganrisiko', 'identifikasi_opd.risiko_id', '=', 'tbl_baganrisiko.id')
                 ->join('ref_kemungkinan', 'analisis_opd.kemungkinan_id', '=', 'ref_kemungkinan.id')
                 ->join('ref_dampak', 'analisis_opd.dampak_id', '=', 'ref_dampak.id')
                 ->where('identifikasi_opd.key_sort_unit', Auth::user()->key_sort_unit)
                 ->where('identifikasi_opd.sisa_risiko','Ada')
                 ->orderBy('analisis_opd.tingkat_risiko', 'desc')
                 ->get();

            $nama_opd = Unitkerja::where('key_sort_unit', Auth::user()->key_sort_unit)->first();
                return view('home.useropd1', compact('chart', 'dt', 'nama_opd'));
            }

//            if (Auth::user()->hasRole('User PWK 2')) {
            if (Auth::user()->role_id == '6') {

                $chart = DB::table('analisis_opd')
                ->select('analisis_opd.id as id', 
                    'ref_kemungkinan.nilai as kemungkinan',
                    'ref_dampak.nilai as dampak',
                    'analisis_opd.tingkat_risiko as tingkat_risiko',
                    'tbl_baganrisiko.nama_risiko as nama_risiko', 'sasaran_opd.nama_sasaran as nama_sasaran')

                ->join('ref_kemungkinan', 'ref_kemungkinan.id', 'analisis_opd.kemungkinan_id')
                ->join('ref_dampak', 'ref_dampak.id', 'analisis_opd.dampak_id')
                ->join('identifikasi_opd', 'analisis_opd.identifikasi_id', 'identifikasi_opd.id')
                ->join('tbl_baganrisiko', 'identifikasi_opd.risiko_id', 'tbl_baganrisiko.id')
                ->join('sasaran_opd', 'identifikasi_opd.sasaran_id', 'sasaran_opd.id')
                ->where('identifikasi_opd.key_sort_unit', Auth::user()->key_sort_unit)
                ->orderBy('analisis_opd.tingkat_risiko', 'desc')
                ->limit(10)
                ->get();

            $dt = DB::table('analisis_opd')
                ->select('analisis_opd.id as id','sasaran_opd.nama_sasaran as nama_sasaran',
                 'tbl_baganrisiko.nama_risiko as nama_risiko', 'ref_kemungkinan.nilai as kemungkinan_id',
                  'ref_dampak.id as dampak_id', 'analisis_opd.tingkat_risiko')

                ->join('identifikasi_opd', 'analisis_opd.identifikasi_id', '=', 'identifikasi_opd.id')
                ->join('sasaran_opd', 'identifikasi_opd.sasaran_id', '=', 'sasaran_opd.id')
                 ->join('tbl_baganrisiko', 'identifikasi_opd.risiko_id', '=', 'tbl_baganrisiko.id')
                 ->join('ref_kemungkinan', 'analisis_opd.kemungkinan_id', '=', 'ref_kemungkinan.id')
                 ->join('ref_dampak', 'analisis_opd.dampak_id', '=', 'ref_dampak.id')
                 ->where('identifikasi_opd.key_sort_unit', Auth::user()->key_sort_unit)
                 ->where('identifikasi_opd.sisa_risiko','Ada')
                 ->orderBy('analisis_opd.tingkat_risiko', 'desc')
                 ->get();

            $nama_opd = Unitkerja::where('key_sort_unit', Auth::user()->key_sort_unit)->first();

                return view('home.useropd2', compact('chart', 'dt', 'nama_opd'));
            }

//            if (Auth::user()->hasRole('User PWK Kegiatan 1')) {
            if (Auth::user()->role_id == '7') {

                $chart = DB::table('analisis_kegiatan')
                ->select('analisis_kegiatan.id as id','kegiatan_opd.nama_kegiatan as nama_kegiatan',
                 'tbl_baganrisiko.nama_risiko as nama_risiko', 'ref_kemungkinan.nilai as kemungkinan',
                  'ref_dampak.id as dampak', 'analisis_kegiatan.tingkat_risiko')
                ->join('identifikasi_kegiatan', 'analisis_kegiatan.identifikasi_id', '=', 'identifikasi_kegiatan.id')
                ->join('kegiatan_opd', 'identifikasi_kegiatan.kegiatan_id', '=', 'kegiatan_opd.id')
                 ->join('tbl_baganrisiko', 'identifikasi_kegiatan.risiko_id', '=', 'tbl_baganrisiko.id')
                 ->join('ref_kemungkinan', 'analisis_kegiatan.kemungkinan_id', '=', 'ref_kemungkinan.id')
                 ->join('ref_dampak', 'analisis_kegiatan.dampak_id', '=', 'ref_dampak.id')
                 ->where('identifikasi_kegiatan.key_sort_unit', Auth::user()->key_sort_unit)
                 ->where('identifikasi_kegiatan.sisa_risiko','Ada')
                 ->orderBy('analisis_kegiatan.tingkat_risiko', 'desc')
                 ->limit(10)
                 ->get();

                $nama_opd = Unitkerja::where('key_sort_unit', Auth::user()->key_sort_unit)->first();
                return view('home.useropdkegiatan1', compact('chart', 'dt', 'nama_opd'));
            }

//            if (Auth::user()->hasRole('User PWK Kegiatan 2')) {
            if (Auth::user()->role_id == '8') {

                $chart = DB::table('analisis_kegiatan')
                ->select('analisis_kegiatan.id as id','kegiatan_opd.nama_kegiatan as nama_kegiatan',
                 'tbl_baganrisiko.nama_risiko as nama_risiko', 'ref_kemungkinan.nilai as kemungkinan',
                  'ref_dampak.id as dampak', 'analisis_kegiatan.tingkat_risiko')
                ->join('identifikasi_kegiatan', 'analisis_kegiatan.identifikasi_id', '=', 'identifikasi_kegiatan.id')
                ->join('kegiatan_opd', 'identifikasi_kegiatan.kegiatan_id', '=', 'kegiatan_opd.id')
                 ->join('tbl_baganrisiko', 'identifikasi_kegiatan.risiko_id', '=', 'tbl_baganrisiko.id')
                 ->join('ref_kemungkinan', 'analisis_kegiatan.kemungkinan_id', '=', 'ref_kemungkinan.id')
                 ->join('ref_dampak', 'analisis_kegiatan.dampak_id', '=', 'ref_dampak.id')
                 ->where('identifikasi_kegiatan.key_sort_unit', Auth::user()->key_sort_unit)
                 ->where('identifikasi_kegiatan.sisa_risiko','Ada')
                 ->orderBy('analisis_kegiatan.tingkat_risiko', 'desc')
                 ->limit(10)
                 ->get();

                $nama_opd = Unitkerja::where('key_sort_unit', Auth::user()->key_sort_unit)->first();
                return view('home.useropdkegiatan2', compact('chart', 'dt', 'nama_opd'));
            }
    }

    public function getSkorRisiko($dampak,$kemungkinan){
        if($dampak!='' OR $kemungkinan!=''){
            $skor_risiko = DB::table('tbl_matriks')
                    ->where('skor_dampak', $dampak)
                    ->where('skor_kemungkinan', $kemungkinan)
                    ->value('skor_risiko');
                    // ->pluck('skor_risiko', 'id');
        }else{
            $skor_risiko = null;
        }

        return json_encode($skor_risiko);
    } 
}
