<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\adminpemda\Role;
use App\Models\adminpemda\User;
use DB; 
use App\Models\adminpemda\UserHrm;
use App\Models\utils\Functions;
 
class AuthCtrl extends Controller
{

    public function __construct(){

    }
    public function getLogin()
    {   

    	return view('auth.login_fresh');
    }

    public function postLogin(Request $request)
    {

            $ldap = Functions::LoginLDAP($request->get('username'),$request->get('password'));

            $userFromLDAP = UserHrm::where('user_nip',$ldap->niplama)->first();
            
            // Auth::login($userFromLDAP, true);

            // $userFromLDAP = UserHrm::where('username',$request->get('username'))->first();

            if($userFromLDAP){
                //Auth::attempt(['username'=>$request['username'],'password'=>$request['password']]);
                Auth::login($userFromLDAP, true);
            } else {
                $renpeglast = DB::table('vw_renpeglast')
                            ->select('vw_renpeglast.carinama_nip as carinama_nip','vw_renpeglast.s_nama_lengkap as s_nama_lengkap','vw_renpeglast.niplama as niplama','vw_renpeglast.key_sort_unit as key_sort_unit')
                            ->where('niplama', '=', $ldap->niplama)
                            ->first();
                $now = Carbon::now('Asia/Jakarta');

                $id = new UserHrm;
                $id->name = $renpeglast->s_nama_lengkap;
                $id->email = "123@bpkp.go.id";
                $id->username = $request->get('username');
                $id->password = bcrypt($request->get('password'));
                $id->role_id = 9;
                $id->user_nip = $renpeglast->niplama;
                $id->key_sort_unit = $renpeglast->key_sort_unit;
                $id->created_at = $now;
                $id->save();

                Auth::login($userFromLDAP, true);

                //Auth::attempt(['username'=>$request['username'],'password'=>$request['password']]);
            }

            if (Auth::check()) {
              return redirect()->route('peran');
            } else {
              return redirect()->back();
            }

    }

    public function getPeran()
    {   
        $count = DB::table('tbl_user_role')
            ->select('tbl_user_role.role_id as id','tbl_role.nama_role as   nama_role')
            ->join('tbl_role', 'tbl_user_role.role_id', '=', 'tbl_role.id')
                ->orderBy('id', 'asc')
            ->where('tbl_user_role.user_nip', Auth::user()->user_nip)
            ->count();
        if ($count > 0){
            $roles = DB::table('tbl_user_role')
            ->select('tbl_user_role.role_id as id','tbl_role.nama_role as   nama_role')
            ->join('tbl_role', 'tbl_user_role.role_id', '=', 'tbl_role.id')
                ->orderBy('id', 'asc')
            ->where('tbl_user_role.user_nip', Auth::user()->user_nip)
            ->get();
        }
        else {
            $roles = DB::table('tbl_role')->orderBy('id', 'desc')->where('id', 9)->get();
        }
        
        return view('auth.pilih_role', compact('roles','count'));
    }

    public function postPeran(Request $request)
    {
        
            $userFromLDAP = UserHrm::where('name', Auth::user()->name)->first();

            if($request->get('peran')!='0'){
                $userFromLDAP->role_id = $request->get('peran');
                $userFromLDAP->save();
            }

            if ($userFromLDAP->role_id == 1) {
              return redirect()->route('heatmap');
            }

            if ($userFromLDAP->role_id == 2) {
              return redirect()->route('peta');
            }

            if ($userFromLDAP->role_id == 3) {
              return redirect()->route('heatmapbpkp');
            }

            if ($userFromLDAP->role_id == 4|$userFromLDAP->role_id == 5|$userFromLDAP->role_id == 6|$userFromLDAP->role_id == 7|$userFromLDAP->role_id == 9|$userFromLDAP->role_id == 10) {
                return redirect()->route('heatmapunit');
            }

            if ($userFromLDAP->role_id == 8) {
              return redirect()->route('petapibr');
            }

        return redirect()->back();
    }

    public function getlogout()
    {

        Auth::guard('web')->logout();
        Auth::guard('web1')->logout();
    	return redirect('/');
    }

}
