<?php

namespace App\Http\Controllers\litbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;



class PelaporanCtrl extends Controller
{

    public function semuaBulan()
    {
        $pelaksanaan = DB::connection('dblitbang')
            ->table('t_st')
            ->selectRaw('t_st.id_st as nomor,
                t_st.no_st as st,
                t_st.uraian_st as uraian,
                t_st.progress as progress,
                SUM(t_spj.nilai_spj) as nilai,
                ref_bidang.skt_bidang as bidang,
                ref_pkau.kode_pkau as kode' 
                )
            ->leftjoin('ref_pkau', 't_st.id_pkau', '=', 'ref_pkau.id_pkau')
            ->leftjoin('ref_bidang', 'ref_pkau.id_bidang', '=', 'ref_bidang.id_bidang')
            ->leftjoin('t_spj', 't_st.id_st', '=', 't_spj.id_st')
            ->groupBy('t_st.id_st')
            ->get();

        return json_encode($pelaksanaan);
    }

    public function pilihBulan($id)
    {
        $pelaksanaan = DB::connection('dblitbang')
            ->table('t_st')
            ->selectRaw('t_st.id_st as nomor,
                t_st.no_st as st,
                t_st.uraian_st as uraian,
                t_st.progress as progress,
                SUM(t_spj.nilai_spj) as nilai,
                ref_bidang.skt_bidang as bidang,
                ref_pkau.kode_pkau as kode' 
                )
            ->leftjoin('ref_pkau', 't_st.id_pkau', '=', 'ref_pkau.id_pkau')
            ->leftjoin('ref_bidang', 'ref_pkau.id_bidang', '=', 'ref_bidang.id_bidang')
            ->leftjoin('t_spj', 't_st.id_st', '=', 't_spj.id_st')
            ->whereRaw('MONTH(t_st.tgl_st) = ?', $id)
            ->groupBy('t_st.id_st')
            ->get();

        return json_encode($pelaksanaan);
    } 

    public function getSaldoAktivitas()
    {

        $anggaran = DB::connection('dblitbang')
            ->table('vw_anggaran')
            ->selectRaw('vw_anggaran.id_detil_aktivitas as id,
                vw_anggaran.nama_aktivitas as nama_aktivitas,
                vw_anggaran.nama_detil_aktivitas as nama_detil_aktivitas,
                vw_anggaran.pagu_anggaran as pagu_anggaran,
                SUM(t_spj.nilai_spj) AS nilai_spj'
                )
            ->leftjoin('t_st', 'vw_anggaran.id_detil_aktivitas', '=', 't_st.id_detil_aktivitas')
            ->leftjoin('t_spj', 't_st.id_st', '=', 't_spj.id_st')
            ->groupBy('vw_anggaran.id_detil_aktivitas')
            ->get();
        $totala = DB::connection('dblitbang')
            ->table('t_anggaran_kinerja')
            ->sum('t_anggaran_kinerja.pagu_anggaran');
        $totalr = DB::connection('dblitbang')
            ->table('t_spj')
            ->sum('t_spj.nilai_spj');

        return view('1litbang.salaktiv', compact('anggaran','totala','totalr'));

    }

    public function getSaldoMAK()
    {

        $anggaran = DB::connection('dblitbang')
            ->table('t_anggaran_kinerja')
            ->selectRaw('t_anggaran_kinerja.id_anggaran_kinerja as id,
                ref_aktivitas.kode_aktivitas as kode_aktivitas,
                ref_sub_komponen.nama_sub_komponen as nama_sub_komponen,
                ref_mak.nama_mak as nama_mak,
                t_anggaran_kinerja.pagu_anggaran as pagu_anggaran,
                SUM(t_spj.nilai_spj) AS nilai_spj'
                )
            ->leftjoin('ref_mak', 't_anggaran_kinerja.id_mak', '=', 'ref_mak.id_mak')
            ->leftjoin('ref_sub_komponen', 'ref_mak.id_sub_komponen', '=', 'ref_sub_komponen.id_sub_komponen')
            ->leftjoin('t_spj', 't_anggaran_kinerja.id_anggaran_kinerja', '=', 't_spj.id_anggaran_kinerja')
            ->leftjoin('ref_detil_aktivitas', 't_anggaran_kinerja.id_detil_aktivitas', '=', 'ref_detil_aktivitas.id_detil_aktivitas')
            ->leftjoin('ref_aktivitas', 'ref_detil_aktivitas.id_aktivitas', '=', 'ref_aktivitas.id_aktivitas')
            ->groupBy('t_anggaran_kinerja.id_anggaran_kinerja')
            ->get();
        $totala = DB::connection('dblitbang')
            ->table('t_anggaran_kinerja')
            ->sum('t_anggaran_kinerja.pagu_anggaran');
        $totalr = DB::connection('dblitbang')
            ->table('t_spj')
            ->sum('t_spj.nilai_spj');

        return view('1litbang.salmak', compact('anggaran','totala','totalr'));

    }

    public function getRealisasiST()
    {

        $this->data['bulan_st'] = [1 => "Januari",
            2 => "Februari", 
            3 => "Maret", 
            4 => "April", 
            5 => "Mei", 
            6 => "Juni", 
            7 => "Juli", 
            8 => "Agustus", 
            9 => "September", 
            10 => "Oktober", 
            11 => "November", 
            12 => "Desember"];


        return view('1litbang.realst', $this->data);

    }

    public function getIndikator()
    {

        $anggaran = DB::connection('dblitbang')
            ->table('vw_kinerja')    
            ->get();

        return view('1litbang.indikator', compact('anggaran'));

    }

    public function getManfaat()
    {

        $anggaran = DB::connection('dblitbang')
            ->table('t_penelitian')
            ->where('t_penelitian.id_ikk', '=', 1)
            ->get();

        return view('1litbang.manfaat', compact('anggaran'));

    }

}
