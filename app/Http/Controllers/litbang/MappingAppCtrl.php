<?php

namespace App\Http\Controllers\litbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class MappingAppCtrl extends Controller
{

    public function data($id)
    {
        $bisma = DB::connection('dbbisma')
                    ->table('d_detailapp')
                    ->selectRaw('
                        LEFT(d_detailapp.kdindex,4) as tahun,
                        MID(d_detailapp.kdindex,5,6) as satker,
                        MID(d_detailapp.kdindex,11,2) as program,
                        MID(d_detailapp.kdindex,13,4) as kegiatan,
                        MID(d_detailapp.kdindex,17,3) as kro,
                        MID(d_detailapp.kdindex,20,3) as ro,
                        MID(d_detailapp.kdindex,23,3) as komp,
                        MID(d_detailapp.kdindex,26,2) as subkomp,
                        MID(d_detailapp.kdindex,28,6) as akun,
                        MID(d_detailapp.kdindex,34,1) as beban,
                        MID(d_detailapp.kdindex,35,2) as ib,
                        t_app.kode_app as kode_app,
                        t_app.nama_app as nama_app,
                        d_detailapp.rupiah_tahapan as nilai
                        ')
                    ->leftJoin('t_app', 'd_detailapp.id_app', '=', 't_app.id')
                    ->whereRaw('MID(d_detailapp.kdindex,5,6) = ?', $id)
                    ->get();

        return json_encode($bisma);             
    }    

    public function excel($id)
    {
        
        $kolom = ['No.', 
                'Tahun', 
                'Satker',
                'Program',
                'Kegiatan',
                'KRO',
                'RO',
                'Komp',
                'Sub Komp',
                'Akun',
                'Beban',
                'IB',
                'Kode APP',
                'Nama APP',
                'Nilai'
                ];

        $data = DB::connection('dbbisma')
                    ->table('d_detailapp')
                    ->selectRaw('
                        LEFT(d_detailapp.kdindex,4) as tahun,
                        MID(d_detailapp.kdindex,5,6) as satker,
                        MID(d_detailapp.kdindex,16,2) as program,
                        MID(d_detailapp.kdindex,18,4) as kegiatan,
                        MID(d_detailapp.kdindex,22,3) as kro,
                        MID(d_detailapp.kdindex,25,3) as ro,
                        MID(d_detailapp.kdindex,28,3) as komp,
                        MID(d_detailapp.kdindex,31,2) as subkomp,
                        MID(d_detailapp.kdindex,33,6) as akun,
                        MID(d_detailapp.kdindex,39,1) as beban,
                        MID(d_detailapp.kdindex,40,2) as ib,
                        t_app.kode_app as kode_app,
                        t_app.nama_app as nama_app,
                        d_detailapp.rupiah_tahapan as nilai
                        ')
                    ->leftJoin('t_app', 'd_detailapp.id_app', '=', 't_app.id')
                    ->whereRaw('MID(d_detailapp.kdindex,5,6) = ?', $id)
                    ->get();

        $baris = array_map(function($item) {
                        return (array)$item; 
                    }, $data->toArray());

        $spreadsheet = new Spreadsheet();
        
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);

        $spreadsheet->getActiveSheet()->fromArray($kolom, NULL, 'A1');
        $spreadsheet->getActiveSheet()->fromArray($baris, NULL, 'B2');

        $row = 1;
        foreach ($data as $key => $value) {
            $no = 'A'. (1 + $row);
            $spreadsheet->getActiveSheet()->setCellValue($no, $row);
            $row++;
        }

        $writer = new Xlsx($spreadsheet);
        $filename = 'mapping_app.xlsx';
        $writer->save($filename);
        header('Content-Type: application/x-www-form-urlencoded');
        header('Content-Transfer-Encoding: Binary');
        header("Content-disposition: attachment; filename=\"".$filename."\"");
        readfile($filename);
        exit;

    }
 

}
