<?php

namespace App\Http\Controllers\litbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;
use App\Models\litbang\Penelitian;



class PenelitianCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $tugas = DB::connection('dblitbang')
            ->table('t_penelitian')
            ->select('t_penelitian.id_penelitian as id',
                't_penelitian.nama_penelitian',
                't_penelitian.no_penelitian',
                't_penelitian.tgl_penelitian',
                'ref_ikk.ket_ikk')
            ->join('ref_ikk', 't_penelitian.id_ikk', '=', 'ref_ikk.id_ikk')
            ->get();

        return view('1litbang.lit', compact('tugas'));

    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['id_ikk'] = DB::connection('dblitbang')
            ->table('ref_ikk')
            ->select('ref_ikk.id_ikk as id_ikk','ref_ikk.ket_ikk as ket_ikk')
            ->where('ref_ikk.id_ikk', '<=', 3)
            ->get();

        return view('1litbang.litcreate', $this->data);              
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()){

            $rules = array(

                'no_penelitian' => 'required',
                'tgl_penelitian' => 'required',
                'nama_penelitian' => 'required',
                'id_ikk' => 'required'

            );
            $messages = [
                            'no_penelitian.required' => 'Silahkan isi Nomor Laporan.',
                            'tgl_penelitian.required' => 'Silahkan isi Tanggal Laporan.',
                            'nama_penelitian.required' => 'Silahkan isi Nama Laporan.',
                            'id_ikk.required' => 'Silahkan pilih Jenis Laporan.'

            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            } else {

                $now = Carbon::now('Asia/Jakarta');

                $id = new Penelitian;
                $id->no_penelitian = Input::get('no_penelitian');
                $id->tgl_penelitian = Input::get('tgl_penelitian');
                $id->nama_penelitian = Input::get('nama_penelitian');
                $id->id_ikk = Input::get('id_ikk');
                $id->created_at = $now;
                $id->user_create = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lit.index');

            }
        } else {
            return redirect()->back();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $anggaran = DB::connection('dblitbang')
            ->table('t_penelitian')
            ->select('t_penelitian.id_penelitian as id',
                'ref_ikk.id_ikk as id_ikk',
                'ref_ikk.ket_ikk as ket_ikk',
                't_penelitian.no_penelitian as no_penelitian',
                't_penelitian.tgl_penelitian as tgl_penelitian',
                't_penelitian.nama_penelitian as nama_penelitian',
                't_penelitian.init_a1 as init_a1',
                't_penelitian.init_b1 as init_b1',
                't_penelitian.init_b2 as init_b2',
                't_penelitian.init_b3 as init_b3',
                't_penelitian.init_b4 as init_b4',
                't_penelitian.inter_a1 as inter_a1',
                't_penelitian.inter_b1 as inter_b1',
                't_penelitian.end_a1 as end_a1',
                't_penelitian.end_b1 as end_b1'
                )
            ->join('ref_ikk', 't_penelitian.id_ikk', '=', 'ref_ikk.id_ikk')          
            ->where('t_penelitian.id_penelitian', $id)
            ->first();

        return view('1litbang.litshow', compact('anggaran'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['lit'] = Penelitian::find($id);
        $this->data['id_ikk'] = DB::connection('dblitbang')
            ->table('ref_ikk')
            ->select('ref_ikk.id_ikk as id_ikk','ref_ikk.ket_ikk as ket_ikk')
            ->where('ref_ikk.id_ikk', '<=', 3)
            ->get();

        return view('1litbang.litedit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check()){

            $rules = array(

                'no_penelitian' => 'required',
                'tgl_penelitian' => 'required',
                'nama_penelitian' => 'required',
                'id_ikk' => 'required'

            );
            $messages = [
                            'no_penelitian.required' => 'Silahkan isi Nomor Laporan.',
                            'tgl_penelitian.required' => 'Silahkan isi Tanggal Laporan.',
                            'nama_penelitian.required' => 'Silahkan isi Nama Laporan.',
                            'id_ikk.required' => 'Silahkan pilih Jenis Laporan.'

            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            } else {

                $now = Carbon::now('Asia/Jakarta');
                $id = Penelitian::find($id);
                $id->no_penelitian = Input::get('no_penelitian');
                $id->tgl_penelitian = Input::get('tgl_penelitian');
                $id->nama_penelitian = Input::get('nama_penelitian');
                $id->id_ikk = Input::get('id_ikk');
                $id->init_a1 = Input::get('init_a1');
                $id->init_b1 = Input::get('init_b1');
                $id->init_b2 = Input::get('init_b2');
                $id->init_b3 = Input::get('init_b3');
                $id->init_b4 = Input::get('init_b4');
                $id->inter_a1 = Input::get('inter_a1');
                $id->inter_b1 = Input::get('inter_b1');
                $id->end_a1 = Input::get('end_a1');
                $id->end_b1 = Input::get('end_b1');
                $id->updated_at = $now;
                $id->user_update = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah diubah.', 'Selamat');
                return redirect()->route('lit.index');

            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
