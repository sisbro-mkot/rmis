<?php

namespace App\Http\Controllers\litbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;
use App\Models\litbang\SPJ;


class SPJCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $anggaran = DB::connection('dblitbang')
            ->table('t_spj')
            ->select('t_spj.id_spj as id',
                't_st.no_st as no_st',
                't_st.tgl_st as tgl_st',
                't_st.uraian_st as uraian_st',
                't_spj.nilai_spj as nilai_spj',
                't_anggaran_kinerja.id_detil_aktivitas as aktivitas_spj',
                't_st.id_detil_aktivitas as aktivitas_st'
                )
            ->join('t_st', 't_spj.id_st', '=', 't_st.id_st')
            ->join('t_anggaran_kinerja', 't_spj.id_anggaran_kinerja', '=', 't_anggaran_kinerja.id_anggaran_kinerja')
            ->orderBy('t_spj.id_spj')
            ->get();
        $total = DB::connection('dblitbang')
            ->table('t_spj')
            ->sum('t_spj.nilai_spj');

        return view('1litbang.spj', compact('anggaran','total'));

    }

    public function angkin($id) 
    {  
        $control = DB::connection('dblitbang')->table('t_anggaran_kinerja')
                ->select('t_anggaran_kinerja.id_anggaran_kinerja as id_anggaran_kinerja',
                'ref_mak.ket_mak as ket_mak',
                'ref_sub_komponen.nama_sub_komponen as nama_sub_komponen',
                'ref_mak.nama_mak as nama_mak',
                't_anggaran_kinerja.pagu_anggaran'
                )
                ->join('ref_mak', 't_anggaran_kinerja.id_mak', '=', 'ref_mak.id_mak')
                ->join('ref_sub_komponen', 'ref_mak.id_sub_komponen','=', 'ref_sub_komponen.id_sub_komponen')
                ->join('t_st', 't_anggaran_kinerja.id_detil_aktivitas', '=', 't_st.id_detil_aktivitas')
                ->where('t_st.id_st', $id)
                ->get();
        return json_encode($control);
    }

 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tambah($id)
    {
        
        $this->data['id_st_label'] = DB::connection('dblitbang')
            ->table('t_st')
            ->select('t_st.id_st as id',
                'ref_bidang.nama_bidang as nama_bidang',
                't_st.no_st as no_st',
                't_st.tgl_st as tgl_st',
                't_st.uraian_st as uraian_st',
                't_st.tgl_mulai as tgl_mulai',
                't_st.tgl_selesai as tgl_selesai',
                'ref_pkau.kode_pkau as kode_pkau',
                'ref_pkau.nama_pkau as nama_pkau'
                )
            ->leftjoin('ref_pkau', 't_st.id_pkau', '=', 'ref_pkau.id_pkau')
            ->leftjoin('ref_bidang', 't_st.id_bidang', '=', 'ref_bidang.id_bidang')
            ->where('t_st.id_st', $id)
            ->first();
        $this->data['id_st'] = DB::connection('dblitbang')
            ->table('t_st')
            ->select('t_st.id_st as id',
                't_st.no_st as no_st',
                't_st.tgl_st as tgl_st',
                't_st.uraian_st as uraian_st',
                't_st.tgl_mulai as tgl_mulai',
                't_st.tgl_selesai as tgl_selesai'
                )
            ->where('t_st.id_st', $id)
            ->get();
        $this->data['id_mapping'] = DB::connection('dblitbang')
            ->table('t_mapping')
            ->select('t_mapping.id_mapping as id',
                'ref_dja.id as id_dja',
                'ref_dja.mak as mak',
                'ref_dja.nmitem as nmitem'
                )
            ->join('ref_dja', 't_mapping.id_dja', '=', 'ref_dja.id')
            ->join('t_st', function ($join) use ($id) {
                    $join->on('t_mapping.id_pkau', '=', 't_st.id_pkau')
                         ->where('t_st.id_st', '=', $id);
                })
            ->leftjoin('t_spj', function ($join) use ($id) {
                    $join->on('t_mapping.id_mapping', '=', 't_spj.id_mapping')
                         ->where('t_spj.id_st', '=', $id);
                })            
            ->whereNull('t_spj.id_mapping')
            ->orderBy('id_dja')
            ->get();
        $this->data['id_spj'] = DB::connection('dblitbang')
            ->table('t_spj')
            ->select('t_spj.id_spj as id',
                'ref_dja.mak as mak',
                'ref_dja.nmitem as nmitem',
                't_spj.nilai_spj as nilai_spj'
                )
            ->join('t_mapping', 't_spj.id_mapping', '=', 't_mapping.id_mapping')
            ->join('ref_dja', 't_mapping.id_dja', '=', 'ref_dja.id')
            ->where('t_spj.id_st', '=', $id)
            ->get();  

        return view('1litbang.spjcreate', $this->data);              
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()){

            $rules = array(

                'id_st' => 'required',
                'id_mapping' => 'required',
                'nilai_spj' => 'required'

            );
            $messages = [
                            'id_st.required' => 'Silahkan pilih ST/ND.',
                            'id_mapping.required' => 'Silahkan pilih Pembebanan Anggaran.',
                            'nilai_spj.required' => 'Silahkan isi Nilai SPJ.'

            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            } else {

                $id = new SPJ;
                $id->id_st = Input::get('id_st');
                $id->id_mapping = Input::get('id_mapping');
                $id->nilai_spj = Input::get('nilai_spj');
                $id->status_spj = 1;
                $id->user_create = Auth::user()->user_nip;

                $id->save();

                $pegawai = DB::connection('dblitbang')
                            ->table('t_st')
                            ->select('t_st.id_st as id_st')
                            ->where('t_st.id_st', '=', $id->id_st)
                            ->first(); 
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('spjcreate', $pegawai->id_st);

            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $anggaran = DB::connection('dblitbang')
            ->table('t_spj')
            ->select('t_spj.id_spj as id',
                't_spj.tgl_spj as tgl_spj',
                't_st.no_st as no_st',
                't_st.tgl_st as tgl_st',
                't_st.uraian_st as uraian_st',
                't_spj.nilai_spj as nilai_spj',
                't_spj.ket_spj as ket_spj',
                'ref_aktivitas.kode_aktivitas as kode_aktivitas',
                'ref_aktivitas.nama_aktivitas as nama_aktivitas',
                'ref_detil_aktivitas.nama_detil_aktivitas as nama_detil_aktivitas',
                'ref_sub_komponen.nama_sub_komponen as nama_sub_komponen',
                'ref_mak.nama_mak as nama_mak',
                'ref_mak.ket_mak as ket_mak'
                )
            ->join('t_st', 't_spj.id_st', '=', 't_st.id_st')
            ->join('t_anggaran_kinerja', 't_spj.id_anggaran_kinerja', '=', 't_anggaran_kinerja.id_anggaran_kinerja')
            ->join('ref_detil_aktivitas', 't_anggaran_kinerja.id_detil_aktivitas', '=', 'ref_detil_aktivitas.id_detil_aktivitas')
            ->join('ref_aktivitas', 'ref_detil_aktivitas.id_aktivitas', '=', 'ref_aktivitas.id_aktivitas')
            ->join('ref_mak', 't_anggaran_kinerja.id_mak', '=', 'ref_mak.id_mak')
            ->join('ref_sub_komponen', 'ref_mak.id_sub_komponen', '=', 'ref_sub_komponen.id_sub_komponen')
            ->where('t_spj.id_spj', $id)
            ->first();

        return view('1litbang.spjshow', compact('anggaran'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['spj'] = SPJ::find($id);
        $this->data['id_st'] = DB::connection('dblitbang')
            ->table('t_st')
            ->select('t_st.id_st as id_st','t_st.no_st as no_st','t_st.uraian_st as uraian_st')
            ->get();
        $this->data['id_anggaran_kinerja'] = DB::connection('dblitbang')
            ->table('t_anggaran_kinerja')
            ->select('t_anggaran_kinerja.id_anggaran_kinerja as id_anggaran_kinerja',
                'ref_mak.ket_mak as ket_mak',
                'ref_sub_komponen.nama_sub_komponen as nama_sub_komponen',
                'ref_mak.nama_mak as nama_mak',
                't_anggaran_kinerja.pagu_anggaran'
                )
            ->join('ref_mak', 't_anggaran_kinerja.id_mak', '=', 'ref_mak.id_mak')
            ->join('ref_sub_komponen', 'ref_mak.id_sub_komponen','=', 'ref_sub_komponen.id_sub_komponen')
            ->get();  


        return view('1litbang.spjedit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check()){

            $rules = array(

                'id_st' => 'required',
                'id_anggaran_kinerja' => 'required',
                'nilai_spj' => 'required'

            );
            $messages = [
                            'id_st.required' => 'Silahkan pilih ST/ND.',
                            'id_anggaran_kinerja.required' => 'Silahkan pilih Pembebanan Anggaran.',
                            'nilai_spj' => 'Silahkan isi Nilai SPJ.'

            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            } else {

                $now = Carbon::now('Asia/Jakarta');
                $id = SPJ::find($id);
                $id->id_st = Input::get('id_st');
                $id->id_anggaran_kinerja = Input::get('id_anggaran_kinerja');
                $id->tgl_spj = Input::get('tgl_spj');
                $id->nilai_spj = Input::get('nilai_spj');
                $id->status_spj = 1;
                $id->ket_spj = Input::get('ket_spj');
                $id->updated_at = $now;
                $id->user_update = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah diubah.', 'Selamat');
                return redirect()->route('spj.index');

            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::check()){

            $spj = SPJ::findOrFail($id);

            $spj->delete();

            return redirect()->back();

        } else {
            return redirect()->back();
        }

    }

    public function getDashLitbang()
    {

        return view('1litbang.dashboard');

    }

}
