<?php

namespace App\Http\Controllers\litbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;



class MockupCtrl extends Controller
{

    public function bagipagu()
    {
        $bisma = DB::connection('dbbisma')
            ->table('d_bagipagu')
            ->select('id',
                'thang',
                'kdsatker',
                'kddept',
                'kdunit',
                'kdprogram',
                'kdgiat',
                'kdoutput',
                'kdsoutput',
                'kdkmpnen',
                'kdindex',
                'user_id',
                'unit_id',
                'role_id',
                'ppk_id')
            ->where('kdsatker', 604435)
            ->get();

        return json_encode($bisma);             
    }

    public function costsheet()
    {
        $bisma = DB::connection('dbbisma')
            ->table('d_costsheet')
            ->select('id',
                'id_cs',
                'id_st',
                'kdindex',
                'kdakun',
                'kdbeban',
                'id_tahapan',
                'id_app',
                'nost',
                'uraianst',
                'tglst',
                'tujuanst',
                'biaya',
                'is_active',
                'status_cs')
            ->whereRaw('MID(kdindex,1,10) = "2022604435"')
            ->get();

        return json_encode($bisma);             
    }

    public function pagu()
    {
        $bisma = DB::connection('dbbisma')
            ->table('d_pagu')
            ->select('kdindex',
                'thang',
                'kdsatker',
                'kddept',
                'kdunit',
                'kdprogram',
                'kdgiat',
                'kdoutput',
                'kdsoutput',
                'kdkmpnen',
                'kdskmpnen',
                'kdakun',
                'kdbeban',
                'kdib',
                'rupiah',
                'register',
                'revisike',
                'tgrevisi',
                'norevisi',
                'kdblokir')
            ->where('kdsatker', 604435)
            ->get();

        return json_encode($bisma);             
    }

    public function surattugas()
    {
        $bisma = DB::connection('dbbisma')
            ->table('d_surattugas')
            ->select('id',
                'id_st',
                'kdindex',
                'thang',
                'kdgiat',
                'kdoutput',
                'kdsoutput',
                'kdkmpnen',
                'kdskmpnen',
                'no_kuitansi',
                'nost',
                'tglst',
                'uraianst',
                'tglmulaist',
                'tglselesaist',
                'id_unit',
                'kdsatker',
                'status_id',
                'is_aktif')
            ->where('kdsatker', 604435)
            ->get();

        return json_encode($bisma);             
    }

    public function gaji()
    {
        $bisma = DB::connection('dbbisma')
            ->table('t_gaji')
            ->select('id',
                'thang',
                'uraian',
                'no_gaji',
                'kdsatker',
                'jnsgaji_id',
                'bulan',
                'tgl_gaji',
                'status',
                'total',
                'potongan')
            ->where('kdsatker', 604435)
            ->get();

        return json_encode($bisma);             
    }

    public function gajidetail()
    {
        $bisma = DB::connection('dbbisma')
            ->table('t_gaji_detail')
            ->select('id',
                'gaji_id',
                'potongan',
                'kdindex',
                'kdakun',
                'nilai')
            ->whereRaw('MID(kdindex,1,10) = "2022604435"')
            ->get();

        return json_encode($bisma);             
    }

    public function permintaanpbj()
    {
        $bisma = DB::connection('dbbisma')
            ->table('t_permintaan_pbj')
            ->select('id',
                'tahun_anggaran',
                'nomor_ppbj',
                'no_urut',
                'nama_pbj',
                'nomor_dok_sumber',
                'tanggal_dok_sumber',
                'kdindex',
                'kd_satker',
                'kd_program',
                'kd_giat',
                'kd_output',
                'kd_kmpnen',
                'kd_skmpnen',
                'kd_akun',
                'kd_ib',
                'sumberDana_id',
                'jumlah_uang',
                'unit_id',
                'status')
            ->where('kd_satker', 604435)
            ->get();

        return json_encode($bisma);             
    }

    public function simast()
    {
        $bisma = DB::connection('dbbisma')
            ->table('t_sima_st')
            ->select('id_st',
                'sumber_data',
                'status_st',
                'status_workflow',
                'no_surat_tugas',
                'tanggal_surat_tugas',
                'nama_penugasan',
                'tanggal_mulai',
                'tanggal_selesai',
                'id_unit_kerja',
                'nama_unit_kerja',
                'unit_ro_id',
                'ro_kode',
                'ro_uraian',
                'kdsatker',
                'is_aktif')
            ->where('kdsatker', 604435)
            ->get();

        return json_encode($bisma);             
    }

    public function simatim()
    {
        $bisma = DB::connection('dbbisma')
            ->table('t_sima_st')
            ->select('t_sima_tim.sumber_data as sumber_data',
                't_sima_tim.id_st as id_st',
                't_sima_tim.id_tim as id_tim',
                't_sima_tim.nama as nama',
                't_sima_tim.nip as nip',
                't_sima_tim.peran_jabatan as peran_jabatan',
                't_sima_tim.golongan as golongan',
                't_sima_tim.no_urut as no_urut',
                't_sima_tim.status as status')
            ->join('t_sima_tim','t_sima_st.id_st','=','t_sima_tim.id_st')
            ->where('t_sima_st.kdsatker', 604435)
            ->get();

        return json_encode($bisma);             
    }

    public function itemcs()
    {
        $bisma = DB::connection('dbbisma')
            ->table('t_sima_st')
            ->select('d_itemcs.id as id',
                'd_itemcs.id_st as id_st',
                'd_itemcs.id_cs as id_cs',
                'd_itemcs.nourut as nourut',
                'd_itemcs.nospd as nospd',
                'd_itemcs.nama as nama',
                'd_itemcs.nip as nip',
                'd_itemcs.tglberangkat as tglberangkat',
                'd_itemcs.tglkembali as tglkembali',
                'd_itemcs.jmlhari as jmlhari',
                'd_itemcs.jumlah as jumlah',
                'd_itemcs.is_aktif as is_aktif')
            ->join('d_itemcs','t_sima_st.id_st','=','d_itemcs.id_st')
            ->where('t_sima_st.kdsatker', 604435)
            ->get();

        return json_encode($bisma);             
    }

}
