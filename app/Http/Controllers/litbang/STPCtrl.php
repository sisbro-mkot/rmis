<?php

namespace App\Http\Controllers\litbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;
use App\Models\litbang\STPegawai;



class STPCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $now = Carbon::today();

        $eselon3 = DB::connection('dblitbang')
            ->table('ref_bidang')
            ->select('ref_bidang.id_bidang as id',
                'ref_bidang.nama_bidang as nama_bidang'
                )
            ->get();

        return view('1litbang.stpegawai', compact('now','eselon3'));
    }

    public function semuaSTP($mulai, $selesai) 
    {  
        $tugas = DB::connection('dblitbang')
            ->table('ref_pegawai')
            ->selectRaw('ref_pegawai.id_pegawai as id,
                ref_pegawai.nipbaru AS nipbaru,
                ref_pegawai.s_nama_lengkap AS nama,
                ref_pegawai.jabatan AS jabatan,
                COUNT(t_st.id_st) AS jumlah,
                SUM(t_st.jml_hari) AS hari,
                COUNT(t_st.id_st) AS detil'
                )
            ->leftjoin('t_st_pegawai', 'ref_pegawai.id_pegawai', '=', 't_st_pegawai.id_pegawai')
            ->leftjoin('t_st', function ($join) use ($mulai, $selesai) {
                    $join->on('t_st_pegawai.id_st', '=', 't_st.id_st')
                         ->where('t_st.tgl_mulai', '<=', $selesai)
                         ->where('t_st.tgl_selesai', '>=', $mulai);
                })
            ->where('ref_pegawai.status','=', 'litbang')
            ->where('ref_pegawai.niplama','<>', 'THL')
            ->groupBy('ref_pegawai.id_pegawai')
            ->orderBy('nama')
            ->get();
 
        return json_encode($tugas);
    }

    public function pilihSTP($es3, $mulai, $selesai) 
    {  
        $tugas = DB::connection('dblitbang')
            ->table('ref_pegawai')
            ->selectRaw('ref_pegawai.id_pegawai as id,
                ref_pegawai.nipbaru AS nipbaru,
                ref_pegawai.s_nama_lengkap AS nama,
                ref_pegawai.jabatan AS jabatan,
                COUNT(t_st.id_st) AS jumlah,
                SUM(t_st.jml_hari) AS hari,
                COUNT(t_st.id_st) AS detil'
                )
            ->leftjoin('t_st_pegawai', 'ref_pegawai.id_pegawai', '=', 't_st_pegawai.id_pegawai')
            ->leftjoin('t_st', function ($join) use ($mulai, $selesai) {
                    $join->on('t_st_pegawai.id_st', '=', 't_st.id_st')
                         ->where('t_st.tgl_mulai', '<=', $selesai)
                         ->where('t_st.tgl_selesai', '>=', $mulai);
                })
            ->where('ref_pegawai.status','=', 'litbang')
            ->where('ref_pegawai.id_bidang','=', $es3)
            ->where('ref_pegawai.niplama','<>', 'THL')
            ->groupBy('ref_pegawai.id_pegawai')
            ->orderBy('nama')
            ->get();

        return json_encode($tugas);
    }

    public function showSTP($id, $mulai, $selesai) 
    {  
        $tugas = DB::connection('dblitbang')
            ->table('t_st_pegawai')
            ->selectRaw('t_st_pegawai.id_st_pegawai as no,
                t_st.no_st as nost,
                DATE_FORMAT(t_st.tgl_st,"%d/%m/%Y") as tglst,
                t_st.uraian_st as urst,
                DATE_FORMAT(t_st.tgl_mulai,"%d/%m/%Y") as mulai,
                DATE_FORMAT(t_st.tgl_selesai,"%d/%m/%Y") as selesai,
                t_st.jml_hari as hari'
                )
            ->join('ref_pegawai', 't_st_pegawai.id_pegawai', '=', 'ref_pegawai.id_pegawai')
            ->join('t_st', function ($join) use ($mulai, $selesai) {
                    $join->on('t_st_pegawai.id_st', '=', 't_st.id_st')
                         ->where('t_st.tgl_mulai', '<=', $selesai)
                         ->where('t_st.tgl_selesai', '>=', $mulai);
                })
            ->where('t_st_pegawai.id_pegawai','=', $id)
            ->get();

        return json_encode($tugas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tambah($id)
    {
        
        $this->data['id_st_label'] = DB::connection('dblitbang')
            ->table('t_st')
            ->select('t_st.id_st as id',
                'ref_bidang.nama_bidang as nama_bidang',
                't_st.no_st as no_st',
                't_st.tgl_st as tgl_st',
                't_st.uraian_st as uraian_st',
                't_st.tgl_mulai as tgl_mulai',
                't_st.tgl_selesai as tgl_selesai',
                'ref_aktivitas.kode_aktivitas as kode_aktivitas',
                'ref_aktivitas.nama_aktivitas as nama_aktivitas',
                'ref_detil_aktivitas.nama_detil_aktivitas as nama_detil_aktivitas'
                )
            ->leftjoin('ref_detil_aktivitas', 't_st.id_detil_aktivitas', '=', 'ref_detil_aktivitas.id_detil_aktivitas')
            ->leftjoin('ref_aktivitas', 'ref_detil_aktivitas.id_aktivitas', '=', 'ref_aktivitas.id_aktivitas')
            ->leftjoin('ref_bidang', 't_st.id_bidang', '=', 'ref_bidang.id_bidang')
            ->where('t_st.id_st', $id)
            ->first();
        $this->data['id_st'] = DB::connection('dblitbang')
            ->table('t_st')
            ->select('t_st.id_st as id',
                't_st.no_st as no_st',
                't_st.tgl_st as tgl_st',
                't_st.uraian_st as uraian_st',
                't_st.tgl_mulai as tgl_mulai',
                't_st.tgl_selesai as tgl_selesai'
                )
            ->where('t_st.id_st', $id)
            ->get();
        $this->data['id_pegawai'] = DB::connection('dblitbang')
            ->table('ref_pegawai')
            ->select('ref_pegawai.id_pegawai as id',
                'ref_pegawai.nipbaru as nipbaru',
                'ref_pegawai.s_nama_lengkap as s_nama_lengkap'
                )
            ->leftjoin('t_st_pegawai', function ($join) use ($id) {
                    $join->on('ref_pegawai.id_pegawai', '=', 't_st_pegawai.id_pegawai')
                         ->where('t_st_pegawai.id_st', '=', $id);
                })
            ->whereNull('t_st_pegawai.id_pegawai')
            ->orderBy('s_nama_lengkap')
            ->get();
        $this->data['id_st_pegawai'] = DB::connection('dblitbang')
            ->table('t_st_pegawai')
            ->select('t_st_pegawai.id_st_pegawai as id',
                'ref_pegawai.nipbaru as nipbaru',
                'ref_pegawai.s_nama_lengkap as s_nama_lengkap',
                'ref_pegawai.jabatan as jabatan'
                )
            ->join('ref_pegawai', 't_st_pegawai.id_pegawai', '=', 'ref_pegawai.id_pegawai')
            ->where('t_st_pegawai.id_st', '=', $id)
            ->get(); 

        return view('1litbang.stptambah', $this->data);              
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()){

            $rules = array(

                'id_st' => 'required',
                'id_pegawai' => 'required'

            );
            $messages = [
                            'id_st.required' => 'Silahkan pilih ST.',
                            'id_pegawai.required' => 'Silahkan pilih pegawai.'

            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            } else {

                $id = new STPegawai;
                $id->id_st = Input::get('id_st');
                $id->id_pegawai = Input::get('id_pegawai');
                $id->user_create = Auth::user()->user_nip;

                $id->save();

                $pegawai = DB::connection('dblitbang')
                            ->table('t_st')
                            ->select('t_st.id_st as id_st')
                            ->where('t_st.id_st', '=', $id->id_st)
                            ->first(); 
                Alert::success('Data telah ditambahkan.', 'Selamat');

                return redirect()->route('stptambah', $pegawai->id_st);
                // return redirect()->back();

            }
        } else {
            return redirect()->back();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = STPegawai::findOrFail($id);

        $hapus->delete();

        return redirect('stugas');
    }

    

}
