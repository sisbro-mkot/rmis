<?php

namespace App\Http\Controllers\litbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;



class AktivitasCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $aktivitas = DB::connection('dblitbang')
            ->table('ref_pkau')
            ->select('ref_pkau.id_pkau as id',
                'ref_pkau.kode_pkau as kode_pkau',
                'ref_pkau.nama_pkau as nama_pkau',
                'jml_st.count_st as count_st',
                'mapping.nilai_pkau as nilai_pkau'
                )
            ->leftjoin(DB::connection('dblitbang')->raw('(SELECT id_pkau, COUNT(id_st) AS count_st FROM `t_st` GROUP BY id_pkau) AS jml_st'), 
                            function($join)
                            {
                               $join->on('ref_pkau.id_pkau', '=', 'jml_st.id_pkau');
                            })
            ->leftjoin(DB::connection('dblitbang')->raw('(SELECT id_pkau, SUM(nilai) AS nilai_pkau FROM `t_mapping` GROUP BY id_pkau) AS mapping'), 
                            function($join)
                            {
                               $join->on('ref_pkau.id_pkau', '=', 'mapping.id_pkau');
                            })
            ->get();

        return view('1litbang.aktivitas', compact('aktivitas'));

    }

    public function showST($id) 
    {  
        $control = DB::connection('dblitbang')->table('t_st')
                ->selectRaw('t_st.id_st as nomor,
                    t_st.no_st as nost,
                    DATE_FORMAT(t_st.tgl_st,"%d %b %Y") as tglst,
                    t_st.uraian_st as uraianst,
                    DATE_FORMAT(t_st.tgl_mulai,"%d %b %Y") as tglmulai,
                    DATE_FORMAT(t_st.tgl_selesai,"%d %b %Y") as tglselesai'
                )
                ->join('ref_pkau', 't_st.id_pkau', '=', 'ref_pkau.id_pkau')
                ->where('ref_pkau.id_pkau', $id)
                ->get();
        return json_encode($control);
    }

 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //             
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getDashLitbang()
    {

        return view('1litbang.dashboard');

    }

}
