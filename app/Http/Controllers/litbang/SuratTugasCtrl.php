<?php

namespace App\Http\Controllers\litbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;
use App\Models\litbang\SuratTugas;



class SuratTugasCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $tugas = DB::connection('dblitbang')
            ->table('t_st')
            ->selectRaw('t_st.id_st as id,
                t_st.no_st as no_st,
                t_st.tgl_st as tgl_st,
                t_st.uraian_st as uraian_st,
                t_st.tgl_mulai as tgl_mulai,
                t_st.tgl_selesai as tgl_selesai,
                t_st.jml_hari as jml_hari,
                ref_pkau.kode_pkau as kode_pkau,
                count(t_st_pegawai.id_st) as jml_pegawai,
                t_st.progress as progress,
                sum(t_spj.nilai_spj) as nilai_spj'
                )
            ->leftjoin('ref_pkau', 't_st.id_pkau', '=', 'ref_pkau.id_pkau')
            ->leftjoin('t_st_pegawai', 't_st.id_st', '=', 't_st_pegawai.id_st')
            ->leftjoin('t_spj', 't_st.id_st', '=', 't_spj.id_st')
            ->where('t_st.id_jns_st', 1)
            ->groupBy('t_st.id_st')
            ->orderBy('t_st.id_st', 'desc')
            ->get();

        return view('1litbang.stugas', compact('tugas'));

    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $this->data['id_bidang'] = DB::connection('dblitbang')
            ->table('ref_bidang')
            ->select('ref_bidang.id_bidang as id_bidang','ref_bidang.nama_bidang as nama_bidang','ref_bidang.kd_konsep as kd_konsep')
            ->get();
        $this->data['id_pkau'] = DB::connection('dblitbang')
            ->table('ref_pkau')
            ->select('ref_pkau.id_pkau as id_pkau',
                'ref_pkau.nama_pkau as nama_pkau',
                'ref_pkau.kode_pkau as kode_pkau'
                )
            ->get(); 

        return view('1litbang.stugascreate', $this->data);              
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()){

            $rules = array(

                'id_bidang' => 'required',
                'no_st' => 'required',
                'tgl_st' => 'required',
                'uraian_st' => 'required',
                'tgl_mulai' => 'required',
                'tgl_selesai' => 'required',
                'jml_hari' => 'required'

            );
            $messages = [
                            'id_bidang.required' => 'Silahkan pilih Konseptor ST.',
                            'no_st.required' => 'Silahkan isi Nomor ST.',
                            'tgl_st.required' => 'Silahkan isi Tanggal ST.',
                            'uraian_st.required' => 'Silahkan isi Uraian ST.',
                            'tgl_mulai.required' => 'Silahkan isi Tanggal Mulai.',
                            'tgl_selesai.required' => 'Silahkan isi Tanggal Selesai.',
                            'jml_hari.required' => 'Silahkan isi Jumlah Hari.'

            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            } else {

                $id = new SuratTugas;
                $id->id_jns_st = 1;
                $id->id_bidang = Input::get('id_bidang');
                $id->no_st = Input::get('no_st');
                $id->tgl_st = Input::get('tgl_st');
                $id->uraian_st = Input::get('uraian_st');
                $id->tgl_mulai = Input::get('tgl_mulai');
                $id->tgl_selesai = Input::get('tgl_selesai');
                $id->jml_hari = Input::get('jml_hari');
                $id->id_pkau = Input::get('id_pkau');
                $id->status_st = 1;
                $id->user_create = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('stugas.index');

            }
        } else {
            return redirect()->back();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $anggaran = DB::connection('dblitbang')
            ->table('t_st')
            ->select('t_st.id_st as id',
                'ref_bidang.nama_bidang as nama_bidang',
                't_st.no_st as no_st',
                't_st.tgl_st as tgl_st',
                't_st.uraian_st as uraian_st',
                't_st.tgl_mulai as tgl_mulai',
                't_st.tgl_selesai as tgl_selesai',
                't_st.jml_hari as jml_hari',
                'ref_pkau.kode_pkau as kode_pkau',
                'ref_pkau.nama_pkau as nama_pkau',
                't_st.progress as progress'
                )
            ->join('ref_bidang', 't_st.id_bidang', '=', 'ref_bidang.id_bidang')
            ->leftjoin('ref_pkau', 't_st.id_pkau', '=', 'ref_pkau.id_pkau')          
            ->where('t_st.id_st', $id)
            ->first();
        $pegawai  = DB::connection('dblitbang')
            ->table('t_st_pegawai')
            ->select('t_st_pegawai.id_st_pegawai as id',
                'ref_pegawai.nipbaru as nipbaru',
                'ref_pegawai.s_nama_lengkap as s_nama_lengkap',
                'ref_pegawai.jabatan as jabatan'
                )
            ->join('ref_pegawai', 't_st_pegawai.id_pegawai', '=', 'ref_pegawai.id_pegawai')
            ->where('t_st_pegawai.id_st', '=', $id)
            ->get();

        return view('1litbang.stugasshow', compact('anggaran','pegawai'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['stugas'] = SuratTugas::find($id);
        $this->data['id_bidang'] = DB::connection('dblitbang')
            ->table('ref_bidang')
            ->select('ref_bidang.id_bidang as id_bidang','ref_bidang.nama_bidang as nama_bidang','ref_bidang.kd_konsep as kd_konsep')
            ->get();
        $this->data['id_pkau'] = DB::connection('dblitbang')
            ->table('ref_pkau')
            ->select('ref_pkau.id_pkau as id_pkau',
                'ref_pkau.nama_pkau as nama_pkau',
                'ref_pkau.kode_pkau as kode_pkau'
                )
            ->get(); 


        return view('1litbang.stugasedit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check()){

            $rules = array(

                'id_bidang' => 'required',
                'no_st' => 'required',
                'tgl_st' => 'required',
                'uraian_st' => 'required',
                'tgl_mulai' => 'required',
                'tgl_selesai' => 'required',
                'jml_hari' => 'required'

            );
            $messages = [
                            'id_bidang.required' => 'Silahkan pilih Konseptor ST.',
                            'no_st.required' => 'Silahkan isi Nomor ST.',
                            'tgl_st.required' => 'Silahkan isi Tanggal ST.',
                            'uraian_st.required' => 'Silahkan isi Uraian ST.',
                            'tgl_mulai.required' => 'Silahkan isi Tanggal Mulai.',
                            'tgl_selesai.required' => 'Silahkan isi Tanggal Selesai.',
                            'jml_hari.required' => 'Silahkan isi Jumlah Hari.'

            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            } else {

                $now = Carbon::now('Asia/Jakarta');
                $id = SuratTugas::find($id);
                $id->id_jns_st = 1;
                $id->id_bidang = Input::get('id_bidang');
                $id->no_st = Input::get('no_st');
                $id->tgl_st = Input::get('tgl_st');
                $id->uraian_st = Input::get('uraian_st');
                $id->tgl_mulai = Input::get('tgl_mulai');
                $id->tgl_selesai = Input::get('tgl_selesai');
                $id->jml_hari = Input::get('jml_hari');
                $id->id_pkau = Input::get('id_pkau');
                $id->progress = Input::get('progress');
                $id->status_st = 1;
                $id->updated_at = $now;
                $id->user_update = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah diubah.', 'Selamat');
                return redirect()->route('stugas.index');

            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getDashLitbang()
    {

        return view('1litbang.dashboard');

    }

}
