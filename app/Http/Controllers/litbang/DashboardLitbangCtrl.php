<?php

namespace App\Http\Controllers\litbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;



class DashboardLitbangCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $tugas = DB::connection('dblitbang')
            ->table('t_st')
            ->select('t_st.id_st as id',
                't_st.no_st as no_st',
                't_st.tgl_st as tgl_st',
                't_st.uraian_st as uraian_st',
                't_st.tgl_mulai as tgl_mulai',
                't_st.tgl_selesai as tgl_selesai',
                'ref_aktivitas.kode_aktivitas as kode_aktivitas'
                )
            ->leftjoin('ref_detil_aktivitas', 't_st.id_detil_aktivitas', '=', 'ref_detil_aktivitas.id_detil_aktivitas')
            ->leftjoin('ref_aktivitas', 'ref_detil_aktivitas.id_aktivitas', '=', 'ref_aktivitas.id_aktivitas')
            ->get();

        return view('lini3perencanaan.index', compact('perencanaan'));

    }

    public function bismadan()
    {
        $bisma = DB::connection('dbbisma')
        ->select("
        SELECT a.*,CONCAT(a.kdakun,'-',a.nmakun) AS akun,
        IFNULL(b.outstand, 0) + IFNULL(e.outstand, 0) + IFNULL(h.outstand, 0) AS outstand, 
        IFNULL(c.draft, 0) + IFNULL(f.draft, 0) + IFNULL(i.draft, 0) AS draft, 
        IFNULL(d.realisasi, 0) + IFNULL(g.realisasi, 0) + IFNULL(j.realisasi, 0) AS realisasi, 
        b.pagu_index

        FROM (SELECT d_pagu.*, t_akun.nmakun AS nmakun
        FROM d_pagu 
        LEFT JOIN d_bagipagu ON d_pagu.kdindex = d_bagipagu.kdindex 
        JOIN t_akun ON d_pagu.kdakun = t_akun.kdakun 

        WHERE (d_bagipagu.kdgiat IN ('4221','4222','4223','4224','4225') OR (d_bagipagu.kdgiat='3667' AND d_bagipagu.ppk_id=236) OR (d_bagipagu.kdgiat='3676' AND d_bagipagu.ppk_id=236))
           AND d_pagu.thang = '2022' ORDER BY d_pagu.kdindex) AS a 
        
        
        LEFT JOIN (SELECT SUM(d_costsheet.biaya) AS outstand, d_costsheet.kdindex AS pagu_index 
        FROM d_surattugas 
        RIGHT JOIN d_costsheet ON d_costsheet.id_st = d_surattugas.id_st
        WHERE d_surattugas.is_aktif = 1 AND d_costsheet.status_cs IN (1,2,3,12,4) AND d_surattugas.from_data_st=d_costsheet.from_data 
        AND d_surattugas.sumber_data=d_costsheet.sumber_data
        GROUP BY d_costsheet.kdindex) AS b 
        ON a.kdindex = b.pagu_index

        LEFT JOIN (SELECT SUM(d_costsheet.biaya) AS draft, d_costsheet.kdindex AS pagu_index 
        FROM d_surattugas 
        JOIN d_costsheet ON d_costsheet.id_st = d_surattugas.id_st
        WHERE d_surattugas.is_aktif = 1 AND d_costsheet.status_cs = 11 AND d_surattugas.from_data_st=d_costsheet.from_data 
        AND d_surattugas.sumber_data=d_costsheet.sumber_data
        GROUP BY d_costsheet.kdindex) AS c 
        ON a.kdindex = c.pagu_index


        LEFT JOIN (SELECT SUM(d_costsheet.biaya) AS realisasi, d_costsheet.kdindex AS pagu_index 
        FROM d_surattugas
        JOIN d_costsheet ON d_costsheet.id_st = d_surattugas.id_st
        WHERE d_surattugas.is_aktif = 1 AND d_costsheet.status_cs IN (5,6,7,8,9) AND d_surattugas.from_data_st=d_costsheet.from_data 
        AND d_surattugas.sumber_data=d_costsheet.sumber_data        
        GROUP BY d_costsheet.kdindex) AS d
        ON a.kdindex = d.pagu_index 
        
        LEFT JOIN (SELECT SUM(t_permintaan_pbj.jumlah_uang) AS outstand, t_permintaan_pbj.kdindex AS pagu_index FROM t_permintaan_pbj
        WHERE t_permintaan_pbj.tahun_anggaran = '2022' AND t_permintaan_pbj.status IN  (1,2,3,12,4)
        GROUP BY t_permintaan_pbj.kdindex) AS e
        ON REPLACE(a.kdindex, ' ', '') = e.pagu_index
        
        LEFT JOIN (SELECT SUM(t_permintaan_pbj.jumlah_uang) AS draft, t_permintaan_pbj.kdindex AS pagu_index FROM t_permintaan_pbj
        WHERE t_permintaan_pbj.tahun_anggaran = '2022' AND  t_permintaan_pbj.status = 11
        GROUP BY t_permintaan_pbj.kdindex) AS f
        ON REPLACE(a.kdindex, ' ', '') = f.pagu_index
        
        LEFT JOIN (SELECT SUM(t_permintaan_pbj.jumlah_uang) AS realisasi, t_permintaan_pbj.kdindex AS pagu_index FROM t_permintaan_pbj
        WHERE t_permintaan_pbj.tahun_anggaran = '2022' AND t_permintaan_pbj.status IN  (5,6,7,8,9)
        GROUP BY t_permintaan_pbj.kdindex) AS g
        ON REPLACE(a.kdindex, ' ', '') = g.pagu_index

        LEFT JOIN (SELECT SUM(t_gaji_detail.nilai) AS outstand, t_gaji_detail.kdindex AS pagu_index FROM t_gaji 
        RIGHT JOIN t_gaji_detail ON t_gaji_detail.gaji_id = t_gaji.id 
        WHERE t_gaji.status IN (1, 2, 3, 12, 4) AND t_gaji.thang = '2022' 
        GROUP BY t_gaji_detail.kdindex) AS h  
        ON REPLACE(a.kdindex, ' ', '') = h.pagu_index 

        LEFT JOIN (SELECT SUM(t_gaji_detail.nilai) AS draft, t_gaji_detail.kdindex AS pagu_index FROM t_gaji 
        RIGHT JOIN t_gaji_detail ON t_gaji_detail.gaji_id = t_gaji.id 
        WHERE t_gaji.status = 11 AND t_gaji.thang = '2022'
        GROUP BY t_gaji_detail.kdindex) AS i  
        ON REPLACE(a.kdindex, ' ', '') = i.pagu_index 

        LEFT JOIN (SELECT SUM(t_gaji_detail.nilai) AS realisasi, t_gaji_detail.kdindex AS pagu_index FROM t_gaji 
        RIGHT JOIN t_gaji_detail ON t_gaji_detail.gaji_id = t_gaji.id 
        WHERE t_gaji.status IN  (5,6,7,8,9) AND t_gaji.thang = '2022'
        GROUP BY t_gaji_detail.kdindex) AS j  
        ON REPLACE(a.kdindex, ' ', '') = j.pagu_index 
    

        
        ORDER BY a.kdprogram, a.kdgiat, a.kdoutput, a.kdsoutput, a.kdkmpnen, a.kdskmpnen, a.kdakun ASC


            ");

        return json_encode($bisma);             
    }

    public function danhome()
    {
        $bisma = DB::connection('dbbisma')
        ->select("

        SELECT k.kdgiat as kegiatan,
        sum(k.rupiah) as t_anggaran,
        (sum(k.realisasi)+sum(k.outstand)) as t_realisasi,
        sum(k.rupiah)-sum(k.realisasi)-sum(k.outstand) as sisa,
        (sum(k.realisasi)+sum(k.outstand))/sum(k.rupiah)*100 as penyerapan

        FROM

        (SELECT a.*,CONCAT(a.kdakun,'-',a.nmakun) AS akun,
        IFNULL(b.outstand, 0) + IFNULL(e.outstand, 0) + IFNULL(h.outstand, 0) AS outstand, 
        IFNULL(c.draft, 0) + IFNULL(f.draft, 0) + IFNULL(i.draft, 0) AS draft, 
        IFNULL(d.realisasi, 0) + IFNULL(g.realisasi, 0) + IFNULL(j.realisasi, 0) AS realisasi, 
        b.pagu_index

        FROM (SELECT d_pagu.*, t_akun.nmakun AS nmakun
        FROM d_pagu 
        LEFT JOIN d_bagipagu ON d_pagu.kdindex = d_bagipagu.kdindex 
        JOIN t_akun ON d_pagu.kdakun = t_akun.kdakun 

        WHERE (d_bagipagu.kdgiat IN ('4221','4222','4223','4224','4225') OR (d_bagipagu.kdgiat='3667' AND d_bagipagu.ppk_id=236) OR (d_bagipagu.kdgiat='3676' AND d_bagipagu.ppk_id=236))
           AND d_pagu.thang = '2022' ORDER BY d_pagu.kdindex) AS a 
        
        
        LEFT JOIN (SELECT SUM(d_costsheet.biaya) AS outstand, d_costsheet.kdindex AS pagu_index 
        FROM d_surattugas 
        RIGHT JOIN d_costsheet ON d_costsheet.id_st = d_surattugas.id_st
        WHERE d_surattugas.is_aktif = 1 AND d_costsheet.status_cs IN (1,2,3,12,4) AND d_surattugas.from_data_st=d_costsheet.from_data 
        AND d_surattugas.sumber_data=d_costsheet.sumber_data
        GROUP BY d_costsheet.kdindex) AS b 
        ON a.kdindex = b.pagu_index

        LEFT JOIN (SELECT SUM(d_costsheet.biaya) AS draft, d_costsheet.kdindex AS pagu_index 
        FROM d_surattugas 
        JOIN d_costsheet ON d_costsheet.id_st = d_surattugas.id_st
        WHERE d_surattugas.is_aktif = 1 AND d_costsheet.status_cs = 11 AND d_surattugas.from_data_st=d_costsheet.from_data 
        AND d_surattugas.sumber_data=d_costsheet.sumber_data
        GROUP BY d_costsheet.kdindex) AS c 
        ON a.kdindex = c.pagu_index


        LEFT JOIN (SELECT SUM(d_costsheet.biaya) AS realisasi, d_costsheet.kdindex AS pagu_index 
        FROM d_surattugas
        JOIN d_costsheet ON d_costsheet.id_st = d_surattugas.id_st
        WHERE d_surattugas.is_aktif = 1 AND d_costsheet.status_cs IN (5,6,7,8,9) AND d_surattugas.from_data_st=d_costsheet.from_data 
        AND d_surattugas.sumber_data=d_costsheet.sumber_data        
        GROUP BY d_costsheet.kdindex) AS d
        ON a.kdindex = d.pagu_index 
        
        LEFT JOIN (SELECT SUM(t_permintaan_pbj.jumlah_uang) AS outstand, t_permintaan_pbj.kdindex AS pagu_index FROM t_permintaan_pbj
        WHERE t_permintaan_pbj.tahun_anggaran = '2022' AND t_permintaan_pbj.status IN  (1,2,3,12,4)
        GROUP BY t_permintaan_pbj.kdindex) AS e
        ON REPLACE(a.kdindex, ' ', '') = e.pagu_index
        
        LEFT JOIN (SELECT SUM(t_permintaan_pbj.jumlah_uang) AS draft, t_permintaan_pbj.kdindex AS pagu_index FROM t_permintaan_pbj
        WHERE t_permintaan_pbj.tahun_anggaran = '2022' AND  t_permintaan_pbj.status = 11
        GROUP BY t_permintaan_pbj.kdindex) AS f
        ON REPLACE(a.kdindex, ' ', '') = f.pagu_index
        
        LEFT JOIN (SELECT SUM(t_permintaan_pbj.jumlah_uang) AS realisasi, t_permintaan_pbj.kdindex AS pagu_index FROM t_permintaan_pbj
        WHERE t_permintaan_pbj.tahun_anggaran = '2022' AND t_permintaan_pbj.status IN  (5,6,7,8,9)
        GROUP BY t_permintaan_pbj.kdindex) AS g
        ON REPLACE(a.kdindex, ' ', '') = g.pagu_index

        LEFT JOIN (SELECT SUM(t_gaji_detail.nilai) AS outstand, t_gaji_detail.kdindex AS pagu_index FROM t_gaji 
        RIGHT JOIN t_gaji_detail ON t_gaji_detail.gaji_id = t_gaji.id 
        WHERE t_gaji.status IN (1, 2, 3, 12, 4) AND t_gaji.thang = '2022' 
        GROUP BY t_gaji_detail.kdindex) AS h  
        ON REPLACE(a.kdindex, ' ', '') = h.pagu_index 

        LEFT JOIN (SELECT SUM(t_gaji_detail.nilai) AS draft, t_gaji_detail.kdindex AS pagu_index FROM t_gaji 
        RIGHT JOIN t_gaji_detail ON t_gaji_detail.gaji_id = t_gaji.id 
        WHERE t_gaji.status = 11 AND t_gaji.thang = '2022'
        GROUP BY t_gaji_detail.kdindex) AS i  
        ON REPLACE(a.kdindex, ' ', '') = i.pagu_index 

        LEFT JOIN (SELECT SUM(t_gaji_detail.nilai) AS realisasi, t_gaji_detail.kdindex AS pagu_index FROM t_gaji 
        RIGHT JOIN t_gaji_detail ON t_gaji_detail.gaji_id = t_gaji.id 
        WHERE t_gaji.status IN  (5,6,7,8,9) AND t_gaji.thang = '2022'
        GROUP BY t_gaji_detail.kdindex) AS j  
        ON REPLACE(a.kdindex, ' ', '') = j.pagu_index
        
        ORDER BY a.kdprogram, a.kdgiat, a.kdoutput, a.kdsoutput, a.kdkmpnen, a.kdskmpnen, a.kdakun ASC) AS k
        
        GROUP BY k.kdgiat

            ");

        return json_encode($bisma);             
    }

 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //             
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getDashLitbang()
    {
        
 
        $ikk1 = DB::connection('dblitbang')
            ->table('t_penelitian')
            ->where('t_penelitian.id_ikk', '=', 1)
            ->whereYear('t_penelitian.tgl_penelitian', Carbon::now()->year)
            ->get()
            ->count();

        $lap1 = DB::connection('dblitbang')
            ->table('t_penelitian')
            ->where('t_penelitian.id_ikk', '=', 1)
            ->whereYear('t_penelitian.tgl_penelitian', Carbon::now()->year)
            ->get();

        $ikk2 = DB::connection('dblitbang')
            ->table('t_penelitian')
            ->where('t_penelitian.id_ikk', '=', 2)
            ->whereYear('t_penelitian.tgl_penelitian', Carbon::now()->year)
            ->get()
            ->count();

        $lap2 = DB::connection('dblitbang')
            ->table('t_penelitian')
            ->where('t_penelitian.id_ikk', '=', 2)
            ->whereYear('t_penelitian.tgl_penelitian', Carbon::now()->year)
            ->get();

        $ikk3 = DB::connection('dblitbang')
            ->table('t_penelitian')
            ->where('t_penelitian.id_ikk', '=', 3)
            ->whereYear('t_penelitian.tgl_penelitian', Carbon::now()->year)
            ->get()
            ->count();

        $lap3 = DB::connection('dblitbang')
            ->table('t_penelitian')
            ->where('t_penelitian.id_ikk', '=', 3)
            ->whereYear('t_penelitian.tgl_penelitian', Carbon::now()->year)
            ->get();

        $ikk4 = DB::connection('dblitbang')
            ->table('t_penelitian')
            ->where('t_penelitian.id_ikk', '=', 4)
            ->whereYear('t_penelitian.tgl_penelitian', Carbon::now()->year)
            ->get()
            ->count();

        $lap4 = DB::connection('dblitbang')
            ->table('t_penelitian')
            ->where('t_penelitian.id_ikk', '=', 4)
            ->whereYear('t_penelitian.tgl_penelitian', Carbon::now()->year)
            ->get();

        $now = Carbon::today();

        $pegawai = DB::connection('dblitbang')
            ->table('ref_bidang')
            ->selectRaw('ref_bidang.id_bidang as id,
                ref_bidang.nama_bidang AS nama_bidang,
                COUNT(t_st.id_st) AS jml_st'
                )
            ->leftjoin('t_st', 'ref_bidang.id_bidang', '=', 't_st.id_bidang')
            ->where('t_st.tgl_mulai', '<=', $now)
            ->where('t_st.tgl_selesai', '>=', $now)
            ->groupBy('ref_bidang.id_bidang')
            ->orderBy('ref_bidang.ket_bidang')
            ->get();

        $anggaranch = DB::connection('dblitbang')
            ->table('t_mapping')
            ->selectRaw('SUM(t_mapping.nilai) AS anggaranb')
            ->join('ref_pkau', 't_mapping.id_pkau', '=', 'ref_pkau.id_pkau')
            ->join('ref_bidang', 'ref_pkau.id_bidang', '=', 'ref_bidang.id_bidang')
            ->groupBy('ref_pkau.id_bidang')
            ->orderBy('ref_bidang.ket_bidang')
            ->get();

        $realisasich = DB::connection('dblitbang')
            ->table('t_mapping')
            ->selectRaw('SUM(t_spj.nilai_spj) AS realisasib')
            ->join('ref_pkau', 't_mapping.id_pkau', '=', 'ref_pkau.id_pkau')
            ->join('ref_bidang', 'ref_pkau.id_bidang', '=', 'ref_bidang.id_bidang')
            ->join('t_spj', 't_mapping.id_mapping', '=', 't_spj.id_mapping')
            ->groupBy('ref_pkau.id_bidang')
            ->orderBy('ref_bidang.ket_bidang')
            ->get();

        $tu = DB::connection('dblitbang')
            ->table('t_st')
            ->select()
            ->where('t_st.tgl_mulai', '<=', $now)
            ->where('t_st.tgl_selesai', '>=', $now)
            ->where('t_st.id_bidang', '=', 1)
            ->get();

        $b1 = DB::connection('dblitbang')
            ->table('t_st')
            ->select()
            ->where('t_st.tgl_mulai', '<=', $now)
            ->where('t_st.tgl_selesai', '>=', $now)
            ->where('t_st.id_bidang', '=', 2)
            ->get();

        $b2 = DB::connection('dblitbang')
            ->table('t_st')
            ->select()
            ->where('t_st.tgl_mulai', '<=', $now)
            ->where('t_st.tgl_selesai', '>=', $now)
            ->where('t_st.id_bidang', '=', 3)
            ->get();

        
        return view('1litbang.dashboard', compact('ikk1', 'ikk2','ikk3', 'ikk4', 'lap1', 'lap2', 'lap3', 'lap4', 'pegawai', 'anggaranch', 'realisasich', 'now', 'tu', 'b1', 'b2'));

    }

}
