<?php

namespace App\Http\Controllers\litbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;
use App\Models\litbang\Mapping;



class AnggaranCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $anggaran = DB::connection('dblitbang')
            ->table('ref_dja')
            ->selectRaw('ref_dja.id as id, ref_dja.mak as mak, ref_dja.nmitem as nmitem, ref_dja.jumlah as jumlah, sum(t_mapping.nilai) as nilai'
                )
            ->leftjoin('t_mapping', 'ref_dja.id', '=', 't_mapping.id_dja')
            ->groupBy('ref_dja.id')
            ->get();
        $total = DB::connection('dblitbang')
            ->table('ref_dja')
            ->sum('ref_dja.jumlah');
        $nilai = DB::connection('dblitbang')
            ->table('t_mapping')
            ->sum('t_mapping.nilai');
 
        return view('1litbang.anggaran', compact('anggaran','total','nilai'));

    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //             
    }

    public function tambah($id)
    {
        $this->data['id_dja_label'] = DB::connection('dblitbang')
            ->table('ref_dja')
            ->selectRaw('ref_dja.id as id, ref_dja.mak as mak, ref_dja.nmitem as nmitem, ref_dja.nmitem as nmitem, ref_dja.jumlah as jumlah, sum(t_mapping.nilai) as nilai'
                )
            ->leftjoin('t_mapping', 'ref_dja.id', '=', 't_mapping.id_dja')
            ->groupBy('ref_dja.id')
            ->where('ref_dja.id', $id)
            ->first();
        $this->data['id_dja'] = DB::connection('dblitbang')
            ->table('ref_dja')
            ->select('ref_dja.id as id',
                'ref_dja.mak as mak',
                'ref_dja.jumlah as jumlah'
                )
            ->where('ref_dja.id', $id)
            ->get();
        $this->data['id_pkau'] = DB::connection('dblitbang')
            ->table('ref_pkau')
            ->select('ref_pkau.id_pkau as id',
                'ref_pkau.kode_pkau as kode_pkau',
                'ref_pkau.nama_pkau as nama_pkau'
                )
            ->leftjoin('t_mapping', function ($join) use ($id) {
                    $join->on('ref_pkau.id_pkau', '=', 't_mapping.id_pkau')
                         ->where('t_mapping.id_dja', '=', $id);
                })
            ->whereNull('t_mapping.id_pkau')
            ->orderBy('id')
            ->get(); 
        $this->data['id_mapping'] = DB::connection('dblitbang')
            ->table('t_mapping')
            ->select('t_mapping.id_mapping as id',
                'ref_pkau.kode_pkau as kode_pkau',
                'ref_pkau.nama_pkau as nama_pkau',
                't_mapping.nilai as nilai'
                )
            ->join('ref_pkau', 't_mapping.id_pkau', '=', 'ref_pkau.id_pkau')
            ->where('t_mapping.id_dja', '=', $id)
            ->get();

        return view('1litbang.mappingtambah', $this->data);             
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()){

            $rules = array(

                'id_dja' => 'required',
                'id_pkau' => 'required'

            );
            $messages = [
                            'id_dja.required' => 'Silahkan pilih Anggaran.',
                            'id_pkau.required' => 'Silahkan pilih PKAU.'

            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            } else {

                $id = new Mapping;
                $id->id_dja = Input::get('id_dja');
                $id->id_pkau = Input::get('id_pkau');
                $id->nilai = Input::get('nilai');

                $id->save();

                $pegawai = DB::connection('dblitbang')
                            ->table('ref_dja')
                            ->select('ref_dja.id as id')
                            ->where('ref_dja.id', '=', $id->id_dja)
                            ->first(); 
                Alert::success('Data telah ditambahkan.', 'Selamat');

                return redirect()->route('mappingtambah', $pegawai->id);
                // return redirect()->back();

            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = Mapping::findOrFail($id);

        $hapus->delete();

        return redirect('anggaran');
    }

    public function getDashLitbang()
    {

        return view('1litbang.dashboard');

    }

}
