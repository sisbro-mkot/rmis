<?php

namespace App\Http\Controllers\adminlitbang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\adminpemda\Role;
use App\Models\adminpemda\User;
use DB; 
use App\Models\adminpemda\UserHrm;
use App\Models\utils\Functions;
use App\Models\dashboard\Renpeglast;
use App\Models\dashboard\ViewRenpeglast;
 
class AuthCtrl extends Controller
{

    public function __construct(){

    }
    public function getLogin()
    {   

    	return view('1auth.login_fresh');
    }

    public function postLogin(Request $request)
    {

            $ldap = Functions::LoginLDAP($request->get('username'),$request->get('password'));

            $userFromLDAP = UserHrm::where('user_nip',$ldap->niplama)->first();    

            $vwrenpeglast = ViewRenpeglast::select('id')->where('niplama', $ldap->niplama)->first();

            $now = Carbon::now('Asia/Jakarta');

            if($userFromLDAP){
                
                Auth::login($userFromLDAP, true); 

            } else {

                if($vwrenpeglast){
                    $id2 = ViewRenpeglast::find($vwrenpeglast->id);

                    $id = new UserHrm;
                    $id->name = $id2->nama;
                    $id->email = $ldap->email;
                    $id->username = $request->get('username');
                    $id->password = bcrypt($request->get('password'));
                    $id->role_id = 9;
                    $id->user_nip = $id2->niplama;
                    $id->key_sort_unit = $id2->key_sort_unit;
                    $id->created_at = $now;
                    $id->save();

                    Auth::login($userFromLDAP, true);

                }
            }

            if (Auth::check()) {
              return redirect()->route('dashlitbang');
            } else {
              return redirect()->back();
            }

    }

    public function getLogout()
    {

        Auth::guard('web2')->logout();
    	return redirect('/litbang/login');
    }

}
