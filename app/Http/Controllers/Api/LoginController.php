<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Models\adminpemda\UserHrm;
use App\Models\utils\Functions;
class LoginController extends Controller
{
    public function postLogin(Request $request)
    {
        $ldap = Functions::LoginLDAP($request->get('username'),$request->get('password'));
        $api_token = MD5(rand()).MD5(rand());

        if($ldap!=null){
            $userFromLDAP = UserHrm::where('user_nip',$ldap->niplama)->first();
            // $create_token = UserHrm::where('id', $userFromLDAP->id)->update(['api_token' => $api_token]);
            $userFromLDAP->api_token = $api_token;
            // $userFromLDAP->role_id = $request->get('peran');
            $userFromLDAP->save();

            // Auth::guard('api')->login($userFromLDAP, true);
            if ($userFromLDAP) {
                $res['success'] = true;
                $res['api_token'] = $api_token;
                $res['message']['id'] = $userFromLDAP->id;
                $res['message']['name'] = $userFromLDAP->name;
                $res['message']['email'] = $userFromLDAP->email;
                $res['message']['namaunit'] = $userFromLDAP->unit->unitkerja_skt;
                $res['message']['role_id'] = $userFromLDAP->role_id;
                $res['message']['key_sort_unit'] = $userFromLDAP->key_sort_unit;
            }
            return response($res);
        }else{
        	$res['success'] = false;
        	$res['api_token'] = NULL;
        	$res['message'] = NULL;
        	return response($res);
        }

    }

    public function logout(Request $request){
        $user = Auth::guard('api')->user();
        if ($user) {
            $user->api_token = null;
            $user->save();
        }  
        $res['success'] = true;
        $res['message'] = "User logged out.";
        return response($res); 
    }
}
