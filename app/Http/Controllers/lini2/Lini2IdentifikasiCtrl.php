<?php

namespace App\Http\Controllers\lini2;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Illuminate\Support\Facades\Input;
use Alert;
use App\Models\dashboard\IdentifikasiRisiko;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\RefBaganRisiko;


class Lini2IdentifikasiCtrl extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $identifikasi = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 't_identifikasi_risiko.tahun as tahun', 'ref_jns_konteks.nama_jns_konteks as nama_jns_konteks', 'ref_konteks.nama_konteks as nama_konteks', 'wm_jabdetail.s_nmjabdetail as s_nmjabdetail','ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko','t_identifikasi_risiko.nama_dampak as nama_dampak')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                ->join('wm_jabdetail', 't_identifikasi_risiko.s_kd_jabdetail', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->whereRaw('(ISNULL(t_penetapan_konteks.catatan_hapus) AND (ref_data_umum.tahun = ?))', Auth::user()->tahun)
                ->get();

        return view('lini2identifikasi.index', compact('identifikasi'));


    }

    public function pilihKonteksUnit($id) 
    {  

            $konteks = DB::table('t_penetapan_konteks')
                ->select('ref_konteks.nama_konteks as nama_konteks', 't_penetapan_konteks.id_penetapan_konteks as id_penetapan_konteks', 'ref_jns_konteks.nama_jns_konteks as nama_jns_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', 'ref_data_umum.id_data_umum')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                ->where('s_kd_jabdetail_pemilik', $id)
                ->get();
            return json_encode($konteks);
        
    }

    public function showRank($id) 
    {  
            $now = Auth::user()->tahun;
            $rank = DB::table('ref_bagan_risiko')
                    ->select('ref_bagan_risiko.id_bagan_risiko as nomor', 
                        't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode', 
                        'wm_instansiunitorg.s_nama_instansiunitorg as pemilik',                        
                        'Pengendalian.count_pengendalian as ec',
                        'ref_matriks_residual.skor_risiko as residual',
                        'RTP.count_rtp as rtp',
                        'ref_matriks_treated.skor_risiko as treated')
                    ->leftjoin('t_identifikasi_risiko', 'ref_bagan_risiko.id_bagan_risiko', '=', 't_identifikasi_risiko.id_bagan_risiko')
                    ->leftjoin('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                    ->leftjoin('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->leftjoin('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->leftjoin('wm_instansiunitorg', 'ref_data_umum.s_kd_instansiunitorg', '=', 'wm_instansiunitorg.s_kd_instansiunitorg')
                    ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->leftjoin(DB::raw('(SELECT id_identifikasi, COUNT(id_pengendalian) AS count_pengendalian FROM `t_pengendalian` GROUP BY id_identifikasi) AS Pengendalian'), 
                            function($join)
                            {
                               $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'Pengendalian.id_identifikasi');
                            })
                    ->leftjoin(DB::raw('(SELECT 
                            C.id_identifikasi AS id_identifikasi,
                            C.id_penyebab AS id_penyebab,
                            D.count_rtp AS count_rtp 
                            FROM t_penyebab_rca AS C
                            LEFT JOIN
                                (SELECT B.id_penyebab AS id_penyebab, COUNT(A.id_rtp) AS count_rtp 
                                FROM t_rtp AS A
                                LEFT JOIN t_penyebab_rca AS B
                                ON A.id_penyebab = B.id_penyebab
                                GROUP BY B.id_penyebab) AS D
                                ON C.id_penyebab = D.id_penyebab
                                GROUP BY C.id_identifikasi) AS RTP'), 
                            function($join)
                            {
                               $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'RTP.id_identifikasi');
                            })
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->where('ref_bagan_risiko.id_bagan_risiko', $id)
                    ->orderBy('ref_bagan_risiko.id_bagan_risiko', 'asc')
                    ->get();
            return json_encode($rank);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['tahun'] = Carbon::now()->year;
        $this->data['s_kd_jabdetail'] = RefDataUmum::pluck('s_nmjabdetail_pemilik','s_kd_jabdetail_pemilik as s_kd_jabdetail');
        $this->data['id_penetapan_konteks'] = DB::table('t_penetapan_konteks')
                ->select('ref_konteks.nama_konteks as nama_konteks', 't_penetapan_konteks.id_penetapan_konteks as id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', 'ref_data_umum.id_data_umum')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->get();
        $this->data['id_bagan_risiko'] = RefBaganRisiko::pluck('nama_bagan_risiko as nama_bagan_risiko','id_bagan_risiko');

        return view('lini2identifikasi.createidentifikasi', $this->data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


      if (Auth::check())
        {
            $rules = array(
                'tahun' => 'required',
                's_kd_jabdetail' => 'required',
                'id_penetapan_konteks' => 'required',
                'id_bagan_risiko' => 'required',
                'nama_dampak' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $id = new IdentifikasiRisiko;
                $id->tahun = Input::get('tahun');
                $id->s_kd_jabdetail = Input::get('s_kd_jabdetail');
                $id->id_penetapan_konteks = Input::get('id_penetapan_konteks');
                $id->id_bagan_risiko = Input::get('id_bagan_risiko');
                $id->nama_dampak = Input::get('nama_dampak');
                $id->user_create = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini2identifikasi.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function getDashPeta()
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {
            $now = Auth::user()->tahun;
            $risikoteridentifikasi = DB::table('t_identifikasi_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->get()
                        ->count();
            $risikotermitigasi = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id', 'ref_data_umum.skor_selera as skor_selera', 'ref_matriks_residual.skor_risiko as skor_risiko_residual', 'ref_matriks_treated.skor_risiko as skor_risiko_treated')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko <= ref_data_umum.skor_selera) AND (ref_data_umum.tahun = ?)) OR ((ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera) AND (ref_data_umum.tahun = ?))', [Auth::user()->tahun, Auth::user()->tahun])
                        ->get()
                        ->count();
            $sebabteridentifikasi = DB::table('t_identifikasi_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->get()
                        ->count();
            $sebabtermitigasi = DB::table('t_identifikasi_risiko')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.id_jns_sebab as id_jns_sebab', 't_rtp.id_penyebab as id_rtp')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                        ->join('t_rtp', 't_penyebab_rca.id_penyebab', '=', 't_rtp.id_penyebab')
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->groupBy('t_rtp.id_penyebab')
                        ->get()
                        ->count();
            $rtpjadwal = DB::table('t_identifikasi_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                        ->join('t_rtp', 't_penyebab_rca.id_penyebab', '=', 't_rtp.id_penyebab')
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->get()
                        ->count();
            $rtprealisasi = DB::table('t_identifikasi_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                        ->join('t_rtp', 't_penyebab_rca.id_penyebab', '=', 't_rtp.id_penyebab')
                        ->leftjoin('t_pemantauan', 't_rtp.id_rtp', '=', 't_pemantauan.id_rtp')
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->whereNotNull('realisasi_waktu')
                        ->get()
                        ->count();
            $insidenk = DB::table('t_keterjadian')
                        ->join('ref_data_umum', 't_keterjadian.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->count();
            $insidenr = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab','t_identifikasi_risiko.id_identifikasi')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $insidenp = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->groupBy('t_penyebab_rca.id_penyebab')
                        ->get()
                        ->count();
            $unitmaxes = DB::table('wm_instansiunitorg')
                      ->selectRaw('wm_instansiunitorg.id_instansiunitorg as id_instansiunitorg, wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg, COUNT(t_identifikasi_risiko.id_identifikasi) as jml_identifikasi')
                      ->join('ref_data_umum', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'ref_data_umum.s_kd_instansiunitorg')
                      ->leftjoin('t_penetapan_konteks', 'ref_data_umum.id_data_umum', '=', 't_penetapan_konteks.id_data_umum')
                      ->leftjoin('t_identifikasi_risiko', 't_penetapan_konteks.id_penetapan_konteks', '=', 't_identifikasi_risiko.id_penetapan_konteks')
                      ->where('ref_data_umum.tahun', Auth::user()->tahun)
                      ->groupBy('wm_instansiunitorg.id_instansiunitorg')
                      ->get();
            $units = DB::table('wm_instansiunitorg')
                        ->selectRaw('wm_instansiunitorg.id_instansiunitorg as id_instansiunitorg, wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg, COUNT(t_analisis_risiko.id_analisis) as jml_termitigasi')
                        ->join('ref_data_umum', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'ref_data_umum.s_kd_instansiunitorg')
                        ->leftjoin('t_penetapan_konteks', 'ref_data_umum.id_data_umum', '=', 't_penetapan_konteks.id_data_umum')
                        ->leftjoin('t_identifikasi_risiko', 't_penetapan_konteks.id_penetapan_konteks', '=', 't_identifikasi_risiko.id_penetapan_konteks')
                        ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko <= ref_data_umum.skor_selera) AND (ref_data_umum.tahun = ?)) OR ((ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera) AND (ref_data_umum.tahun = ?)) OR ((ISNULL(ref_matriks_residual.skor_risiko)) AND (ref_data_umum.tahun = ?))', [Auth::user()->tahun, Auth::user()->tahun, Auth::user()->tahun])
                        ->groupBy('wm_instansiunitorg.id_instansiunitorg')
                        ->get();
            $rankjumlah = DB::table('ref_bagan_risiko')
                    ->selectRaw('ref_bagan_risiko.id_bagan_risiko as id_bagan_risiko, ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko, COUNT(t_identifikasi_risiko.id_identifikasi) as jumlah_risiko')
                    ->leftjoin('t_identifikasi_risiko', 'ref_bagan_risiko.id_bagan_risiko', '=', 't_identifikasi_risiko.id_bagan_risiko')
                    ->leftjoin('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->leftjoin('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->groupBy('ref_bagan_risiko.id_bagan_risiko')
                    ->orderBy('jumlah_risiko', 'desc')
                    ->take(5)
                    ->get();
            $ranklevel = DB::table('ref_bagan_risiko')
                    ->selectRaw('ref_bagan_risiko.id_bagan_risiko as id_bagan_risiko, ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko, CEILING(AVG((CASE WHEN ISNULL(t_analisis_risiko.id_matriks_treated) THEN matriks_residual.skor_risiko ELSE matriks_treated.skor_risiko END))) AS level_risiko')
                    ->leftjoin('t_identifikasi_risiko', 'ref_bagan_risiko.id_bagan_risiko', '=', 't_identifikasi_risiko.id_bagan_risiko')
                    ->leftjoin('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->leftjoin('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'matriks_treated.id_matriks')
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->groupBy('ref_bagan_risiko.id_bagan_risiko')
                    ->orderBy('level_risiko', 'desc')
                    ->take(5)
                    ->get();


            return view('lini2identifikasi.dashpeta',  
                compact('risikoteridentifikasi',
                'risikotermitigasi',
                'sebabteridentifikasi',
                'sebabtermitigasi',
                'rtpjadwal',
                'rtprealisasi',
                'insidenk',
                'insidenr',
                'insidenp',
                'unitmaxes',
                'units',
                'rankjumlah',
                'ranklevel'
                ));
        }
    }

    public function getHeatmap()
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {
            $risikoteridentifikasi = DB::table('t_identifikasi_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->get()
                        ->count();
            $risikotermitigasi = DB::table('t_analisis_risiko')
                        ->select('t_analisis_risiko.id_analisis as id', 'ref_data_umum.skor_selera as skor_selera', 'ref_matriks_residual.skor_risiko as skor_risiko_residual', 'ref_matriks_treated.skor_risiko as skor_risiko_treated')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko <= ref_data_umum.skor_selera) AND (ref_data_umum.tahun = ?)) OR ((ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera) AND (ref_data_umum.tahun = ?))', [Auth::user()->tahun, Auth::user()->tahun])
                        ->get()
                        ->count();
            $sebabteridentifikasi = DB::table('t_identifikasi_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->get()
                        ->count();
            $sebabtermitigasi = DB::table('t_identifikasi_risiko')
                        ->select('t_penyebab_rca.id_penyebab as id_sebab', 't_penyebab_rca.id_jns_sebab as id_jns_sebab', 't_rtp.id_penyebab as id_rtp')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                        ->join('t_rtp', 't_penyebab_rca.id_penyebab', '=', 't_rtp.id_penyebab')
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->groupBy('t_rtp.id_penyebab')
                        ->get()
                        ->count();
            $rtpjadwal = DB::table('t_identifikasi_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                        ->join('t_rtp', 't_penyebab_rca.id_penyebab', '=', 't_rtp.id_penyebab')
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->get()
                        ->count();
            $rtprealisasi = DB::table('t_identifikasi_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                        ->join('t_rtp', 't_penyebab_rca.id_penyebab', '=', 't_rtp.id_penyebab')
                        ->leftjoin('t_pemantauan', 't_rtp.id_rtp', '=', 't_pemantauan.id_rtp')
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->whereNotNull('realisasi_waktu')
                        ->get()
                        ->count();
            $insidenk = DB::table('t_keterjadian')
                        ->join('ref_data_umum', 't_keterjadian.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->count();
            $insidenr = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab','t_identifikasi_risiko.id_identifikasi')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $insidenp = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->groupBy('t_penyebab_rca.id_penyebab')
                        ->get()
                        ->count();
            $l1 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 1)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 1))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l2 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 2)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 2))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l3 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 3)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 3))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l4 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 4)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 4))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l5 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 5)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 5))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l6 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 6)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 6))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l7 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 7)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 7))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l8 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 8)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 8))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l9 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 9)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 9))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l10 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 10)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 10))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l11 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 11)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 11))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l12 = DB::table('t_identifikasi_risiko')
               ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
               ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 12)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 12))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l13 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 13)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 13))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l14 = DB::table('t_identifikasi_risiko')
               ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
               ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 14)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 14))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l15 = DB::table('t_identifikasi_risiko')
               ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
               ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 15)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 15))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l16 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 16)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 16))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l17 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 17)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 17))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l18 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 18)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 18))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l19 = DB::table('t_identifikasi_risiko')
               ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
               ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 19)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 19))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l20 = DB::table('t_identifikasi_risiko')
               ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
               ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 20)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 20))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l21 = DB::table('t_identifikasi_risiko')
               ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
               ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 21)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 21))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l22 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 22)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 22))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l23 = DB::table('t_identifikasi_risiko')
              ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
              ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 23)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 23))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l24 = DB::table('t_identifikasi_risiko')
               ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
               ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 24)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 24))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();
            $l25 = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->whereRaw('((ref_data_umum.tahun = ?) AND (ISNULL(ref_matriks_treated.skor_risiko)) AND (ref_matriks_residual.skor_risiko = 25)) OR ((ref_data_umum.tahun = ?) AND (ref_matriks_treated.skor_risiko = 25))', [Auth::user()->tahun, Auth::user()->tahun])
                ->count();

        return view('lini2identifikasi.heatmap',  compact('risikoteridentifikasi','risikotermitigasi','sebabteridentifikasi','sebabtermitigasi','rtpjadwal','insidenk','insidenr','insidenp','rtprealisasi','l1','l2','l3','l4','l5','l6','l7','l8','l9','l10','l11','l12','l13','l14','l15','l16','l17','l18','l19','l20','l21','l22','l23','l24','l25'));
        }
    }

    public function getDashRegister()
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') { 
            $identifikasi = DB::table('t_identifikasi_risiko')
                        ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks_residual.skor_risiko as skor_risiko_residual', 'ref_matriks_treated.skor_risiko as skor_risiko_treated', 'Penyebab.count_penyebab as count_penyebab', 'RTP.count_rtp as count_rtp')
                        ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->leftjoin(DB::raw('(SELECT id_identifikasi, COUNT(id_penyebab) AS count_penyebab FROM `t_penyebab_rca` GROUP BY id_identifikasi) AS Penyebab'), 
                            function($join)
                            {
                               $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'Penyebab.id_identifikasi');
                            })
                        ->leftjoin(DB::raw('(SELECT 
                            C.id_identifikasi AS id_identifikasi,
                            C.id_penyebab AS id_penyebab,
                            D.count_rtp AS count_rtp 
                            FROM t_penyebab_rca AS C
                            LEFT JOIN
                                (SELECT B.id_penyebab AS id_penyebab, COUNT(A.id_rtp) AS count_rtp 
                                FROM t_rtp AS A
                                LEFT JOIN t_penyebab_rca AS B
                                ON A.id_penyebab = B.id_penyebab
                                GROUP BY B.id_penyebab) AS D
                                ON C.id_penyebab = D.id_penyebab
                                GROUP BY C.id_identifikasi) AS RTP'), 
                            function($join)
                            {
                               $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'RTP.id_identifikasi');
                            })
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->get();
        return view('lini2identifikasi.register', compact('identifikasi'));
        }
    }

    public function getFilterRegister($id)
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') {
            
            $identifikasi = DB::table('t_identifikasi_risiko')
                            ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 't_identifikasi_risiko.nama_dampak as nama_dampak', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks_residual.skor_risiko as skor_risiko_residual', 'ref_matriks_treated.skor_risiko as skor_risiko_treated', 'Penyebab.count_penyebab as count_penyebab', 'RTP.count_rtp as count_rtp')
                            ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                            ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                            ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                            ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                            ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                            ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                            ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                            ->leftjoin(DB::raw('(SELECT id_identifikasi, COUNT(id_penyebab) AS count_penyebab FROM `t_penyebab_rca` GROUP BY id_identifikasi) AS Penyebab'), 
                                function($join)
                                {
                                   $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'Penyebab.id_identifikasi');
                                })
                            ->leftjoin(DB::raw('(SELECT 
                                C.id_identifikasi AS id_identifikasi,
                                C.id_penyebab AS id_penyebab,
                                D.count_rtp AS count_rtp 
                                FROM t_penyebab_rca AS C
                                LEFT JOIN
                                    (SELECT B.id_penyebab AS id_penyebab, COUNT(A.id_rtp) AS count_rtp 
                                    FROM t_rtp AS A
                                    LEFT JOIN t_penyebab_rca AS B
                                    ON A.id_penyebab = B.id_penyebab
                                    GROUP BY B.id_penyebab) AS D
                                    ON C.id_penyebab = D.id_penyebab
                                    GROUP BY C.id_identifikasi) AS RTP'), 
                                function($join)
                                {
                                   $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'RTP.id_identifikasi');
                                })
                            ->where([['ref_matriks_treated.skor_risiko', $id],
                                    ['ref_data_umum.tahun', Auth::user()->tahun]])
                            ->orWhere([
                                ['ref_matriks_residual.skor_risiko', '=', $id],
                                ['ref_data_umum.tahun', Auth::user()->tahun],
                                ['ref_matriks_treated.skor_risiko', '=', NULL],
                            ])
                            ->get();
        return view('lini2identifikasi.register', compact('identifikasi'));
        }
    }

    public function getRegisterUnit($id)
    {
        if(Auth::user()->role_id == '1'|Auth::user()->role_id == '2'|Auth::user()->role_id == '3'|Auth::user()->role_id == '8') { 
            $identifikasi = DB::table('t_identifikasi_risiko')
                        ->select('t_identifikasi_risiko.id_identifikasi as id', 
                            't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko',
                            'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko',
                            't_identifikasi_risiko.nama_dampak as nama_dampak',
                            'residual.skor_risiko as skor_risiko_residual',
                            'treated.skor_risiko as skor_risiko_treated')
                        ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_instansiunitorg', 'ref_data_umum.s_kd_instansiunitorg', '=', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                        ->leftjoin('ref_matriks as residual', 't_analisis_risiko.id_matriks_residual', '=', 'residual.id_matriks')
                        ->leftjoin('ref_matriks as treated', 't_analisis_risiko.id_matriks_treated', '=', 'treated.id_matriks')
                        ->where('wm_instansiunitorg.id_instansiunitorg', $id)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->get();

            $unit = DB::table('wm_instansiunitorg')
                    ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg','ref_data_umum.skor_selera as skor_selera')
                    ->join('ref_data_umum', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'ref_data_umum.s_kd_instansiunitorg')
                    ->where('wm_instansiunitorg.id_instansiunitorg', $id)
                    ->first();

        return view('lini2identifikasi.registerunit', compact('identifikasi','unit'));
        }
    }

}
