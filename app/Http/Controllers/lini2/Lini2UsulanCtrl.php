<?php

namespace App\Http\Controllers\lini2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;
use App\Models\dashboard\RefKategoriRisiko;
use App\Models\dashboard\RefTujuanSPIP;
use App\Models\dashboard\Usulan;
use App\Models\dashboard\RefBaganRisiko;

class Lini2UsulanCtrl extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kamus = DB::table('t_usulan')
                ->select('t_usulan.id_usulan as id',
                    't_usulan.nama_bagan_risiko as nama_bagan_risiko',
                    'ref_kategori_risiko.nama_kategori_risiko as nama_kategori_risiko',
                    'ref_tujuan_spip.nama_tujuan_spip as nama_tujuan_spip',
                    't_usulan.status_usulan as status_usulan',
                    'wm_instansiunitorg.s_skt_instansiunitorg')
                ->join('ref_kategori_risiko','t_usulan.id_kategori_risiko','=','ref_kategori_risiko.id_kategori_risiko')
                ->join('ref_tujuan_spip','t_usulan.id_tujuan_spip','=','ref_tujuan_spip.id_tujuan_spip')
                ->join('wm_instansiunitorg', 't_usulan.s_kd_instansiunitorg', '=', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->orderBy('t_usulan.id_usulan')
                ->get();

        return view('lini2usulan.index', compact('kamus'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usulan = DB::table('t_usulan')
                ->select('t_usulan.id_usulan as id',
                    't_usulan.nama_bagan_risiko as nama_bagan_risiko',
                    'ref_kategori_risiko.nama_kategori_risiko as nama_kategori_risiko',
                    'ref_tujuan_spip.nama_tujuan_spip as nama_tujuan_spip',
                    't_usulan.status_usulan as status_usulan',
                    't_usulan.status_ubah as status_ubah',
                    't_usulan.tanggapan as tanggapan',
                    'wm_instansiunitorg.s_skt_instansiunitorg')
                ->join('ref_kategori_risiko','t_usulan.id_kategori_risiko','=','ref_kategori_risiko.id_kategori_risiko')
                ->join('ref_tujuan_spip','t_usulan.id_tujuan_spip','=','ref_tujuan_spip.id_tujuan_spip')
                ->join('wm_instansiunitorg', 't_usulan.s_kd_instansiunitorg', '=', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('t_usulan.id_usulan','=', $id)
                ->first();

        return view('lini2usulan.show', compact('usulan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $this->data['show'] = DB::table('t_usulan')
                ->select('t_usulan.id_usulan as id',
                    't_usulan.nama_bagan_risiko as nama_bagan_risiko',
                    'ref_kategori_risiko.nama_kategori_risiko as nama_kategori_risiko',
                    'ref_tujuan_spip.nama_tujuan_spip as nama_tujuan_spip',
                    't_usulan.status_usulan as status_usulan',
                    't_usulan.status_ubah as status_ubah',
                    't_usulan.tanggapan as tanggapan',
                    'wm_instansiunitorg.s_skt_instansiunitorg')
                ->join('ref_kategori_risiko','t_usulan.id_kategori_risiko','=','ref_kategori_risiko.id_kategori_risiko')
                ->join('ref_tujuan_spip','t_usulan.id_tujuan_spip','=','ref_tujuan_spip.id_tujuan_spip')
                ->join('wm_instansiunitorg', 't_usulan.s_kd_instansiunitorg', '=', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('t_usulan.id_usulan','=', $id)
                ->first();

        $this->data['s_kd_instansiunitorg'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_kd_instansiunitorg as s_kd_instansiunitorg')
                ->get();

        $this->data['id_kategori_risiko'] = DB::table('ref_kategori_risiko')
                ->select('ref_kategori_risiko.id_kategori_risiko as id_kategori_risiko','ref_kategori_risiko.nama_kategori_risiko as nama_kategori_risiko')
                ->get();

        $this->data['id_tujuan_spip'] = DB::table('ref_tujuan_spip')
                ->select('ref_tujuan_spip.id_tujuan_spip as id_tujuan_spip','ref_tujuan_spip.nama_tujuan_spip as nama_tujuan_spip')
                ->get();

        $this->data['usulan'] = Usulan::find($id);  

        return view('lini2usulan.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check()){
            $rules = array(
                'nama_bagan_risiko' => 'required',
                'id_kategori_risiko' => 'required',
                'id_tujuan_spip' => 'required'
            );
            $messages = [
                            'nama_bagan_risiko.required' => 'Silahkan isi nama risiko.',
                            'id_kategori_risiko.required' => 'Silahkan pilih kategori risiko.',
                            'id_tujuan_spip.required' => 'Silahkan pilih metode pencapaian tujuan SPIP.'
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $now = Carbon::now('Asia/Jakarta');

                $id = Usulan::find($id);
                $id->s_kd_instansiunitorg = Input::get('s_kd_instansiunitorg');
                $id->nama_bagan_risiko = Input::get('nama_bagan_risiko');
                $id->id_kategori_risiko = Input::get('id_kategori_risiko');
                $id->id_tujuan_spip = Input::get('id_tujuan_spip');
                $id->status_usulan = Input::get('status_usulan');
                $id->tanggapan = Input::get('tanggapan');
                $id->updated_at = $now;
                $id->user_update = Auth::user()->user_nip;
                if($id->status_usulan == 4){
                    $id->status_ubah = "Y";

                    $id2 = new RefBaganRisiko;
                    $id2->id_kategori_risiko = Input::get('id_kategori_risiko');
                    $id2->nama_bagan_risiko = Input::get('nama_bagan_risiko');
                    $id2->id_tujuan_spip = Input::get('id_tujuan_spip');
                    $id2->created_at = $now;
                    $id2->user_create = Auth::user()->user_nip;
                    $id2->save();
                }

                $id->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini2usulan.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
