<?php

namespace App\Http\Controllers\lini2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;
use App\Models\dashboard\AnalisisRisiko;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\RefMatriks;


class Lini2LaporanCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->data['pemilik_risiko'] = DB::table('wm_instansiunitorg')
        ->select('wm_instansiunitorg.id_instansiunitorg as id', 'wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
        ->get();

        return view('lini2laporan.index', $this->data);

    }

    public function cetakid($id)
    {
        $identifikasi = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 't_identifikasi_risiko.tahun as tahun', 'ref_jns_konteks.nama_jns_konteks as nama_jns_konteks', 'ref_konteks.nama_konteks as nama_konteks', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko','t_identifikasi_risiko.nama_dampak as nama_dampak','ref_tujuan_spip.nama_tujuan_spip as nama_tujuan_spip')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('ref_tujuan_spip', 'ref_bagan_risiko.id_tujuan_spip', '=', 'ref_tujuan_spip.id_tujuan_spip')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('wm_instansiunitorg', 'ref_data_umum.s_kd_instansiunitorg', '=', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('wm_instansiunitorg.id_instansiunitorg', $id)
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->get();

        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.id_instansiunitorg', $id)
                ->first();

        $tahun = Auth::user()->tahun;

        $pdf = PDF::loadView('lini2laporan.cetakid',  compact('identifikasi', 'unit', 'tahun'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('identifikasi_risiko.pdf', array('Attachment' => false));
        exit(0);
    }

    public function cetakan($id)
    {
        $analisis = DB::table('t_analisis_risiko')
                ->select('t_analisis_risiko.id_analisis as id', 
                    't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 
                    'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko',
                    'ref_matriks_inherent.skor_dampak as skor_dampak_inherent',
                    'ref_matriks_inherent.skor_kemungkinan as skor_kemungkinan_inherent', 
                    'ref_matriks_inherent.skor_risiko as skor_risiko_inherent', 
                    'Pengendalian.count_pengendalian as count_pengendalian', 
                    't_analisis_risiko.existing_control_memadai as existing_control_memadai',
                    'ref_matriks_residual.skor_dampak as skor_dampak_residual',
                    'ref_matriks_residual.skor_kemungkinan as skor_kemungkinan_residual', 
                    'ref_matriks_residual.skor_risiko as skor_risiko_residual', 
                    'ref_data_umum.skor_selera as skor_selera', 
                    't_identifikasi_risiko.nama_dampak as nama_dampak')
                ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')                
                ->join('wm_instansiunitorg', 'ref_data_umum.s_kd_instansiunitorg', '=', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->leftjoin('ref_matriks as ref_matriks_inherent', 't_analisis_risiko.id_matriks_inherent', '=', 'ref_matriks_inherent.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin(DB::raw('(SELECT id_identifikasi, COUNT(id_pengendalian) AS count_pengendalian FROM `t_pengendalian` GROUP BY id_identifikasi) AS Pengendalian'), 
                            function($join)
                            {
                               $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'Pengendalian.id_identifikasi');
                            })
                ->where('wm_instansiunitorg.id_instansiunitorg', $id)
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->orderBy('kode_identifikasi_risiko','asc')
                ->get();

        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.id_instansiunitorg', $id)
                ->first();

        $tahun = Auth::user()->tahun;

        $pdf = PDF::loadView('lini2laporan.cetakan',  compact('analisis', 'unit', 'tahun'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('analisis_risiko.pdf', array('Attachment' => false));
        exit(0);
    }

    public function cetakev($id)
    {
        $evaluasi = DB::table('t_analisis_risiko')
                ->select('t_analisis_risiko.id_analisis as id', 
                    't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 
                    'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko',
                    'ref_matriks_residual.skor_dampak as skor_dampak_residual',
                    'ref_matriks_residual.skor_kemungkinan as skor_kemungkinan_residual', 
                    'ref_matriks_residual.skor_risiko as skor_risiko_residual')
                ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('wm_instansiunitorg', 'ref_data_umum.s_kd_instansiunitorg', '=', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->whereRaw('(ref_matriks_residual.skor_risiko > ref_data_umum.skor_selera) AND (wm_instansiunitorg.id_instansiunitorg = ?) AND (ref_data_umum.tahun = ?)', [$id, Auth::user()->tahun])
                ->orderBy('skor_risiko_residual', 'desc')
                ->get();

        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.id_instansiunitorg', $id)
                ->first();

        $selera = DB::table('ref_data_umum')
                ->select('ref_data_umum.skor_selera as skor_selera')
                ->join('wm_instansiunitorg', 'ref_data_umum.s_kd_instansiunitorg', '=', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('wm_instansiunitorg.id_instansiunitorg', $id)
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->first();

        $tahun = Auth::user()->tahun;

        $pdf = PDF::loadView('lini2laporan.cetakev',  compact('evaluasi', 'unit', 'tahun', 'selera'));
        $pdf->setPaper('A4', 'portrait');
        return $pdf->stream('evaluasi_risiko_prioritas.pdf', array('Attachment' => false));
        exit(0);
    }

    public function cetakrca($id)
    {
        $evaluasi = DB::table('t_penyebab_rca')
                    ->select('t_penyebab_rca.id_penyebab as id', 't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_penyebab_rca.nama_penyebab_1 as nama_penyebab_1', 't_penyebab_rca.nama_penyebab_2 as nama_penyebab_2', 't_penyebab_rca.nama_penyebab_3 as nama_penyebab_3', 't_penyebab_rca.nama_penyebab_4 as nama_penyebab_4', 't_penyebab_rca.nama_penyebab_5 as nama_penyebab_5', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_penyebab_rca_kode.kode_penyebab as kode_penyebab')
                    ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                    ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                    ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                    ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                    ->join('ref_jns_sebab', 't_penyebab_rca.id_jns_sebab', '=', 'ref_jns_sebab.id_jns_sebab')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('wm_instansiunitorg', 'ref_data_umum.s_kd_instansiunitorg', '=', 'wm_instansiunitorg.s_kd_instansiunitorg')
                    ->where('wm_instansiunitorg.id_instansiunitorg', $id)
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->get();

        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.id_instansiunitorg', $id)
                ->first();

        $tahun = Auth::user()->tahun;

        $pdf = PDF::loadView('lini2laporan.cetakrca',  compact('evaluasi', 'unit', 'tahun'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('evaluasi_risiko_rca.pdf', array('Attachment' => false));
        exit(0);
    }

    public function cetakres($id)
    {
        $respons = DB::table('t_rtp')
                    ->select('t_rtp.id_rtp as id', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_penyebab_rca_kode.kode_penyebab as kode_penyebab', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_rtp.respon_risiko as respon_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 'ref_sub_unsur_spip.nama_sub_unsur as nama_sub_unsur', 'wm_jabdetail.s_nmjabdetail as s_nmjabdetail', 'ref_output.nama_output as nama_output', 'ref_periode.nama_periode as nama_periode')
                    ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                    ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                    ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                    ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                    ->join('ref_sub_unsur_spip', 't_rtp.id_sub_unsur', '=', 'ref_sub_unsur_spip.id_sub_unsur')
                    ->join('wm_jabdetail', 't_rtp.s_kd_jabdetail_pic', '=', 'wm_jabdetail.s_kd_jabdetail')
                    ->join('ref_output', 't_rtp.id_output', '=', 'ref_output.id_output')
                    ->join('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('wm_instansiunitorg', 'ref_data_umum.s_kd_instansiunitorg', '=', 'wm_instansiunitorg.s_kd_instansiunitorg')
                    ->where('wm_instansiunitorg.id_instansiunitorg', $id)
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->get();

        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.id_instansiunitorg', $id)
                ->first();

        $tahun = Auth::user()->tahun;

        $pdf = PDF::loadView('lini2laporan.cetakres',  compact('respons', 'unit', 'tahun'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('respons_risiko_rtp.pdf', array('Attachment' => false));
        exit(0);
    }

    public function cetakreal($id)
    {
        $respons = DB::table('t_pemantauan')
                ->select('t_pemantauan.id_pemantauan as id', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian', 't_penyebab_rca_kode.kode_penyebab as kode_penyebab', 'wm_jabdetail.s_nmjabdetail as s_nmjabdetail', 'periode_rencana.nama_periode as nama_periode_rencana', 't_pemantauan.realisasi_waktu as realisasi_waktu', 'ref_output.nama_output as nama_output', 't_pemantauan.nama_hambatan as nama_hambatan')
                ->join('t_rtp', 't_pemantauan.id_rtp', '=', 't_rtp.id_rtp')
                ->join('wm_jabdetail', 't_rtp.s_kd_jabdetail_pic', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->join('ref_output', 't_rtp.id_output', '=', 'ref_output.id_output')
                ->join('ref_periode as periode_rencana', 't_rtp.id_periode', '=', 'periode_rencana.id_periode')
                ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('wm_instansiunitorg', 'ref_data_umum.s_kd_instansiunitorg', '=', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('wm_instansiunitorg.id_instansiunitorg', $id)
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->get();

        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.id_instansiunitorg', $id)
                ->first();

        $tahun = Auth::user()->tahun;

        $triwulan = Carbon::now()->quarter;

        $pdf = PDF::loadView('lini2laporan.cetakreal',  compact('respons', 'unit', 'tahun', 'triwulan'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('respons_risiko_realisasi.pdf', array('Attachment' => false));
        exit(0);
    }

    public function cetakmon($id)
    {
        $monitoring = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id', 't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian', 't_keterjadian.skor_dampak as skor_dampak', 't_keterjadian.pemicu_kejadian as pemicu_kejadian')
                        ->leftjoin('t_identifikasi_risiko', 't_keterjadian.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->leftjoin('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->leftjoin('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('ref_data_umum', 't_keterjadian.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('wm_instansiunitorg', 'ref_data_umum.s_kd_instansiunitorg', '=', 'wm_instansiunitorg.s_kd_instansiunitorg')
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->where('wm_instansiunitorg.id_instansiunitorg', $id)
                        ->get();

        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.id_instansiunitorg', $id)
                ->first();

        $tahun = Auth::user()->tahun;

        $triwulan = Carbon::now()->quarter;

        $pdf = PDF::loadView('lini2laporan.cetakmon',  compact('monitoring', 'unit', 'tahun', 'triwulan'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('pemantauan_risiko.pdf', array('Attachment' => false));
        exit(0);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
