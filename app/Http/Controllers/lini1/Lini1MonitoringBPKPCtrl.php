<?php

namespace App\Http\Controllers\lini1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\Keterjadian;


class Lini1MonitoringBPKPCtrl extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $monitoring = DB::table('t_keterjadian')
                    ->select('t_keterjadian.id_keterjadian as id', 'ref_periode.nama_periode as nama_periode', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_keterjadian.nama_kejadian as nama_kejadian', 't_keterjadian.waktu_kejadian as waktu_kejadian', 't_keterjadian.tempat_kejadian as tempat_kejadian', 't_keterjadian.skor_dampak as skor_dampak', 't_keterjadian.pemicu_kejadian as pemicu_kejadian')
                    ->join('ref_data_umum', 't_keterjadian.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('ref_periode', 't_keterjadian.id_periode', '=', 'ref_periode.id_periode')
                    ->leftjoin('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                    ->where('ref_data_umum.id_data_umum', 1)
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->get();
        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.s_kd_instansiunitorg', "07001500001000")
                ->first();


        return view('lini1monitoringbpkp.index', compact('monitoring','unit'));

    }

    public function pilihSebab2($id) 
    {  

            $sebab = DB::table('t_penyebab_rca')
                ->select('t_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_penyebab_rca.id_penyebab as id_penyebab')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->where('t_identifikasi_risiko.id_identifikasi', $id)
                ->get();
            return json_encode($sebab);
        
    }

    public function cetak()
    {
        $monitoring = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id', 't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian', 't_keterjadian.skor_dampak as skor_dampak', 't_keterjadian.pemicu_kejadian as pemicu_kejadian')
                        ->leftjoin('t_identifikasi_risiko', 't_keterjadian.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->leftjoin('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->leftjoin('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('ref_data_umum', 't_keterjadian.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->get();

        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.s_kd_instansiunitorg', "07001500001000")
                ->first();

        $tahun = Auth::user()->tahun;

        $triwulan = Carbon::now()->quarter;

        $pdf = PDF::loadView('lini1monitoringbpkp.cetak',  compact('monitoring', 'unit', 'tahun', 'triwulan'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('pemantauan_risiko.pdf', array('Attachment' => false));
        exit(0);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['s_kd_jabdetail'] = DB::table('ref_data_umum')
                ->select('ref_data_umum.s_nmjabdetail_pemilik as s_nmjabdetail_pemilik','ref_data_umum.s_kd_jabdetail_pemilik as s_kd_jabdetail')
                ->where('ref_data_umum.id_data_umum', 1)
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->get();
        $this->data['id_data_umum'] = DB::table('ref_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->pluck('s_nmjabdetail_pemilik as s_nmjabdetail_pemilik','id_data_umum');
        $this->data['id_identifikasi'] = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->where('ref_data_umum.id_data_umum', 1)
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->pluck('ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_identifikasi_risiko.id_identifikasi as id_identifikasi');
        $this->data['id_penyebab'] = DB::table('t_penyebab_rca')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->where('ref_data_umum.id_data_umum', 1)
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->pluck('t_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_penyebab_rca.id_penyebab as id_penyebab');
        $this->data['skor_dampak'] = ['1', '2', '3', '4', '5'];
        $this->data['nama_instansiunitorg'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.s_kd_instansiunitorg', "07001500001000")
                ->first();

        return view('lini1monitoringbpkp.createmonitorbpkp', $this->data);

    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
        if (Auth::check())
        {
            $rules = array(

                'id_data_umum' => 'required',
                'nama_kejadian' => 'required',
                'waktu_kejadian' => 'required',
                'tempat_kejadian' => 'required',
                'skor_dampak' => 'required',
                'pemicu_kejadian' => 'required',

            );
            $messages = [
                            'id_data_umum.required' => 'Silahkan pilih pemilik risiko.',
                            'nama_kejadian.required' => 'Silahkan isi nama kejadian.',
                            'waktu_kejadian.required' => 'Silahkan isi waktu kejadian.',
                            'tempat_kejadian.required' => 'Silahkan isi tempat kejadian.',
                            'skor_dampak.required' => 'Silahkan pilih skor dampak.',
                            'pemicu_kejadian.required' => 'Silahkan isi pemicu kejadian.',
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $now = Carbon::now('Asia/Jakarta');

                $id = new Keterjadian;
                $id->id_data_umum = Input::get('id_data_umum');
                $id->id_identifikasi = Input::get('id_identifikasi');
                $id->id_penyebab = Input::get('id_penyebab');
                $id->nama_kejadian = Input::get('nama_kejadian');
                $id->waktu_kejadian = Input::get('waktu_kejadian');
                $id->tempat_kejadian = Input::get('tempat_kejadian');
                $id->skor_dampak = Input::get('skor_dampak');
                $id->pemicu_kejadian = Input::get('pemicu_kejadian');
                $id->created_at = $now;
                $id->user_create = Auth::user()->user_nip;

                $dt = Carbon::parse($id->waktu_kejadian);
                $dn = $dt->quarter;
                $id->id_periode = $dn + 2;

                $id->save();

                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini1monitoringbpkp.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['s_kd_jabdetail'] = DB::table('ref_data_umum')
                ->select('ref_data_umum.s_nmjabdetail_pemilik as s_nmjabdetail_pemilik','ref_data_umum.s_kd_jabdetail_pemilik as s_kd_jabdetail')
                ->where('ref_data_umum.id_data_umum', 1)
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->get();
        $this->data['id_data_umum'] = DB::table('ref_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->pluck('s_nmjabdetail_pemilik as s_nmjabdetail_pemilik','id_data_umum');
        $this->data['id_identifikasi'] = DB::table('t_identifikasi_risiko')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->where('ref_data_umum.id_data_umum', 1)
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->pluck('ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 't_identifikasi_risiko.id_identifikasi as id_identifikasi');
        $this->data['id_penyebab'] = DB::table('t_penyebab_rca')
                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->where('ref_data_umum.id_data_umum', 1)
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->pluck('t_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_penyebab_rca.id_penyebab as id_penyebab');
        $this->data['skor_dampak'] = ['1', '2', '3', '4', '5'];
        $this->data['nama_instansiunitorg'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.s_kd_instansiunitorg', "07001500001000")
                ->first();
        $this->data['monitoring'] = Keterjadian::find($id);
        $this->data['risiko'] = DB::table('t_keterjadian')
                    ->select('t_keterjadian.id_keterjadian as id', 't_keterjadian.id_identifikasi as id_identifikasi', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko')
                    ->leftjoin('t_identifikasi_risiko', 't_keterjadian.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                    ->leftjoin('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                    ->where('t_keterjadian.id_keterjadian',$id)
                    ->first();
        $this->data['penyebab'] = DB::table('t_keterjadian')
                    ->select('t_keterjadian.id_keterjadian as id', 't_keterjadian.id_penyebab as id_penyebab', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab')
                    ->leftjoin('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                    ->where('t_keterjadian.id_keterjadian',$id)
                    ->first();

        return view('lini1monitoringbpkp.editmonitorbpkp', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check())
        {
            $rules = array(

                'id_data_umum' => 'required',
                'nama_kejadian' => 'required',
                'waktu_kejadian' => 'required',
                'tempat_kejadian' => 'required',
                'skor_dampak' => 'required',
                'pemicu_kejadian' => 'required',

            );
            $messages = [
                            'id_data_umum.required' => 'Silahkan pilih pemilik risiko.',
                            'nama_kejadian.required' => 'Silahkan isi nama kejadian.',
                            'waktu_kejadian.required' => 'Silahkan isi waktu kejadian.',
                            'tempat_kejadian.required' => 'Silahkan isi tempat kejadian.',
                            'skor_dampak.required' => 'Silahkan pilih skor dampak.',
                            'pemicu_kejadian.required' => 'Silahkan isi pemicu kejadian.',
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $now = Carbon::now('Asia/Jakarta');
                $id = Keterjadian::find($id);
                $id->id_data_umum = Input::get('id_data_umum');
                $id->id_identifikasi = Input::get('id_identifikasi');
                $id->id_penyebab = Input::get('id_penyebab');
                $id->nama_kejadian = Input::get('nama_kejadian');
                $id->waktu_kejadian = Input::get('waktu_kejadian');
                $id->tempat_kejadian = Input::get('tempat_kejadian');
                $id->skor_dampak = Input::get('skor_dampak');
                $id->pemicu_kejadian = Input::get('pemicu_kejadian');
                $id->updated_at = $now;
                $id->user_delete = Auth::user()->user_nip;

                $dt = Carbon::parse($id->waktu_kejadian);
                $dn = $dt->quarter;
                $id->id_periode = $dn + 2;

                $id->save();

                Alert::success('Data telah diubah.', 'Selamat');
                return redirect()->route('lini1monitoringbpkp.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function getInsidenBPKP()
    {
        if(Auth::user()->role_id == '3') {
            $risikoteridentifikasi = DB::table('t_identifikasi_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->count();
            $risikotermitigasi = DB::table('t_analisis_risiko')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko <= ref_data_umum.skor_selera) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                        ->count();
            $sebabteridentifikasi = DB::table('t_penyebab_rca')
                        ->leftjoin('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->count();
            $sebabtermitigasi = DB::table('t_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->leftjoin('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->groupBy('t_rtp.id_penyebab')
                        ->get()
                        ->count();
            $rtpjadwal = DB::table('t_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->count();
            $rtprealisasi = DB::table('t_pemantauan')
                        ->join('t_rtp', 't_pemantauan.id_rtp', '=', 't_rtp.id_rtp')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->whereNotNull('realisasi_waktu')
                        ->count();
            $insidenk = DB::table('t_keterjadian')
                        ->join('ref_data_umum', 't_keterjadian.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->count();
            $insidenr = DB::table('t_keterjadian')
                        ->join('t_identifikasi_risiko', 't_keterjadian.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $insidenp = DB::table('t_keterjadian')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->groupBy('t_penyebab_rca.id_penyebab')
                        ->get()
                        ->count();
            $r1 = DB::table('t_keterjadian')
                        ->join('t_identifikasi_risiko', 't_keterjadian.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->where('ref_bagan_risiko.id_kategori_risiko', 1)
                        ->count();
            $r2 = DB::table('t_keterjadian')
                        ->join('t_identifikasi_risiko', 't_keterjadian.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->where('ref_bagan_risiko.id_kategori_risiko', 2)
                        ->count();
            $r3 = DB::table('t_keterjadian')
                        ->join('t_identifikasi_risiko', 't_keterjadian.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->where('ref_bagan_risiko.id_kategori_risiko', 3)
                        ->count();
            $r4 = DB::table('t_keterjadian')
                        ->join('t_identifikasi_risiko', 't_keterjadian.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->where('ref_bagan_risiko.id_kategori_risiko', 4)
                        ->count();
            $r5 = DB::table('t_keterjadian')
                        ->join('t_identifikasi_risiko', 't_keterjadian.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->where('ref_bagan_risiko.id_kategori_risiko', 5)
                        ->count();
            $r6 = DB::table('t_keterjadian')
                        ->join('t_identifikasi_risiko', 't_keterjadian.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->where('ref_bagan_risiko.id_kategori_risiko', 6)
                        ->count();
            $p1 = DB::table('t_keterjadian')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->where('t_penyebab_rca.id_jns_sebab', 1)
                        ->count();
            $p2 = DB::table('t_keterjadian')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->where('t_penyebab_rca.id_jns_sebab', 2)
                        ->count();
            $p3 = DB::table('t_keterjadian')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->where('t_penyebab_rca.id_jns_sebab', 3)
                        ->count();
            $p4 = DB::table('t_keterjadian')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->where('t_penyebab_rca.id_jns_sebab', 4)
                        ->count();
            $p5 = DB::table('t_keterjadian')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->where('t_penyebab_rca.id_jns_sebab', 5)
                        ->count();
            $p6 = DB::table('t_keterjadian')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->where('t_penyebab_rca.id_jns_sebab', 6)
                        ->count();

        return view('lini1monitoringbpkp.insidenbpkp', compact('risikoteridentifikasi','risikotermitigasi','sebabteridentifikasi','sebabtermitigasi','rtpjadwal','rtprealisasi','insidenk','insidenr','insidenp','r1','r2','r3','r4','r5','r6','p1','p2','p3','p4','p5','p6'));
        }
    }

    public function getInsKejadianBPKP()
    {
        if(Auth::user()->role_id == '3') {

            $monitoring = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id', 't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian')
                        ->leftjoin('t_identifikasi_risiko', 't_keterjadian.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->leftjoin('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->join('ref_data_umum', 't_keterjadian.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->get();
            $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.s_kd_instansiunitorg', "07001500001000")
                ->first();

        return view('lini1monitoringbpkp.inskejadianbpkp', compact('monitoring','unit'));
        }
    }

    public function getInsRisikoBPKP()
    {
        if(Auth::user()->role_id == '3') {

            $monitoring = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id', 't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian')
                        ->join('t_identifikasi_risiko', 't_keterjadian.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->get();
            $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.s_kd_instansiunitorg', "07001500001000")
                ->first();

        return view('lini1monitoringbpkp.insrisikobpkp', compact('monitoring','unit'));
        }
    }


    public function getFilterInsRisikoBPKP($id)
    {
        if(Auth::user()->role_id == '3') {

            $monitoring = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id', 't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko','t_keterjadian.nama_kejadian as nama_kejadian','t_keterjadian.waktu_kejadian as waktu_kejadian','t_keterjadian.tempat_kejadian as tempat_kejadian')
                        ->join('t_identifikasi_risiko', 't_keterjadian.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->where('id_kategori_risiko', $id)
                        ->get();
            $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.s_kd_instansiunitorg', "07001500001000")
                ->first();

        return view('lini1monitoringbpkp.insrisikobpkp', compact('monitoring','unit'));
        }
    }

    public function getInsSebabBPKP()
    {
        if(Auth::user()->role_id == '3') {

            $monitoring = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko','t_keterjadian.pemicu_kejadian as pemicu_kejadian','t_penyebab_rca.id_jns_sebab as id_jns_sebab','t_penyebab_rca_kode.kode_penyebab as kode_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->get();
            $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.s_kd_instansiunitorg', "07001500001000")
                ->first();

        return view('lini1monitoringbpkp.inssebabbpkp', compact('monitoring','unit'));
        }
    }

    public function getFilterInsSebabBPKP($id)
    {
        if(Auth::user()->role_id == '3') {

            $monitoring = DB::table('t_keterjadian')
                        ->select('t_keterjadian.id_keterjadian as id','t_keterjadian.nama_kejadian as nama_kejadian','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko','t_keterjadian.pemicu_kejadian as pemicu_kejadian','t_penyebab_rca.id_jns_sebab as id_jns_sebab','t_penyebab_rca_kode.kode_penyebab as kode_penyebab')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->where('t_penyebab_rca.id_jns_sebab', $id)
                        ->get();
            $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.s_kd_instansiunitorg', "07001500001000")
                ->first();

        return view('lini1monitoringbpkp.inssebabbpkp', compact('monitoring','unit'));
        }
    }



}
