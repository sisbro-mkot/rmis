<?php

namespace App\Http\Controllers\lini1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use App\Models\dashboard\AnalisisRisiko;
use App\Models\dashboard\RefMatriks;

class Lini1ResidualBPKPCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
                
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $this->data['inherent'] = DB::table('t_analisis_risiko')
                                ->select('ref_matriks.skor_dampak as skor_dampak', 'ref_matriks.skor_kemungkinan as skor_kemungkinan', 'ref_matriks.skor_risiko as skor_risiko')
                                ->leftjoin('ref_matriks', 't_analisis_risiko.id_matriks_inherent', '=', 'ref_matriks.id_matriks')
                                ->where('t_analisis_risiko.id_analisis', $id)
                                ->first();
        $this->data['residual'] = DB::table('t_rtp')
                                ->select('t_rtp.id_rtp as id', 't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 't_rtp.respon_risiko as respon_risiko', 't_rtp.kegiatan_pengendalian as kegiatan_pengendalian')
                                ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                                ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                                ->where('t_analisis_risiko.id_analisis', $id)
                                ->get();
        $this->data['kemungkinan_residual'] = ['1', '2', '3', '4', '5'];
        $this->data['dampak_residual'] = ['1', '2', '3', '4', '5'];
        $this->data['id_matriks_residual'] = RefMatriks::pluck('skor_risiko', 'id_matriks');
        $this->data['analisis'] = AnalisisRisiko::find($id);
        $this->data['skor'] = DB::table('t_analisis_risiko')
                ->select('t_analisis_risiko.id_analisis as id',
                    'ref_matriks_inherent.skor_dampak as skor_dampak_inherent',
                    'ref_matriks_inherent.skor_kemungkinan as skor_kemungkinan_inherent', 
                    'ref_matriks_inherent.skor_risiko as skor_risiko_inherent',
                    'ref_matriks_residual.skor_dampak as skor_dampak_residual',
                    'ref_matriks_residual.skor_kemungkinan as skor_kemungkinan_residual', 
                    'ref_matriks_residual.skor_risiko as skor_risiko_residual')
                ->leftjoin('ref_matriks as ref_matriks_inherent', 't_analisis_risiko.id_matriks_inherent', '=', 'ref_matriks_inherent.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->where('t_analisis_risiko.id_analisis', $id)
                ->first();

        return view('lini1residualbpkp.edit', $this->data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
      if (Auth::check())
        {
            $rules = array(

                'kemungkinan_residual' => 'required',
                'dampak_residual' => 'required',
                'id_matriks_residual' => 'required',

            );
            $messages = [
                            'kemungkinan_residual.required' => 'Silahkan isi Level Kemungkinan Residual.',
                            'dampak_residual.required' => 'Silahkan isi Level Dampak Residual.',
                            'id_matriks_residual.required' => 'Silahkan isi Level Risiko Residual.',
                        ];

            $id_matriks_inherent = AnalisisRisiko::leftjoin('ref_matriks', 't_analisis_risiko.id_matriks_inherent', '=', 'ref_matriks.id_matriks')
                                ->where('t_analisis_risiko.id_analisis', $id)
                                ->firstOrFail();
            $skor_inherent = $id_matriks_inherent->skor_risiko;

            $id_matriks_residual = RefMatriks::where('id_matriks', $request->input('id_matriks_residual'))->firstOrFail();
            $skor_residual = $id_matriks_residual->skor_risiko;

            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } elseif($skor_residual > $skor_inherent) {
                return redirect()->back()->withErrors('Level Risiko Residual tidak boleh lebih tinggi daripada Level Risiko Inherent');
            } else {

                $idn = AnalisisRisiko::find($id);
                $idn->id_matriks_residual = Input::get('id_matriks_residual');
                $idn->user_update = Auth::user()->user_nip;

                $idn->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini1analisisbpkp.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
