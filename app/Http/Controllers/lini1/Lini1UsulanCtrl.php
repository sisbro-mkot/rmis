<?php

namespace App\Http\Controllers\lini1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;
use App\Models\dashboard\RefKategoriRisiko;
use App\Models\dashboard\RefTujuanSPIP;
use App\Models\dashboard\Usulan;

class Lini1UsulanCtrl extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kamus = DB::table('t_usulan')
                ->select('t_usulan.id_usulan as id',
                    't_usulan.nama_bagan_risiko as nama_bagan_risiko',
                    'ref_kategori_risiko.nama_kategori_risiko as nama_kategori_risiko',
                    'ref_tujuan_spip.nama_tujuan_spip as nama_tujuan_spip',
                    't_usulan.status_usulan as status_usulan')
                ->join('ref_kategori_risiko','t_usulan.id_kategori_risiko','=','ref_kategori_risiko.id_kategori_risiko')
                ->join('ref_tujuan_spip','t_usulan.id_tujuan_spip','=','ref_tujuan_spip.id_tujuan_spip')
                ->join('ref_data_umum', 't_usulan.s_kd_instansiunitorg', '=', 'ref_data_umum.s_kd_instansiunitorg')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->where('ref_data_umum.tahun', Auth::user()->tahun) 
                ->orderBy('t_usulan.id_usulan')
                ->get();

        return view('lini1usulan.index', compact('kamus'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createusunit()
    {
        
        $this->data['s_kd_instansiunitorg'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_kd_instansiunitorg as s_kd_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->get();
        $this->data['id_kategori_risiko'] = RefKategoriRisiko::pluck('nama_kategori_risiko as nama_kategori_risiko','id_kategori_risiko');
        $this->data['id_tujuan_spip'] = RefTujuanSPIP::pluck('nama_tujuan_spip as nama_tujuan_spip','id_tujuan_spip');

        return view('lini1usulan.createus', $this->data);
    }

    public function createusbpkp()
    {

        $this->data['s_kd_instansiunitorg'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_kd_instansiunitorg as s_kd_instansiunitorg')
                ->where('wm_instansiunitorg.s_kd_instansiunitorg', "07001500001000")
                ->get();
        $this->data['id_kategori_risiko'] = RefKategoriRisiko::pluck('nama_kategori_risiko as nama_kategori_risiko','id_kategori_risiko');
        $this->data['id_tujuan_spip'] = RefTujuanSPIP::pluck('nama_tujuan_spip as nama_tujuan_spip','id_tujuan_spip');

        return view('lini1usulan.createus', $this->data);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()){
            $rules = array(
                'nama_bagan_risiko' => 'required',
                'id_kategori_risiko' => 'required',
                'id_tujuan_spip' => 'required'
            );
            $messages = [
                            'nama_bagan_risiko.required' => 'Silahkan isi nama risiko.',
                            'id_kategori_risiko.required' => 'Silahkan pilih kategori risiko.',
                            'id_tujuan_spip.required' => 'Silahkan pilih metode pencapaian tujuan SPIP.'
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $now = Carbon::now('Asia/Jakarta');

                $id = new Usulan;
                $id->s_kd_instansiunitorg = Input::get('s_kd_instansiunitorg');
                $id->nama_bagan_risiko = Input::get('nama_bagan_risiko');
                $id->id_kategori_risiko = Input::get('id_kategori_risiko');
                $id->id_tujuan_spip = Input::get('id_tujuan_spip');
                $id->created_at = $now;
                $id->user_create = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini1usulan.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usulan = DB::table('t_usulan')
                ->select('t_usulan.id_usulan as id',
                    't_usulan.nama_bagan_risiko as nama_bagan_risiko',
                    'ref_kategori_risiko.nama_kategori_risiko as nama_kategori_risiko',
                    'ref_tujuan_spip.nama_tujuan_spip as nama_tujuan_spip',
                    't_usulan.status_usulan as status_usulan',
                    't_usulan.status_ubah as status_ubah',
                    't_usulan.tanggapan as tanggapan')
                ->join('ref_kategori_risiko','t_usulan.id_kategori_risiko','=','ref_kategori_risiko.id_kategori_risiko')
                ->join('ref_tujuan_spip','t_usulan.id_tujuan_spip','=','ref_tujuan_spip.id_tujuan_spip')
                ->where('t_usulan.id_usulan','=', $id)
                ->first();

        return view('lini1usulan.show', compact('usulan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
