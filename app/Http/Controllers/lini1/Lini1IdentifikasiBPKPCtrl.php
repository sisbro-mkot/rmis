<?php
 
namespace App\Http\Controllers\lini1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Models\dashboard\IdentifikasiRisiko;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\RefBaganRisiko;
 
class Lini1IdentifikasiBPKPCtrl extends Controller
{
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $identifikasi = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 't_identifikasi_risiko.tahun as tahun', 'ref_jns_konteks.nama_jns_konteks as nama_jns_konteks', 'ref_konteks.nama_konteks as nama_konteks', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko','t_identifikasi_risiko.nama_dampak as nama_dampak','ref_tujuan_spip.nama_tujuan_spip as nama_tujuan_spip')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('ref_tujuan_spip', 'ref_bagan_risiko.id_tujuan_spip', '=', 'ref_tujuan_spip.id_tujuan_spip')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->where('ref_data_umum.id_data_umum', 1)
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->get();
        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.s_kd_instansiunitorg', "07001500001000")
                ->first();
        $konteks = DB::table('t_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->whereRaw('ref_data_umum.id_data_umum = 1 AND ISNULL(t_penetapan_konteks.catatan_hapus)')
                ->where('t_penetapan_konteks.tahun', Auth::user()->tahun)
                ->count();


        return view('lini1identifikasibpkp.index', compact('identifikasi','unit','konteks'));
    }

    public function cetak()
    {
        $identifikasi = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 't_identifikasi_risiko.tahun as tahun', 'ref_jns_konteks.nama_jns_konteks as nama_jns_konteks', 'ref_konteks.nama_konteks as nama_konteks', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko','t_identifikasi_risiko.nama_dampak as nama_dampak','ref_tujuan_spip.nama_tujuan_spip as nama_tujuan_spip')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('ref_tujuan_spip', 'ref_bagan_risiko.id_tujuan_spip', '=', 'ref_tujuan_spip.id_tujuan_spip')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->where('ref_data_umum.id_data_umum', 1)
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->get();

        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.s_kd_instansiunitorg', "07001500001000")
                ->first();

        $tahun = Auth::user()->tahun;

        $pdf = PDF::loadView('lini1identifikasibpkp.cetak',  compact('identifikasi', 'unit', 'tahun'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('identifikasi_risiko.pdf', array('Attachment' => false));
        exit(0);
    }

    public function excelan()
    {
        $kolom = [
                'ID Identifikasi',
                'Pemilik Risiko',
                'Jenis Konteks', 
                'Nama Konteks', 
                'Kode Risiko',
                'Kategori Risiko',
                'Pernyataan Risiko',
                'Uraian Dampak',
                'Kemungkinan Inherent',
                'Dampak Inherent',
                'Inherent Risk',
                'Jumlah Pengendalian',
                'Kemungkinan Residual',
                'Dampak Residual',
                'Residual Risk',
                'Selera Risiko'
                ];
        $baris = DB::table('t_identifikasi_risiko')
                ->select(
                    't_identifikasi_risiko.id_identifikasi as id_identifikasi',
                    'ref_data_umum.s_nmjabdetail_pemilik as s_nmjabdetail_pemilik',
                    'ref_jns_konteks.nama_jns_konteks as nama_jns_konteks', 
                    'ref_konteks.nama_konteks as nama_konteks',
                    't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko',
                    'ref_kategori_risiko.nama_kategori_risiko', 
                    'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko',
                    't_identifikasi_risiko.nama_dampak as nama_dampak',
                    'ref_matriks_inherent.skor_kemungkinan as skor_kemungkinan_inherent',
                    'ref_matriks_inherent.skor_dampak as skor_dampak_inherent',
                    'ref_matriks_inherent.skor_risiko as skor_risiko_inherent', 
                    'Pengendalian.count_pengendalian as count_pengendalian',
                    'ref_matriks_residual.skor_kemungkinan as skor_kemungkinan_residual',
                    'ref_matriks_residual.skor_dampak as skor_dampak_residual', 
                    'ref_matriks_residual.skor_risiko as skor_risiko_residual',
                    'ref_data_umum.skor_selera as skor_selera'
                    )
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('ref_kategori_risiko', 'ref_bagan_risiko.id_kategori_risiko', '=', 'ref_kategori_risiko.id_kategori_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_inherent', 't_analisis_risiko.id_matriks_inherent', '=', 'ref_matriks_inherent.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin(DB::raw('(SELECT id_identifikasi, COUNT(id_pengendalian) AS count_pengendalian FROM `t_pengendalian` GROUP BY id_identifikasi) AS Pengendalian'), 
                            function($join)
                            {
                               $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'Pengendalian.id_identifikasi');
                            })
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->orderBy('t_identifikasi_risiko.id_identifikasi')
                ->get();
        $barisData = array_map(function($item) {
                        return (array)$item; 
                    }, $baris->toArray());
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet()->fromArray($kolom, NULL, 'A1');
        $sheet = $spreadsheet->getActiveSheet()->fromArray($barisData, NULL, 'A2');
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(255);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(255);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(255);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(12);
        $spreadsheet->getActiveSheet()->getStyle('A1:P1000')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A1:P1000')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
        $writer = new Xlsx($spreadsheet);
        $filename = 'analisis_risiko.xlsx';
        $writer->save($filename);
        header('Content-Type: application/x-www-form-urlencoded');
        header('Content-Transfer-Encoding: Binary');
        header("Content-disposition: attachment; filename=\"".$filename."\"");
        readfile($filename);
        exit;

    }

    public function excelrtp()
    {
        $kolom = [
                'ID Identifikasi',
                'Pemilik Risiko',
                'Jenis Konteks', 
                'Nama Konteks', 
                'Kode Risiko',
                'Kategori Risiko',
                'Pernyataan Risiko',
                'Uraian Dampak',
                'Kode Penyebab',                
                'Nama Pengendalian',
                'Sub Unsur SPIP',
                'Penanggung Jawab',
                'Output',
                'Periode Rencana',
                'Waktu Realisasi',
                'Uraian Hambatan'
                ];
        $baris = DB::table('t_identifikasi_risiko')
                ->select(
                    't_identifikasi_risiko.id_identifikasi as id_identifikasi',
                    'ref_data_umum.s_nmjabdetail_pemilik as s_nmjabdetail_pemilik',
                    'ref_jns_konteks.nama_jns_konteks as nama_jns_konteks', 
                    'ref_konteks.nama_konteks as nama_konteks',
                    't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko',
                    'ref_kategori_risiko.nama_kategori_risiko', 
                    'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko',
                    't_identifikasi_risiko.nama_dampak as nama_dampak',
                    't_penyebab_rca_kode.kode_penyebab as kode_penyebab',                    
                    't_rtp.kegiatan_pengendalian as kegiatan_pengendalian',
                    'ref_sub_unsur_spip.nama_sub_unsur as nama_sub_unsur',
                    'wm_jabdetail.s_nmjabdetail as s_nmjabdetail',
                    'ref_output.nama_output as nama_output', 
                    'ref_periode.nama_periode as nama_periode',
                    't_pemantauan.realisasi_waktu as realisasi_waktu', 
                    't_pemantauan.nama_hambatan as nama_hambatan'
                    )
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('ref_kategori_risiko', 'ref_bagan_risiko.id_kategori_risiko', '=', 'ref_kategori_risiko.id_kategori_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')                
                ->leftjoin('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                ->leftjoin('ref_jns_sebab', 't_penyebab_rca.id_jns_sebab', '=', 'ref_jns_sebab.id_jns_sebab')
                ->leftjoin('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                ->leftjoin('t_rtp', 't_penyebab_rca.id_penyebab', '=', 't_rtp.id_penyebab')
                ->leftjoin('ref_sub_unsur_spip', 't_rtp.id_sub_unsur', '=', 'ref_sub_unsur_spip.id_sub_unsur')
                ->leftjoin('wm_jabdetail', 't_rtp.s_kd_jabdetail_pic', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->leftjoin('ref_output', 't_rtp.id_output', '=', 'ref_output.id_output')
                ->leftjoin('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                ->leftjoin('t_pemantauan', 't_rtp.id_rtp', '=', 't_pemantauan.id_rtp')
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->get();
        $barisData = array_map(function($item) {
                        return (array)$item; 
                    }, $baris->toArray());
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet()->fromArray($kolom, NULL, 'A1');
        $sheet = $spreadsheet->getActiveSheet()->fromArray($barisData, NULL, 'A2');
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(255);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(255);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(255);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(100);
        $spreadsheet->getActiveSheet()->getStyle('A1:P1000')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A1:P1000')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
        $writer = new Xlsx($spreadsheet);
        $filename = 'rtp.xlsx';
        $writer->save($filename);
        header('Content-Type: application/x-www-form-urlencoded');
        header('Content-Transfer-Encoding: Binary');
        header("Content-disposition: attachment; filename=\"".$filename."\"");
        readfile($filename);
        exit;

    }

    public function excelmon()
    {
        $kolom = [
                'ID Keterjadian',
                'Periode',
                'Pemilik Risiko',
                'Nama Kejadian',
                'Kode Risiko (Teridentifikasi)',
                'Pernyataan Risiko (Teridentifikasi)',
                'Kode Penyebab (Teridentifikasi)',
                'Nama Akar Penyebab (Teridentifikasi)',                
                'Waktu Kejadian',
                'Tempat Kejadian',
                'Pemicu Kejadian',
                'Skor Dampak'
                ];
        $baris = DB::table('t_keterjadian')
                ->select(
                    't_keterjadian.id_keterjadian as id_keterjadian',
                    'ref_periode.nama_periode as nama_periode',
                    'wm_jabdetail.s_nmjabdetail as s_nmjabdetail',
                    't_keterjadian.nama_kejadian as nama_kejadian',
                    't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko',
                    'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko',
                    't_penyebab_rca_kode.kode_penyebab as kode_penyebab',
                    't_penyebab_rca_kode.nama_akar_penyebab as nama_akar_penyebab',
                    't_keterjadian.waktu_kejadian as waktu_kejadian',
                    't_keterjadian.tempat_kejadian as tempat_kejadian',
                    't_keterjadian.pemicu_kejadian as pemicu_kejadian',
                    't_keterjadian.skor_dampak as skor_dampak'

                    )
                ->join('ref_periode', 't_keterjadian.id_periode', '=', 'ref_periode.id_periode')
                ->join('ref_data_umum', 't_keterjadian.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('wm_jabdetail', 'ref_data_umum.s_kd_jabdetail_pemilik', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->leftjoin('t_identifikasi_risiko', 't_keterjadian.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->leftjoin('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->leftjoin('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->leftjoin('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                ->leftjoin('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->get();
        $barisData = array_map(function($item) {
                        return (array)$item; 
                    }, $baris->toArray());
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet()->fromArray($kolom, NULL, 'A1');
        $sheet = $spreadsheet->getActiveSheet()->fromArray($barisData, NULL, 'A2');
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(255);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(255);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(255);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(255);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(12);
        $spreadsheet->getActiveSheet()->getStyle('A1:L1000')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A1:L1000')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
        $writer = new Xlsx($spreadsheet);
        $filename = 'keterjadian.xlsx';
        $writer->save($filename);
        header('Content-Type: application/x-www-form-urlencoded');
        header('Content-Transfer-Encoding: Binary');
        header("Content-disposition: attachment; filename=\"".$filename."\"");
        readfile($filename);
        exit;

    }

    public function excel($id)
    {
        $kolom = ['Jenis Konteks', 
                'Nama Konteks', 
                'Kode Risiko',
                'Pernyataan Risiko',
                'Uraian Dampak',
                'Kemungkinan Inherent',
                'Dampak Inherent',
                'Inherent Risk',
                'Jumlah Pengendalian',
                'Kemungkinan Residual',
                'Dampak Residual',
                'Residual Risk',
                'Why 1', 
                'Why 2', 
                'Why 3', 
                'Why 4', 
                'Why 5', 
                'Akar Penyebab',                 
                'Jenis Penyebab',
                'Kode Penyebab',
                'Nama Pengendalian',
                'Sub Unsur SPIP',
                'Penanggung Jawab',
                'Output',
                'Periode Rencana',
                'Waktu Realisasi',
                'Uraian Hambatan',
                'Kemungkinan Treated',
                'Dampak Treated',
                'Treated Risk',
                'ID'
                ];
        $baris = DB::table('t_identifikasi_risiko')
                ->select(
                    'ref_jns_konteks.nama_jns_konteks as nama_jns_konteks', 
                    'ref_konteks.nama_konteks as nama_konteks',
                    't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 
                    'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko',
                    't_identifikasi_risiko.nama_dampak as nama_dampak',
                    'ref_matriks_inherent.skor_kemungkinan as skor_kemungkinan_inherent',
                    'ref_matriks_inherent.skor_dampak as skor_dampak_inherent',
                    'ref_matriks_inherent.skor_risiko as skor_risiko_inherent', 
                    'Pengendalian.count_pengendalian as count_pengendalian',
                    'ref_matriks_residual.skor_kemungkinan as skor_kemungkinan_residual',
                    'ref_matriks_residual.skor_dampak as skor_dampak_residual', 
                    'ref_matriks_residual.skor_risiko as skor_risiko_residual',
                    't_penyebab_rca.nama_penyebab_1 as nama_penyebab_1', 
                    't_penyebab_rca.nama_penyebab_2 as nama_penyebab_2', 
                    't_penyebab_rca.nama_penyebab_3 as nama_penyebab_3', 
                    't_penyebab_rca.nama_penyebab_4 as nama_penyebab_4', 
                    't_penyebab_rca.nama_penyebab_5 as nama_penyebab_5', 
                    't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 
                    'ref_jns_sebab.nama_jns_sebab as nama_jns_sebab',
                    't_penyebab_rca_kode.kode_penyebab as kode_penyebab',
                    't_rtp.kegiatan_pengendalian as kegiatan_pengendalian',
                    'ref_sub_unsur_spip.nama_sub_unsur as nama_sub_unsur',
                    'wm_jabdetail.s_nmjabdetail as s_nmjabdetail',
                    'ref_output.nama_output as nama_output', 
                    'ref_periode.nama_periode as nama_periode',
                    't_pemantauan.realisasi_waktu as realisasi_waktu', 
                    't_pemantauan.nama_hambatan as nama_hambatan',
                    'ref_matriks_treated.skor_kemungkinan as skor_kemungkinan_treated',
                    'ref_matriks_treated.skor_dampak as skor_dampak_treated',
                    'ref_matriks_treated.skor_risiko as skor_risiko_treated',
                    't_identifikasi_risiko.id_identifikasi as id_identifikasi'
                    )
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_inherent', 't_analisis_risiko.id_matriks_inherent', '=', 'ref_matriks_inherent.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin(DB::raw('(SELECT id_identifikasi, COUNT(id_pengendalian) AS count_pengendalian FROM `t_pengendalian` GROUP BY id_identifikasi) AS Pengendalian'), 
                            function($join)
                            {
                               $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'Pengendalian.id_identifikasi');
                            })
                ->leftjoin('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                ->leftjoin('ref_jns_sebab', 't_penyebab_rca.id_jns_sebab', '=', 'ref_jns_sebab.id_jns_sebab')
                ->leftjoin('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                ->leftjoin('t_rtp', 't_penyebab_rca.id_penyebab', '=', 't_rtp.id_penyebab')
                ->leftjoin('ref_sub_unsur_spip', 't_rtp.id_sub_unsur', '=', 'ref_sub_unsur_spip.id_sub_unsur')
                ->leftjoin('wm_jabdetail', 't_rtp.s_kd_jabdetail_pic', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->leftjoin('ref_output', 't_rtp.id_output', '=', 'ref_output.id_output')
                ->leftjoin('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->leftjoin('t_pemantauan', 't_rtp.id_rtp', '=', 't_pemantauan.id_rtp')
                ->join('wm_instansiunitorg', 'ref_data_umum.s_kd_instansiunitorg', '=', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('wm_instansiunitorg.id_instansiunitorg', $id)
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->get();
        $barisData = array_map(function($item) {
                        return (array)$item; 
                    }, $baris->toArray());
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet()->fromArray($kolom, NULL, 'A1');
        $sheet = $spreadsheet->getActiveSheet()->fromArray($barisData, NULL, 'A2');
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('R')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('S')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('T')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('U')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('V')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('W')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('X')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('Y')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('Z')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('AA')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('AB')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('AC')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('AD')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('AE')->setWidth(12);
        $spreadsheet->getActiveSheet()->getStyle('A1:AE1000')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A1:AE1000')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
        $writer = new Xlsx($spreadsheet);
        $filename = 'identifikasi_risiko.xlsx';
        $writer->save($filename);
        header('Content-Type: application/x-www-form-urlencoded');
        header('Content-Transfer-Encoding: Binary');
        header("Content-disposition: attachment; filename=\"".$filename."\"");
        readfile($filename);
        exit;

    }

public function excelall() 
    {
        $kolom = ['Pemilik Risiko',
                'Selera Risiko',
                'Jenis Konteks', 
                'Nama Konteks',
                'Keterangan Konteks', 
                'Kode Risiko',
                'Pernyataan Risiko',
                'Uraian Dampak',
                'Kemungkinan Inherent',
                'Dampak Inherent',
                'Inherent Risk',
                'Jumlah Pengendalian',
                'Kemungkinan Residual',
                'Dampak Residual',
                'Residual Risk',
                'Why 1', 
                'Why 2', 
                'Why 3', 
                'Why 4', 
                'Why 5', 
                'Akar Penyebab',                 
                'Jenis Penyebab',
                'Kode Penyebab',
                'Respon Risiko',
                'Nama Pengendalian',
                'Sub Unsur SPIP',
                'Penanggung Jawab',
                'Output',
                'Periode Rencana',
                'Waktu Realisasi',
                'Uraian Hambatan',
                'Kemungkinan Treated',
                'Dampak Treated',
                'Treated Risk',
                'Kemungkinan Aktual',
                'Dampak Aktual',
                'Actual Risk',
                'ID Identifikasi',
                'ID Penyebab',
                'ID RTP',
                'Kode Unit'
                ];
        $baris = DB::table('t_identifikasi_risiko')
                ->select(
                    'ref_data_umum.s_nmjabdetail_pemilik as s_nmjabdetail_pemilik',
                    'ref_data_umum.skor_selera as skor_selera',
                    'ref_jns_konteks.nama_jns_konteks as nama_jns_konteks', 
                    'ref_konteks.nama_konteks as nama_konteks',
                    'ref_konteks.ket_konteks as ket_konteks',
                    't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 
                    'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko',
                    't_identifikasi_risiko.nama_dampak as nama_dampak',
                    'ref_matriks_inherent.skor_kemungkinan as skor_kemungkinan_inherent',
                    'ref_matriks_inherent.skor_dampak as skor_dampak_inherent',
                    'ref_matriks_inherent.skor_risiko as skor_risiko_inherent', 
                    'Pengendalian.count_pengendalian as count_pengendalian',
                    'ref_matriks_residual.skor_kemungkinan as skor_kemungkinan_residual',
                    'ref_matriks_residual.skor_dampak as skor_dampak_residual', 
                    'ref_matriks_residual.skor_risiko as skor_risiko_residual',
                    't_penyebab_rca.nama_penyebab_1 as nama_penyebab_1', 
                    't_penyebab_rca.nama_penyebab_2 as nama_penyebab_2', 
                    't_penyebab_rca.nama_penyebab_3 as nama_penyebab_3', 
                    't_penyebab_rca.nama_penyebab_4 as nama_penyebab_4', 
                    't_penyebab_rca.nama_penyebab_5 as nama_penyebab_5', 
                    't_penyebab_rca.nama_akar_penyebab as nama_akar_penyebab', 
                    'ref_jns_sebab.nama_jns_sebab as nama_jns_sebab',
                    't_penyebab_rca_kode.kode_penyebab as kode_penyebab',
                    't_rtp.respon_risiko as respon_risiko',
                    't_rtp.kegiatan_pengendalian as kegiatan_pengendalian',
                    'ref_sub_unsur_spip.nama_sub_unsur as nama_sub_unsur',
                    'wm_jabdetail.s_nmjabdetail as s_nmjabdetail',
                    'ref_output.nama_output as nama_output', 
                    'ref_periode.nama_periode as nama_periode',
                    't_pemantauan.realisasi_waktu as realisasi_waktu', 
                    't_pemantauan.nama_hambatan as nama_hambatan',
                    'ref_matriks_treated.skor_kemungkinan as skor_kemungkinan_treated',
                    'ref_matriks_treated.skor_dampak as skor_dampak_treated',
                    'ref_matriks_treated.skor_risiko as skor_risiko_treated',
                    'ref_matriks_aktual.skor_kemungkinan as skor_kemungkinan_aktual',
                    'ref_matriks_aktual.skor_dampak as skor_dampak_aktual',
                    'ref_matriks_aktual.skor_risiko as skor_risiko_aktual',
                    't_identifikasi_risiko.id_identifikasi as id_identifikasi',
                    't_penyebab_rca.id_penyebab as id_penyebab',
                    't_rtp.id_rtp as id_rtp',
                    'wm_instansiunitorg.s_format_surat as s_format_surat'
                    )
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('wm_instansiunitorg', 'ref_data_umum.s_kd_instansiunitorg', '=', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_inherent', 't_analisis_risiko.id_matriks_inherent', '=', 'ref_matriks_inherent.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin(DB::raw('(SELECT id_identifikasi, COUNT(id_pengendalian) AS count_pengendalian FROM `t_pengendalian` GROUP BY id_identifikasi) AS Pengendalian'), 
                            function($join)
                            {
                               $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'Pengendalian.id_identifikasi');
                            })
                ->leftjoin('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                ->leftjoin('ref_jns_sebab', 't_penyebab_rca.id_jns_sebab', '=', 'ref_jns_sebab.id_jns_sebab')
                ->leftjoin('t_penyebab_rca_kode', 't_penyebab_rca.id_penyebab', '=', 't_penyebab_rca_kode.id_penyebab')
                ->leftjoin('t_rtp', 't_penyebab_rca.id_penyebab', '=', 't_rtp.id_penyebab')
                ->leftjoin('ref_sub_unsur_spip', 't_rtp.id_sub_unsur', '=', 'ref_sub_unsur_spip.id_sub_unsur')
                ->leftjoin('wm_jabdetail', 't_rtp.s_kd_jabdetail_pic', '=', 'wm_jabdetail.s_kd_jabdetail')
                ->leftjoin('ref_output', 't_rtp.id_output', '=', 'ref_output.id_output')
                ->leftjoin('ref_periode', 't_rtp.id_periode', '=', 'ref_periode.id_periode')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->leftjoin('t_pemantauan', 't_rtp.id_rtp', '=', 't_pemantauan.id_rtp')
                ->leftjoin('t_pemantauan_level', 't_identifikasi_risiko.id_identifikasi', '=', 't_pemantauan_level.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_aktual', 't_pemantauan_level.id_matriks_aktual', '=', 'ref_matriks_aktual.id_matriks')
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->get();
        $barisData = array_map(function($item) {
                        return (array)$item; 
                    }, $baris->toArray());
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet()->fromArray($kolom, NULL, 'A1');
        $sheet = $spreadsheet->getActiveSheet()->fromArray($barisData, NULL, 'A2');
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('R')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('S')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('T')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('U')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('V')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('W')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('X')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('Y')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('Z')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('AA')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('AB')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('AC')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('AD')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('AE')->setWidth(24);
        $spreadsheet->getActiveSheet()->getColumnDimension('AF')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('AG')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('AH')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('AI')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('AJ')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('AK')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('AL')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('AM')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('AN')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('AO')->setWidth(12);
        $spreadsheet->getActiveSheet()->getStyle('A1:AO1000')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
        $writer = new Xlsx($spreadsheet);
        $filename = 'identifikasi_risiko.xlsx';
        $writer->save($filename);
        header('Content-Type: application/x-www-form-urlencoded');
        header('Content-Transfer-Encoding: Binary');
        header("Content-disposition: attachment; filename=\"".$filename."\"");
        readfile($filename);
        exit;

    }

    public function excelec($id)
    {
        $kolom = [
                'ID Pengendalian', 
                'Kode Risiko',
                'Nama Konteks',
                'Pernyataan Risiko',
                'Existing Control',
                'Nama Sub Unsur SPIP',
                'Nilai Inherent Risk',
                'Nilai Residual Risk' 
                ];
        $baris = DB::table('t_identifikasi_risiko')
                ->select(
                    't_pengendalian.id_pengendalian as id_pengendalian',
                    't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko',
                    'ref_konteks.nama_konteks as nama_konteks',
                    'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko',
                    't_pengendalian.nama_pengendalian as nama_pengendalian',
                    'ref_sub_unsur_spip.nama_sub_unsur as nama_sub_unsur',
                    'ref_matriks_inherent.skor_risiko as skor_risiko_inherent',
                    'ref_matriks_residual.skor_risiko as skor_risiko_residual'
                    )
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('t_pengendalian', 't_identifikasi_risiko.id_identifikasi', '=', 't_pengendalian.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_inherent', 't_analisis_risiko.id_matriks_inherent', '=', 'ref_matriks_inherent.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_sub_unsur_spip', 't_pengendalian.id_sub_unsur', '=', 'ref_sub_unsur_spip.id_sub_unsur')
                ->join('wm_instansiunitorg', 'ref_data_umum.s_kd_instansiunitorg', '=', 'wm_instansiunitorg.s_kd_instansiunitorg')
                ->where('wm_instansiunitorg.id_instansiunitorg', $id)
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->orderBy('t_identifikasi_risiko.id_identifikasi')
                ->get();
        $barisData = array_map(function($item) {
                        return (array)$item; 
                    }, $baris->toArray());
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet()->fromArray($kolom, NULL, 'A1');
        $sheet = $spreadsheet->getActiveSheet()->fromArray($barisData, NULL, 'A2');
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(6);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(12);
        $spreadsheet->getActiveSheet()->getStyle('A1:H1000')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A1:H1000')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
        $writer = new Xlsx($spreadsheet);
        $filename = 'existing_control.xlsx';
        $writer->save($filename);
        header('Content-Type: application/x-www-form-urlencoded');
        header('Content-Transfer-Encoding: Binary');
        header("Content-disposition: attachment; filename=\"".$filename."\"");
        readfile($filename);
        exit;

    }

    public function excelecall()
    {
        $kolom = [
                'ID Pengendalian',  
                'Pemilik Risiko',
                'Kode Risiko',
                'Nama Konteks',
                'Pernyataan Risiko',
                'Existing Control',
                'Nama Sub Unsur SPIP',
                'Nilai Inherent Risk',
                'Nilai Residual Risk' 
                ];
        $baris = DB::table('t_identifikasi_risiko')
                ->select(
                    't_pengendalian.id_pengendalian as id_pengendalian',
                    'ref_data_umum.s_nmjabdetail_pemilik as s_nmjabdetail_pemilik', 
                    't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko',
                    'ref_konteks.nama_konteks as nama_konteks',
                    'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko',
                    't_pengendalian.nama_pengendalian as nama_pengendalian',
                    'ref_sub_unsur_spip.nama_sub_unsur as nama_sub_unsur',
                    'ref_matriks_inherent.skor_risiko as skor_risiko_inherent',
                    'ref_matriks_residual.skor_risiko as skor_risiko_residual'
                    )
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->join('t_pengendalian', 't_identifikasi_risiko.id_identifikasi', '=', 't_pengendalian.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_inherent', 't_analisis_risiko.id_matriks_inherent', '=', 'ref_matriks_inherent.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin('ref_sub_unsur_spip', 't_pengendalian.id_sub_unsur', '=', 'ref_sub_unsur_spip.id_sub_unsur')
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->orderBy('t_identifikasi_risiko.id_identifikasi')
                ->get();
        $barisData = array_map(function($item) {
                        return (array)$item; 
                    }, $baris->toArray());
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet()->fromArray($kolom, NULL, 'A1');
        $sheet = $spreadsheet->getActiveSheet()->fromArray($barisData, NULL, 'A2');
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(6);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(255);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(255);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(12);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(12);
        $spreadsheet->getActiveSheet()->getStyle('A1:I1000')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A1:I1000')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
        $writer = new Xlsx($spreadsheet);
        $filename = 'existing_control.xlsx';
        $writer->save($filename);
        header('Content-Type: application/x-www-form-urlencoded');
        header('Content-Transfer-Encoding: Binary');
        header("Content-disposition: attachment; filename=\"".$filename."\"");
        readfile($filename);
        exit;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create()
    {
        $this->data['tahun'] = Auth::user()->tahun;
        $this->data['s_kd_jabdetail'] = DB::table('ref_data_umum')
                ->select('ref_data_umum.s_nmjabdetail_pemilik as s_nmjabdetail_pemilik','ref_data_umum.s_kd_jabdetail_pemilik as s_kd_jabdetail')
                ->where('ref_data_umum.id_data_umum', 1)
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->get();
        $this->data['id_penetapan_konteks'] = DB::table('t_penetapan_konteks')
                ->select('ref_konteks.nama_konteks as nama_konteks', 't_penetapan_konteks.id_penetapan_konteks as id_penetapan_konteks','ref_konteks.ket_konteks as ket_konteks','ref_konteks.id_konteks as id_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=','ref_data_umum.id_data_umum')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->whereRaw('ref_data_umum.id_data_umum = 1 AND ISNULL(t_penetapan_konteks.catatan_hapus)')
                ->where('t_penetapan_konteks.tahun', Auth::user()->tahun)
                ->get();
        $this->data['id_bagan_risiko'] = RefBaganRisiko::where('nama_bagan_risiko', 'not like', "%deleted%")->pluck('nama_bagan_risiko as nama_bagan_risiko','id_bagan_risiko');
        $this->data['nama_instansiunitorg'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.s_kd_instansiunitorg', "07001500001000")
                ->first();

        return view('lini1identifikasibpkp.createidbpkp', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

      if (Auth::check())
        {
            $rules = array(
                'tahun' => 'required',
                's_kd_jabdetail' => 'required',
                'id_penetapan_konteks' => 'required',
                'id_bagan_risiko' => 'required',
                'nama_dampak' => 'required',
            );
            $messages = [
                            'tahun.required' => 'Silahkan isi tahun.',
                            's_kd_jabdetail.required' => 'Silahkan pilih pemilik risiko.',
                            'id_penetapan_konteks.required' => 'Silahkan pilih nama konteks.',
                            'id_bagan_risiko.required' => 'Silahkan pilih nama risiko.',
                            'nama_dampak.required' => 'Silahkan isi nama dampak.',
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $now = Carbon::now('Asia/Jakarta');

                $id = new IdentifikasiRisiko;
                $id->tahun = Input::get('tahun');
                $id->s_kd_jabdetail = Input::get('s_kd_jabdetail');
                $id->id_penetapan_konteks = Input::get('id_penetapan_konteks');
                $id->id_bagan_risiko = Input::get('id_bagan_risiko');
                $id->nama_dampak = Input::get('nama_dampak');
                $id->created_at = $now;
                $id->user_create = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini1identifikasibpkp.index');
            }
        } else {
            return redirect()->back();
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['tahun'] = Auth::user()->tahun;
        $this->data['s_kd_jabdetail'] = DB::table('ref_data_umum')
                ->select('ref_data_umum.s_nmjabdetail_pemilik as s_nmjabdetail_pemilik','ref_data_umum.s_kd_jabdetail_pemilik as s_kd_jabdetail')
                ->where('ref_data_umum.id_data_umum', 1)
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->get();
        $this->data['id_penetapan_konteks'] = DB::table('t_penetapan_konteks')
                ->select('ref_konteks.nama_konteks as nama_konteks', 't_penetapan_konteks.id_penetapan_konteks as id_penetapan_konteks','ref_konteks.ket_konteks as ket_konteks','ref_konteks.id_konteks as id_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=','ref_data_umum.id_data_umum')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->whereRaw('ref_data_umum.id_data_umum = 1 AND ISNULL(t_penetapan_konteks.catatan_hapus)')
                ->where('t_penetapan_konteks.tahun', Auth::user()->tahun)
                ->get();
        $this->data['id_bagan_risiko'] = RefBaganRisiko::where('nama_bagan_risiko', 'not like', "%deleted%")->pluck('nama_bagan_risiko as nama_bagan_risiko','id_bagan_risiko');
        $this->data['nama_instansiunitorg'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.s_kd_instansiunitorg', "07001500001000")
                ->first();
        $this->data['identifikasi'] = IdentifikasiRisiko::find($id);

        return view('lini1identifikasibpkp.editidbpkp', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check())
        {
            $rules = array(
                'tahun' => 'required',
                's_kd_jabdetail' => 'required',
                'id_penetapan_konteks' => 'required',
                'id_bagan_risiko' => 'required',
                'nama_dampak' => 'required',
            );
            $messages = [
                            'tahun.required' => 'Silahkan isi tahun.',
                            's_kd_jabdetail.required' => 'Silahkan pilih pemilik risiko.',
                            'id_penetapan_konteks.required' => 'Silahkan pilih nama konteks.',
                            'id_bagan_risiko.required' => 'Silahkan pilih nama risiko.',
                            'nama_dampak.required' => 'Silahkan isi nama dampak.',
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $now = Carbon::now('Asia/Jakarta');
                $id = IdentifikasiRisiko::find($id);
                $id->tahun = Input::get('tahun');
                $id->s_kd_jabdetail = Input::get('s_kd_jabdetail');
                $id->id_penetapan_konteks = Input::get('id_penetapan_konteks');
                $id->id_bagan_risiko = Input::get('id_bagan_risiko');
                $id->nama_dampak = Input::get('nama_dampak');
                $id->updated_at = $now;
                $id->user_update = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah diubah.', 'Selamat');
                return redirect()->route('lini1identifikasibpkp.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getHeatmapBPKP()
    {
        if(Auth::user()->role_id == '3') {
            $risikoteridentifikasi = DB::table('t_identifikasi_risiko')
                      ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                      ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                      ->where('ref_data_umum.id_data_umum', 1)
                      ->where('ref_data_umum.tahun', Auth::user()->tahun)
                      ->count();
            $risikotermitigasi = DB::table('t_analisis_risiko')
                        ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko <= ref_data_umum.skor_selera) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko <= ref_data_umum.skor_selera) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                        ->count();
            $sebabteridentifikasi = DB::table('t_penyebab_rca')
                        ->leftjoin('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->count();
            $sebabtermitigasi = DB::table('t_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->leftjoin('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->groupBy('t_rtp.id_penyebab')
                        ->get()
                        ->count();
            $rtpjadwal = DB::table('t_rtp')
                        ->leftjoin('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->count();
            $rtprealisasi = DB::table('t_pemantauan')
                        ->join('t_rtp', 't_pemantauan.id_rtp', '=', 't_rtp.id_rtp')
                        ->join('t_penyebab_rca', 't_rtp.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->whereNotNull('realisasi_waktu')
                        ->count();
            $insidenk = DB::table('t_keterjadian')
                        ->join('ref_data_umum', 't_keterjadian.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->count();
            $insidenr = DB::table('t_keterjadian')
                        ->join('t_identifikasi_risiko', 't_keterjadian.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->groupBy('t_identifikasi_risiko.id_identifikasi')
                        ->get()
                        ->count();
            $insidenp = DB::table('t_keterjadian')
                        ->join('t_penyebab_rca', 't_keterjadian.id_penyebab', '=', 't_penyebab_rca.id_penyebab')
                        ->join('t_identifikasi_risiko', 't_penyebab_rca.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->groupBy('t_penyebab_rca.id_penyebab')
                        ->get()
                        ->count();
            $l1 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 1) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 1) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l2 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 2) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 2) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count(); 
            $l3 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 3) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 3) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count(); 
            $l4 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 4) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 4) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l5 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 5) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 5) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l6 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 6) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 6) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l7 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 7) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 7) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l8 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 8) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 8) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l9 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 9) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 9) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l10 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 10) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 10) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l11 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 11) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 11) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l12 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 12) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 12) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l13 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 13) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 13) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l14 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 14) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 14) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l15 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 15) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 15) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l16 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 16) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 16) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l17 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 17) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 17) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l18 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 18) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 18) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l19 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 19) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 19) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l20 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 20) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 20) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l21 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 21) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 21) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l22 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 22) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 22) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l23 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 23) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 23) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l24 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 24) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 24) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $l25 = DB::table('t_identifikasi_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                    ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                    ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                    ->whereRaw('(ISNULL(ref_matriks_treated.skor_risiko) AND (ref_matriks_residual.skor_risiko = 25) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?) OR ((ref_matriks_treated.skor_risiko = 25) AND ref_data_umum.id_data_umum = 1 AND ref_data_umum.tahun = ?)', [Auth::user()->tahun, Auth::user()->tahun])
                    ->count();
            $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.s_kd_instansiunitorg', "07001500001000")
                ->first();
            $selera = DB::table('ref_data_umum')
                    ->select('ref_data_umum.skor_selera as skor_selera')
                    ->where('ref_data_umum.id_data_umum', 1)
                    ->first();

        return view('lini1identifikasibpkp.heatmapbpkp', compact('risikoteridentifikasi','risikotermitigasi','sebabteridentifikasi','sebabtermitigasi','rtpjadwal','rtprealisasi','insidenk','insidenr','insidenp','l1','l2','l3','l4','l5','l6','l7','l8','l9','l10','l11','l12','l13','l14','l15','l16','l17','l18','l19','l20','l21','l22','l23','l24','l25','unit','selera'));
        }
    }


    public function getDashRegisterBPKP()
    {
        if(Auth::user()->role_id == '3') {
            $identifikasi = DB::table('t_identifikasi_risiko')
                        ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks_residual.skor_risiko as skor_risiko_residual', 'ref_matriks_treated.skor_risiko as skor_risiko_treated', 'Penyebab.count_penyebab as count_penyebab', 'RTP.count_rtp as count_rtp')
                        ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                        ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                        ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                        ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                        ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                        ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                        ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                        ->leftjoin(DB::raw('(SELECT id_identifikasi, COUNT(id_penyebab) AS count_penyebab FROM `t_penyebab_rca` GROUP BY id_identifikasi) AS Penyebab'), 
                            function($join)
                            {
                               $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'Penyebab.id_identifikasi');
                            })
                        ->leftjoin(DB::raw('(SELECT 
                            C.id_identifikasi AS id_identifikasi,
                            C.id_penyebab AS id_penyebab,
                            D.count_rtp AS count_rtp 
                            FROM t_penyebab_rca AS C
                            LEFT JOIN
                                (SELECT B.id_penyebab AS id_penyebab, COUNT(A.id_rtp) AS count_rtp 
                                FROM t_rtp AS A
                                LEFT JOIN t_penyebab_rca AS B
                                ON A.id_penyebab = B.id_penyebab
                                GROUP BY B.id_penyebab) AS D
                                ON C.id_penyebab = D.id_penyebab
                                GROUP BY C.id_identifikasi) AS RTP'), 
                            function($join)
                            {
                               $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'RTP.id_identifikasi');
                            })
                        ->where('ref_data_umum.id_data_umum', 1)
                        ->where('ref_data_umum.tahun', Auth::user()->tahun)
                        ->get();
        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.s_kd_instansiunitorg', "07001500001000")
                ->first();
            
        return view('lini1identifikasibpkp.registerbpkp', compact('identifikasi','unit'));
        }
    }

    public function getFilterRegisterBPKP($id)
    {
        if(Auth::user()->role_id == '3') {
            
            $identifikasi = DB::table('t_identifikasi_risiko')
                            ->select('t_identifikasi_risiko.id_identifikasi as id','t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 'ref_matriks_residual.skor_risiko as skor_risiko_residual', 'ref_matriks_treated.skor_risiko as skor_risiko_treated', 'Penyebab.count_penyebab as count_penyebab', 'RTP.count_rtp as count_rtp')
                            ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                            ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                            ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                            ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                            ->leftjoin('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                            ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                            ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                            ->leftjoin(DB::raw('(SELECT id_identifikasi, COUNT(id_penyebab) AS count_penyebab FROM `t_penyebab_rca` GROUP BY id_identifikasi) AS Penyebab'), 
                                function($join)
                                {
                                   $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'Penyebab.id_identifikasi');
                                })
                            ->leftjoin(DB::raw('(SELECT 
                                C.id_identifikasi AS id_identifikasi,
                                C.id_penyebab AS id_penyebab,
                                D.count_rtp AS count_rtp 
                                FROM t_penyebab_rca AS C
                                LEFT JOIN
                                    (SELECT B.id_penyebab AS id_penyebab, COUNT(A.id_rtp) AS count_rtp 
                                    FROM t_rtp AS A
                                    LEFT JOIN t_penyebab_rca AS B
                                    ON A.id_penyebab = B.id_penyebab
                                    GROUP BY B.id_penyebab) AS D
                                    ON C.id_penyebab = D.id_penyebab
                                    GROUP BY C.id_identifikasi) AS RTP'), 
                                function($join)
                                {
                                   $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'RTP.id_identifikasi');
                                })
                            ->where([['ref_data_umum.id_data_umum', '=', 1],
                                    ['ref_data_umum.tahun', Auth::user()->tahun],
                                    ['ref_matriks_treated.skor_risiko', '=', $id],
                                    ])
                            ->orWhere([['ref_data_umum.id_data_umum', '=', 1],
                                    ['ref_data_umum.tahun', Auth::user()->tahun],
                                    ['ref_matriks_residual.skor_risiko', '=', $id],
                                    ['ref_matriks_treated.skor_risiko', '=', NULL],
                                    ])
                            ->get();
            $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->where('wm_instansiunitorg.s_kd_instansiunitorg', "07001500001000")
                ->first();

        return view('lini1identifikasibpkp.registerbpkp', compact('identifikasi','unit'));
        }
    }






}
