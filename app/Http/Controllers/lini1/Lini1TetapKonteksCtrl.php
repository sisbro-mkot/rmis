<?php

namespace App\Http\Controllers\lini1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Models\dashboard\RefDataUmum;
use App\Models\dashboard\RefJnsKonteks;
use App\Models\dashboard\RefKonteks;
use App\Models\dashboard\PenetapanKonteks;

class Lini1TetapKonteksCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
 
        $konteks = DB::table('t_penetapan_konteks')
                ->select('t_penetapan_konteks.id_penetapan_konteks as id', 't_penetapan_konteks.tahun as tahun', 'ref_data_umum.s_nmjabdetail_pemilik as s_nmjabdetail_pemilik', 't_penetapan_konteks.id_konteks as id_konteks', 'ref_konteks.nama_konteks as nama_konteks', 'ref_jns_konteks.nama_jns_konteks as nama_jns_konteks', 'ref_konteks.ket_konteks as ket_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                ->whereRaw('vw_renpeglast.niplama = ? AND ISNULL(t_penetapan_konteks.catatan_hapus)', Auth::user()->user_nip)
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->get();
        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        return view('lini1tetapkonteks.index', compact('konteks','unit'));

    }
 
    public function pilihKonteks($id) 
    {  

            $konteks = DB::table('ref_konteks')
                ->select('nama_konteks', 'id_konteks')
                ->where('id_jns_konteks', $id)
                ->get();
            return json_encode($konteks);
        
    }

    public function excel()
    {
        $dataUmum = [
                        ['Nama Pemilik Risiko'],
                        ['Jabatan Pemilik Risiko'],
                        ['Nama Koordinator Pengelola Risiko'],
                        ['Jabatan Koordinator Pengelola Risiko'],
                        ['Periode Penerapan'],
                    ];
        $dataPemilik = DB::table('ref_data_umum')
                    ->select('ref_data_umum.s_nmjabdetail_pemilik as s_nmjabdetail_pemilik')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->get();
        $barisPemilik = array_map(function($item) {
                        return (array)$item; 
                    }, $dataPemilik->toArray());
        $dataPengelola = DB::table('ref_data_umum')
                    ->select('ref_data_umum.s_nmjabdetail_koordinator as s_nmjabdetail_koordinator')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->get();
        $barisPengelola = array_map(function($item) {
                        return (array)$item; 
                    }, $dataPengelola->toArray());
        $dataPeriode = DB::table('ref_data_umum')
                    ->select('ref_data_umum.tahun as tahun')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->get();
        $barisPeriode = array_map(function($item) {
                        return (array)$item; 
                    }, $dataPeriode->toArray());
        $barisUmum = [
                        NULL, $dataPemilik, NULL, $dataPengelola,  $dataPeriode
                    ];
        $dataSelera = DB::table('ref_data_umum')
                    ->select('ref_data_umum.skor_selera as skor_selera')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->get();
        $barisSelera = array_map(function($item) {
                        return (array)$item; 
                    }, $dataSelera->toArray());
        $kolom1 = ['No.', 
                'Ancaman', 
                'Nama Konteks',
                'Indikator'
                ];
        $dataGoing = DB::table('t_identifikasi_risiko')
                    ->select('ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko',
                        'ref_konteks.nama_konteks as nama_konteks',
                        'ref_konteks.ket_konteks as ket_konteks'
                    )
                    ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                    ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->where('ref_konteks.id_jns_konteks', 1)
                    ->get();
        $countGoing = DB::table('t_identifikasi_risiko')
                    ->select('ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko',
                        'ref_konteks.nama_konteks as nama_konteks',
                        'ref_konteks.ket_konteks as ket_konteks'
                    )
                    ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                    ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                    ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                    ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->where('ref_konteks.id_jns_konteks', 1)
                    ->count();
        $jmlGoing = $countGoing + 4;
        $barisGoing = array_map(function($item) {
                        return (array)$item; 
                    }, $dataGoing->toArray());

        $kolom2 = ['No.', 
                'Nama Konteks', 
                'Indikator'
                ];
        $dataSasaran = DB::table('t_penetapan_konteks')
                    ->select(
                        'ref_konteks.nama_konteks as nama_konteks',
                        'ref_konteks.ket_konteks as ket_konteks'
                    )
                    ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                    ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->where('ref_konteks.id_jns_konteks', '>',1)
                    ->where('ref_konteks.id_jns_konteks', '<',5)
                    ->get();
        $countSasaran = DB::table('t_penetapan_konteks')
                    ->select(
                        'ref_konteks.nama_konteks as nama_konteks',
                        'ref_konteks.ket_konteks as ket_konteks'
                    )
                    ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                    ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->where('ref_konteks.id_jns_konteks', '>',1)
                    ->where('ref_konteks.id_jns_konteks', '<',5)
                    ->count();
        $jmlSasaran = $countSasaran + 4;
        $barisSasaran = array_map(function($item) {
                        return (array)$item; 
                    }, $dataSasaran->toArray());

        $dataProbis = DB::table('t_penetapan_konteks')
                    ->select(
                        'ref_konteks.nama_konteks as nama_konteks',
                        'ref_konteks.ket_konteks as ket_konteks'
                    )
                    ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                    ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->where('ref_konteks.id_jns_konteks', 5)
                    ->get();
        $countProbis = DB::table('t_penetapan_konteks')
                    ->select(
                        'ref_konteks.nama_konteks as nama_konteks',
                        'ref_konteks.ket_konteks as ket_konteks'
                    )
                    ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
                    ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
                    ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->where('ref_konteks.id_jns_konteks', 5)
                    ->count();
        $jmlProbis = $countProbis + 4;
        $barisProbis = array_map(function($item) {
                        return (array)$item; 
                    }, $dataProbis->toArray());

        $kolom3 = [
                'No.',
                'Daftar Pemangku Kepentingan', 
                'Keterangan'
                ];

        $dataStake = DB::table('t_stakeholder')
                    ->select(
                        't_stakeholder.nama_stakeholder as nama_stakeholder',
                        't_stakeholder.uraian as uraian'
                    )
                    ->join('ref_data_umum', 't_stakeholder.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->get();
        $countStake = DB::table('t_stakeholder')
                    ->select(
                        't_stakeholder.nama_stakeholder as nama_stakeholder',
                        't_stakeholder.uraian as uraian'
                    )
                    ->join('ref_data_umum', 't_stakeholder.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                    ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                    ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                    ->where('ref_data_umum.tahun', Auth::user()->tahun)
                    ->count();
        $jmlStake = $countStake + 4;
        $barisStake = array_map(function($item) {
                        return (array)$item; 
                    }, $dataStake->toArray());

        $kolom4 = [                
                'Skor Selera Risiko', 
                'Keterangan'
                ];

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()->setCellValue('A1', 'FORMULIR PENETAPAN KONTEKS MANAJEMEN RISIKO');
        $spreadsheet->getActiveSheet()->fromArray($dataUmum, NULL, 'B3');
        $spreadsheet->getActiveSheet()->getStyle('B3:B7')->getNumberFormat()->setFormatCode('@ * ":"');
        $spreadsheet->getActiveSheet()->fromArray($barisPemilik, NULL, 'C4');
        $spreadsheet->getActiveSheet()->fromArray($barisPengelola, NULL, 'C6');
        $spreadsheet->getActiveSheet()->fromArray($barisPeriode, NULL, 'C7');
        $spreadsheet->getActiveSheet()->getStyle('C7')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
        $spreadsheet->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->setCellValue('A9', '1. ');
        $spreadsheet->getActiveSheet()->setCellValue('B9', 'Hal-hal yang dapat mengancam eksistensi unit pemilik risiko');

        $spreadsheet->getActiveSheet()->mergeCells('A1:D1');
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

        $spreadsheet->getActiveSheet()->fromArray($kolom1, NULL, 'A11');
        $spreadsheet->getActiveSheet()->fromArray($barisGoing, NULL, 'B12');

        $row1 = 1;
        foreach ($dataGoing as $key => $value) {
            $no1 = 'A'. (11 + $row1);
            $spreadsheet->getActiveSheet()->setCellValue($no1, $row1);
            $row1++;
        }

        $no2A = 'A'. (9 + $jmlGoing);
        $no2B = 'B'. (9 + $jmlGoing);

        $spreadsheet->getActiveSheet()->setCellValue($no2A, '2. ');
        $spreadsheet->getActiveSheet()->setCellValue($no2B, 'Sasaran Strategis/Program/Kegiatan Unit Pemilik Risiko');

        $data2A = 'A'. (11 + $jmlGoing);
        $data2B = 'B'. (12 + $jmlGoing);

        $spreadsheet->getActiveSheet()->fromArray($kolom2, NULL, $data2A);
        $spreadsheet->getActiveSheet()->fromArray($barisSasaran, NULL, $data2B);

        $row2 = 1;
        foreach ($dataSasaran as $key => $value) {
            $no2 = 'A'. (11 + $row2 + $jmlGoing);
            $spreadsheet->getActiveSheet()->setCellValue($no2, $row2);
            $row2++;
        }

        $no3A = 'A'. (9 + $jmlGoing + $jmlSasaran);
        $no3B = 'B'. (9 + $jmlGoing + $jmlSasaran);

        $spreadsheet->getActiveSheet()->setCellValue($no3A, '3. ');
        $spreadsheet->getActiveSheet()->setCellValue($no3B, 'Proses Bisnis Unit Pemilik Risiko');

        $data3A = 'A'. (11 + $jmlGoing + $jmlSasaran);
        $data3B = 'B'. (12 + $jmlGoing + $jmlSasaran);

        $spreadsheet->getActiveSheet()->fromArray($kolom2, NULL, $data3A);
        $spreadsheet->getActiveSheet()->fromArray($barisProbis, NULL, $data3B);

        $row3 = 1;
        foreach ($dataProbis as $key => $value) {
            $no3 = 'A'. (11 + $row3 + $jmlGoing + $jmlSasaran);
            $spreadsheet->getActiveSheet()->setCellValue($no3, $row3);
            $row3++;
        }

        $no4A = 'A'. (9 + $jmlGoing + $jmlSasaran + $jmlProbis);
        $no4B = 'B'. (9 + $jmlGoing + $jmlSasaran + $jmlProbis);

        $spreadsheet->getActiveSheet()->setCellValue($no4A, '4. ');
        $spreadsheet->getActiveSheet()->setCellValue($no4B, 'Daftar Pemangku Kepentingan');

        $data4A = 'A'. (11 + $jmlGoing + $jmlSasaran + $jmlProbis);
        $data4B = 'B'. (12 + $jmlGoing + $jmlSasaran + $jmlProbis);

        $spreadsheet->getActiveSheet()->fromArray($kolom3, NULL, $data4A);
        $spreadsheet->getActiveSheet()->fromArray($barisStake, NULL, $data4B);

        $row4 = 1;
        foreach ($dataStake as $key => $value) {
            $no4 = 'A'. (11 + $row4 + $jmlGoing + $jmlSasaran + $jmlProbis);
            $spreadsheet->getActiveSheet()->setCellValue($no4, $row4);
            $row4++;
        }

        $no5A = 'A'. (9 + $jmlGoing + $jmlSasaran + $jmlProbis + $jmlStake);
        $no5B = 'B'. (9 + $jmlGoing + $jmlSasaran + $jmlProbis + $jmlStake);

        $spreadsheet->getActiveSheet()->setCellValue($no5A, '5. ');
        $spreadsheet->getActiveSheet()->setCellValue($no5B, 'Selera Risiko');

        $data5A = 'B'. (11 + $jmlGoing + $jmlSasaran + $jmlProbis + $jmlStake);
        $data5B = 'B'. (12 + $jmlGoing + $jmlSasaran + $jmlProbis + $jmlStake);

        $spreadsheet->getActiveSheet()->fromArray($kolom4, NULL, $data5A);
        $spreadsheet->getActiveSheet()->fromArray($barisSelera, NULL, $data5B);
        $spreadsheet->getActiveSheet()->getStyle($data5B)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

        $writer = new Xlsx($spreadsheet);
        $filename = 'penetapan_konteks.xlsx';
        $writer->save($filename);
        header('Content-Type: application/x-www-form-urlencoded');
        header('Content-Transfer-Encoding: Binary');
        header("Content-disposition: attachment; filename=\"".$filename."\"");
        readfile($filename);
        exit;

    }
 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $this->data['tahun'] = Auth::user()->tahun;
        $this->data['id_data_umum'] = DB::table('ref_data_umum')
                            ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                            ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                            ->where('ref_data_umum.tahun', Auth::user()->tahun)
                            ->pluck('s_nmjabdetail_pemilik as s_nmjabdetail_pemilik','id_data_umum');
        $this->data['id_jns_konteks'] = RefJnsKonteks::pluck('nama_jns_konteks as nama_jns_konteks','id_jns_konteks');
        $this->data['id_konteks'] = RefKonteks::select('nama_konteks as nama_konteks','id_konteks as id_konteks', 'ket_konteks as ket_konteks');

        return view('lini1tetapkonteks.createtkunit', $this->data);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      if (Auth::check())
        {
            $rules = array(
                'tahun' => 'required',
                'id_data_umum' => 'required',
                'id_konteks' => 'required',

            );
            $messages = [
                            'tahun.required' => 'Silahkan isi tahun.',
                            'id_data_umum.required' => 'Silahkan pilih data umum.',
                            'id_konteks.required' => 'Silahkan pilih jenis dan nama konteks.',
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $now = Carbon::now('Asia/Jakarta');

                $id = new PenetapanKonteks;
                $id->tahun = Input::get('tahun');
                $id->id_data_umum = Input::get('id_data_umum');
                $id->id_konteks = Input::get('id_konteks');
                $id->created_at = $now;
                $id->user_create = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini1tetapkonteks.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['tahun'] = Auth::user()->tahun;
        $this->data['id_data_umum'] = DB::table('ref_data_umum')
                            ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                            ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                            ->where('ref_data_umum.tahun', Auth::user()->tahun)
                            ->pluck('s_nmjabdetail_pemilik as s_nmjabdetail_pemilik','id_data_umum');
        $this->data['id_jns_konteks'] = RefJnsKonteks::pluck('nama_jns_konteks as nama_jns_konteks','id_jns_konteks');
        $this->data['id_konteks'] = RefKonteks::select('nama_konteks as nama_konteks','id_konteks as id_konteks', 'ket_konteks as ket_konteks');
        $this->data['penetapan_konteks'] = PenetapanKonteks::find($id);
        $this->data['jenis_konteks'] = DB::table('t_penetapan_konteks')
            ->select('ref_konteks.id_konteks as id_konteks','ref_jns_konteks.nama_jns_konteks as nama_jns_konteks','ref_konteks.id_jns_konteks as id_jenis_konteks')
            ->join('ref_konteks', 't_penetapan_konteks.id_konteks', '=', 'ref_konteks.id_konteks')
            ->join('ref_jns_konteks', 'ref_konteks.id_jns_konteks', '=', 'ref_jns_konteks.id_jns_konteks')
            ->where('t_penetapan_konteks.id_penetapan_konteks', $id)
            ->first();

        return view('lini1tetapkonteks.edittkunit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      if (Auth::check())
        {
            $rules = array(
                'tahun' => 'required',
                'id_data_umum' => 'required',
                'id_konteks' => 'required',

            );
            $messages = [
                            'tahun.required' => 'Silahkan isi tahun.',
                            'id_data_umum.required' => 'Silahkan pilih data umum.',
                            'id_konteks.required' => 'Silahkan pilih jenis dan nama konteks.',
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $now = Carbon::now('Asia/Jakarta');
                $id = PenetapanKonteks::find($id);
                $id->tahun = Input::get('tahun');
                $id->id_data_umum = Input::get('id_data_umum');
                $id->id_konteks = Input::get('id_konteks');
                $id->updated_at = $now;
                $id->user_update = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah diubah.', 'Selamat');
                return redirect()->route('lini1tetapkonteks.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
