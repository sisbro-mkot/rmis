<?php

namespace App\Http\Controllers\lini1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use PDF;
use App\Models\dashboard\Efektivitas;

class Lini1EfektivitasCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $now = Carbon::now('Asia/Jakarta');
        $tahun = Auth::user()->tahun;
        $batas = Carbon::createFromFormat('Y-m-d', $tahun.'-12-01');
        $selisih = $now->diffInDays($batas, false);

        $identifikasi = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko.id_identifikasi as id',
                    't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 
                    'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko',
                    't_identifikasi_risiko.nama_dampak as nama_dampak',
                    't_pengendalian.nama_pengendalian as nama_pengendalian',
                    'ref_sub_unsur_ec.nama_sub_unsur as nama_sub_unsur_ec',
                    't_rtp.kegiatan_pengendalian as kegiatan_pengendalian',
                    'ref_sub_unsur_rtp.nama_sub_unsur as nama_sub_unsur_rtp',
                    'efektivitas_ec.efektif as efektif_ec',
                    'efektivitas_rtp.efektif as efektif_rtp',
                    'efektivitas_ec.ket_efektivitas as ket_efektivitas_ec',
                    'efektivitas_rtp.ket_efektivitas as ket_efektivitas_rtp')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->join('t_pemantauan_level', 't_identifikasi_risiko.id_identifikasi', '=', 't_pemantauan_level.id_identifikasi')
                ->leftjoin('t_pengendalian', 't_identifikasi_risiko.id_identifikasi', '=', 't_pengendalian.id_identifikasi')
                ->leftjoin('t_penyebab_rca', 't_identifikasi_risiko.id_identifikasi', '=', 't_penyebab_rca.id_identifikasi')
                ->leftjoin('t_rtp', 't_penyebab_rca.id_penyebab', '=', 't_rtp.id_penyebab')
                ->leftjoin('t_pemantauan', 't_pemantauan.id_rtp', '=', 't_rtp.id_rtp')
                ->leftjoin('ref_sub_unsur_spip as ref_sub_unsur_ec', 't_pengendalian.id_sub_unsur', '=', 'ref_sub_unsur_ec.id_sub_unsur')
                ->leftjoin('ref_sub_unsur_spip as ref_sub_unsur_rtp', 't_rtp.id_sub_unsur', '=', 'ref_sub_unsur_rtp.id_sub_unsur')
                ->leftjoin('t_efektivitas as efektivitas_ec', 't_pengendalian.id_pengendalian', '=', 'efektivitas_ec.id_pengendalian')
                ->leftjoin('t_efektivitas as efektivitas_rtp', 't_pemantauan.id_pemantauan', '=', 'efektivitas_rtp.id_pemantauan')
                ->whereNotNull('t_pemantauan.id_rtp')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->where('ref_data_umum.tahun', Auth::user()->tahun) 
                ->get();

        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        return view('lini1efektivitas.index', compact('now','tahun','batas','selisih','identifikasi','unit'));

    }

    public function cetak()
    {
        $analisis = DB::table('t_analisis_risiko')
                ->select('t_analisis_risiko.id_analisis as id', 
                    't_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 
                    'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko',
                    'ref_matriks_aktual.skor_dampak as skor_dampak_aktual',
                    'ref_matriks_aktual.skor_kemungkinan as skor_kemungkinan_aktual', 
                    'ref_matriks_aktual.skor_risiko as skor_risiko_aktual',
                    'ref_matriks_treated.skor_dampak as skor_dampak_treated',
                    'ref_matriks_treated.skor_kemungkinan as skor_kemungkinan_treated', 
                    'ref_matriks_treated.skor_risiko as skor_risiko_treated',  
                    'Keterjadian.count_keterjadian as count_keterjadian', 
                    't_analisis_risiko.existing_control_memadai as existing_control_memadai',
                    'ref_matriks_residual.skor_dampak as skor_dampak_residual',
                    'ref_matriks_residual.skor_kemungkinan as skor_kemungkinan_residual', 
                    'ref_matriks_residual.skor_risiko as skor_risiko_residual', 
                    'ref_data_umum.skor_selera as skor_selera', 
                    't_identifikasi_risiko.nama_dampak as nama_dampak',
                    't_pemantauan_level.nama_rekomendasi as nama_rekomendasi')
                ->join('t_identifikasi_risiko', 't_analisis_risiko.id_identifikasi', '=', 't_identifikasi_risiko.id_identifikasi')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->leftjoin('t_pemantauan_level', 't_identifikasi_risiko.id_identifikasi', '=', 't_pemantauan_level.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_aktual', 't_pemantauan_level.id_matriks_aktual', '=', 'ref_matriks_aktual.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')
                ->leftjoin(DB::raw('(SELECT id_identifikasi, COUNT(id_keterjadian) AS count_keterjadian FROM t_keterjadian GROUP BY id_identifikasi) AS Keterjadian'), 
                            function($join)
                            {
                               $join->on('t_identifikasi_risiko.id_identifikasi', '=', 'Keterjadian.id_identifikasi');
                            })
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->orderBy('kode_identifikasi_risiko','asc')
                ->get();

        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        $tahun = Auth::user()->tahun;

        $pdf = PDF::loadView('lini1aktual.cetak',  compact('analisis', 'unit', 'tahun'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('risiko_aktual.pdf', array('Attachment' => false));
        exit(0);
    }

 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tambah($id)
    {

        $this->data['id_identifikasi'] = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 
                    'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 
                    't_identifikasi_risiko.id_identifikasi as id_identifikasi', 
                    't_identifikasi_risiko.nama_dampak as nama_dampak',
                    'ref_matriks_residual.skor_dampak as skor_dampak_residual',
                    'ref_matriks_residual.skor_kemungkinan as skor_kemungkinan_residual', 
                    'ref_matriks_residual.skor_risiko as skor_risiko_residual',
                    'ref_matriks_treated.skor_dampak as skor_dampak_treated',
                    'ref_matriks_treated.skor_kemungkinan as skor_kemungkinan_treated', 
                    'ref_matriks_treated.skor_risiko as skor_risiko_treated',
                    't_pemantauan_level.id_pemantauan_level as id_pemantauan_level')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')                
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->leftjoin('t_pemantauan_level', 't_identifikasi_risiko.id_identifikasi', '=', 't_pemantauan_level.id_identifikasi')
                ->where('t_identifikasi_risiko.id_identifikasi', $id)
                ->get();
        $this->data['id_identifikasi_read'] = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 
                    'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 
                    't_identifikasi_risiko.id_identifikasi as id_identifikasi', 
                    't_identifikasi_risiko.nama_dampak as nama_dampak',
                    'ref_matriks_residual.skor_dampak as skor_dampak_residual',
                    'ref_matriks_residual.skor_kemungkinan as skor_kemungkinan_residual', 
                    'ref_matriks_residual.skor_risiko as skor_risiko_residual',
                    'ref_matriks_treated.skor_dampak as skor_dampak_treated',
                    'ref_matriks_treated.skor_kemungkinan as skor_kemungkinan_treated', 
                    'ref_matriks_treated.skor_risiko as skor_risiko_treated')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')                
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->where('t_identifikasi_risiko.id_identifikasi', $id)
                ->first();
        $this->data['kemungkinan_aktual'] = ['1', '2', '3', '4', '5'];
        $this->data['dampak_aktual'] = ['1', '2', '3', '4', '5'];
        $this->data['id_matriks_aktual'] = RefMatriks::pluck('skor_risiko', 'id_matriks');
        $this->data['id_keterjadian'] = DB::table('t_identifikasi_risiko')
                ->selectRaw('t_identifikasi_risiko.id_identifikasi as id_identifikasi, COUNT(t_keterjadian.id_keterjadian) AS jml_keterjadian, MAX(t_keterjadian.skor_dampak) AS max_dampak')
                ->leftjoin('t_keterjadian', 't_identifikasi_risiko.id_identifikasi', '=', 't_keterjadian.id_identifikasi')
                ->where('t_identifikasi_risiko.id_identifikasi', $id)
                ->first();
        $this->data['nama_instansiunitorg'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        return view('lini1aktual.createaktualunit', $this->data);
                
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (Auth::check())
        {
            $rules = array(

                'id_identifikasi' => 'required',
                'id_matriks_aktual' => 'required',

            );
            $messages = [
                            'id_identifikasi.required' => 'Silahkan pilih nama risiko.',
                            'id_matriks_aktual.required' => 'Silahkan pilih skala kemungkinan dan dampak risiko aktual.',

            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $now = Carbon::now('Asia/Jakarta');

                $id = new PemantauanLevel;
                $id->id_identifikasi = Input::get('id_identifikasi');
                $id->jumlah_kejadian_risiko = Input::get('jml_keterjadian');
                $id->id_matriks_aktual = Input::get('id_matriks_aktual');
                $id->nama_rekomendasi = Input::get('nama_rekomendasi');
                $id->created_at = $now;
                $id->user_create = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini1aktual.index');

            }
        } else {
            return redirect()->back();
        }

    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
public function ubah($id)
    {

        $this->data['id_identifikasi'] = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 
                    'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 
                    't_identifikasi_risiko.id_identifikasi as id_identifikasi', 
                    't_identifikasi_risiko.nama_dampak as nama_dampak',
                    'ref_matriks_residual.skor_dampak as skor_dampak_residual',
                    'ref_matriks_residual.skor_kemungkinan as skor_kemungkinan_residual', 
                    'ref_matriks_residual.skor_risiko as skor_risiko_residual',
                    'ref_matriks_treated.skor_dampak as skor_dampak_treated',
                    'ref_matriks_treated.skor_kemungkinan as skor_kemungkinan_treated', 
                    'ref_matriks_treated.skor_risiko as skor_risiko_treated',
                    't_pemantauan_level.id_pemantauan_level as id_pemantauan_level')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')                
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->leftjoin('t_pemantauan_level', 't_identifikasi_risiko.id_identifikasi', '=', 't_pemantauan_level.id_identifikasi')
                ->where('t_pemantauan_level.id_pemantauan_level', $id)
                ->get();
        $this->data['id_identifikasi_read'] = DB::table('t_identifikasi_risiko')
                ->select('t_identifikasi_risiko_kode.kode_identifikasi_risiko as kode_identifikasi_risiko', 
                    'ref_bagan_risiko.nama_bagan_risiko as nama_bagan_risiko', 
                    't_identifikasi_risiko.id_identifikasi as id_identifikasi', 
                    't_identifikasi_risiko.nama_dampak as nama_dampak',
                    'ref_matriks_residual.skor_dampak as skor_dampak_residual',
                    'ref_matriks_residual.skor_kemungkinan as skor_kemungkinan_residual', 
                    'ref_matriks_residual.skor_risiko as skor_risiko_residual',
                    'ref_matriks_treated.skor_dampak as skor_dampak_treated',
                    'ref_matriks_treated.skor_kemungkinan as skor_kemungkinan_treated', 
                    'ref_matriks_treated.skor_risiko as skor_risiko_treated',
                    'ref_matriks_aktual.skor_dampak as skor_dampak_aktual',
                    'ref_matriks_aktual.skor_kemungkinan as skor_kemungkinan_aktual', 
                    'ref_matriks_aktual.skor_risiko as skor_risiko_aktual')
                ->join('t_identifikasi_risiko_kode', 't_identifikasi_risiko.id_identifikasi', '=', 't_identifikasi_risiko_kode.id_identifikasi')
                ->join('ref_bagan_risiko', 't_identifikasi_risiko.id_bagan_risiko', '=', 'ref_bagan_risiko.id_bagan_risiko')
                ->join('t_penetapan_konteks', 't_identifikasi_risiko.id_penetapan_konteks', '=', 't_penetapan_konteks.id_penetapan_konteks')
                ->join('ref_data_umum', 't_penetapan_konteks.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->join('t_analisis_risiko', 't_identifikasi_risiko.id_identifikasi', '=', 't_analisis_risiko.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_residual', 't_analisis_risiko.id_matriks_residual', '=', 'ref_matriks_residual.id_matriks')                
                ->leftjoin('ref_matriks as ref_matriks_treated', 't_analisis_risiko.id_matriks_treated', '=', 'ref_matriks_treated.id_matriks')
                ->leftjoin('t_pemantauan_level', 't_identifikasi_risiko.id_identifikasi', '=', 't_pemantauan_level.id_identifikasi')
                ->leftjoin('ref_matriks as ref_matriks_aktual', 't_pemantauan_level.id_matriks_aktual', '=', 'ref_matriks_aktual.id_matriks')
                ->where('t_pemantauan_level.id_pemantauan_level', $id)
                ->first();
        $this->data['kemungkinan_aktual'] = ['1', '2', '3', '4', '5'];
        $this->data['dampak_aktual'] = ['1', '2', '3', '4', '5'];
        $this->data['id_matriks_aktual'] = RefMatriks::pluck('skor_risiko', 'id_matriks');
        $this->data['id_keterjadian'] = DB::table('t_identifikasi_risiko')
                ->selectRaw('t_identifikasi_risiko.id_identifikasi as id_identifikasi, COUNT(t_keterjadian.id_keterjadian) AS jml_keterjadian, MAX(t_keterjadian.skor_dampak) AS max_dampak')
                ->leftjoin('t_keterjadian', 't_identifikasi_risiko.id_identifikasi', '=', 't_keterjadian.id_identifikasi')
                ->leftjoin('t_pemantauan_level', 't_identifikasi_risiko.id_identifikasi', '=', 't_pemantauan_level.id_identifikasi')
                ->where('t_pemantauan_level.id_pemantauan_level', $id)
                ->first();
        $this->data['nama_instansiunitorg'] = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();
        $this->data['aktual'] = PemantauanLevel::find($id);

        return view('lini1aktual.editaktualunit', $this->data);
                
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check())
        {
            $rules = array(

                'id_identifikasi' => 'required',
                'id_matriks_aktual' => 'required',

            );
            $messages = [
                            'id_identifikasi.required' => 'Silahkan pilih nama risiko.',
                            'id_matriks_aktual.required' => 'Silahkan pilih skala kemungkinan dan dampak risiko aktual.',

            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $now = Carbon::now('Asia/Jakarta');

                $id = PemantauanLevel::find($id);
                $id->id_identifikasi = Input::get('id_identifikasi');
                $id->jumlah_kejadian_risiko = Input::get('jml_keterjadian');
                $id->id_matriks_aktual = Input::get('id_matriks_aktual');
                $id->nama_rekomendasi = Input::get('nama_rekomendasi');
                $id->updated_at = $now;
                $id->user_delete = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah diubah.', 'Selamat');
                return redirect()->route('lini1aktual.index');

            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
