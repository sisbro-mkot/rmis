<?php

namespace App\Http\Controllers\lini1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use Session;
use \Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Alert;
use App\Models\dashboard\Stakeholder;

class Lini1StakeholderCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
 
        $konteks = DB::table('t_stakeholder')
                ->select('t_stakeholder.id_stakeholder as id', 't_stakeholder.nama_stakeholder as nama_stakeholder', 't_stakeholder.uraian as uraian')
                ->join('ref_data_umum', 't_stakeholder.id_data_umum', '=', 'ref_data_umum.id_data_umum')
                ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->where('ref_data_umum.tahun', Auth::user()->tahun)
                ->get();
        $unit = DB::table('wm_instansiunitorg')
                ->select('wm_instansiunitorg.s_nama_instansiunitorg as s_nama_instansiunitorg')
                ->join('vw_renpeglast', 'wm_instansiunitorg.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                ->first();

        return view('lini1stakeholder.index', compact('konteks','unit'));

    }
 
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        
        $this->data['id_data_umum'] = DB::table('ref_data_umum')
                            ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                            ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                            ->where('ref_data_umum.tahun', Auth::user()->tahun)
                            ->pluck('s_nmjabdetail_pemilik as s_nmjabdetail_pemilik','id_data_umum');

        return view('lini1stakeholder.createstakeunit', $this->data);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      if (Auth::check())
        {
            $rules = array(
                'id_data_umum' => 'required'

            );
            $messages = [
                            'id_data_umum.required' => 'Silahkan pilih data umum.',
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $now = Carbon::now('Asia/Jakarta');

                $id = new Stakeholder;
                $id->id_data_umum = Input::get('id_data_umum');
                $id->nama_stakeholder = Input::get('nama_stakeholder');
                $id->uraian = Input::get('uraian');
                $id->created_at = $now;
                $id->user_create = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah ditambahkan.', 'Selamat');
                return redirect()->route('lini1stakeholder.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['id_data_umum'] = DB::table('ref_data_umum')
                            ->join('vw_renpeglast', 'ref_data_umum.s_kd_instansiunitorg', '=', 'vw_renpeglast.s_kd_instansiunitorg')
                            ->where('vw_renpeglast.niplama', Auth::user()->user_nip)
                            ->where('ref_data_umum.tahun', Auth::user()->tahun)
                            ->pluck('s_nmjabdetail_pemilik as s_nmjabdetail_pemilik','id_data_umum');
        $this->data['penetapan_konteks'] = Stakeholder::find($id);

        return view('lini1stakeholder.editstakeunit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      if (Auth::check())
        {
            $rules = array(
                'id_data_umum' => 'required'

            );
            $messages = [
                            'id_data_umum.required' => 'Silahkan pilih data umum.'
            ];
            $validator = Validator::make(Input::all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $now = Carbon::now('Asia/Jakarta');
                $id = Stakeholder::find($id);
                $id->id_data_umum = Input::get('id_data_umum');
                $id->nama_stakeholder = Input::get('nama_stakeholder');
                $id->uraian = Input::get('uraian');
                $id->updated_at = $now;
                $id->user_update = Auth::user()->user_nip;

                $id->save();
                Alert::success('Data telah diubah.', 'Selamat');
                return redirect()->route('lini1stakeholder.index');
            }
        } else {
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = Stakeholder::findOrFail($id);

        $hapus->delete();

        return redirect()->route('lini1stakeholder.index');
    }
}
