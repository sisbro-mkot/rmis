<?php

namespace App\Http\Controllers\adminpemda;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\adminpemda\Program;
use App\Models\adminpemda\Sasaran;
use DB;
use Auth;
use Session;
use Validator;
use Illuminate\Support\Facades\Input;
use Alert;

class ProgramCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $program = DB::table('program_pemda')
            ->select('program_pemda.id as id', 'sasaran_pemda.nama_sasaran as nama_sasaran', 'program_pemda.nama_program as nama_program', 'program_pemda.indikator_program as indikator_program', 'program_pemda.target_kinerja as target_kinerja', 'program_pemda.satuan as satuan')
            ->join('sasaran_pemda', 'program_pemda.sasaran_id', '=', 'sasaran_pemda.id')
            ->get();
        return view('adminpemda.program.index', compact('program'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $sasaran = Sasaran::pluck('nama_sasaran', 'id');
        return view('adminpemda.program.create', compact('sasaran'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        if (Auth::check())
        {
            $rules = array(
                'sasaran_id' => 'required',
                'nama_program' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->route('program.create')
                ->withErrors($validator);
            } else {

                $program = new Program;
                $program->sasaran_id = Input::get('sasaran_id');
                $program->nama_program = Input::get('nama_program');
                $program->indikator_program = Input::get('indikator_program');
                $program->target_kinerja = Input::get('target_kinerja');
                $program->save();
                Alert::success('Data program telah ditambahkan.', 'Selamat');
                return redirect()->route('program.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $sasaran = Sasaran::pluck('nama_sasaran', 'id');
        $program = Program::find($id);
        return view('adminpemda.program.edit', compact('sasaran', 'program'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        if (Auth::check())
        {
            $rules = array(
                'sasaran_id' => 'required',
                'nama_program' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $program = Program::find($id);
                $program->sasaran_id = Input::get('sasaran_id');
                $program->nama_program = Input::get('nama_program');
                $program->indikator_program = Input::get('indikator_program');
                $program->target_kinerja = Input::get('target_kinerja');
                $program->save();
                Alert::success('Data telah di edit.', 'Selamat');
                return redirect()->route('program.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $program = Program::find($id);
        $program->delete();
        Alert::info('Data telah dihapus.');
        return redirect()->route('program.index');
    }
}
