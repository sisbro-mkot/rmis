<?php

namespace App\Http\Controllers\adminpemda;

use App\Models\utils\Unitkerja;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Models\adminpemda\Sasaran;
use App\Models\adminopd\SasaranOPD;
use App\Models\adminpemda\OPD;
use App\Models\adminpemda\Mapping;
use Auth;
use Illuminate\Support\Collection;
use Session;
use Validator;
use Illuminate\Support\Facades\Input;
use Alert;

class MappingCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = DB::table('tbl_maping')
        //     ->select('tbl_maping.id as id', 'sasaran_pemda.nama_sasaran as nama_sasaran_pemda', 'tbl_opd.nama_opd as nama_opd', 'sasaran_opd.nama_sasaran as nama_sasaran_opd', 'tbl_maping.bobot as bobot')
        //     ->join('sasaran_pemda', 'sasaran_pemda.id', 'tbl_maping.sasaranpemda_id')
        //     ->join('tbl_opd', 'tbl_opd.id', 'tbl_maping.opd_id')
        //     ->join('sasaran_opd', 'sasaran_opd.id', 'tbl_maping.sasaranopd_id')
        //     ->get();
        $data = DB::table('tbl_maping')
            ->select('tbl_maping.id as id', 'sasaran_pemda.nama_sasaran as nama_sasaran_pemda', 'ref_unitkerja.unitkerja as nama_opd', 'sasaran_opd.nama_sasaran as nama_sasaran_opd', 'tbl_maping.bobot as bobot')
            ->join('sasaran_pemda', 'sasaran_pemda.id', 'tbl_maping.sasaranpemda_id')
            ->join('ref_unitkerja', 'ref_unitkerja.key_sort_unit', 'tbl_maping.key_sort_unit')
            ->join('sasaran_opd', 'sasaran_opd.id', 'tbl_maping.sasaranopd_id')
            ->get();
//        $sasaran = Sasaran::all();
        return view('adminpemda.mapping.index', compact('data'));
    }

    public function index2()
    {
        return view('adminpemda.mapping.index2');
    }

    public function loadMappingData()
    {
        $data = [];
        $sasaran = Sasaran::all();

        foreach ($sasaran as $key => $ss){
            $target = new \stdClass();
            $target->id = $ss->id;
            $target->tujuan_id = $ss->tujuan_id;
            $target->nama_sasaran = $ss->nama_sasaran;

            $map = [];
            $mapping = Mapping::where('sasaranpemda_id', $ss->id)->get();
            foreach ($mapping as $k => $mp){
                $ma = new \stdClass();
                $ma->id             = $mp->id;
                $ma->sasaranpemda_id= $mp->sasaranpemda_id;
                $ma->key_sort_unit  = $mp->key_sort_unit;
                $ma->unit           = $mp->unit->unitkerja;
                $ma->sasaranopd_id  = $mp->sasaranopd_id;
                $ma->sasaran_unit    = $mp->sasaran_unit ? $mp->sasaran_unit->nama_sasaran : "";
                $ma->bobot          = $mp->bobot;
                $map[]              = $ma;
            }
            $mappings = new Collection($map);
            $target->mapping = $mappings;
            $data[] = $target;
        }

        $result = new Collection($data);
//        return view('adminpemda.mapping.index2', compact('result'));
        return response()->json($result);
    }

    public function getMappingFormData(Request $request)
    {
        $data               = new \stdClass();
        $data->mapping      = Mapping::with('unit', 'sasaran_unit')->where('id', $request->get('id'))->first();
        $data->units        = Unitkerja::all(['key_sort_unit', 'unitkerja']);

        return response()->json($data);
    }

    public function getUnitTarget(Request $request)
    {
        $data = SasaranOPD::where('key_sort_unit', $request->get('key_sort_unit'))
            ->get(['id', 'nama_sasaran']);
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->data['sasaranpemda'] = Sasaran::pluck('nama_sasaran', 'id');
        // $this->data['opd'] = OPD::pluck('nama_opd', 'id');
        $this->data['opd'] = DB::table('ref_unitkerja')
                            ->where('status','1')
                            ->orderBy('key_sort_unit','ASC')
                            ->pluck('unitkerja as nama_opd', 'key_sort_unit');
        return view('adminpemda.mapping.create', $this->data);
    }

    public function pilihSasaranOpd($id)
    {
        // $data = DB::table('sasaran_opd')
        //     ->where('opd_id', $id)
        //     ->get()
        //     ->pluck('nama_sasaran', 'id');
        $data = DB::table('sasaran_opd')
            ->where('key_sort_unit', $id)
            ->get()
            ->pluck('nama_sasaran', 'id');
        return json_encode($data);
    }


    public function cariSasaran(Request $request)
    {
        $data = [];

        if($request->has('q')){
            $search = $request->q;
            $data = DB::table("sasaran_pemda")
                    ->select("id","nama_sasaran")
                    ->where('nama_sasaran','LIKE',"%$search%")
                    ->get();
        }

        return response()->json($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
//    public function store()
//    {
//        if (Auth::check())
//        {
//            $rules = array(
//                'sasaranpemda_id' => 'required',
//                'opd_id' => 'required',
//                'sasaranopd_id' => 'required',
//            );
//            $validator = Validator::make(Input::all(), $rules);
//            if ($validator->fails()) {
//                return redirect()->route('mapping.create')
//                ->withErrors($validator);
//            } else {
//
//                $mapping = new Mapping;
//                $mapping->sasaranpemda_id   = Input::get('sasaranpemda_id');
//                $mapping->key_sort_unit     = Input::get('opd_id');
//                $mapping->opd_id            = '0';
//                $mapping->sasaranopd_id     = Input::get('sasaranopd_id');
//                $mapping->bobot             = Input::get('bobot');
//                $mapping->save();
//                Alert::success('Data mapping telah ditambahkan.', 'Selamat');
//                return redirect()->route('mapping.index');
//            }
//        } else {
//            return redirect()->back();
//        }
//    }

    public function store(Request $request)
    {
        $ids                = $request->get('id');
        $key_sort_units     = $request->get('key_sort_unit');
        $sasaranopd_ids     = $request->get('sasaranopd_id');
        $bobots             = $request->get('bobot');

        foreach ($request->get('sasaranpemda_id') as $i => $sasaranpemda){
            if($ids[$i] != ''){
                $mapping = Mapping::find($ids[$i]);
            }else{
                $mapping = new Mapping();
            }
            $mapping->sasaranpemda_id   = $sasaranpemda;
            $mapping->key_sort_unit     = $key_sort_units[$i];
            $mapping->sasaranopd_id     = $sasaranopd_ids[$i];
            $mapping->bobot             = $bobots[$i];
            $mapping->save();
        }

        return redirect()->route('mapping.index')->with('status', 'Data berhasil diubah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->data['mapping'] = Mapping::find($id);
        $this->data['sasaranpemda'] = Sasaran::pluck('nama_sasaran', 'id');
        // $this->data['opd'] = OPD::pluck('nama_opd', 'id');
        $this->data['opd'] = DB::table('ref_unitkerja')->pluck('unitkerja as nama_opd', 'key_sort_unit');
        return view('adminpemda.mapping.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        if (Auth::check())
        {
            $rules = array(
                'sasaranpemda_id' => 'required',
                'opd_id' => 'required',
                'sasaranopd_id' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $mapping = Mapping::find($id);
                $mapping->sasaranpemda_id = Input::get('sasaranpemda_id');
                $mapping->opd_id = Input::get('opd_id');
                $mapping->sasaranopd_id = Input::get('sasaranopd_id');
                $mapping->save();
                Alert::success('Data telah di edit.', 'Selamat');
                return redirect()->route('mapping.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mapping = Mapping::find($id);
        $mapping->delete();
//        Alert::info('Data telah dihapus.');
        return redirect()->route('mapping.index')->with('status', 'Data telah berhasil dihapus');
    }
}
