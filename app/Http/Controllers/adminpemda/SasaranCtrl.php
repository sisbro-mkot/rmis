<?php

namespace App\Http\Controllers\adminpemda;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\adminpemda\Sasaran;
use App\Models\adminpemda\Tujuan;
use DB;
use Auth;
use Session;
use Validator;
use Illuminate\Support\Facades\Input;
use Alert;

class SasaranCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sasaran = DB::table('sasaran_pemda')
            ->select('sasaran_pemda.id as id', 'tujuan_pemda.nama_tujuan as nama_tujuan', 'sasaran_pemda.nama_sasaran as nama_sasaran', 'sasaran_pemda.indikator_sasaran as indikator_sasaran', 'sasaran_pemda.target_kinerja as target_kinerja')
            ->join('tujuan_pemda', 'sasaran_pemda.tujuan_id', '=', 'tujuan_pemda.id')
            ->get();
        return view('adminpemda.sasaran.index', compact('sasaran'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tujuan = Tujuan::pluck('nama_tujuan', 'id');
        return view('adminpemda.sasaran.create', compact('tujuan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        if (Auth::check())
        {
            $rules = array(
                'tujuan_id' => 'required',
                'nama_sasaran' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->route('sasaran.create')
                ->withErrors($validator);
            } else {

                $sasaran = new Sasaran;
                $sasaran->tujuan_id = Input::get('tujuan_id');
                $sasaran->nama_sasaran = Input::get('nama_sasaran');
                $sasaran->indikator_sasaran = Input::get('indikator_sasaran');
                $sasaran->target_kinerja = Input::get('target_kinerja');
                $sasaran->save();
                Alert::success('Data sasaran telah ditambahkan.', 'Selamat');
                return redirect()->route('sasaran.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tujuan = Tujuan::pluck('nama_tujuan', 'id');
        $sasaran = Sasaran::find($id);
        return view('adminpemda.sasaran.edit', compact('tujuan', 'sasaran'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        if (Auth::check())
        {
            $rules = array(
                'tujuan_id' => 'required',
                'nama_sasaran' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $sasaran = Sasaran::find($id);
                $sasaran->tujuan_id = Input::get('tujuan_id');
                $sasaran->nama_sasaran = Input::get('nama_sasaran');
                $sasaran->indikator_sasaran = Input::get('indikator_sasaran');
                $sasaran->target_kinerja = Input::get('target_kinerja');
                $sasaran->save();
                Alert::success('Data telah di edit.', 'Selamat');
                return redirect()->route('sasaran.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $sasaran = Sasaran::find($id);
        $sasaran->delete();
        Alert::info('Data telah dihapus.');
        return redirect()->route('sasaran.index');
    }
}
