<?php

namespace App\Http\Controllers\adminpemda;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\adminpemda\Kategori;
use DB;
use Auth;
use Session;
use Validator;
use Illuminate\Support\Facades\Input;
use Alert;

class KategoriCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $kategori = DB::table('tbl_kategori')
            ->select('tbl_kategori.id as id', 'tbl_kategori.nama_kategori as nama_kategori', 'tbl_kategori.selera_risiko as selera_risiko', 'tbl_kategori.dasar_penetapan as dasar_penetapan')
            ->get();
        return view('adminpemda.kategori.index', compact('kategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->data['kategori'] = DB::table('tbl_kategori');
        return view('adminpemda.kategori.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        if (Auth::check())
        {
            $rules = array(
                'nama_kategori' => 'required',
                'selera_risiko' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->route('kategori.create')
                ->withErrors($validator);
            } else {

                $kategori = new Kategori;
                $kategori->nama_kategori = Input::get('nama_kategori');
                $kategori->selera_risiko = Input::get('selera_risiko');
                $kategori->dasar_penetapan = Input::get('dasar_penetapan');
                $kategori->save();
                Alert::success('Data kategori telah ditambahkan.', 'Selamat');
                return redirect()->route('kategori.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->data['kategori'] = Kategori::find($id);
        return view('adminpemda.kategori.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        if (Auth::check())
        {
            $rules = array(
                'nama_kategori' => 'required',
                'selera_risiko' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $risk = Kategori::find($id);
                $risk->nama_kategori = Input::get('nama_kategori');
                $risk->selera_risiko = Input::get('selera_risiko');
                $risk->dasar_penetapan = Input::get('dasar_penetapan');
                $risk->save();
                Alert::success('Data telah di edit.', 'Selamat');
                return redirect()->route('kategori.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $kategori = Kategori::find($id);
        $kategori->delete();
        Alert::info('Data telah di hapus.');
        return redirect()->route('kategori.index');
    }
}