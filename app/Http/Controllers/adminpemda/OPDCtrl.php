<?php

namespace App\Http\Controllers\adminpemda;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\adminpemda\OPD;
use App\Models\adminpemda\Urusan;
use App\Models\adminpemda\Bidang;
use DB;
use Auth;
use Session;
use Validator;
use Illuminate\Support\Facades\Input;
use Alert;
use App\Struktur;
use Illuminate\Database\Eloquent\Collection;
use App\PegawaiSQL;

class OPDCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $opd = DB::table('tbl_opd')
        //         ->select('tbl_opd.id as id', 'ref_urusan.nama_urusan as nama_urusan', 'ref_bidang.nama_bidang as nama_bidang', 'tbl_opd.nama_opd as nama_opd', 'tbl_opd.kepala_opd as kepala_opd', 'tbl_opd.jabatan as jabatan')
        //         ->join('ref_urusan', 'tbl_opd.urusan_id', '=', 'ref_urusan.id')
        //         ->join('ref_bidang', 'tbl_opd.bidang_id', '=', 'ref_bidang.id')
        //         ->get();
        $opd0 = DB::table('ref_unitkerja')->where('status','1')->orderBy('key_sort_unit','ASC')->get();
        $result = [];
        foreach($opd0 as $key => $v){
            $jabatan = Struktur::with('pegawai')->where('key_unit_aktif', $v->key_sort_unit)->first();
            $pegawai = $jabatan ? PegawaiSQL::where('s_kd_jabdetail', $jabatan->s_kd_jab_detail)->where('status', 'Aktif')->value('nipbaru') : "-";
            // dd($jabatan);
            $obj = new \stdClass();
            $obj->id            = $v->id;
            $obj->key_sort_unit = $v->key_sort_unit;
            $obj->flag          = $v->flag;
            $obj->unitkerja     = $v->unitkerja;
            $obj->kota          = $v->kota;
            $obj->ttd           = $v->ttd;
            $obj->nip           = $pegawai;
            $result[] = $obj;
        }
        $opd = new Collection($result);

        return view('adminpemda.opd.index', compact('opd'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $urusan = Urusan::pluck('nama_urusan', 'id');
        return view('adminpemda.opd.create', compact('urusan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        if (Auth::check())
        {
            $rules = array(
                'urusan_id' => 'required',
                'bidang_id' => 'required',
                'nama_opd' => 'required',
                'kepala_opd' => 'required',
                'jabatan' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->route('opd.create')
                ->withErrors($validator);
            } else {

                $opd = new OPD;
                $opd->urusan_id = Input::get('urusan_id');
                $opd->bidang_id = Input::get('bidang_id');
                $opd->nama_opd = Input::get('nama_opd');
                $opd->kepala_opd = Input::get('kepala_opd');
                $opd->jabatan = Input::get('jabatan');
                $opd->save();
                Alert::success('Data opd telah ditambahkan.', 'Selamat');
                return redirect()->route('opd.index');
            }
        } else {
            return redirect()->back();
        }
    }

    public function pilihBidang($id) 
    {  
        $bidang = DB::table('ref_bidang')
                ->where('urusan_id', $id)
                ->get()
                ->pluck('nama_bidang', 'id');
        return json_encode($bidang);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->data['urusan'] = Urusan::pluck('nama_urusan', 'id');
        $this->data['opd'] = OPD::find($id);
        return view('adminpemda.opd.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        if (Auth::check())
        {
            $rules = array(
                'urusan_id' => 'required',
                'bidang_id' => 'required',
                'nama_opd' => 'required',
                'kepala_opd' => 'required',
                'jabatan' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $opd = OPD::find($id);
                $opd->urusan_id = Input::get('urusan_id');
                $opd->bidang_id = Input::get('bidang_id');
                $opd->nama_opd = Input::get('nama_opd');
                $opd->kepala_opd = Input::get('kepala_opd');
                $opd->jabatan = Input::get('jabatan');
                $opd->save();
                Alert::success('Data telah di edit.', 'Selamat');
                return redirect()->route('opd.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $opd = OPD::find($id);
        $opd->delete();
        Alert::info('Data telah dihapus.');
        return redirect()->route('opd.index');
    }
}
