<?php

namespace App\Http\Controllers\adminpemda;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\adminpemda\Misi;
use App\Models\adminpemda\Visi;
use DB;
use Auth;
use Session;
use Validator;
use Illuminate\Support\Facades\Input;
use Alert;

class MisiCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $misi = DB::table('misi_pemda')
            ->select('misi_pemda.id as id', 'visi_pemda.nama_visi as nama_visi', 'misi_pemda.nama_misi as nama_misi')
            ->join('visi_pemda', 'misi_pemda.visi_id', '=', 'visi_pemda.id')
            ->get();
        return view('adminpemda.misi.index', compact('misi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $visi = Visi::pluck('nama_visi', 'id');
        return view('adminpemda.misi.create', compact('visi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        if (Auth::check())
        {
            $rules = array(
                'visi_id' => 'required',
                'nama_misi' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->route('misi.create')
                ->withErrors($validator);
            } else {

                $misi = new Misi;
                $misi->visi_id = Input::get('visi_id');
                $misi->nama_misi = Input::get('nama_misi');
                $misi->save();
                Alert::success('Data misi telah ditambahkan.', 'Selamat');
                return redirect()->route('misi.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $visi = Visi::pluck('nama_visi', 'id');
        $misi = Misi::find($id);
        return view('adminpemda.misi.edit', compact('visi', 'misi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        if (Auth::check())
        {
            $rules = array(
                'visi_id' => 'required',
                'nama_misi' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator);
            } else {

                $misi = Misi::find($id);
                $misi->visi_id = Input::get('visi_id');
                $misi->nama_misi = Input::get('nama_misi');
                $misi->save();
                Alert::success('Data telah di edit.', 'Selamat');
                return redirect()->route('misi.index');
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $misi = Misi::find($id);
        $misi->delete();
        Alert::info('Data telah dihapus.');
        return redirect()->route('misi.index');
    }
}
