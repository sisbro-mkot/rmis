<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class RefSubUnsurSPIP extends Model
{
    //
    protected $table = 'ref_sub_unsur_spip';
    public $timestamps = false;
    protected $fillable = ['id_sub_unsur','nama_sub_unsur','ket_sub_unsur','catatan_hapus','user_create','user_delete'];
}
