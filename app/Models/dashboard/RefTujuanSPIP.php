<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class RefTujuanSPIP extends Model
{
    //
    protected $table = 'ref_tujuan_spip';
    public $timestamps = false;
    protected $fillable = ['id_tujuan_spip','nama_tujuan_spip','ket_tujuan_spip','user_create','user_update','user_delete'];
}
