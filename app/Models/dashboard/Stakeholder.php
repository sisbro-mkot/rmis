<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class Stakeholder extends Model
{
    //
    protected $table = 't_stakeholder';
    public $timestamps = false;
    protected $primaryKey = 'id_stakeholder';
    protected $fillable = ['id_stakeholder','id_data_umum','nama_stakeholder','uraian','ket_stakeholder','catatan_hapus','user_create','user_update'];
}
