<?php

namespace App\Models\useropd1;

use Illuminate\Database\Eloquent\Model;

class RefUnitKerja extends Model
{
    //
    protected $table = 'ref_unitkerja';
    public $timestamps = false;
    protected $fillable = ['id','unitkerja','unitkerja_skt','alamat','kota','kota_kppn','no_telp','fax','email','ttd','ttd_skt','kd_wilayah_univ','kanreg_bkn','format_surat','key_sort_unit','id_peta','flag','status'];
}
