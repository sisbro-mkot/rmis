<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class RTP extends Model
{
    //
    protected $table = 't_rtp';
    public $timestamps = false;
    protected $primaryKey = 'id_rtp';
    protected $fillable = ['id_rtp','id_penyebab','respon_risiko','kegiatan_pengendalian','id_sub_unsur','s_kd_jabdetail_pic','id_output','id_periode','id_matriks_treated','ket_rtp','catatan_hapus','user_create','user_delete'];
}
