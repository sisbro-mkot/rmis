<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class RefPeriode extends Model
{
    //
    protected $table = 'ref_periode';
    public $timestamps = false;
    protected $fillable = ['id_periode','nama_periode','jenis_periode','catatan_hapus','user_create','user_delete'];
}
