<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class Renpeglast extends Model
{
    protected $connection = 'dbhrm';
    protected $table = 'ren_peg_last';
    public $timestamps = false;
    protected $fillable = ['id','niplama','nama','s_kd_jabdetail','s_kd_instansiunitorg','key_sort_unit'];
}
