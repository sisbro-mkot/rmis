<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class RefJnsSebab extends Model
{
    //
    protected $table = 'ref_jns_sebab';
    public $timestamps = false;
    protected $fillable = ['id_jns_sebab','kd_jns_sebab','nama_jns_sebab','ket_jns_sebab','catatan_hapus','user_create','user_delete'];
}
