<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class RefKategoriRisiko extends Model
{
    //
    protected $table = 'ref_kategori_risiko';
    public $timestamps = false;
    protected $fillable = ['id_kategori_risiko','nama_kategori_risiko','ket_kategori_risiko','catatan_hapus','user_create','user_update'];
}
