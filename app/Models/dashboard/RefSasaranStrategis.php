<?php

namespace App\Models\useropd1;

use Illuminate\Database\Eloquent\Model;

class RefSasaranStrategis extends Model
{
    //
    protected $table = 'ref_sasaran_strategis';
    public $timestamps = false;
    protected $fillable = ['id_sasaran_strategis','nama_sasaran_strategis','ket_sasaran_strategis','catatan_hapus','user_create','user_delete'];
}
