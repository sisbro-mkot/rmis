<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class Usulan extends Model
{
    //
    protected $table = 't_usulan';
    public $timestamps = false;
    protected $primaryKey = 'id_usulan';
    protected $fillable = ['id_usulan','s_kd_instansiunitorg','id_kategori_risiko','nama_bagan_risiko','id_tujuan_spip','ket_usulan','status_usulan','tanggapan','created_at','updated_at','user_create','user_update'];
}
