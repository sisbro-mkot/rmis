<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class RefBaganRisiko extends Model
{
    //
    protected $table = 'ref_bagan_risiko';
    public $timestamps = false;
    protected $primaryKey = 'id_bagan_risiko';
    protected $fillable = ['id_bagan_risiko','id_lingkup_risiko','id_kategori_risiko','s_kd_jabdetail','nama_bagan_risiko','usulan','tahun_usulan','id_periode_usulan','status_usulan','ket_usulan','ket_bagan_risiko','id_tujuan_spip','catatan_hapus','user_create','user_update'];
}
