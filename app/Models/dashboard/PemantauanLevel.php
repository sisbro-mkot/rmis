<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class PemantauanLevel extends Model
{
    //
    protected $table = 't_pemantauan_level';
    public $timestamps = false;
    protected $primaryKey = 'id_pemantauan_level';
    protected $fillable = ['id_pemantauan_level','id_identifikasi','jumlah_kejadian_risiko','id_matriks_aktual','nama_rekomendasi','ket_komentar','ket_pemantauan_level','catatan_hapus','user_create','user_delete'];
}
