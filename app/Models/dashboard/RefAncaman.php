<?php

namespace App\Models\useropd1;

use Illuminate\Database\Eloquent\Model;

class RefAncaman extends Model
{
    //
    protected $table = 'ref_ancaman';
    public $timestamps = false;
    protected $fillable = ['id_ancaman','nama_ancaman','ket_ancaman','catatan_hapus','user_create','user_delete'];
}
