<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class RefJnsKonteks extends Model
{
    //
    protected $table = 'ref_jns_konteks';
    public $timestamps = false;
    protected $fillable = ['id_jns_konteks','nama_jns_konteks','ket_jns_konteks','catatan_hapus','user_create','user_update'];
}
