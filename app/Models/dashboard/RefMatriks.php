<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class RefMatriks extends Model
{
    //
    protected $table = 'ref_matriks';
    public $timestamps = false;
    protected $fillable = ['id_matriks','skor_dampak','skor_kemungkinan','skor_risiko','id_ket_warna','ket_matriks','catatan_hapus','user_create','user_update'];
}
