<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class TblRole extends Model
{
    //
    protected $table = 'tbl_role';
    public $timestamps = false;
    protected $fillable = ['nama_role'];
}
