<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class RefSimpulanRisiko extends Model
{
    //
    protected $table = 'ref_simpulan_risiko';
    public $timestamps = false;
    protected $fillable = ['id_simpulan_risiko','nama_simpulan_risiko','ket_simpulan_risiko','catatan_hapus','user_create','user_delete'];
}
