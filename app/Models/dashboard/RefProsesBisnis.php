<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class RefProsesBisnis extends Model
{
    //
    protected $table = 'ref_proses_bisnis';
    public $timestamps = false;
    protected $fillable = ['id_proses_bisnis','nama_proses_bisnis','ket_proses_bisnis','catatan_hapus','user_create','user_update'];
}
