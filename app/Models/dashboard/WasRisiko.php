<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class WasRisiko extends Model
{
    //
    protected $table = 't_was_risiko';
    public $timestamps = false;
    protected $primaryKey = 'id_was_risiko';
    protected $fillable = ['id_was_risiko','id_analisis','tahun','kondisi_risiko','kriteria_risiko','sebab_risiko','akibat_risiko','rekomendasi_risiko','id_simpulan_risiko','id_status_temuan','ket_was_risiko','id_tanggapan','uraian_tanggapan','id_simpulan_risiko2','kondisi_risiko2','rekomendasi_risiko2','tindak_lanjut','catatan_hapus','user_create','user_update','user_delete'];
}
