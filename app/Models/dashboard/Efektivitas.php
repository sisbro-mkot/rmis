<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class Efektivitas extends Model
{
    //
    protected $table = 't_efektivitas';
    public $timestamps = false;
    protected $primaryKey = 'id_efektivitas';
    protected $fillable = ['id_efektivitas','id_pengendalian','id_pemantauan','efektif','ket_efektivitas','catatan_hapus','user_create','user_update','user_delete'];
}
