<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class JabDetail extends Model
{
    //
    protected $table = 'wm_jabdetail';
    public $timestamps = false;
}
