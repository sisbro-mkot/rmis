<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class ViewRenpeglast extends Model
{
    protected $table = 'vw_renpeglast';
    public $timestamps = false;
    protected $fillable = ['id','niplama','nama','s_kd_jabdetail','s_kd_instansiunitorg','key_sort_unit'];
}
