<?php

namespace App\Models\dashboard;

use Illuminate\Database\Eloquent\Model;

class RefLingkupRisiko extends Model
{
    //
    protected $table = 'ref_lingkup_risiko';
    public $timestamps = false;
    protected $fillable = ['id_lingkup_risiko','kd_lingkup_risiko','nama_lingkup_risiko','ket_lingkup_risiko','catatan_hapus','user_create','user_delete'];
}
