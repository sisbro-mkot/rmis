<?php

namespace App\Models\adminpemda;

use App\Models\adminopd\SasaranOPD;
use App\Models\utils\Unitkerja;
use Illuminate\Database\Eloquent\Model;

class Mapping extends Model
{
    //
    protected $table = 'tbl_maping';
    public $timestamps = false;
    protected $fillable = ['sasaranpemda_id','key_sort_unit', 'opd_id', 'sasaranopd_id','bobot'];

    public function unit(){
        return $this->hasOne(Unitkerja::class, 'key_sort_unit', 'key_sort_unit');
    }

    public function sasaran_unit(){
        return $this->hasOne(SasaranOPD::class, 'id', 'sasaranopd_id');
    }
}
