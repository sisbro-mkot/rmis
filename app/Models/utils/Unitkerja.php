<?php

namespace App\Models\utils;

use Illuminate\Database\Eloquent\Model;

class Unitkerja extends Model
{
    protected $table = 'ref_unitkerja';
    public $timestamps = false;
    protected $fillable = ['unitkerja', 'unitkerja_skt'];

}
