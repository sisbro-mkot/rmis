<?php

namespace App\Models\litbang;

use Illuminate\Database\Eloquent\Model;

class STPegawai extends Model
{
    protected $connection = 'dblitbang';
    protected $table = 't_st_pegawai';
    public $timestamps = false;
    protected $primaryKey = 'id_st_pegawai';
    protected $fillable = ['id_st_pegawai', 'id_st', 'id_pegawai', 'catatan_hapus', 'user_create', 'user_update', 'user_delete'];
}
