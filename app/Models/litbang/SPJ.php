<?php

namespace App\Models\litbang;

use Illuminate\Database\Eloquent\Model;

class SPJ extends Model
{
    protected $connection = 'dblitbang';
    protected $table = 't_spj';
    public $timestamps = false;
    protected $primaryKey = 'id_spj';
    protected $fillable = ['id_spj','id_st','id_anggaran_kinerja','tgl_spj','nilai_spj','status_spj','tgl_bendahara','tgl_perbaikan','ket_perbaikan','ket_spj','catatan_hapus','user_create','user_update','user_delete'];
}
