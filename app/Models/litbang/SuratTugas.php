<?php

namespace App\Models\litbang;

use Illuminate\Database\Eloquent\Model;

class SuratTugas extends Model
{
    protected $connection = 'dblitbang';
    protected $table = 't_st';
    public $timestamps = false;
    protected $primaryKey = 'id_st';
    protected $fillable = ['id_st','id_jns_st','id_bidang','no_st','tgl_st','uraian_st','tgl_mulai','tgl_selesai','jml_hari','id_detil_aktivitas','status_st','id_ikk','ket_st','catatan_hapus','user_create','user_update','user_delete'];
}
