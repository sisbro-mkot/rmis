<?php

namespace App\Models\litbang;

use Illuminate\Database\Eloquent\Model;

class Penelitian extends Model
{
    protected $connection = 'dblitbang';
    protected $table = 't_penelitian';
    public $timestamps = false;
    protected $primaryKey = 'id_penelitian';
    protected $fillable = ['id_penelitian','nama_penelitian','no_penelitian','tgl_penelitian','init_a1','init_b1','init_b2','init_b3','init_b4','inter_a1','inter_b1','end_a1','end_b1','ket_penelitian','user_create','user_update','user_delete'];
}
