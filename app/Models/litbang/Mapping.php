<?php

namespace App\Models\litbang;

use Illuminate\Database\Eloquent\Model;

class Mapping extends Model
{
    protected $connection = 'dblitbang';
    protected $table = 't_mapping';
    public $timestamps = false;
    protected $primaryKey = 'id_mapping';
    protected $fillable = ['id_mapping', 'id_dja', 'id_pkau'];
}
