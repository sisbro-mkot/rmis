<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeySortUnitColumnToIdentifikasiKegiatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('identifikasi_kegiatan', function (Blueprint $table) {
            $table->string('key_sort_unit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('identifikasi_kegiatan', function (Blueprint $table) {
            $table->dropColumn(['key_sort_unit']);
        });
    }
}
