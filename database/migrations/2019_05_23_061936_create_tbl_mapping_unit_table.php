<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblMappingUnitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_mapping_unit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sasaran_unit_id');
            $table->string('key_sort_unit');
            $table->integer('program_id');
            $table->integer('kegiatan_id');
            $table->integer('risiko_id');
            $table->double('bobot_risiko');
            $table->double('bobot_kegiatan');
            $table->double('bobot_program');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_mapping_unit');
    }
}
