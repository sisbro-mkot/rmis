<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndikatorSasaranAndTargetKinerjaToSasaranPemdaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sasaran_pemda', function (Blueprint $table) {
            $table->string('indikator_sasaran');
            $table->integer('target_kinerja');
            $table->string('satuan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sasaran_pemda', function (Blueprint $table) {
            //
        });
    }
}
